<?php

class home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session', 'form_validation', 'email'));
        $this->load->database();
        $this->load->model('user_model');
        $this->load->helper('security');
    }

    function index() {
        $this->load->view('header');
        $this->load->view('user_registration');
        $this->load->view('home');
       // $this->load-view('home');
        $this->load->view('footer');
        //$this->register();
    }

    function register() {
        //set validation rules
        $this->load->view('header');
        
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required|alpha|min_length[3]|max_length[30]|xss_clean');
        $this->form_validation->set_rules('sname', 'Last Name', 'trim|required|alpha|min_length[3]|max_length[30]|xss_clean');
        $this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|md5');
//        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
//        $this->form_validation->set_rules('cc_number', 'Credit card number', 'trim|required|max_length[19]');
//        $this->form_validation->set_rules('cc_csv_number', 'Credit card CVV number', 'trim|required|max_length[3]|min_length[3]');
//        $this->form_validation->set_rules('cc_exp_date', 'Credit card expiry date', 'required');
//        $this->form_validation->set_rules('dob', 'Birth Date', 'required');
        //validate form input
        if ($this->form_validation->run() == FALSE) {
            // fails
            //$this->load->view('user_registration');
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                redirect();
        } else {
            //insert the user registration details into database
            $b_dd = $this->input->post('b_dd');
            $b_mm = $this->input->post('b_mm');
            $b_yy = $this->input->post('b_yy');
            $dob = $b_yy . $b_mm . $b_dd;

            $data = array(
                'fname' => $this->input->post('fname'),
                'sname' => $this->input->post('sname'),
                'email' => $this->input->post('email'),
                'password' => $this->input->post('password'),
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'dob' => $dob
            );
            $exp_dd = $this->input->post('ex_dd');
            $exp_mm = $this->input->post('ex_mm');
            $exp_yy = $this->input->post('ex_yy');
            $expiryDate = $exp_yy . $exp_mm . $exp_dd;

            $data1 = array(
                'cc_number' => $this->input->post('cc_number'),
                'cc_csv_number' => $this->input->post('cc_csv_number'),
                'cc_exp_date' => $expiryDate
            );




            // insert form data into database
            if ($this->user_model->insertUser($data, $data1)) {

                // send email
                if ($this->user_model->sendEmail($this->input->post('email'))) {
                    // successfully sent mail
                    $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">You are Successfully Registered! Please confirm the mail sent to your Email-ID!!!</div>');
                    redirect();
                } /*else {
                    // error
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                    redirect();
                }*/
            } else {
                // error
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                redirect();
            }
        }
        $this->load->view('footer');
    }

    function verify($hash = NULL) {
        if ($this->user_model->verifyEmailID($hash)) {
            $this->session->set_flashdata('verify_msg', '<div class="alert alert-success text-center">Your Email Address is successfully verified! Please login to access your account!</div>');
            redirect();
        } else {
            $this->session->set_flashdata('verify_msg', '<div class="alert alert-danger text-center">Sorry! There is error verifying your Email Address!</div>');
            redirect('home/register');
        }
    }

    // Check for user login process
    public function user_login_process() {
        $this->load->view('header');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
        
        if ($this->form_validation->run() == FALSE) {
           
            
             if(isset($this->session->userdata['logged_in'])){
              $session = $this->session->userdata['logged_in'];
              $data2["userinfo"] = $this->user_model->read_user_information($session['email']);
              $data2["cardinfo"] = $this->user_model->readUserCardInfo($session['id']);
              //echo $this->user_model->readUserCardInfo($session['id']);
              //print_r($data3);
              $this->load->view('user_dashboard', $data2);
              //$this->load->view('user_dashboard');
              }else{
              $this->load->view('home');
              } 
        } else {
             
            $data = array(
                'email' => $this->input->post('email'),
                'password' => $this->input->post('password')
            );
            $result = $this->user_model->loginUser($data);
            if ($result == TRUE) {
                

                $email = $this->input->post('email');
                $result = $this->user_model->read_user_information($email);
                
                if ($result != false) {
                    $session_data = array(
                        'fname' => $result[0]->fname,
                        'email' => $result[0]->email,
                        'id' => $result[0]->id
                    );
//                if(!empty($email)){
//                    
//                    $data2["userinfo"] = $this->user_model->read_user_information($email);
//                     $this->load->view('user_dashboard', $data2);
//                } 
//                else{
//                    
//                    $this->session->set_userdata('logged_in', $session_data);
//                    $user_data = $this->session->userdata('logged_in');
//                    print_r($user_data);
//                    $userId = $user_data['id'];
//                    $email = $user_data['email'];
//                    $data2["userinfo"] = $this->user_model->read_user_information($email);
//                     $this->load->view('user_dashboard', $data2, $data3);
//                }
                    // Add user data in session
                    
                    $this->session->set_userdata('logged_in', $session_data);
                    $user_data = $this->session->userdata('logged_in');
                    //print_r($user_data);
                    $userId = $user_data['id'];
                    
                   $data2["userinfo"] = $this->user_model->read_user_information($email);
                   
                   //$data3["cardinfo"] = $this->user_model->readUserCardInfo($userId);
                   
                    $this->load->view('user_dashboard', $data2);
                }
            } else {
                
                $data = array(
                    'error_message' => 'Invalid Username or Password'
                );
                $this->load->view('home', $data);
            }
        }
        $this->load->view('footer');
    }
    
        // Logout from admin page
        public function logout() {

        // Removing session data
        $this->load->view('header');
        $sess_array = array(
        'email' => ''
        );
        $this->session->unset_userdata('logged_in', $sess_array);
        $data['message_display'] = 'Successfully Logout';
        $this->load->view('home', $data);
        redirect();
         $this->load->view('footer');
        }


    
    function secondSignup(){
        $b_dd = $this->input->post('b_dd');
            $b_mm = $this->input->post('b_mm');
            $b_yy = $this->input->post('b_yy');
            $dob = $b_yy . $b_mm . $b_dd;
            
       $data = array(
                'id' => $this->input->post('id'),
                'fname' => $this->input->post('fname'),
                'sname' => $this->input->post('sname'),
                'address' => $this->input->post('address'),
                'p_address' => $this->input->post('p_address'),
                'contact_number' => $this->input->post('contact_number'),
                'job_title' => $this->input->post('job_title'),
                'company_employed' => $this->input->post('company_employed'),
                'next_of_kin' => $this->input->post('next_of_kin'),
                'company_employed' => $this->input->post('company_employed'),
                'medical_aid' => $this->input->post('medical_aid'),
                'profile_pic' => $this->input->post('profile_pic'),
                'dob' => $dob
            );
       $data1 = array(
                'id' => $this->input->post('id'),
                'cc_number' => $this->input->post('cc_number'),
                'cc_csv_number' => $this->input->post('cc_csv_number'),
                
            );
       
       
       $this->user_model->updateUser($data);
       $this->user_model->updateCardInfo($data1);
       redirect('home/user_login_process');
       
       
    }

}

?>