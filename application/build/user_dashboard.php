<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User dashboard</title>
    <link href="<?php echo base_url("/css/bootstrap.css"); ?>" rel="stylesheet" type="text/css" />
</head>
<body>
    
   <?php //print_r($cardinfo);
        //foreach ($cardinfo as $row2) :?> 
<?php //endforeach; 
 ?>
    
   <?php foreach ($userinfo as $row) :?> 
<?php endforeach; 
//echo $row->dob;
        ?>
    
    
     <div class="signup-form-step">
           <?php $attributes = array("name" => "registrationform");
                echo form_open("home/secondSignup", $attributes);?>
         
	 <div class="container">
		<div class="personal-info">
		<div class="row">
			<div class="col-lg-2 col-md-2 padding-right-0">
				<img src="<?php echo base_url(); ?>/images/<?php echo $row->profile_pic; ?>" alt="" style="width:100%;">
                                <input type="file" name="profile_pic" value="<?php echo $row->profile_pic; ?>"> 
                        </div>
                    
			<div class="col-lg-3 col-md-3">
			<div class="col-lg-12 col-md-12"><h2>Personal Details</h2></div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-2 control-label">Name</label>
					<div class="col-lg-12">
                                            <input name="id" type="hidden" class="form-control" value="<?php echo $row->id; //echo $userinfo->fname; ?>" placeholder="First name">
                                            <input name="fname" type="text" class="form-control" value="<?php echo $row->fname; ?>" placeholder="First name" id="inputEmail">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-2 control-label">Surname</label>
					<div class="col-lg-12">
                                            <input name="sname" type="text" class="form-control" value="<?php echo $row->sname;?>"  placeholder="Surname" id="inputEmail">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-2 control-label">Date of birth</label>
						<div class="col-lg-12 col-md-12">
				<select class="form-control" name="b_dd" id="select">
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                         </select>
                            <select name="b_mm" class="form-control" id="select">
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                            </select>
		<select name="b_yy" class="form-control" id="select" value="<?php //echo $row->dob; ?>">
          <?php
                                    for($i=date('Y'); $i>1899; $i--) {
                                        $selected = '';
                                        if ($birthdayYear == $i) $selected = ' selected="selected"';
                                        print('<option value="'.$i.'"'.$selected.'>'.$i.'</option>'."\n");
                                    }
                                    ?>
        </select>
				</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
			<div class="col-lg-12 col-md-12"><h2>Contact Details</h2></div>
				<div class="form-group">
      <label for="textArea" class="col-lg-2 control-label">Residential Address</label>
      <div class="col-lg-12">
          <input name="address" type="text" class="form-control" value="<?php echo $row->address;?>" placeholder="Residential Address" id="textArea">
  
      </div>
    </div>
	<div class="form-group">
      <label for="textArea" class="col-lg-2 control-label">Postal Address</label>
      <div class="col-lg-12">
          <input name="p_address" type="text" class="form-control" value="<?php echo $row->p_address;?>" placeholder="Postal Address" id="textArea">
        
      </div>
    </div>
			</div>
			<div class="col-lg-3 col-md-3 contact-detail-last">
			
			
				<div class="form-group">
					<label for="inputEmail" class="col-lg-2 control-label">Contact Number</label>
					<div class="col-lg-12">
                                            <input name="contact_number" type="text" class="form-control" value="<?php echo $row->contact_number;?>" placeholder="Contact number" id="inputEmail">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-2 control-label">Cell Number </label>
					<div class="col-lg-12">
                                            <input name="phone" type="text" class="form-control" value="<?php echo $row->phone;?>" placeholder="Phone number" id="inputEmail">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-2 control-label">Email Address </label>
					<div class="col-lg-12">
                                            <input name="email" type="text" class="form-control" value="<?php echo $row->email;?>" placeholder="Email address" id="inputEmail" readonly>
					</div>
				</div>
			</div>
		</div>
		</div>
	 </div>
             
 </div>
    
    
     <div class="statutory-section">
	<div class="container">
		<div class="statutory-section-inner">
		<div class="statutory-section-heading col-lg-12">
			<h2>Statutory</h2>
		</div>

		<div class="row">
			<div class="col-lg-8 col-md-8">
			<div class="row">
				<div class="col-lg-6 col-md-6">
					<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Job Title </label>
					<div class="col-lg-12">
                                            <input name="job_title" type="text" class="form-control" value="<?php echo $row->job_title;?>" placeholder="Job title" id="inputEmail">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Company Employed</label>
					<div class="col-lg-12">
						<input name="company_employed" type="text" class="form-control" value="<?php echo $row->company_employed;?>" placeholder="Company Employed">
					</div>
				</div>
				 
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Next of Keen </label>
					<div class="col-lg-12">
						<input name="next_of_kin" type="text" class="form-control" value="<?php echo $row->next_of_kin;?>" placeholder="Next of kin">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Medical Aid </label>
					<div class="col-lg-12">
						<input name="medical_aid" type="text" class="form-control" value="<?php echo $row->medical_aid;?>" placeholder="Medical Aid">
					</div>
				</div>
				 
				</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 border-left-cl">
			<div class="row">
				<div class="col-lg-12 col-md-12">
				<label for="textArea" class="  control-label">Credit card number</label>
                                <input type="text" class="form-control" value="<?php //echo $row2->cc_number ?>" >
				</div>
				 
			</div>
			<div class="row">
		 
				<div class="col-lg-8 col-md-8 expiry">
				 <label for="textArea" class="  control-label">Credit card expiry</label>
				<select class="form-control" id="select">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                  </select>
                                  <select class="form-control" id="select">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                  </select>
                                  <select class="form-control" id="select">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                  </select>
				</div>
				<div class="col-lg-4 col-md-4">
				<label for="textArea" class=" control-label">CCV</label>
				<input type="text" class="form-control" value="<?php //echo $row2->cc_csv_number ?>" >
				</div>
                             
                                    <button name="submit" type="submit" class="btn btn-default submit-button">Submit</button>
			 
			</div>
			</div>
			</div>
		</div>
	</div>
         <?php echo form_close(); ?>
                <?php echo $this->session->flashdata('msg'); ?>
 </div>
    
    
    
    
    
    
    
<!--div class="container">
    <div class="signup-form">
                     <?php //$attributes = array("name" => "registrationform");
                //echo form_open("home/secondSignup", $attributes);?>
			<div class="row">
                            <img src="<?php echo base_url(); ?>/images/<?php echo $row->profile_pic; ?>" width="200" height="200"/>
				<div class="col-lg-6 col-md-6">
                                    <input name="id" type="hidden" class="form-control" value="<?php echo $row->id; //echo $userinfo->fname; ?>" placeholder="First name">
                                    <input name="fname" type="text" class="form-control" value="<?php echo $row->fname; //echo $userinfo->fname; ?>" placeholder="First name">
				</div>
                            
				<div class="col-lg-6 col-md-6">
                                    <input name="sname" type="text" class="form-control" value="<?php echo $row->sname;?>"  placeholder="Surname">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12">
                                    <input name="email" type="text" class="form-control" value="<?php echo $row->email;?>" placeholder="Email address">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12">
				<input name="phone" type="text" class="form-control" value="<?php echo $row->phone;?>" placeholder="Phone number">
				</div>
			</div>
                        <div class="row">
				<div class="col-lg-12 col-md-12">
				<input name="address" type="text" class="form-control" value="<?php echo $row->address;?>" placeholder="Residential Address">
				</div>
			</div>
                        <div class="row">
				<div class="col-lg-12 col-md-12">
				<input name="p_address" type="text" class="form-control" value="<?php echo $row->p_address;?>" placeholder="Postal Address">
				</div>
			</div>
                        <div class="row">
				<div class="col-lg-12 col-md-12">
				<input name="contact_number" type="text" class="form-control" value="<?php echo $row->contact_number;?>" placeholder="Contact number">
				</div>
			</div>
                        <!--Extra four feilds -->
                        <!--div class="row">
				<div class="col-lg-12 col-md-12">
				<input name="job_title" type="text" class="form-control" value="<?php echo $row->job_title;?>" placeholder="Job title">
				</div>
			</div>
                        <div class="row">
				<div class="col-lg-12 col-md-12">
				<input name="company_employed" type="text" class="form-control" value="<?php echo $row->company_employed;?>" placeholder="Company Employed">
				</div>
			</div>
                        <div class="row">
				<div class="col-lg-12 col-md-12">
				<input name="next_of_kin" type="text" class="form-control" value="<?php echo $row->next_of_kin;?>" placeholder="Next of kin">
				</div>
			</div>
                        <div class="row">
				<div class="col-lg-12 col-md-12">
				<input name="medical_aid" type="text" class="form-control" value="<?php echo $row->medical_aid;?>" placeholder="Medical Aid">
				</div>
			</div>
                        <input type="file" name="profile_pic" class="form-control" />
                        
                        <!--div class="row">
				<div class="col-lg-12 col-md-12">
				<input name="cc_number" type="text" class="form-control" value="<?php //echo $row->cc_number;?>" placeholder="Medical Aid">
				</div>
			</div>
                        
                        <div class="row">
				<div class="col-lg-12 col-md-12">
				<input name="cc_csv_number" type="text" class="form-control" value="<?php //echo $row->cc_csv_number;?>" placeholder="Medical Aid">
				</div>
			</div-->
			
			
			<!--div class="row">
			<div class="col-lg-12 col-md-12">
				Birthday
				</div>
			<div class="col-lg-12 col-md-12">
                                    <select name="b_dd" class="form-control" id="select">
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                                <select name="b_mm" class="form-control" id="select">
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                <select name="b_yy" class="form-control" id="select">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
			</div>
			</div-->
			<!--div class="row">
				<div class="col-lg-12 col-md-12">
			 <input type="hidden" class="form-control" name="gender" >
                         <a href="#" class="male active"> Male</a>
			 <a href="#" class="female">Female</a>
			  </div> 
			  </div-->
			
                        
                       
			
                    <?php echo form_close(); ?>
                <?php echo $this->session->flashdata('msg'); ?>
		</div>
</div>
</body>
</html>