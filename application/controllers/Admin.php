<?php
class Admin extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session', 'form_validation', 'email', 'upload'));
        $this->load->database();
        $this->load->model('admin_model');
        $this->load->helper('security');
        if(empty($this->session->userdata['logged_in']))
        {
            $url = uri_string();
            if(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'])
                $url .= '?' . $_SERVER['QUERY_STRING'];
            $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">Please login before you visit the site!</div>');
            $this->session->set_userdata('redirectUrl', $url);
            redirect();
        }
        elseif($this->session->userdata['logged_in']['role'] != 1)
        {
            $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">You are not authorized to visit this page. Sorry!</div>');
            redirect();
        }
    }
    
    
    
    function usersuspend($type, $id)
    {
       
        $data=array('status'=>3);
        $this->db->where('id',$id);
        $this->db->update('users',$data);
        $this->session->set_flashdata('msg_suspend','<div class="alert alert-success text-center">User suspended successfully.</div>');
        redirect('admin/userlist'.'/'.$type);
        
    }
    
    function useractive($type, $id)
    {
       
        $data=array('status'=>1);
        $this->db->where('id',$id);
        $this->db->update('users',$data);
       
        $this->session->set_flashdata('msg_suspend','<div class="alert alert-success text-center">User activated successfully.</div>');
        redirect('admin/userlist'.'/'.$type);
        
    }

    function index() 
    {
        if(isset($this->session->userdata['logged_in']) && ($this->session->userdata['logged_in']['role'])== 1) 
        {   
            $data['upcoming_appointments'] = $this->admin_model->getAppointments(array('meeting_approval' => 1, 'start_time >' => date('Y-m-d H:i:s', time())));
            $data['userslist'] = $this->admin_model->readUsersList();
            $data['doctorProfile'] = $this->admin_model->readDoctorProfiles();
            
            $this->load->view('new_header');        
            $this->load->view('admin/new_admin_dashboard', $data);
            $this->load->view('new_footer');
            
        }
        else{ 
            redirect();
        }
    }

    function userList()
    {    
        if(isset($this->session->userdata['logged_in']) && ($this->session->userdata['logged_in']['role'])== 1) 
        {
            $data['userslist'] = $this->admin_model->readUsersList();
            $data['doctorRates'] = $this->admin_model->readDoctorRates();
            $data['doctorProfile'] = $this->admin_model->readDoctorProfiles();
            $data['pages'] = array('terms' => 'Terms', 'faqs' => 'Faqs', 'policy' => 'policy', 'fees' => 'Fees', 'footer' => 'Footer', 'apply_doctor_page' => 'Apply Doctor');
            
//            print_r($this->uri->segment(3));
            
            if($this->uri->segment(3) == 'patient'){
                $header_data['breadcrumbs']['admin_patient'] = TRUE;
            }else if($this->uri->segment(3) == 'doctor'){
                $header_data['breadcrumbs']['admin_doctor'] = TRUE;
            }else if($this->uri->segment(3) == 'pages'){
                $header_data['breadcrumbs']['admin_pages'] = TRUE;
            }else if($this->uri->segment(3) == 'doctor_rates'){
                $header_data['breadcrumbs']['admin_doctor_rates'] = TRUE;
            }
            
//            $header_data['breadcrumbs']['admin_dashboard'] = TRUE;
//            $this->load->view('header', $header_data);
//            $this->load->view('admin/admin_dashboard', $data);
//            $this->load->model('page_model');
//            $footer['footer'] = $this->page_model->get_content('footer');
//            $this->load->view('footer', $footer);
            
            $this->load->view('new_header', $header_data);
            $this->load->view('admin/new_userlist', $data);
            $this->load->view('new_footer');
        }
        else
           redirect();
    }
    
    function viewUserDetail($id)
    {
        if(isset($this->session->userdata['logged_in']) && ($this->session->userdata['logged_in']['role'])== 1) 
        {
            $data['row'] = $this->admin_model->readUserDetails($id);
            $data['userOtherProfile'] = $this->admin_model->readUserOtherProfiles($id);            
            
//            $this->load->view('header');
//            $this->load->view('admin/userdetail', $data);
//            $this->load->model('page_model');
//            $footer['footer'] = $this->page_model->get_content('footer');
//            $this->load->view('footer', $footer);
//            
            $this->load->view('new_header', $header_data);
            $this->load->view('admin/new_userdetail', $data);
            $this->load->view('new_footer');
        }
        else
            redirect();
    }
    
    function viewDoctorDetail($id)
    {
       
       if(isset($this->session->userdata['logged_in']) && ($this->session->userdata['logged_in']['role'])== 1) 
       {
            $data['row'] = $this->admin_model->readDoctorDetails($id);
            $data['doctorAvailableDays'] = $this->admin_model->readDoctorAvailableDays($id);
            $data['doctorTimings'] = $this->admin_model->readDoctorTimings($id);
            $data['doctorStatus'] = $this->admin_model->doctorStatus($id);

//            $this->load->view('header');
//            $this->load->view('admin/doctordetail', $data);
//            $this->load->model('page_model');
//            $footer['footer'] = $this->page_model->get_content('footer');
//            $this->load->view('footer', $footer);
            
            $this->load->view('new_header', $header_data);
            $this->load->view('admin/new_doctordetail', $data);
            $this->load->view('new_footer');
            
        }
        else
            redirect();
    }
    
    function getDoctorEmail($id)
    {
        if(isset($this->session->userdata['logged_in']) && ($this->session->userdata['logged_in']['role'])== 1) 
        {
            $data = $this->admin_model->doctorEmail($id);
            if($data)
            {
                $toEmail = $data[0]->email;
                $name = $data[0]->fname;
                $this->admin_model->sendDoctorVerifyEmail($toEmail, $name);
            
                $this->admin_model->verfiyDoctorEmail($id); 
                $this->session->set_flashdata('email_success', '<div class="alert alert-success text-center">Doctor request approved!!!</div>');
            
                redirect('admin/viewDoctorDetail/'.$id);
            }
            else
            {
                $this->session->set_flashdata('email_fail', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                redirect('');
            }
        }
        else
          redirect();       
    }

    function editDoctorInfo()
    {
        if(isset($this->session->userdata['logged_in']) && ($this->session->userdata['logged_in']['role'])== 1) 
        {
            $data = array(
                'id' => $this->input->post('id'),
                'fname' => $this->input->post('fname'),
                'sname' => $this->input->post('sname'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),   
            ); 
    
            $data1 = array(
                'id' => $this->input->post('id'),
                'degree' => $this->input->post('degree'),
                'experience' => $this->input->post('experience'),  
                'speciality' => $this->input->post('speciality'),
                'doctor_type' => $this->input->post('doctor_type')
            );
       
            $this->admin_model->updateDoctor($data);
            $this->admin_model->updateDoctorInfo($data1);
            redirect('admin/userList');
        }
        else
            redirect();
    }

    function doctorRates()
    {    
        $data = array(
            'id' => $this->input->post('rateid'),
            'rates' => $this->input->post('rates')
        );
        $this->admin_model->updateDoctorRates($data);
        redirect('admin/userlist/doctor_rates');
    }
    
    function addDoctor()
    {    
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required|alpha|min_length[3]|max_length[30]|xss_clean');
        $this->form_validation->set_rules('sname', 'Last Name', 'trim|required|alpha|min_length[3]|max_length[30]|xss_clean');
        $this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|md5');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('degree', 'Degree', 'trim|required');
        $this->form_validation->set_rules('experience', 'Experience', 'trim|required');
        $this->form_validation->set_rules('doctor_type', 'Doctor Type', 'trim|required');

        //validate form input
        if ($this->form_validation->run() == FALSE) 
        {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
            redirect();
        } 
        else 
        {
            $encPass = md5($this->input->post('password'));
            $orgPass = $this->input->post('password');
            $data = array(     
                'fname' => $this->input->post('fname'),
                'sname' => $this->input->post('sname'),
                'email' => $this->input->post('email'),
                'password' => $encPass,
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'role' => 3
            );
            
            $data1 = array(
                'degree' => $this->input->post('degree'),
                'experience' => $this->input->post('experience'),
                'speciality' => $this->input->post('speciality'),
                'doctor_type' => $this->input->post('doctor_type'),
                'role' => 3,               
            );
            
            // insert form data into database
            $result = $this->admin_model->insertDoctor($data, $data1);
            if($result == 1)
              $this->session->set_flashdata('success', '<div class="alert alert-danger text-center">Doctor has been added successfully </div>');
              
            $to_email = $this->input->post('email');
            $password = $this->input->post('password');
            
            if ($this->admin_model->sendEmail($to_email, $orgPass))
                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">You are Successfully Registered! Please confirm the mail sent to your Email-ID!!!</div>'); 
            redirect('admin/userList');
        }
    }

    function verify($hash = NULL) 
    {
        if ($this->load->admin_model->verifyEmailID($hash)) 
        {
            $this->session->set_flashdata('verify_msg', '<div class="alert alert-success text-center">Your Email Address is successfully verified! Please login to access your account!</div>');
            redirect();
        } 
        else 
        {
            $this->session->set_flashdata('verify_msg', '<div class="alert alert-danger text-center">Sorry! There is error verifying your Email Address!</div>');
            redirect('home/register');
        }
    }
    
    function payments()
    {
        $data['records'] = $this->admin_model->getPayments();
        $header_data['breadcrumbs']['admin_payment'] = TRUE;
        
//        $this->load->view('header', $header_data);
//        $this->load->view('admin/payments', $data);
//        $this->load->model('page_model');
//        $footer['footer'] = $this->page_model->get_content('footer');
//        $this->load->view('footer', $footer);
        

        $this->load->view('new_header', $header_data);
        $this->load->view('admin/new_payment', $data);
        $this->load->view('new_footer');
        
    }

    function appointments()
    {
        $condition = array('meeting_approval' => 1, 'start_time >' => date('Y-m-d H:i:s', time()));
        $data['upcoming_appointments'] = $this->admin_model->getAppointments($condition);

        $condition = array('meeting_approval' => 3);
        $data['finished_appointments'] = $this->admin_model->getAppointments($condition);
        foreach($data['finished_appointments'] as $value)
        {
            if($value->latitude && $value->longitude)
            {
                $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $value->latitude . ',' . $value->longitude . '&key=' . $this->config->item('map_key');
                $location = json_decode(file_get_contents($url));
                if(isset($location->results[0]->formatted_address))
                    $value->user_address = $location->results[0]->formatted_address;
        
            }    
        }
        
        $condition = array('meeting_approval' => 1, 'start_time <' => date('Y-m-d H:i:s', time()));
        $data['missed_appointments'] = $this->admin_model->getAppointments($condition);

//        $header_data['breadcrumbs']['admin_appointment'] = TRUE;
        
//        $this->load->view('header', $header_data);
//        $this->load->view('admin/appointments', $data);
//        $this->load->model('page_model');
//        $footer['footer'] = $this->page_model->get_content('footer');
//        $this->load->view('footer', $footer);
          
        
            if($this->uri->segment(3) == 'upcoming'){
                $header_data['breadcrumbs']['admin_appointment'] = TRUE;
            }else if($this->uri->segment(3) == 'finished'){
                $header_data['breadcrumbs']['admin_finished'] = TRUE;
            }else if($this->uri->segment(3) == 'missed'){
                $header_data['breadcrumbs']['admin_missed'] = TRUE;
            }
        
          $this->load->view('new_header', $header_data);
          $this->load->view('admin/new_appointments', $data);
          $this->load->view('new_footer');
    }
    
    
    //Admin should be able to: -Suspend a patient or doctor.  add by raju ()
    function changeUserStatus($id){
        echo $id;
    }
    
    function UpdateDoctorDetail($id)
    {
       
       if(isset($this->session->userdata['logged_in']) && ($this->session->userdata['logged_in']['role'])== 1) 
       {
            $data['record'] = $this->admin_model->readDoctorDetails($id);
//            $data['doctorAvailableDays'] = $this->admin_model->readDoctorAvailableDays($id);
            $data['doctorAvailableDays'] = $this->admin_model->getDoctorAvailableDays($id);
//            $data['doctorTimings'] = $this->admin_model->readDoctorTimings($id);
            $data['doctorStatus'] = $this->admin_model->doctorStatus($id);
            $data['id'] = $id;
//            echo '<pre>'; print_r($data['doctorAvailableDays']); die;
            $this->load->view('new_header', $header_data);
            $this->load->view('admin/new_doctordetail_new12', $data);
            $this->load->view('new_footer');
            
        }
        else{
            redirect();
        }
    }
    
    function Savedoctorprofile($user_id){
        
        if(!$user_id)
            return NULL;
       
        
//        $this->load->library('form_validation');
//        $this->form_validation->set_rules('fname', 'Name', 'required');
//        
//        if ($this->form_validation->run() == FALSE)
//        {
//                $this->load->view('admin/new_doctordetail_new12');
//        }
//        echo 'here'; die;
        $doctorId = $user_id; 
        
        $this->load->model('Doctor_model', 'doctor_model');
        $data = array(
            'fname'   => $this->input->post('fname'),
            'sname'  => $this->input->post('sname'),
            'email'  => $this->input->post('email'),
            'phone'  => $this->input->post('phone'),
            'address'=> $this->input->post('address'),
            'city'=> $this->input->post('city'),
            'province'=> $this->input->post('province'),
            'role'   => 3,
            'gender' => $this->input->post('gender')
        );

        $data1 = array(  
            'description' => $this->input->post('description'),
            'profession_number' => $this->input->post('profession_number'),
            'practice_number' => $this->input->post('practice_number'),
            'speciality' => $this->input->post('speciality'),
            'type_of_doctor' => $this->input->post('type_of_doctor'),
        );
        
        if($_FILES['profile_pic']['name'] != '' || $_FILES['profile_pic']['cv'] != '')
         { 
            // Upload Doctor Profile
            $config['upload_path']          = './uploads/profile/';
            $config['file_name']            = 'pic-' . $doctorId;
            $config['allowed_types']        = 'gif|jpg|png';
            
            //$config['allowed_types']        = 'gif|jpg|png|docx|doc';
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $thumb_path = realpath(getcwd()) . '/uploads/' ;
            if (!is_dir($thumb_path))
            {
                mkdir($thumb_path);
                chmod($thumb_path, 0777);
            }

            if(isset($_FILES['profile_pic']['tmp_name']))
            {
                $thumb_path = realpath(getcwd()) . '/uploads/profile/' ;
                if (!is_dir($thumb_path))
                {
                    mkdir($thumb_path);
                    chmod($thumb_path, 0777);
                }

               if($this->upload->do_upload('profile_pic'))
               {
                  $data_file = array('upload_data' => $this->upload->data());
                  $data1['profile_pic'] = $data_file['upload_data']['file_name'];
                  generate_preset($data_file['upload_data']['full_path']);
               }
            }

             // Upload Doctor Profile
            $config['upload_path']          = './uploads/cv/';
            $config['file_name']            = 'cv-' . $doctorId;
            $config['allowed_types']        = 'docx|doc|pdf';
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if(isset($_FILES['cv']['tmp_name']))
            {
              $thumb_path = realpath(getcwd()) . '/uploads/cv/' ;
              if (!is_dir($thumb_path))
              {
                  mkdir($thumb_path);
                  chmod($thumb_path, 0777);
              }

              if ($this->upload->do_upload('cv'))
              {
                $data_file = array('upload_data' => $this->upload->data(), '');
                $data1['cv'] = $data_file['upload_data']['file_name'];
              }
            }

        }
         
        $this->doctor_model->updateDoctor($data, $doctorId);  // presonal info update
        $this->doctor_model->updateDoctorInfo($data1, $doctorId); // Other info update
        $this->admin_model->AddAvailableDays($user_id); // doctor timing insert/update
        $this->admin_model->UpdateExperience($user_id); // experience update
        $this->admin_model->UpdateAcademicInfo($user_id); // academic info update
       
       $this->UpdateDoctorDetail($user_id);
    }
}