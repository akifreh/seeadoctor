<?php
class Appointment extends CI_Controller
{
  public function __construct()
  {
      parent::__construct();
      $this->load->helper(array('form','url'));
      $this->load->library(array('session', 'form_validation', 'email'));
      $this->load->database();
      $this->load->model('user_model');
      $this->load->model('appointment_model');
      $this->load->helper('security');
      $this->load->model('doctor_model');
  }
  
  function index()
  {
    if(isset($this->session->userdata['logged_in'])) 
    {
        
      
      $userId = $this->session->userdata['logged_in']['id'];
      
      $header_data['breadcrumbs']['appointment'] = TRUE;
      $this->load->view('header', $header_data);

      $data['nutritionists'] = $this->appointment_model->readNutritionists();
      $data['psychologists'] = $this->appointment_model->readPsychologists();
      $data['medicalDoctors'] = $this->appointment_model->readMedicalDoctor();
      $data['allpsychologists'] = $this->appointment_model->readAllPsychologists();
      $data['allmedicaldoctors'] = $this->appointment_model->readAllMedicalDoctor();
      $data['myappointments'] = $this->appointment_model->readAllMyAppointments($userId);
      $data['is_profile_incomplete'] = $this->appointment_model->is_profile_incomplete($userId);
      $data['has_card_expired'] = $this->appointment_model->has_card_expired($userId);
      $data['appointment_link'] = $this->appointment_model->getLastestAppointmentLink($userId);

      $this->load->view('user/dashboard',$data);
      $this->load->model('page_model');
      $footer['footer'] = $this->page_model->get_content('footer');
      $this->load->view('footer', $footer);
    }
    else
    {
      $url = uri_string();
      if(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'])
          $url .= '?' . $_SERVER['QUERY_STRING'];
      $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">Please login before you visit the site!</div>');
      $this->session->set_userdata('redirectUrl', $url);
      redirect();
    }
  }
  
  
  function startcall($id)
  {
      if(!$id)
      redirect('appointment');
      
      
       $this->db->where('status', 1);
       $this->db->where('appointment_id', $id);
       $query = $this->db->get('sessionkey');
       $result = $query->row();
        
       if($result)
       {
           
           redirect('call?sessionkey='.$result->session_key);
       }
       else
       {
           
          if($this->session->userdata['logged_in']['role'] == 2)
          {    
//            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">You are not allow to make a call their is still time in your appointment.</div>');
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">It is too early to start the call now, we will send you a reminder closer to scheduled time.</div>');
            redirect('appointment'); 
          }
          else
          {
//             $this->session->set_flashdata('approved', '<div class="alert alert-danger text-center">You are not allow to make a call their is still time in your appointment.</div>');
            $this->session->set_flashdata('approved', '<div class="alert alert-danger text-center">It is too early to start the call now, we will send you a reminder closer to scheduled time.</div>');
            redirect('doctordashboard');
          }    
       }    
          
  }
    
  function psychologistDays()
  {
    $doctorId = $this->input->post('userid');
    $data = $this->appointment_model->readOneMedicalDoctor($doctorId);

    // Set timezone
    date_default_timezone_set('UTC');

    // Start date
    $date = date('Y-m-d');

    // End date
    $end_date = strtotime ( '+14 days' , strtotime ( $date ) ) ;
    $end_date = date ( 'Y-m-d' , $end_date );

    echo "<option value='0'>Select Appointment Day</option>";
    while (strtotime($date) <= strtotime($end_date)) 
    {
      //echo "$date\n";
      for($i=0; $i< count($data); $i++)
      {
        if(ucfirst($data[$i]->day) == date('l', strtotime($date)))
        {
          echo "<option style='color:#000' value='" . $date."'>".$date."</option>";
        }
      }
      
      date('l', strtotime($date));
      $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));                       
    }  
  }

  function nutritionistDays()
  {
    $doctorId = $this->input->post('userid');
    $data = $this->appointment_model->readOneMedicalDoctor($doctorId);
  
    // Set timezone
    date_default_timezone_set('UTC');

    // Start date
    $date = date('Y-m-d');
    // End date
    $end_date = strtotime ( '+14 days' , strtotime ( $date ) ) ;
    $end_date = date ( 'Y-m-d' , $end_date );
    echo "<option value='0'>Select Appointment Day</option>";
    while (strtotime($date) <= strtotime($end_date)) 
    {
      for($i=0; $i< count($data); $i++)
      {
        if(ucfirst($data[$i]->day) == date('l', strtotime($date)))
        {                               
          echo "<option style='color:#000' value='" . $date."'>".$date."</option>";
        }
      }
 
      date('l', strtotime($date));
      $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
                            
    }     
  }

  function nutritionistAvailableTiming()
  {
    $doctorId = $this->input->post('userid');
    $result = $this->appointment_model->readNutriTime($doctorId);
    $result2 = $this->appointment_model->readNutriAvailableTime($doctorId);
    $appointmentDate  = $this->input->post('appointmentDate');
  
    for ($i=0; $i<count($result); $i++)
    {
      
      $startTime = strtotime($appointmentDate. ' ' . $result[$i]->start_time);
      $endTime = strtotime($result[$i]->end_time);
      $timeDiffrence = (strtotime($result[$i]->end_time) - strtotime($result[$i]->start_time))/3600;
      $timeDiffrenceMin = $timeDiffrence * 60;
      
      echo "<option value='0'>Select an Appointment Time</option>";
        
      for($j=0; $j< $timeDiffrenceMin; $j = $j + 15)
      {      
            $k = $j+15;
            $found = false;
            foreach ($result2 as $row)
            {
              if( $row->start_time == date("Y-m-d H:i:s", strtotime("+{$j} minutes", $startTime)))
              {
                echo "<option value='" .date("H:i:s", strtotime("+{$j} minutes", $startTime)) .' - '. date("H:i:s", strtotime("+{$k} minutes", $startTime)) ."' disabled>" . date("H:i:s", strtotime("+{$j} minutes", $startTime)) . "</option>";   
                $found = true;
                break;
              } 
            }
            if($found) 
              continue;
            echo "<option value='" .date("H:i:s", strtotime("+{$j} minutes", $startTime)) .' - '. date("H:i:s", strtotime("+{$k} minutes", $startTime)) ."' " . (date("Y-m-d H:i:s", strtotime($result2[$i]->start_time)) == date("Y-m-d H:i:s", strtotime("+{$j} minutes", $startTime)) ? "disabledzzz":"") . ">" . date("H:i:s", strtotime("+{$j} minutes", $startTime)) . "</option>";
 
      }
    }
  }

  function doctorDays()
  {
    $doctorId = $this->input->post('userid');
    $data = $this->appointment_model->readOneMedicalDoctor($doctorId);
    
    // Set timezone
    date_default_timezone_set('UTC');

    // Start date
    $date = date('Y-m-d');
    // End date
    $end_date = strtotime ( '+14 days' , strtotime ( $date ) ) ;
    $end_date = date ( 'Y-m-d' , $end_date );
    echo "<option value='0'>Select Appointment Day</option>";
    while (strtotime($date) <= strtotime($end_date)) 
    {
      for($i=0; $i< count($data); $i++)
      {
        if(ucfirst($data[$i]->day) == date('l', strtotime($date)))
        {
          echo "<option style='color:#000' value='" . $date."'>".$date."</option>";
        }
      }

      date('l', strtotime($date));
      $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));                   
    }    
  }


  function doctorAvailableTiming()
  {

    $doctorId = $this->input->post('userid');
    $result = $this->appointment_model->readDoctorTime($doctorId);
    $result2 = $this->appointment_model->readDoctorAvailableTime($doctorId);
    $appointmentDate  = $this->input->post('appointmentDate');
    for ($i=0; $i<count($result); $i++)
    {    
      $startTime = strtotime($appointmentDate. ' ' . $result[$i]->start_time);
      $endTime = strtotime($result[$i]->end_time);
      $timeDiffrence = (strtotime($result[$i]->end_time) - strtotime($result[$i]->start_time))/3600;
      $timeDiffrenceMin = $timeDiffrence * 60;
      echo "<option value='0'>Select an Appointment Time</option>";
          
      for($j=0; $j< $timeDiffrenceMin; $j = $j + 15)
      {
            
        $k = $j+15;
        $found = false;
        foreach ($result2 as $row)
        {
          if( $row->start_time == date("Y-m-d H:i:s", strtotime("+{$j} minutes", $startTime)))
          {
            echo "<option value='" .date("H:i:s", strtotime("+{$j} minutes", $startTime)) .' - '. date("H:i:s", strtotime("+{$k} minutes", $startTime)) ."' disabled>" . date("H:i:s", strtotime("+{$j} minutes", $startTime))."</option>";   
            $found = true;
            break;
          } 
        }
        if($found)
          continue;
        echo "<option value='" .date("H:i:s", strtotime("+{$j} minutes", $startTime)) .' - '. date("H:i:s", strtotime("+{$k} minutes", $startTime)) ."' " . (date("Y-m-d H:i:s", strtotime($result2[$i]->start_time)) == date("Y-m-d H:i:s", strtotime("+{$j} minutes", $startTime)) ? "disabledzzz":"") . ">" . date("H:i:s", strtotime("+{$j} minutes", $startTime))."</option>";
      }
    }
   }

  function psychologistAvailableTiming()
  {
    $doctorId = $this->input->post('userid');
    $result = $this->appointment_model->readPsychoTime($doctorId);
    $result2 = $this->appointment_model->readPsychoAvailableTime($doctorId);
    $appointmentDate  = $this->input->post('appointmentDate');
    for ($i=0; $i<count($result); $i++)
    {
      $startTime = strtotime($appointmentDate. ' ' . $result[$i]->start_time);
      $endTime = strtotime($result[$i]->end_time);
      $timeDiffrence = (strtotime($result[$i]->end_time) - strtotime($result[$i]->start_time))/3600;
      $timeDiffrenceMin = $timeDiffrence * 60;

      echo "<option value='0'>Select an Appointment Time</option>";
    
      for($j=0; $j< $timeDiffrenceMin; $j = $j + 15)
      {      
        $k = $j + 15;
        $found = false;
        foreach ($result2 as $row)
        {
          if( $row->start_time == date("Y-m-d H:i:s", strtotime("+{$j} minutes", $startTime)))
          {
            //echo "<option value='" .date("H:i:s", strtotime("+{$j} minutes", $startTime)) .' - '. date("H:i:s", strtotime("+{$k} minutes", $startTime)) ."' disabled>" . date("H:i:s", strtotime("+{$j} minutes", $startTime)) .' - '. date("H:i:s", strtotime("+{$k} minutes", $startTime)) . "</option>";   
            echo "<option value='" .date("H:i:s", strtotime("+{$j} minutes", $startTime)) .' - '. date("H:i:s", strtotime("+{$k} minutes", $startTime)) ."' disabled>" . date("H:i:s", strtotime("+{$j} minutes", $startTime))."</option>";     
            $found = true;
            break;
          } 
        }

        if($found)
          continue;
        //echo "<option value='" .date("H:i:s", strtotime("+{$j} minutes", $startTime)) .' - '. date("H:i:s", strtotime("+{$k} minutes", $startTime)) ."' " . (date("Y-m-d H:i:s", strtotime($result2[$i]->start_time)) == date("Y-m-d H:i:s", strtotime("+{$j} minutes", $startTime)) ? "disabledzzz":"") . ">" . date("H:i:s", strtotime("+{$j} minutes", $startTime)) .' - '. date("H:i:s", strtotime("+{$k} minutes", $startTime)) . "</option>";
         echo "<option value='" .date("H:i:s", strtotime("+{$j} minutes", $startTime)) .' - '. date("H:i:s", strtotime("+{$k} minutes", $startTime)) ."' " . (date("Y-m-d H:i:s", strtotime($result2[$i]->start_time)) == date("Y-m-d H:i:s", strtotime("+{$j} minutes", $startTime)) ? "disabledzzz":"") . ">" . date("H:i:s", strtotime("+{$j} minutes", $startTime)) ."</option>";
 
      }
    }
  }

  function setAppointmentWithMedicalDoctor()
  {      
    $this->form_validation->set_rules('patient[]', 'Patient', 'trim|required');
    $this->form_validation->set_rules('medicaldoctor', 'medicaldoctor', 'trim|required');
    $this->form_validation->set_rules('availableDays', 'availableDays', 'trim|required');
    $this->form_validation->set_rules('desc', 'desc', 'trim|required');
    $this->form_validation->set_rules('availableTime', 'availableTime', 'trim|required');

    if($this->form_validation->run() === FALSE)
    {
      $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Please Fill the form & try again!!!</div>');
      redirect('appointment'); 
    }
    else
    {        
      $patient = $this->input->post('patient[]');
      $patients = implode(',',$patient);
      $patientId = $this->session->userdata['logged_in']['id'];
      $availableDay = $this->input->post('availableDays');
      $availableTime = explode('-', $this->input->post('availableTime'));
      
      $data = array(
           'p_id' => $patientId,
           'd_id' => $this->input->post('medicaldoctor'),
           'brief_description' => $this->input->post('desc'),
           'med_taken' => $this->input->post('medication'),
           'patient' => $patients,
           'start_time' => $availableDay.' '.$availableTime[0],
           'end_time' => $availableDay.' '.$availableTime[1]
      );  
         
      $doctorId = $this->input->post('medicaldoctor');    
      $appId = $this->appointment_model->insertMedicalDoctorAppointment($data); 
      $symptom = $this->input->post('symptom');
      
      if($symptom)
      {
        foreach ($symptom as $key => $value) 
        {
          foreach ($value as $val) 
          {
            $data = array(
                'appointment_id' => $appId,
                'p_id' => $patientId,
                'category' => $key,
                'symptom' => $val
            );
            $this->appointment_model->insertSymptoms($data); 
          }
        }
      }  
       
      $data1 = $this->appointment_model->readPatientScheduledAppointment($appId);
      $patientInfo = $this->doctor_model->readPatient($patientId);
      $doctorInfo = $this->doctor_model->readPatient($doctorId);
      if($data1 && $patientInfo && $doctorInfo)
      {
        $patientEmail = $patientInfo[0]->email;
        $patientName = $patientInfo[0]->fname .' '. $patientInfo[0]->sname;
        $doctorEmail = $doctorInfo[0]->email;
        $doctorName = $doctorInfo[0]->fname .' '. $doctorInfo[0]->sname;
        $appDate = date("d-m-Y",strtotime($data1[0]->start_time));
        $appTime = date("h:i:s",strtotime($data1[0]->start_time));
        
        $this->appointment_model->sendEmailToPatientForAppointment($patientEmail, $patientName, $appDate, $appTime);
        
        
        
        
        
            $doctor_message = 'You have a request for appointment on <b>'. $appDate .'</b> at <b>' . $appTime . '</b> You will recieve a meeting room url 30 minutes before your meeting time.';
            $notification_array =  array();
            $notification_array['notification_type'] = "new";
            $notification_array['appointment_type'] = "schedule";
            $notification_array['message'] = $doctor_message;
            $notification_array['user_id'] = $doctorId;
            $notification_array['p_id'] = $patientId;
            $notification_array['d_id'] = $doctorId;
            $notification_array['appointment_id'] = $appId;
            $this->appointment_model->insertNotification($notification_array);
          
          
        
        
        $this->appointment_model->sendEmailToDoctorForAppointment($doctorEmail, $doctorName, $appDate, $appTime);
      }

      $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Your Appointment has been successfully scheduled</div>');
      redirect('appointment');
    }    
  }   
  
  function setAppointmentWithNutritionist()
  {    
    $this->form_validation->set_rules('patient[]', 'Patient', 'trim|required');
    $this->form_validation->set_rules('nutritionist', 'Nutritionist', 'trim|required');
    $this->form_validation->set_rules('nutravailableDays', 'NutravailableDays', 'trim|required');
    $this->form_validation->set_rules('problem-description', 'Problem-description', 'trim|required');
    $this->form_validation->set_rules('nutravailableTime', 'NutravailableTime', 'trim|required');   
   
    if($this->form_validation->run() === FALSE)
    {
     $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Please Fill the form & try again!!!</div>');
     redirect('appointment'); 
    }
    else
    {  
      $patient = $this->input->post('patient[]');
      $patients = implode(',',$patient);
      $patientId = $this->session->userdata['logged_in']['id'];
      $availableDay = $this->input->post('nutravailableDays');
      $availableTime = explode('-', $this->input->post('nutravailableTime'));

       $data = array(
         'p_id' => $patientId,
         'd_id' => $this->input->post('nutritionist'),
         'brief_description' => $this->input->post('problem-description'),
         'med_taken' => $this->input->post('medication'),
         'patient' => $patients,
         'start_time' => $availableDay.' '.$availableTime[0],
         'end_time' => $availableDay.' '.$availableTime[1]
             ); 
       
      $doctorId = $this->input->post('nutritionist');    
      $appId = $this->appointment_model->insertNutritionistAppointment($data); 

      $data1 = $this->appointment_model->readPatientScheduledAppointment($appId);
      $patientInfo = $this->doctor_model->readPatient($patientId);
      $doctorInfo = $this->doctor_model->readPatient($doctorId);
      if($data1 && $patientInfo && $doctorInfo)
      {
        $patientEmail = $patientInfo[0]->email;
        $patientName = $patientInfo[0]->fname .' '. $patientInfo[0]->sname;
        $doctorEmail = $doctorInfo[0]->email;
        $doctorName = $doctorInfo[0]->fname .' '. $doctorInfo[0]->sname;
        $appDate = date("d-m-Y",strtotime($data1[0]->start_time));
        $appTime = date("h:i:s",strtotime($data1[0]->start_time));

        $this->appointment_model->sendEmailToPatientForAppointment($patientEmail, $patientName, $appDate, $appTime);
        
        
           $doctor_message = 'You have a request for appointment on <b>'. $appDate .'</b> at <b>' . $appTime . '</b> You will recieve a meeting room url 30 minutes before your meeting time.';
            $notification_array =  array();
            $notification_array['notification_type'] = "new";
            $notification_array['appointment_type'] = "schedule";
            $notification_array['message'] = $doctor_message;
            $notification_array['user_id'] = $doctorId;
            $notification_array['p_id'] = $patientId;
            $notification_array['d_id'] = $doctorId;
            $notification_array['appointment_id'] = $appId;
            $this->appointment_model->insertNotification($notification_array);
          
          
            
        $this->appointment_model->sendEmailToDoctorForAppointment($doctorEmail, $doctorName, $appDate, $appTime);
      }
    
      $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Your Appointment has been successfully scheduled</div>');
      redirect('appointment');
    }
  }

  function setAppointmentWithPsychologist()
  {    
    $this->form_validation->set_rules('patient[]', 'Patient', 'trim|required');
    $this->form_validation->set_rules('psychologist', 'psychologist', 'trim|required');
    $this->form_validation->set_rules('availablePsychoDays', 'availablePsychoDays', 'trim|required');
    $this->form_validation->set_rules('description', 'description', 'trim|required');
    $this->form_validation->set_rules('availablePsychoTime', 'availablePsychoTime', 'trim|required');
   
    if($this->form_validation->run() === FALSE)
    {
       $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Please Fill the form & try again!!!</div>');
       redirect('appointment'); 
    }  
    else
    { 
      $patient = $this->input->post('patient[]');
      $patients = implode(',',$patient);
      $patientId = $this->session->userdata['logged_in']['id'];
      $availableDay = $this->input->post('availablePsychoDays');
      $availableTime = explode('-', $this->input->post('availablePsychoTime'));
     
      $data = array(
         'p_id' => $patientId,
         'd_id' => $this->input->post('psychologist'),
         'brief_description' => $this->input->post('description'),
         'med_taken' => $this->input->post('medication'),
         'patient' => $patients,
         'start_time' => $availableDay.' '.$availableTime[0],
         'end_time' => $availableDay.' '.$availableTime[1]
             ); 
      $doctorId = $this->input->post('psychologist');    
      $appId = $this->appointment_model->insertPsychologistAppointment($data); 
      
      $data1 = $this->appointment_model->readPatientScheduledAppointment($appId);
      $patientInfo = $this->doctor_model->readPatient($patientId);
      $doctorInfo = $this->doctor_model->readPatient($doctorId);
     
      if($data1 && $patientInfo && $doctorInfo)
      {
        $patientEmail = $patientInfo[0]->email;
        $patientName = $patientInfo[0]->fname .' '. $patientInfo[0]->sname;
        $doctorEmail = $doctorInfo[0]->email;
        $doctorName = $doctorInfo[0]->fname .' '. $doctorInfo[0]->sname;
        $appDate = date("d-m-Y",strtotime($data1[0]->start_time));
        $appTime = date("h:i:s",strtotime($data1[0]->start_time));

        $this->appointment_model->sendEmailToPatientForAppointment($patientEmail, $patientName, $appDate, $appTime);
        
        
         $doctor_message = 'You have a request for appointment on <b>'. $appDate .'</b> at <b>' . $appTime . '</b> You will recieve a meeting room url 30 minutes before your meeting time.';
            $notification_array =  array();
            $notification_array['notification_type'] = "new";
            $notification_array['appointment_type'] = "schedule";
            $notification_array['message'] = $doctor_message;
            $notification_array['user_id'] = $doctorId;
            $notification_array['p_id'] = $patientId;
            $notification_array['d_id'] = $doctorId;
            $notification_array['appointment_id'] = $appId;
            $this->appointment_model->insertNotification($notification_array);
          
            
        $this->appointment_model->sendEmailToDoctorForAppointment($doctorEmail, $doctorName, $appDate, $appTime);
      }

      $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Your Appointment has been successfully scheduled</div>');
      redirect('appointment');
    }
  }

  function sendScheduleAppointmentEmails()
  {
    //date_default_timezone_set('Asia/Calcutta');  //dev07
    ini_set('max_execution_time', 30000);
    ini_set('memory_limit', '512M');
    echo '<pre>';
    $start = time();
    echo 'Process Start - ' . date('H:i:s', $start);
    echo '<br /><br />';
    
    $data = $this->appointment_model->readAllAppointments();
    //echo count($data); die;
    $count = 0 ;
    $loop = 0; 
    if($data)
    {
      foreach($data as $value)
      {
        $loop++;
        $currentDateTime = date('Y-m-d h:i:s A', time());
        d('Current Time : '. $currentDateTime, 0);
        d('Start Time : ' . $value->start_time, 0);
        $currentTime = strtotime($currentDateTime);
        $appointmentDateTime = strtotime($value->start_time);
        $timeDiffrence = round(($appointmentDateTime - $currentTime) / 60,2);
        //echo $timeDiffrence; die;
        d('Time Difference : ' .$timeDiffrence,0);
        d('<br /><br />', 0);
        if($timeDiffrence > 0 )
        {
          //echo $timeDiffrence; die;
         // die('11');
           
          if($timeDiffrence < 31)
          {
           //die('dd');
            $count++;
            $patientId = $value->p_id;
            $doctorId = $value->d_id;
            $patientInfo = $this->doctor_model->readPatient($patientId);
            $doctorInfo = $this->doctor_model->readPatient($doctorId);
            $patientEmail = $patientInfo[0]->email;
            $patientName = $patientInfo[0]->fname .' '. $patientInfo[0]->sname;
            $doctorEmail = $doctorInfo[0]->email;
            $doctorName = $doctorInfo[0]->fname .' '. $doctorInfo[0]->sname;
            $appDate = date("d-m-Y",strtotime($value->start_time));
            $appTime = date("h:i:s",strtotime($value->start_time));
            $length = 10;
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) 
            {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

            $appointment = array(
              'meeting_approval' => 1,
            );

            $this->doctor_model->updateAppointment($appointment, $value->id);  

            $sessInfo = array(
                'p_id' => $patientId,
                'd_id' => $doctorId,
                'session_key' => $randomString,
                'appointment_id' => $value->id
             );
             
            $this->doctor_model->insertSessionKey($sessInfo);
            
            
            //send notification to patient 
            
            $patient_message = 'Your appointment with Dr ' . $doctorName . ' on <b>'. $appDate .'</b> at <b>' . $appTime . '</b> will start in 30 minutes.';
            $notification_array =  array();
            $notification_array['notification_type'] = "reminder-30-min";
            $notification_array['appointment_type'] = "schedule";
            $notification_array['message'] = $patient_message;
            $notification_array['user_id'] = $patientId;
            $notification_array['p_id'] = $patientId;
            $notification_array['d_id'] = $doctorId;
            $notification_array['appointment_id'] = $value->id;
            $this->appointment_model->insertNotification($notification_array);
          
          
            //send notification to doctor
          
            $doctor_message = 'Your meeting with ' . $patientName .' on <b>'. $appDate .'</b> at <b>' . $appTime . '</b> will start in 30 minutes.';
            $notification_array =  array();
            $notification_array['notification_type'] = "reminder-30-min";
            $notification_array['appointment_type'] = "schedule";
            $notification_array['message'] = $doctor_message;
            $notification_array['user_id'] = $doctorId;
            $notification_array['p_id'] = $patientId;
            $notification_array['d_id'] = $doctorId;
            $notification_array['appointment_id'] = $value->id;
            $this->appointment_model->insertNotification($notification_array);
            
            $this->appointment_model->sendCronEmailToPatientForAppointment($patientEmail, $patientName, $appDate, $appTime, $randomString, $doctorName);
            
            $this->appointment_model->sendCronEmailToDoctorForAppointment($doctorEmail, $doctorName, $appDate, $appTime, $randomString, $patientName);
            
          }
        }
      }
    }  

    echo $count . ' number of emails send.<br /><br />';
    $end = time();
    echo 'Process End - ' . date('H:i:s', $end);

    echo '<br /><br />';
    echo $end - $start . ' Seconds';

    echo '<br />';
    echo 'Exiting';
    echo '</pre>';
    ini_set('max_execution_time', 300);

  }
    
  function PatientAppointments()
  {
    $pID = $this->session->userdata['logged_in']['id'];  
    $result = $this->appointment_model->readPatientAppointment($pID);
    echo '<pre>';
    print_r($result);   
  }
    
  function setAppointmentWithPsychologistNow()
  {    
    $this->form_validation->set_rules('patient[]', 'Patient', 'trim|required');
    $this->form_validation->set_rules('briefdesc', 'briefdesc', 'trim|required');

    if($this->form_validation->run() === FALSE)
    {
       $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Please Fill the form & try again!!!</div>');
       redirect('appointment'); 
    }
    else
    {
    
      $data['psychologists'] = $this->appointment_model->readPsychologists();
      $patient = $this->input->post('patient[]');
      $patients = implode(',',$patient);
      
      if($data['psychologists'])
      {
        $time = date('Y-m-d H:i:s', time());
        foreach($data['psychologists'] as $value)
        {
          
          $data1 = array(
          'p_id' => $this->session->userdata['logged_in']['id'],
          'd_id' => $value->user_id,
          'brief_description' => $this->input->post('briefdesc'),
          'med_taken' => $this->input->post('medtaken'),
          'patient' => $patients,
          'created_at' => $time
          );

          $result = $this->appointment_model->insertNowAppointments($data1);
          $this->appointment_model->sendAppointmentNowNotificationEmail($value->email, $value->fname);
        }
//        $this->session->set_flashdata('online', '<div class="alert alert-success text-center">Your request has been successfully sent to our healthcare consultant. Please be patient a qualified Doctor will be with you shortly.<br />Also check your email for information to start your appointment.</div>');
        $this->session->set_flashdata('online', '<div class="alert alert-success text-center">Your request has been successfully sent to our healthcare consultant. Please be patient a qualified Doctor will be with you shortly.</div>');
      }     
      else
         $this->session->set_flashdata('online', '<div class="alert alert-danger text-center">No Psychologist is currently online. Please try again later.</div>');
      
      redirect('appointment');            
    }
  }
    
  function setAppointmentWithMedicalDoctorNow()
  {    
    $this->form_validation->set_rules('patient[]', 'Patient', 'trim|required');
    $this->form_validation->set_rules('brief-des', 'brief-des', 'trim|required');
    
    if($this->form_validation->run() === FALSE)
    {
       $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Please Fill the form & try again!!!</div>');
       redirect('appointment'); 
    }
    else
    {
      $data['medicaldoctor'] = $this->appointment_model->readMedicalDoctor();
      $patient = $this->input->post('patient[]');
      $patients = implode(',', $patient);

      if($data['medicaldoctor'])
      {
        $time = date('Y-m-d H:i:s', time());
        foreach($data['medicaldoctor'] as $value)
        {
          $data1 = array(
            'p_id' => $this->session->userdata['logged_in']['id'],
            'd_id' => $value->user_id,
            'brief_description' => $this->input->post('brief-des'),
            'med_taken' => $this->input->post('medtaken'),
            'patient' => $patients,
            'created_at' => $time
          );
          
          $result = $this->appointment_model->insertNowAppointments($data1);
          
          //send notification to doctor 
          $notification_array =  array();
          $notification_array['notification_type'] = "new";
          $notification_array['appointment_type'] = "now";
          $notification_array['message'] = str_replace('{pname}', $this->session->userdata['logged_in']['fname'], NOW_APPOINTMENT_NOTIFY_DOCTOR);
          $notification_array['user_id'] = $value->user_id;
          $notification_array['p_id'] = $this->session->userdata['logged_in']['id'];
          $notification_array['d_id'] = $value->user_id;
          
          $notification_array['appointment_id'] = $result;
          $this->appointment_model->insertNotification($notification_array);
          
          
          
          
          $this->appointment_model->sendAppointmentNowNotificationEmail($value->email, $value->fname);
        }
        
        $symptom = $this->input->post('symptom');
        if($symptom)
        {
          foreach ($symptom as $key => $value) 
          {
            foreach ($value as $val) 
            {
              $data = array(
                  'appointment_id' => NULL,
                  'p_id' => $this->session->userdata['logged_in']['id'],
                  'category' => $key,
                  'symptom' => $val
              );
              $this->appointment_model->insertSymptoms($data); 
            }
          }
        }

        $this->session->set_flashdata('online', '<div class="alert alert-success text-center">Your request has been successfully sent to our healthcare consultant. Please be patient a qualified Doctor will be with you shortly."</div>');
//        $this->session->set_flashdata('online', '<div class="alert alert-success text-center">Your request has been successfully sent to our healthcare consultant. Please be patient a qualified Doctor will be with you shortly.<br />Also check your email for information to start your appointment."</div>');
      }
      else
        $this->session->set_flashdata('online', '<div class="alert alert-danger text-center">No Medical Doctor is currently online. Please try again later.</div>');
      
      redirect('appointment');    
    }
  } 

  function cancelAppoinment($id)
  {
    if(!$id)
      redirect('appointment');
    
    $record = $this->appointment_model->getAppointment($id);
    
    if($record)
    {
      $patient = $this->appointment_model->getUserById($record->p_id);
      $doctor = $this->appointment_model->getUserById($record->d_id);
      
      $this->appointment_model->deleteAppoinment($id);
      
      
      //send notification to doctor 
          $doctor_message = $patient->fname. ' has cancelled following appointment with you Date : '. date('d M Y', strtotime($record->start_time)) . '&nbsp;&nbsp;&nbsp;Time : '. date('h:i a', strtotime($record->start_time));
          $notification_array =  array();
          $notification_array['notification_type'] = "cancel";
          $notification_array['appointment_type'] = "schedule";
          $notification_array['message'] = $doctor_message;
          $notification_array['user_id'] = $record->d_id;
          $notification_array['p_id'] = $record->p_id;
          $notification_array['d_id'] = $record->d_id;
          
          $notification_array['appointment_id'] = $id;
          $this->appointment_model->insertNotification($notification_array);
          
          
          
      // Send Email to Doctor
      $this->appointment_model->sendCancelAppoinmentEmail($doctor->email, $doctor->fname, $patient->fname, $record->start_time, 'doctor');
      
      // Send Email to Patient
      $this->appointment_model->sendCancelAppoinmentEmail($doctor->email, $doctor->fname, $patient->fname, $record->start_time, 'patient');
      
      $this->session->set_flashdata('online', '<div class="alert alert-danger text-center">Your Appointment has been cancelled successfully. A cancellation email is also send to you.</div>');
    }
    redirect('appointment'); 
  }

    function detail($id)
    {
      if(!$id)
        redirect('doctordashboard');
      
      $record = $this->appointment_model->getAppointment($id);
      if($record)
      {
        $data['patient'] = $this->appointment_model->getUserById($record->p_id);
        $data['record'] = $record;
        
        $this->load->view('header');
        $this->load->view('appointment_detail',$data);
        $this->load->model('page_model');
        $footer['footer'] = $this->page_model->get_content('footer');
        $this->load->view('footer', $footer);
      }
      else
        redirect('doctordashboard');  
    }
    
    
    public function Getcount()
    {
        
        
        $where = '';
        if($_REQUEST['last_notification_id'])
        {
            $last_notification_id = $_REQUEST['last_notification_id'];
            $where = " AND notification_id > $last_notification_id";
        }    
            
                
        $notificationshow = '';
        $query = $this->db->query("SELECT * FROM notification WHERE is_view = 0 AND user_id ='".$this->session->userdata['logged_in']['id']."'$where ORDER BY notification_id DESC");
        $notificationdetail = $query->result_array();
        
        if($notificationdetail)
        {
           $notificationshow = count($notificationdetail);
        }
        
        
        $response = array();
        $response['notification_count'] = $notificationshow; 
        $response['last_notification_id'] = $_REQUEST['last_notification_id'];
        $response['listring'] = '';
        $response['divstring'] = '';
        if($notificationdetail)
        {
            $response['last_notification_id'] = $notificationdetail[0]['notification_id'];
            $listring = '';
            $divlisting = '';
            foreach ($notificationdetail as $value) {
                
                
                    $listring.='<li>
                                    <a href="'.base_url().'">
                                      '.$value['message'].'
                                    </a>
                                </li>';
                    
                    
                     $divlisting.='<div class="alert alert-info">
                                     <a href="javascript:void(0);" class="close notifyclose">×</a>
                                      '.$value['message'].'
                                   </div>';
            }
            $response['listring'] = $listring;
            $response['divstring'] = $divlisting;
        }
        
        echo json_encode($response);    
        die;
       
    }
    
    
    public function viewed()
    {
        if($this->session->userdata['logged_in']['id'])
        {    
            $data=array('is_view'=>1);
            $this->db->where('user_id',$this->session->userdata['logged_in']['id']);
            $this->db->update('notification',$data);
        }    
        echo "ok";
        die;
    }
}