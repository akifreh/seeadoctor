<?php
class Call extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->library(array('session', 'form_validation', 'email'));
        $this->load->database();
        $this->load->model('user_model');
        $this->load->model('appointment_model');
        $this->load->helper('security');
        $this->load->model('call_model');
        
        
        
        
        //$this->load->library('OpenTok');
        // use OpenTok\OpenTok;
        //include APPPATH . 'third_party/OpenTok/OpenTok.php';
    }
    
    function index()
    {
        $userData = $this->session->userdata('logged_in');
        
        if($userData)
        {
            $sessionKey = $this->input->get('sessionkey');
            if(!$sessionKey)
            {
                $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">You are not authorized to visit this page. Sorry!</div>');
                redirect();
            } 
                
            $data['sesskey'] = $this->call_model->readSessionKey($sessionKey);
            if(!$data['sesskey'])
            {
                
                $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">You are not authorized to visit this page. Sorry!</div>');
                redirect();
            } 

            $pId = $data['sesskey']->p_id;
            $dId = $data['sesskey']->d_id;

            if($userData['role'] == 2 && $pId != $userData['id']) 
            {
                $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">You are not authorized to visit this page. Sorry!</div>');
                redirect();
            }
            elseif($userData['role'] == 3 && $dId != $userData['id'])
            {
                $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">You are not authorized to visit this page. Sorry!</div>');
                redirect();
            } 
            
            

            $data['patientInfo'] = $this->call_model->getUserById($pId);
            $data['doctorInfo'] = $this->call_model->getUserById($dId);
            $data['session'] = $this->session->userdata('logged_in');
            $data['username'] = $data['session']['fname'];
            $data['room_name'] = $dId . '-' . $pId .'-'. date('d-M');
            $data['db_name'] = 'Chat-' . date('d-M-y');
            $data['session_key'] = $sessionKey;
            $data['mapKey'] = $this->config->item('map_key');

            
            if($userData['role'] == 2)
            {    
            
          $doctor_message = 'Patient '.$data['session']['fname']. ' has started the call.';
          $notification_array =  array();
          $notification_array['notification_type'] = "call";
          $notification_array['appointment_type'] = "now";
          $notification_array['message'] = $doctor_message;
          $notification_array['user_id'] = $dId;
          $notification_array['p_id'] = $pId;
          $notification_array['d_id'] = $dId;
          
          $notification_array['appointment_id'] = $data['sesskey']->appointment_id;
          $this->appointment_model->insertNotification($notification_array);
            }
            
          
            if($userData['role'] == 3)
            {  
                 $patient_message = 'Doctor '.$data['session']['fname']. ' has started the call.';
                $notification_array =  array();
                $notification_array['notification_type'] = "call";
                $notification_array['appointment_type'] = "now";
                $notification_array['message'] = $patient_message;
                $notification_array['user_id'] = $pId;
                $notification_array['p_id'] = $pId;
                $notification_array['d_id'] = $dId;

                $notification_array['appointment_id'] = $data['sesskey']->appointment_id;
                $this->appointment_model->insertNotification($notification_array);
                 
                 
             }
          
            $this->load->view('header');
            $this->load->view('call', $data);
            $this->load->model('page_model');
            $footer['footer'] = $this->page_model->get_content('footer');
            $this->load->view('footer', $footer);  
        }
        else
        {
            $url = uri_string();
            if(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'])
                $url .= '?' . $_SERVER['QUERY_STRING'];
            $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">Please login before you visit the site!</div>');
            $this->session->set_userdata('redirectUrl', $url);
            redirect();
        }
    }
    
    function callEnd($session_key = NULL)
    {
        $data = array();
        $end_time = time();
        $record = $this->call_model->readSessionKey($session_key);
        if(!$record)
            redirect();

        // Update session key to 0 for redirection of patient
        $row =  array('id' => $record->id, 'status' => 0);
        $this->call_model->updateSessionKey($row);

        // Calculate duration & record to db
        $start_time = $this->session->userdata('start_time');
        $this->session->unset_userdata('start_time');

        $duration = $end_time - $start_time;
        $payment = $this->recordPayment($duration, $record);

        $user_registration = $this->call_model->get_user_registration($record->p_id);
        $patient = $this->call_model->getUserById($record->p_id);

         /*************** DO PAYMENT THINGS HERE *********************/
        $response = $this->takePayment($user_registration, $payment['amount'], $patient, $record->appointment_id);
        if($response['response'] == 'success')
        {
            $patient = $this->call_model->getUserById($record->p_id);
            $doctor = $this->call_model->getUserById($record->d_id);
            $this->call_model->updatePayment($payment['payment_id'], $response['data']);
            $this->call_model->sendPatientInvoiceEmail($patient->email, $patient, $doctor, $payment);
        }
        /*************** END OF PAYMENTS ****************************/

        // Update patient served for doctor
        $this->call_model->updatePatientServed($record->d_id);

        // Update meeting_approval to 3 meaning finished
        $data1 = array(
            'id' => $record->appointment_id,
            'meeting_approval' => 3
        );
        
        $this->call_model->updateAppointmentStatus($data1);
        redirect('call/addPrescription/'. $session_key .'/' . $record->appointment_id);   
    }

    function addPrescription($session_key = NULL, $appointment_id = NULL)
    {
        $data = array();
        if($_POST)
        {
            $prescription = $this->input->post('pres_detail');
            if($prescription)
            {
                $session_key = $this->input->post('session_key');
                $record = $this->call_model->readSessionKey($session_key, FALSE);
                if($record)
                {
                    //delete session key from db
                    $this->call_model->deleteSessionKey($session_key);
                   
                    $appointment_id = $this->input->post('appointment_id');
                    $record = $this->call_model->getAppointmentById($appointment_id);
                    
                    $prescription = $this->input->post('pres_detail');
                    $patient = $this->call_model->getUserById($record->p_id);
                    $this->load->model('admin_model');

                    $doctor = $this->admin_model->readDoctorDetails($record->d_id);
                    $data['appointment_id'] = $appointment_id;
                    $data['pres_detail'] = $prescription;
                    $data['comments'] = $this->input->post('comments');

                    $this->call_model->addPrescription($data);
                    $this->call_model->sendPrescriptionToPatient($patient->email, $patient, $doctor, $prescription);
                    //27122016
                    $this->call_model->sendPrescriptionToAdmin($this->config->item('site_email'), $patient, $doctor, $prescription);
                    // redirect somewhere
                    redirect('call/successPrescription');   
                }
            }
        }

        $data['session_key'] = $session_key;
        $data['appointment_id'] = $appointment_id;

        $this->load->view('header');    
        $this->load->view('add_prescription', $data);
        $this->load->model('page_model');
        $footer['footer'] = $this->page_model->get_content('footer');
        $this->load->view('footer', $footer);
    }

    function successPrescription()
    {
        $this->load->view('header');    
        $this->load->view('success/success_prescription');
        $this->load->model('page_model');
        $footer['footer'] = $this->page_model->get_content('footer');
        $this->load->view('footer', $footer); 
    }

     function successCall($doctorId, $appointmentId)
    {
        $data['doctorId'] = $doctorId;
        $data['appointmentId'] = $appointmentId;
        $this->load->view('header');    
        $this->load->view('success/success_call', $data);
        $this->load->model('page_model');
        $footer['footer'] = $this->page_model->get_content('footer');
        $this->load->view('footer', $footer); 
    }

    function getTimerSession()
    {   
        if(empty($this->session->userdata('start_time')))
            $this->session->set_userdata('start_time', time());

        print $this->session->userdata('start_time');
    }
    
    function checkCallEnd($token = NULL)
    {
        if(!$token)
        {
            $this->session->unset_userdata('start_time');
            echo 'yes';
            exit;
        }    

        $role = 0;

        if(isset($this->session->userdata('logged_in')['role']))
            $role = $this->session->userdata('logged_in')['role'];
        
        if($role == 2)
        {
            $record = $this->call_model->readSessionKey($token);
            if($record)
                print 'no';
            else
             {
                $this->session->unset_userdata('start_time');
                print 'yes';
             }   
        }
        else
            print 'no';         
        exit;
    }

    function recordPayment($duration = 900, $record)
    {
        $mins = floor(($duration / 60) % 60);
        $seconds = $duration % 60;
        if($seconds >= 30)
            $mins = $mins+1;
        
        if($mins < 15)
            $mins = 15;

        $rate = $this->call_model->getDoctorRate($record->d_id);
        $rate = $rate / 15;

        $data['actual_duration']  = gmdate('H:i:s', $duration);
        $data['charged_duration'] = gmdate('H:i:s', $mins * 60);
        $data['rate'] = $rate;
        $data['price'] = round($mins * $rate, 2);
        $data['appointment_id'] = $record->appointment_id;
        $payment_id = $this->call_model->insertPayment($data);
        return array('payment_id' => $payment_id, 'amount' => $data['price']);
    }

    function takePayment($id = NULL, $amount = NULL, $patient, $appointment_id) 
    {
        if(!$id || !$amount)
            return array('response' => 'error', 'error_msg' => 'No registration id or amount');
        $site = $this->config->item('IS_LIVE') ? 'live_' : 'local_';
        $url = "https://test.oppwa.com/v1/registrations/" . $id . "/payments";
        $data = "authentication.userId=" . $this->config->item($site . 'peach_userId') . 
           "&authentication.password=" . $this->config->item($site . 'peach_password') . 
            "&authentication.entityId=" . $this->config->item($site . 'peach_entityId_onceOff') .
            "&amount=" . (string)$amount .
            "&currency=ZAR" .
            "&paymentType=DB" .
            "&recurringType=REPEATED" .
            "&customer.email=" . $patient->email . 
            "&customer.givenName=" . $patient->fname . 
            "&customer.surname=" . $patient->sname . 
            "&merchantTransactionId=" . $appointment_id;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->config->item('IS_LIVE'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
       
        if(curl_errno($ch)) {
            return array('response' => 'error', 'error_msg' => curl_error($ch));
        }
        curl_close($ch);
        $response = json_decode($responseData);
        
        if(!$response)
            return array('response' => 'error', 'error_msg' => 'No data found');

        if($response->result->code = '000.100.110')
            return array('response' => 'success', 'data' => array('transaction_id' => $response->id, 'created_at' => $response->timestamp, 'status' => 1));
        return array('response' => 'error', 'error_msg' => $response->result->description);
    }

    function saveUserLatLng($appointment_id, $lat, $lng)
    {
        if($appointment_id && $lat & $lng)
        {    
            // Update meeting_approval to 3 meaning finished
            $data1 = array(
                'id' => $appointment_id,
                'latitude' => $lat,
                'longitude' => $lng
            );
            
            $this->call_model->updateAppointmentStatus($data1);
            
            print 'User lat-long saved in appointment';
        }
    }

    function getUserLatLng($appointment_id)
    {
        if($appointment_id)
        {    
            // Update meeting_approval to 3 meaning finished
            $result = $this->call_model->getLatLong($appointment_id);
            print $result ? $result['lat'] .','. $result['long'] : NULL;
        }
    }
    
    
    
    function devcalltest()
    {
       //  45710762
       //  1eec7f4892ed1a273f021a1277aa190be2c2f6c1
       $data['apikey'] = '45710762';
       $data['secret'] = '1eec7f4892ed1a273f021a1277aa190be2c2f6c1';
       
       $this->load->view('devcall', $data);   
    }
    
    function devcall($sessionId = null) {
        
       $this->load->library("OpenTok/OpenTok");
	

       $API_KEY     =   '45710762';
       $API_SECRET  =   '1eec7f4892ed1a273f021a1277aa190be2c2f6c1';
        
        //arguments not necessary if you've set the API key in API_config.php in API_Config.php
        $OT = new OpenTok($API_KEY, $API_SECRET);
        ///echo '<pre>'; print_r($OT); die;
   
        if ($sessionId) {
            
                $OT->set_session_id($sessionId);
        } else {
                $OT->generate_session_id();
        }
        
        $OT->generate_token();

        
        echo "Api Key: " . $OT->apiKey;
        echo "<br>Session Id: " . $OT->sessionId;
        echo "<br>token: " . $OT->token; 
        die;
    }


}