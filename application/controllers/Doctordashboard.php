<?php
class Doctordashboard extends CI_Controller
{
    // Local Variables
    private $doctorId;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->library(array('session', 'form_validation', 'email'));
        $this->load->database();
        $this->load->model('user_model');
        $this->load->model('appointment_model');
        $this->load->model('doctor_model');
        $this->load->helper('security');

        $this->doctorId = $this->session->userdata['logged_in']['id'];
         if(!$this->doctorId)
         {
            $url = uri_string();
            if(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'])
                $url .= '?' . $_SERVER['QUERY_STRING'];
            $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">Please login before you visit the site!</div>');
            $this->session->set_userdata('redirectUrl', $url);
            redirect();
         }
        elseif ($this->session->userdata['logged_in']['role'] != 3) 
        {
          $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">You are not authorized to visit this page. Sorry!</div>');
          redirect();
        }
    }
    
    function index()
    {
        $doctorId = $this->doctorId;    
        $data = $this->doctor_model->readDoctorNowAppointment($doctorId);
        
        $data['now_appointment_detail'] = isset($data[0]) ? $data[0] : NULL;
        $patientId = isset($data[0]) ? $data[0]->p_id : NULL;
        $data['apppoinment_now_id'] = isset($data[0]) ? $data[0]->id : NULL;
        $data['appointment_link'] = $this->doctor_model->getLastestAppointmentLink($doctorId);
        $data['doctorrating'] = $this->doctor_model->readDoctorRating($doctorId);
        $data['patientsInfo'] = $patientId ? $this->doctor_model->readPatient($patientId) : NULL;
        $data['allappointments'] = $this->doctor_model->readAllScheduledAppointments($doctorId);
        $data['finished_appointments'] = $this->doctor_model->readAllPreviousAppointments($doctorId);
        $data['missed_appointments'] = $this->doctor_model->readAllPreviousAppointments($doctorId, 1);

        $header_data['breadcrumbs']['doctor_dashboard'] = TRUE;
        $this->load->view('header', $header_data);
        $this->load->view('doctor/doctor_dashboard',$data);
        $this->load->model('page_model');
        $footer['footer'] = $this->page_model->get_content('footer');
        $this->load->view('footer', $footer);
    }
    
  function approveAppointmentRequest()
  {
      
    $doctorId = $this->doctorId;
    $appointment_id = $this->input->post('appointment_id');
    $result = $this->doctor_model->DoctorBookAppointment($doctorId, $appointment_id); // Doctor's Approved patient request of appointment
//    $result = $this->doctor_model->readDoctorNowAppointment($doctorId, FALSE);
    if($result)
    {
       
      $patientId = $result[0]->p_id;
      $patientInfo = $this->doctor_model->readPatient($patientId);    
      $appointmentId = $result[0]->id;

      $action = $this->input->post('button');
      $meeting_approval = ($action == 'deny') ? 2 : 1;

      $data = array(
       'id' => $appointmentId,
       'meeting_approval' => $meeting_approval
      );
      
      $this->doctor_model->updateApproveStatus($data); 
      
      $patientName = $patientInfo[0]->fname .' ' . $patientInfo[0]->sname;
      $patientEmail = $patientInfo[0]->email;
      $toEmail = $this->session->userdata['logged_in']['email'];
      $doctorName = $this->session->userdata['logged_in']['fname'];
      $randomString = '';

      if($action == 'approve')
      {
        
        $dId = $this->session->userdata['logged_in']['id'];
        $pId = $result[0]->p_id;
        $confirmSendEmail = $this->doctor_model->readApprovedNowAppointment($dId,$pId );  
        $length = 10;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        
        for ($i = 0; $i < $length; $i++)
            $randomString .= $characters[rand(0, $charactersLength - 1)];
       
        if($confirmSendEmail)
        {
          $time = new DateTime('+5 mins');
          $appointment = array(
            'p_id' => $confirmSendEmail[0]->p_id,
            'd_id' => $confirmSendEmail[0]->d_id,
            'meeting_approval' => $confirmSendEmail[0]->meeting_approval,
            'brief_description' => $confirmSendEmail[0]->brief_description,
            'med_taken' => $confirmSendEmail[0]->med_taken,
            'patient' => $confirmSendEmail[0]->patient,
            'type' => 'now',
            'start_time' => $time->format('Y-m-d H:i:s'),
            'end_time' => $time->modify('+ 15 mins')->format('Y-m-d H:i:s')
          );

          $appointmentId = $this->doctor_model->addAppointment($appointment);  
          $sessInfo = array(
              'p_id' => $pId,
              'd_id' => $dId,
              'session_key' => $randomString,
              'appointment_id' => $appointmentId
           );
          
          $this->doctor_model->updateSymptoms(array('appointment_id' => $appointmentId), $pId); 
          $this->doctor_model->insertSessionKey($sessInfo);
          $this->doctor_model->deleteAllAppointmentNow($pId, $confirmSendEmail[0]->created_at);
          $this->appointment_model->sendEmailToDoctor($toEmail, $doctorName, $randomString, $action);
          $this->appointment_model->sendEmailToPatient($patientEmail, $patientName, $randomString, $action, $doctorName);
        }
        
      }
      
      
      
//      if($action == 'approve')
//        $message = '<div class="alert alert-success text-center">You have approved the Appointment request. Above is the meeting url which is also emailed to you. Your meeting will start in 5 minutes.</div>';
//      else
//        $message = '<div class="alert alert-danger text-center">You have denied the Appointment request. An email has been sent to notify the patient.</div>';
    }  
    else
//      $message = '<div class="alert alert-danger text-center">Sorry some other doctor has already approved the meeting.</div>';
      $message = '<div class="alert alert-danger text-center">Kindly note that the patient has already been attended to.</div>';

    $this->session->set_flashdata('approved', $message);
    redirect('doctordashboard');      
  }

    function profile()
    {
      $doctorId = $this->doctorId;
      $this->load->model('admin_model');
      $data['row'] = $this->admin_model->readDoctorDetails($doctorId);
      $data['doctorAvailableDays'] = $this->admin_model->readDoctorAvailableDays($doctorId);
      $data['doctorTimings'] = $this->admin_model->readDoctorTimings($doctorId);
      $data['doctorStatus'] = 1;
      $data['editProfile'] = TRUE;
      
      $header_data['breadcrumbs']['doctor_profile'] = TRUE;
      $this->load->view('header', $header_data);  
      $this->load->view('admin/doctordetail', $data);
      $this->load->model('page_model');
      $footer['footer'] = $this->page_model->get_content('footer');
      $this->load->view('footer', $footer);
    }
    
    function editProfile()
    {
      $doctorId = $this->doctorId;
      $this->load->model('admin_model');
      
      if($_POST)
      {
        $data = array(
           // 'fname'   => $this->input->post('fname'),
           //  'sname'  => $this->input->post('sname'),
           //  'email'  => $this->input->post('email'),
            'phone'  => $this->input->post('phone'),
            'address'=> $this->input->post('address'),
            'city'=> $this->input->post('city'),
            'province'=> $this->input->post('province'),
            'role'   => 3,
            'gender' => $this->input->post('gender')
        );
        
        $data1 = array(  
          //  'degree' => $this->input->post('degree'),
            'description' => $this->input->post('description'),
            'profession_number' => $this->input->post('profession_number'),
            'practice_number' => $this->input->post('practice_number'),
          //  'experience' => $this->input->post('experience'),
            'speciality' => $this->input->post('speciality'),
            //'type_of_doctor' => $this->input->post('type_of_doctor'),
        );
        
        $data2      = $this->input->post('dayoftheweek');
        $timings    = $this->input->post('timings');
        if($timings == 'fixed')
        {
          $fromTiming = date('H:i:s', strtotime($this->input->post('from-timimg')));
          $toTimimg = date('H:i:s', strtotime($this->input->post('to-timimg')));
          $data3 =  array(
                      array(
                      'start_time' => $fromTiming,
                      'end_time' => $toTimimg
                    ));
        }
        else
        {
            
           $flexFromTime1 =  $this->input->post('flex-from-timing');
           $flexToTime1 =  $this->input->post('flex-to-timing');
          
           
           $data3 = array(
                        array(
                          'start_time' => date('H:i:s', strtotime($flexFromTime1[0])),
                          'end_time' => date('H:i:s', strtotime($flexToTime1[0])),
                        ),
                        array(
                          'start_time' => date('H:i:s', strtotime($flexFromTime1[1])),
                          'end_time' => date('H:i:s', strtotime($flexToTime1[1])),
                          ));
        }

         if(isset($_FILES))
         {
             // Upload Doctor Profile
            $config['upload_path']          = './uploads/profile/';
            $config['file_name']            = 'pic-' . $doctorId;
            $config['allowed_types']        = 'gif|jpg|png';
            
            //$config['allowed_types']        = 'gif|jpg|png|docx|doc';
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $thumb_path = realpath(getcwd()) . '/uploads/' ;
            if (!is_dir($thumb_path))
            {
                mkdir($thumb_path);
                chmod($thumb_path, 0777);
            }

            if(isset($_FILES['profile_pic']['tmp_name']))
            {
                $thumb_path = realpath(getcwd()) . '/uploads/profile/' ;
                if (!is_dir($thumb_path))
                {
                    mkdir($thumb_path);
                    chmod($thumb_path, 0777);
                }

               if($this->upload->do_upload('profile_pic'))
               {
                  $data_file = array('upload_data' => $this->upload->data());
                  $data1['profile_pic'] = $data_file['upload_data']['file_name'];
                  generate_preset($data_file['upload_data']['full_path']);
               }
            }

             // Upload Doctor Profile
            $config['upload_path']          = './uploads/cv/';
            $config['file_name']            = 'cv-' . $doctorId;
            $config['allowed_types']        = 'docx|doc|pdf';
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if(isset($_FILES['cv']['tmp_name']))
            {
              $thumb_path = realpath(getcwd()) . '/uploads/cv/' ;
              if (!is_dir($thumb_path))
              {
                  mkdir($thumb_path);
                  chmod($thumb_path, 0777);
              }

              if ($this->upload->do_upload('cv'))
              {
                $data_file = array('upload_data' => $this->upload->data(), '');
                $data1['cv'] = $data_file['upload_data']['file_name'];
              }
            }

         }

        $this->load->model('doctor_model', 'admin_model');
        $this->doctor_model->updateDoctor($data, $doctorId); 
        $this->doctor_model->updateDoctorInfo($data1, $doctorId);

        $oldDays = $this->admin_model->readDoctorAvailableDays($doctorId);
        $daysChanged = $shootDayEmail = FALSE;
        foreach($oldDays as $key => $value)
        {
          if(! in_array($key, $data2))
          {
            $shootDayEmail = TRUE;
            $daysChanged = TRUE;
          }
            
        }

        if(! $daysChanged)
        {
          if(count($oldDays) != count($data2))
          {
            $daysChanged = TRUE; 
          }
        }
        
        if($daysChanged)
          $this->doctor_model->updateDoctorDays($data2, $doctorId);

        $oldTimings = $this->admin_model->readDoctorTimings($doctorId);
        $timingsChanged = $shootTimingEmail = FALSE;
        if(count($oldTimings) != count($data3))
        {
          $timingsChanged = TRUE;
          if(count($oldTimings) > count($data3))
          {
              if($oldTimings[0]->start_time < $data3[0]['start_time'])
                $shootTimingEmail = TRUE;
              if($oldTimings[1]->end_time > $data3[0]['end_time'])
                $shootTimingEmail = TRUE;
          }
          else
          {
              if($oldTimings[0]->start_time < $data3[0]['start_time'])
                $shootTimingEmail = TRUE;
              if($oldTimings[0]->end_time > $data3[0]['end_time'])
                $shootTimingEmail = TRUE;
          }
        }
        else
        {
          foreach($data3 as $key => $row)
          {
            if($row['start_time'] != $oldTimings[$key]->start_time)
            {
              if($row['start_time'] > $oldTimings[$key]->start_time)
                $shootTimingEmail = TRUE;
              $timingsChanged = TRUE;
              break;
            }

            if($row['end_time'] != $oldTimings[$key]->end_time)
            {
              if($row['end_time'] < $oldTimings[$key]->end_time)
                $shootTimingEmail = TRUE;
              $timingsChanged = TRUE;
              break;
            }
              
          }
        }
        if($timingsChanged)
          $this->doctor_model->updateDoctorTiming($data3, $doctorId);
        
        // Shoot an email to patients to notify days are changed.
        if($shootDayEmail || $shootTimingEmail)
          $this->doctor_model->sendTimingsChangedEmail($doctorId, $oldTimings, $oldDays);
        
            $qualification = $this->input->post('qualification');
            $institution = $this->input->post('institution');
            $year = $this->input->post('year');

            if(isset($qualification[0]) && $qualification[0])
            {
              $this->doctor_model->deleteAcademics($doctorId);   
              foreach($qualification as $key => $value)
              {
                if($value)
                {
                  $data5 = array(
                    'qualification' => $value,
                    'institution' => $institution[$key],
                    'year' => $year[$key],
                    'user_id' => $doctorId
                  );
                  $this->doctor_model->insertAcademics($data5);
                }
              }
            }
            
            

            $company        = $this->input->post('company');
            $job_title      = $this->input->post('job_title');
            $start_yy       = $this->input->post('start_yy');
            $start_mm       = $this->input->post('start_mm');
            $end_yy         = $this->input->post('end_yy');
            $end_mm         = $this->input->post('end_mm');
            $present_here   = $this->input->post('stay_here');
            $get_present_here  =   $present_here[0];

            if(isset($company[0]) && $company[0])
            {
              $this->doctor_model->deleteExperience($doctorId);
              $checkCounter = 1;
              foreach($company as $key => $value)
              {
                  
                  if($value && $job_title[$key])
                  {   
                    $present_here = ($checkCounter == $get_present_here) ? '1' : '0' ;
                    //echo '<pre>'; print_r($this->input->post()); die;
                    $data6 = array(
                      'company'       => $value,
                      'job_title'     => $job_title[$key],
                      'start_date'    => date('Y-m-d', strtotime($start_yy[$key] . '-' . $start_mm[$key])),
                      'end_date'      => date('Y-m-d', strtotime($end_yy[$key] . '-' . $end_mm[$key])),
                      'present_here'  => $present_here,
                      'user_id'      => $doctorId
                    );
                    //echo '<pre>'; print_r($data6);
                    //echo '<pre>';print_r($data6); die;
                    $this->doctor_model->insertExperience($data6);
                  }  
                    $checkCounter++;
              }
               //die;
              
              
             /* foreach($company as $key => $value)
              {
                if($value)
                {
                    if($present_here[$key] == $checkCounter){
                        $present_here = $getIndex[1];
                    }  else {
                        $present_here = 0;
                    }
                     
                    //echo '<pre>'; print_r($this->input->post()); die;
                    $data6 = array(
                      'company'       => $value,
                      'job_title'     => $job_title[$key],
                      'start_date'    => date('Y-m-d', strtotime($start_yy[$key] . '-' . $start_mm[$key])),
                      'end_date'      => date('Y-m-d', strtotime($end_yy[$key] . '-' . $end_mm[$key])),
                      'present_here'  => $present_here,
                       'user_id'      => $doctorId
                    );
                    //echo '<pre>';print_r($data6); die;
                    $this->doctor_model->insertExperience($data6);
                    $checkCounter++;
                }
              }*/
              
            }
               //die;
        redirect('doctordashboard/profile');
      }

      $data['record'] = $this->admin_model->readDoctorDetails($doctorId);
      $data['doctorDays'] = $this->admin_model->readDoctorAvailableDays($doctorId);
      $data['doctorTimings'] = $this->admin_model->readDoctorTimings($doctorId);
      
      $header_data['breadcrumbs']['doctor_profile'] = TRUE;
      $this->load->view('header', $header_data);
      $this->load->view('doctor/edit_doctor_profile', $data);
      $this->load->model('page_model');
      $footer['footer'] = $this->page_model->get_content('footer');
      $this->load->view('footer', $footer);
    }

    function userProfile($userId)
    {
      if(!$userId)
        return NULL;

      $this->load->model('user_profile');
      $result['userinfo'] = $this->user_profile->readUserInfo($userId);
      $result['userprofile'] = $this->user_profile->readUserProfiles($userId);
      $result['userallergies'] = $this->user_profile->readAllergies($userId);
      $result['userpecs'] = $this->user_profile->readPreExistingCondition($userId); 
      $result['userpersonalupdates'] = $this->user_profile->readPersonalUpdate($userId);  
      $result['userfamilyhistory'] = $this->user_profile->readFamilyHistory($userId);
      $result['usermydoctor'] = $this->user_profile->readMyDoctor($userId);
      $result['previousHistory'] = $this->user_profile->readPreviousHistory($userId);
      $result['dontShowUpdate'] = TRUE;
      
      $this->load->view('header');
//      $this->load->view('user/display_medical_history', $result);
      $this->load->view('user/display_medical_history_new', $result);
      $this->load->model('page_model');
      $footer['footer'] = $this->page_model->get_content('footer');
      $this->load->view('footer', $footer);
    }
}