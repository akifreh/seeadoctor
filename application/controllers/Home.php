<?php
class Home extends CI_Controller 
{    
    public function __construct() 
    {
        parent::__construct();
        $this->load->library(array('session', 'form_validation', 'email', 'upload'));
        $this->load->helper(array('form', 'url'));
        $this->load->database();
        $this->load->model('home_model');
        $this->load->model('user_model');
        $this->load->helper('security');
    }

    function index() 
    {  
   
        if(isset($this->session->userdata['logged_in']['role']))
        {
            $role = $this->session->userdata['logged_in']['role'];
            if($role == 2)
                redirect('appointment');
            elseif($role == 3)
                redirect('doctordashboard');
        }
        
        $data['doctor_count'] = $this->home_model->getUserCount(3);
        //$data['patient_count'] = $this->home_model->getUserCount(2);
        $data['patient_count'] = $this->home_model->getServedPatientCount();
        $data['best_doctors'] = $this->home_model->getBestDoctors();

        $this->load->model('page_model');
        $data['fee_section'] = $this->page_model->get_content('fees');
        
        $this->load->view('header');
        $this->load->view('user_registration');
        $this->load->view('home', $data);
        $footer['footer'] = $this->page_model->get_content('footer');
        $this->load->view('footer', $footer);
    }

    function register() 
    {        
        $b_dd = $this->input->post('b_dd');
        $b_mm = $this->input->post('b_mm');
        $b_yy = $this->input->post('b_yy');
        $dob = $b_yy . $b_mm . $b_dd;

        $data = array(
            'fname' => $this->input->post('fname'),
            'sname' => $this->input->post('sname'),
            'email' => $this->input->post('email'),
            'password' => md5($this->input->post('password')),
            'phone' => $this->input->post('phone'),
            'gender' => $this->input->post('gender'),
            'dob' => $dob,
            'role' => 2       
        );
        
        $cc_number = $this->input->post('cc_number');
        $cc_number = str_replace(' ', '', $cc_number);

        $exp_dd = $this->input->post('ex_dd');
        $exp_mm = $this->input->post('ex_mm');
        $exp_yy = $this->input->post('ex_yy');

        /***********  CLOSING THIS FOR FIRST SIGN UP *************************
        $data1 = array(
            'cc_type' => $this->input->post('cc_type'),
            'cc_name' => $this->input->post('fname') . ' ' . $this->input->post('sname'),
            'cc_number' => $cc_number,
            'cc_cvv' => $this->input->post('cc_csv_number'),
            'cc_exp_yy' => $exp_yy,
            'cc_exp_mm' => $exp_mm
        );

        $result = $this->getUserToken($data1);   
        if($result['response'] == 'error')
        {
            $this->session->set_flashdata('allreadyexist', '<div class="alert alert-danger text-center">' . $result['error_msg'] . '</div>');
            redirect();
        }

        $data1 = array(
                    'registration_id ' => $result['id'],
                    'card_brand' => $this->input->post('cc_type'),
                    'card_expiry' => date('Y-m-d', strtotime($exp_yy.'-'.$exp_mm.'-01')),
                    'card_number' =>  substr($cc_number, -4)
                );
        ****************************  CLOSING THIS FOR FIRST SIGN UP *******************/
        $userEmail = $this->input->post('email');
        $userExist = $this->user_model->readAlreadyExistUser($userEmail);
        if($userExist)
        {
            $this->session->set_flashdata('allreadyexist', '<div class="alert alert-danger text-center">The Email is already exists. Please choose another one.</div>');
            redirect();
        }
        else
        {
            // insert form data into database
            if ($this->user_model->insertUser($data)) // OMITTED CARD DETAILS, $data1)) 
            {
                 // send email
                if ($this->user_model->sendEmail($this->input->post('email'))) 
                {
                    // successfully sent mail
                    $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">You are Successfully Registered! Please confirm the mail sent to your Email-ID!!!</div>');
                    redirect();
                }
            } 
            else 
            {
                // error
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                redirect();
            }
        }  
        
    }

    function verify($hash = NULL) 
    {
        if ($this->user_model->verifyEmailID($hash)) 
        {
            $this->session->set_flashdata('verify_msg', '<div class="alert alert-success text-center">Your Email Address is successfully verified! Please login to access your account!</div>');
            redirect();
        } 
        else 
        {
            $this->session->set_flashdata('verify_msg', '<div class="alert alert-danger text-center">Sorry! There is error verifying your Email Address!</div>');
            redirect('home/register');
        }
    }

    // Check for user login process
    public function user_login_process() 
    {
        $emailPass = array('email' => $this->input->post('email'),'password' => $this->input->post('password'));
        $userResult = $this->user_model->checkUserPass($emailPass);

        if (!$userResult) 
        {
            if(isset($this->session->userdata['logged_in']))
            {
                if($redirectUrl = $this->session->userdata('redirectUrl'))
                {
                    $this->session->unset_userdata('redirectUrl');
                    redirect($redirectUrl);
                }
                
                $role = isset($this->session->userdata['logged_in']['role']) ? $this->session->userdata['logged_in']['role'] : 2;
                if($role == 1)
                    redirect('admin/index');
                elseif($role == 2)
                    redirect('appointment');
                else
                    redirect('doctordashboard');
            }
            else
            {
                $this->session->set_flashdata('error', '<span class="error">Error! Please enter valid username or password</span>');
                redirect();      
            }

        } 
        else 
        {     
            $data = array(
                'email' => $this->input->post('email'),
                'password' => $this->input->post('password')
            );

            $admin = $this->user_model->getAdmin($data);
            if($admin == TRUE )
            {     
                $email = $this->input->post('email');
                $admin = $this->user_model->read_user_information($email);
                
                if($admin) 
                {
                    $session_data = array(
                        'fname' => $admin[0]->fname,
                        'email' => $admin[0]->email,
                        'id' => $admin[0]->id,
                        'role' => $admin[0]->role
                    );

                    // Add user data in session
                    $this->session->set_userdata('logged_in', $session_data);
                }

                if($redirectUrl = $this->session->userdata('redirectUrl'))
                {
                    $this->session->unset_userdata('redirectUrl');
                    redirect($redirectUrl);
                }
                redirect('admin/index');
               
            }

            // Patient Login Process
            $doctor = $this->user_model->loginUser($data);
            if ($doctor == TRUE) 
            {
                $email = $this->input->post('email');
                $doctor = $this->user_model->read_user_information($email);
                
                if ($doctor) 
                {
                    $session_data = array(
                        'fname' => $doctor[0]->fname,
                        'email' => $doctor[0]->email,
                        'id' => $doctor[0]->id,
                        'role' => $doctor[0]->role
                    );
                    
                    $this->session->set_userdata('logged_in', $session_data);
                    
                    if($redirectUrl = $this->session->userdata('redirectUrl'))
                    {
                        $this->session->unset_userdata('redirectUrl');
                        redirect($redirectUrl);
                    }
                    
                    $this->session->set_flashdata('online', '<div class="alert alert-success text-center">Hello ' . ucfirst($doctor[0]->fname) .', ' .  ucfirst($doctor[0]->sname) . ' welcome, we look forward to helping you feel better</div>');
                    redirect('appointment');
                }
            }

            // Doctor Login Process
            $result = $this->user_model->loginDoctor($data);
            if ($result) 
            {                
                $email = $this->input->post('email');
                $result = $this->user_model->read_user_information($email);
                
                if ($result) 
                {
                    $session = session_id();
                    $user_id = $result[0]->id;
                    $time = time();
                    
                    $session_data = array(
                        'fname' => $result[0]->fname,
                        'email' => $result[0]->email,
                        'id' => $user_id,
                        'role' => $result[0]->role,
                        'session' => $session
                    );

                    $sessionData = array(
                        'session' => $session,
                        'time' => $time,
                        'user_id' => $user_id
                    );

                    // Add user data in session
                    $this->session->set_userdata('logged_in', $session_data);
                    $onlineDoctor = $this->user_model->readDoctorSession($session);
                    
                    if($onlineDoctor)
                        $this->user_model->updateDoctorSession($sessionData);
                    else
                        $this->user_model->insertDoctorSession($sessionData);  
                                        
                    if($redirectUrl = $this->session->userdata('redirectUrl'))
                    {
                        $this->session->unset_userdata('redirectUrl');
                        redirect($redirectUrl);
                    } 
                    redirect('doctordashboard');
                }
            } 
            else
            {
                $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">Please type correct email and password or contact the Administrator!</div>');
                redirect();
            }   
        }
    }
    
    // Logout from admin page
    public function logout() 
    {
        $userId = $this->session->userdata['logged_in']['id'];
        $result = $this->user_model->readSessionInfo($userId);
        if($result)
            $this->user_model->deleteDoctorSession($userId);                      
    
        // Removing session data
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('redirectUrl');
        $this->session->unset_userdata('start_time');
        $this->session->set_flashdata('verify_msg','<div class="alert alert-success text-center">Thank you for visiting our site. Hope you come again soon!</div>');
        redirect();
    }
    
    function secondSignup($add_card = NULL)
    {
       
        
        
        if( empty($this->session->userdata['logged_in']['email']))
        {
            $url = uri_string();
            if(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'])
                $url .= '?' . $_SERVER['QUERY_STRING'];
            $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">Please login before you visit the site!</div>');
            $this->session->set_userdata('redirectUrl', $url);
            redirect();
        }
            

        if($_POST)
        {   
            $type = $this->input->post('submit');
            $error = FALSE;
            if($type == 'personal')
            {
                $b_dd = $this->input->post('b_dd');
                $b_mm = $this->input->post('b_mm');
                $b_yy = $this->input->post('b_yy');
                $dob = $b_yy . $b_mm . $b_dd;  

                $data = array(
                    'id' => $this->input->post('id'),
                    'address' => $this->input->post('address'),
                    'city' => $this->input->post('city'),
                    'province' => $this->input->post('province'),   
                    'phone' => $this->input->post('phone'),
                    'p_address' => $this->input->post('p_address'),
                    'p_city' => $this->input->post('p_city'),
                    'p_province' => $this->input->post('p_province'),
                    'sameasres' => $this->input->post('sameasres'),
                    'dob' => $dob
                );

                $config['upload_path']          = './uploads/profile';
                $config['allowed_types']        = 'gif|jpg|png';

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if(isset($_FILES['profile_pic']['tmp_name']) && $_FILES['profile_pic']['tmp_name'])
                {
                    $thumb_path = realpath(getcwd()) . '/uploads/profile/' ;
                    if (!is_dir($thumb_path))
                    {
                        mkdir($thumb_path);
                        chmod($thumb_path, 0777);
                    }

                    if ( ! $this->upload->do_upload('profile_pic'))
                    {
                            $error = TRUE;
                            $data['error'] = $this->upload->display_errors();
                    }
                    else
                    {
                            $data_file = array('upload_data' => $this->upload->data());
                            $data['profile_pic'] = $data_file['upload_data']['file_name'];
                            generate_preset($data_file['upload_data']['full_path']);
                    }
                }  

            }
            elseif($type == 'card')
            {
                $cc_number = str_replace(' ', '', $this->input->post('cc_number')); 
                $data1 = array(
                    'cc_type' => $this->input->post('cc_type'),
                    //'cc_name' => $this->input->post('fname') . ' ' . $this->input->post('sname'),
                    'cc_number' => $cc_number,
                    'cc_cvv' => $this->input->post('cc_csv_number'),
                    'cc_exp_yy' => $this->input->post('ex_yy'),
                    'cc_exp_mm' => $this->input->post('ex_mm')
                );

                $result = $this->getUserToken($data1);

                if($result['response'] == 'error')
                {
                    $data['error'] = $result['error_msg'];
                    $error = TRUE;
                }
                else
                {
                    $data1 = array(
                                'registration_id ' => $result['id'],
                                'card_brand' => $this->input->post('cc_type'),
                                'card_expiry' => date('Y-m-d', strtotime($this->input->post('ex_yy').'-'.$this->input->post('ex_mm').'-01')),
                                'card_number' =>  substr($cc_number, -4)
                            );
                    $this->user_model->updateCardInfo($data1, $this->input->post('id'));                         
                    $error = TRUE;
                    $this->session->set_flashdata('msg','<div class="alert alert-success text-center">Your card information has been updated successfully.</div>');
                    //redirect('home/complete-profile');
                }
                
            }
            elseif($type == 'job')
            {
                $hasjob = $this->input->post('has_job');
                $hasaid = $this->input->post('has_aid');
                if($hasjob == 'no')
                {
                    $job_title = 'No';
                    $company_employed = 'None';
                }
                else
                {
                    $job_title = $this->input->post('job_title');
                    $company_employed = $this->input->post('company_employed');
                }

                if($hasaid == 'no')
                    $medical_aid = 'No';
                else
                    $medical_aid = $this->input->post('medical_aid');

                $data = array(
                    'id' => $this->input->post('id'),
                    'job_title' => $job_title,
                    'company_employed' => $company_employed,
                    'next_of_kin' => $this->input->post('next_of_kin'),
                    'medical_aid' => $medical_aid,
                );
            }

            if(!$error)
            {
                //echo '<pre>'; print_r($data); die;
                $this->user_model->updateUser($data);
                $this->session->set_flashdata('msg','<div class="alert alert-success text-center">Your profile has been updated successfully.</div>');
                //redirect('home/complete-profile');
            }
        }

        $email = $this->session->userdata['logged_in']['email'];
        $id = $this->session->userdata['logged_in']['id'];
        $data['userinfo'] = $this->user_model->read_user_information($email);
        //echo '<pre>'; print_r($userinfo); die;
        $data['otherprofile'] = $this->user_model->readOtherProfile($id);
        $data['cardInfo'] = $this->user_model->readCardInfo($id);
        $date = $data['cardInfo']->card_expiry;
        $data['add_card'] = $add_card;

        if(isset($data['userinfo'][0]) && $data['userinfo'][0])
        {
            $data['userinfo'][0]->profile_pic = get_image_url($data['userinfo'][0]->profile_pic);
        }

        $header_data['breadcrumbs']['user_profile'] = TRUE;
        $this->load->view('header', $header_data);
        $this->load->view('user/edit_profile', $data);
        $this->load->model('page_model');
        $footer['footer'] = $this->page_model->get_content('footer');
        $this->load->view('footer', $footer);
    }

    function getUserToken($data = array()) 
    {
        $site = $this->config->item('IS_LIVE') ? 'live_' : 'local_';
        $url = "https://test.oppwa.com/v1/registrations";
        $data = "authentication.userId=" . $this->config->item($site . 'peach_userId') . 
                "&authentication.password=" . $this->config->item($site . 'peach_password') . 
                "&authentication.entityId=" . $this->config->item($site . 'peach_entityId_onceOff') .
                "&paymentBrand=" . $data['cc_type'] . 
                "&card.number=" . $data['cc_number'] .
            //    "&card.holder=" . $data['cc_name'] .
                "&card.expiryMonth=" . $data['cc_exp_mm'] .
                "&card.expiryYear=" . $data['cc_exp_yy'] .
                "&card.cvv=" . $data['cc_cvv'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->config->item('IS_LIVE'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);

        if(curl_errno($ch)) {
            return array('response' => 'error', 'error_msg' => curl_error($ch));
        }
        curl_close($ch);
        $response = json_decode($responseData);
        
        if(!$response)
            return array('response' => 'error', 'error_msg' => 'Credit Card registration error.');
        
        if($response->result->code == '000.100.110')
            return array('response' => 'success', 'id' => $response->id);
            
        return array('response' => 'error', 'error_msg' => 'Credit Card registration error. ' . $response->result->description);
    }
	
	function viewDoctorDetail($id)
    {       
            $this->load->model('admin_model');
            $this->load->model('doctor_model');
			$data['row'] = $this->admin_model->readDoctorDetails($id);
            $data['doctorAvailableDays'] = $this->admin_model->readDoctorAvailableDays($id);
            $data['doctorTimings'] = $this->admin_model->readDoctorTimings($id);
            $data['doctorStatus'] = $this->admin_model->doctorStatus($id);
            $data['doctorrating'] = $this->doctor_model->readDoctorRating($id);

            $this->load->view('header');
            $this->load->view('admin/doctordetail-home', $data);
            $this->load->model('page_model');
            $footer['footer'] = $this->page_model->get_content('footer');
            $this->load->view('footer', $footer);
     }
}