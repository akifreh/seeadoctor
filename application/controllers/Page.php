<?php
class Page extends CI_Controller 
{    
    public function __construct() 
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session', 'form_validation'));
        $this->load->model('page_model');
        $this->load->database();
	}
	
	public function save($type = 'policy') 
	{		
        if(!isset($this->session->userdata['logged_in']['role']))
        {
            $url = uri_string();
            if(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'])
                $url .= '?' . $_SERVER['QUERY_STRING'];
            $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">Please login before you visit the site!</div>');
            $this->session->set_userdata('redirectUrl', $url);
            redirect();
        }
        elseif ($this->session->userdata['logged_in']['role'] != 1) 
        {
          $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">You are not authorized to visit this page. Sorry!</div>');
          redirect();
        }

		if($_POST)
		{
			$record['title'] = $this->input->post('title');
			$record['content'] = $this->input->post('content');
			$record['type'] = $this->input->post('type');
			$this->page_model->save_content($record);
			redirect('page/show/' . $record['type']);
		}
		
		$data['type'] = $type;
		$data['type_title'] = ($type!='apply_doctor_page') ? ucfirst($type) : 'Apply Doctor';
		$content = $this->page_model->get_content($type);
		$data['title'] = $content ? $content->title : NULL;
		$data['content'] = $content ? $content->content : NULL;

		$this->load->view('header');
		$this->load->view('pages/edit', $data);
        $footer['footer'] = $this->page_model->get_content('footer');
        $this->load->view('footer', $footer);
	}

	public function show($type = 'policy') 
	{	
		$data = array();
		$content = $this->page_model->get_content($type);
		$data['title'] = $content ? $content->title : NULL;
		$data['content'] = $content ? $content->content : NULL;

		$this->load->view('header');
		$this->load->view('pages/show', $data);
        $footer['footer'] = $this->page_model->get_content('footer');
        $this->load->view('footer', $footer);
	}
}