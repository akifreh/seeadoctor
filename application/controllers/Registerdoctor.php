<?php
class Registerdoctor extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->library(array('session', 'form_validation', 'email'));
        $this->load->database();
        $this->load->model('doctor_model');
        $this->load->model('user_model');
        $this->load->helper('security');
    }
    
    function index()
    {
        $this->load->model('page_model');
        $data['page'] = $this->page_model->get_content('apply_doctor_page');

        $header_data['breadcrumbs']['apply_doctor'] = TRUE;
        $this->load->view('header', $header_data);
        $this->load->view('doctor_registration', $data);
       
        $footer['footer'] = $this->page_model->get_content('footer');
        $this->load->view('footer', $footer);
        
    }
    
    function applyAsDoctor()
    {
      $docDay = $this->input->post('dayoftheweek[]');
      $docTime = $this->input->post('timings');
      $docEmail = $this->input->post('email');
      $doctorExist = $this->user_model->readAlreadyExistUser($docEmail);
      $toTime = $this->input->post('to-timimg');
      $fromTime = $this->input->post('from-timimg');
      
      $err = FALSE;
      if(empty($docDay))
      {
           $errorMsg = 'Please Choose your day(s)';
           $err = TRUE;
      }
      
      if(empty($docTime))
      {
           $errorMsg = 'Please Choose your timing(s)';
           $err = TRUE;
      }
      else
      {
        if(!isset($toTime)|| !isset($fromTime))
        {
          $errorMsg = 'Please Choose your timing(s)';
          $err = TRUE;
        }
      }
      
      if($err == FALSE)
      {
        if($doctorExist == TRUE)
        {
          $errorMsg = 'User Already Exists! Try another email to signup';
          $err = TRUE;
        }
      }
      // Pictures and cv removed for now     
      //$picture = $_FILES['profile_pic'];
      $file = $_FILES['cv'];
      
      if ( ($err === TRUE) || (!isset($file)) ) // || (!isset($picture))))
      {     
         $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">'. $errorMsg.'</div>');
         redirect('registerdoctor');  
      } 
      else
      {
        $onceaweek = $this->input->post('doctor-airman');
        $daily = $this->input->post('Rank');
        $onceaweek = $this->input->post('doctor-airman');
        $daily = $this->input->post('Rank');
        
        if($daily === 'daily')
        {
          $dayStr = $daily;
        }
        elseif($daily === 'airman')
        {
            $dayStr = $this->input->post('doctor-airman');
        }
        elseif($daily === 'senior-airman')
        {    
          $customday = $this->input->post('dayoftheweek');
          $dayStr = implode(',', $customday);  
        }

        $data = array(
           'fname' => $this->input->post('fname'),
            'sname' => $this->input->post('sname'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone'),
            //'address' => $this->input->post('address'),
            'role' => 3,
            //'gender' => $this->input->post('gender'),    
        );
        
        $data1 = array(
            //'degree' => $this->input->post('degree'),
            'description' => $this->input->post('description'),
            'profession_number' => $this->input->post('profession_number'),
            'practice_number' => $this->input->post('practice_number'),
            //'experience' => $this->input->post('experience'),
            //'speciality' => $this->input->post('speciality'),
            'type_of_doctor' => $this->input->post('type_of_doctor'),
        );
        
        $data2 = array('day'=> $this->input->post('dayoftheweek'));
         
        $fromTiming = $this->input->post('from-timimg');
        $toTimimg = $this->input->post('to-timimg');
       
        if(!empty($fromTiming) && !empty($toTimimg))
        {
          $fromTiming = date('H:i:s', strtotime($this->input->post('from-timimg')));
          $toTimimg = date('H:i:s', strtotime($this->input->post('to-timimg')));
          $data3 = array(
               'start_time' => $fromTiming,
               'end_time' => $toTimimg
          );    
        }
        else
        {    
          $flexFromTime1 =  $this->input->post('flex-from-timing');
          $flexToTime1 =  $this->input->post('flex-to-timing');
           
          $data3 = array(
               'start_time' => date('H:i:s', strtotime($flexFromTime1[0])),
               'end_time' => date('H:i:s', strtotime($flexToTime1[0])),
               
          );
           
          $data4 = array(
               'start_time' => date('H:i:s', strtotime($flexFromTime1[1])),
               'end_time' => date('H:i:s', strtotime($flexToTime1[1])),
          );
        }

         // Upload Doctor Profile
        /* Upload Files are being removed for now 
            $config['upload_path']          = './uploads/profile/';
            $config['file_name']            = 'pic-' . $doctorId;
            $config['allowed_types']        = 'gif|jpg|png';
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if(isset($_FILES['profile_pic']['tmp_name']))
            {
              $thumb_path = realpath(getcwd()) . '/uploads/profile/' ;
              if (!is_dir($thumb_path))
              {
                  mkdir($thumb_path);
                  chmod($thumb_path, 0777);
              }

              if ( ! $this->upload->do_upload('profile_pic'))
              {
                $error = array('error' => $this->upload->display_errors());
              }
              else
              {
                  
                  $data_file = array('upload_data' => $this->upload->data());
                  $data1['profile_pic'] = $data_file['upload_data']['file_name'];
                  generate_preset($data_file['upload_data']['full_path']);    
              }
            }
            */  
               // Upload Doctor Profile
            $config['upload_path']          = './uploads/cv/';
            $config['file_name']            = 'cv-' . $doctorId;
            $config['allowed_types']        = 'docx|doc|pdf';
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if(isset($_FILES['cv']['tmp_name']))
            {
              $thumb_path = realpath(getcwd()) . '/uploads/cv/' ;
              if (!is_dir($thumb_path))
              {
                  mkdir($thumb_path);
                  chmod($thumb_path, 0777);
              }

              if ( ! $this->upload->do_upload('cv'))
              {
                $error = array('error' => $this->upload->display_errors());         
              }
              else
              {
                $data_file = array('upload_data' => $this->upload->data(), '');
                $data1['cv'] = $data_file['upload_data']['file_name'];     
              }
            }
            
            $result = $this->doctor_model->insertDoctor($data); 
            $data1['user_id'] = $result;
            $this->doctor_model->insertDoctorInfo($data1);
            
            $data2['user_id'] = $result;
            $this->doctor_model->insertDoctorDays($data2);
                
            $data3['user_id'] = $result;
            $this->doctor_model->insertDoctorTiming($data3);
                
            if(!empty($data4))
            {
              $data4['user_id'] = $result;
              $this->doctor_model->insertDoctorTiming($data4);  
            }

            $qualification = $this->input->post('qualification');
            $institution = $this->input->post('institution');
            $year = $this->input->post('year');

            foreach($qualification as $key => $value)
            {
              if($value)
              {
                $data5 = array(
                  'qualification' => $value,
                  'institution' => $institution[$key],
                  'year' => $year[$key],
                  'user_id' => $result
                );
                $this->doctor_model->insertAcademics($data5);
              }   
            }

            $company = $this->input->post('company');
            $job_title = $this->input->post('job_title');
            $start_yy = $this->input->post('start_yy');
            $start_mm = $this->input->post('start_mm');
            $end_yy = $this->input->post('end_yy');
            $end_mm = $this->input->post('end_mm');
             $present_here   = $this->input->post('stay_here');
            $get_present_here  =   $present_here[0];
            
            $checkCounter = 1;

            foreach($company as $key => $value)
            {
                
              if($value)
              {
                  $present_here = ($checkCounter == $get_present_here) ? '1' : '0' ;
                  
                $data6 = array(
                  'company' => $value,
                  'job_title' => $job_title[$key],
                  'start_date' => date('Y-m-d', strtotime($start_yy[$key] . '-' . $start_mm[$key])),
                  'end_date' => date('Y-m-d', strtotime($end_yy[$key] . '-' . $end_mm[$key])),
                  'present_here'  => $present_here,
                  'user_id' => $result
                );
                $this->doctor_model->insertExperience($data6);   
              }
               $checkCounter++;
              
            }
                
            if($result == TRUE)
            {
              $this->doctor_model->DoctorSignupEmailSendToAdmin($data);
              $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">You have successfully applied as Doctor. Please wait for administrator approval Email.</div>');
              redirect('registerdoctor');        
            }
            else
            {
              $this->session->set_flashdata('fail', '<div class="alert alert-danger text-center">Ops! Error occured. Please fill the form properly.</div>');
              redirect('registerdoctor');        
            }
      }        
    }
}