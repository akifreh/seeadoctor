<?php
class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->library(array('session', 'form_validation', 'email'));
        $this->load->database();
        $this->load->model('user_model');
        $this->load->helper('security');
    }
    
    function index()
    {
        
        $this->register();
    }

    function register()
    {
        //set validation rules
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required|alpha|min_length[3]|max_length[30]|xss_clean');
        $this->form_validation->set_rules('sname', 'Last Name', 'trim|required|alpha|min_length[3]|max_length[30]|xss_clean');
        $this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[cpassword]|md5');
        $this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required|md5');
        
        //validate form input
        if ($this->form_validation->run() == FALSE)
        {
            // fails
            $this->load->view('user_registration');
            
        }
        else
        {   
            //insert the user registration details into database
            $data = array(
                'fname' => $this->input->post('fname'),
                'sname' => $this->input->post('sname'),
                'email' => $this->input->post('email'),
                'password' => $this->input->post('password'),
                'cpassword' => $this->input->post('cpassword')
            );
            
           
            
            // insert form data into database
            if ($this->user_model->insertUser($data))
            {
                
                // send email
                if ($this->user_model->sendEmail($this->input->post('email')))
                {
                    // successfully sent mail
                    $this->session->set_flashdata('msg','<div class="alert alert-success text-center">You are Successfully Registered! Please confirm the mail sent to your Email-ID!!!</div>');
                    redirect('user/register');
                }
                else
                {
                    // error
                    $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                    redirect('user/register');
                }
            }
            else
            {
                // error
                $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                redirect('user/register');
            }
        }
    }
    
    function verify($hash=NULL)
    {
        if ($this->user_model->verifyEmailID($hash))
        {
            $this->session->set_flashdata('verify_msg','<div class="alert alert-success text-center">Your Email Address is successfully verified! Please login to access your account!</div>');
            redirect('user/register');
        }
        else
        {
            $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">Sorry! There is error verifying your Email Address!</div>');
            redirect('user/register');
        }
    }
    
    function addProfile()
    {
        $userId = $this->session->userdata('logged_in');
        $this->form_validation->set_rules('name[]', 'Name', 'trim|required|min_length[3]|max_length[30]');
        $this->form_validation->set_rules('sname[]', 'Sname', 'trim|required|min_length[3]|max_length[30]');
        $this->form_validation->set_rules('relationship[]', 'Relationship', 'trim|required|min_length[3]|max_length[30]|xss_clean');
       
        if ($this->form_validation->run() == FALSE) 
        {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
            redirect('home/complete-profile');
        } 
        else 
        {           
            $b_dd = $this->input->post('b_dd');
            $b_mm = $this->input->post('b_mm');
            $b_yy = $this->input->post('b_yy');
            
            $name = $this->input->post('name');
            $sname = $this->input->post('sname');
            $relationShip = $this->input->post('relationship');
            $dob = array();
            $loop = count($name);
            for ( $index =0; $index < $loop; $index++)
            {        
                $dob = $b_yy[$index] . $b_mm[$index] . $b_dd[$index];
                $insertData = array(     
                    'name' =>  $name[$index],
                    'sname' => $sname[$index],
                    'relationship' =>  $relationShip[$index],
                    'dob' => $dob,
                    'user_id' => $userId['id']
                );
                $this->user_model->insertProfile($insertData);
            }
            $this->session->set_flashdata('msg','<div class="alert alert-success text-center">Your other profile record has been added successfully.</div>');
            redirect('home/complete-profile');
        }
    }

    function viewOtherProfiles($id)
    {
        $this->load->view('header');
        $data['otherProfileDetail'] = $this->user_model->readOtherProfileDetails($id);
        $this->load->view('other_profile',$data);
        $this->load->model('page_model');
        $footer['footer'] = $this->page_model->get_content('footer');
        $this->load->view('footer', $footer);
    }

    function deleteOtherProfile($id)
    {
        $this->user_model->deleteProfile($id);
        $this->session->set_flashdata('other_msg','<div class="alert alert-success text-center">Your other profile record has been deleted successfully.</div>');
        redirect('home/complete-profile');
    }

    function editOtherProfile()
    {
        $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]|max_length[30]');
        $this->form_validation->set_rules('sname', 'Sname', 'trim|required|min_length[3]|max_length[30]');
        $this->form_validation->set_rules('relationship', 'Relationship', 'trim|required|min_length[3]|max_length[30]|xss_clean');
        //$this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email|is_unique[users.email]');
        if ($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('other_error','<div class="alert alert-danger text-center">Oops there was an error on other profile update. Please try again.</div>');
            redirect('home/complete-profile');
        }
        else
        {
            $b_dd = $this->input->post('b_dd');
            $b_mm = $this->input->post('b_mm');
            $b_yy = $this->input->post('b_yy');
            
            $dob = $b_yy .'-'. $b_mm .'-'. $b_dd;  
            $data = array(
                'id' => $this->input->post('id'),
                'name' => $this->input->post('name'), 
                'sname' => $this->input->post('sname'), 
                'relationship' => $this->input->post('relationship'),
                'dob' => $dob
            );
            $this->user_model->updateOtherProfile($data);
            $this->session->set_flashdata('other_msg','<div class="alert alert-success text-center">Your other profile has been updated successfully.</div>');
            redirect('home/complete-profile');
        } 
    }

    function forgetPassword()
    {
        $data = array();
        if($_POST)
        {
            $email = $this->input->post('email');
            if($email)
            {
                if($id = $this->user_model->checkUserByEmail($email))
                {
                    $token = $this->user_model->insertToken($id, $email);
                    $this->user_model->sendForgetEmail($email, $token);
                    $data['message'] = 'Check your email and click the link to change your password';
                }
                else
                    $data['error'] = 'Email address '. $email .' is not registered on this platform.'; 
            }
        }

        $this->load->view('header');    
        $this->load->view('forget_password', $data);
        $this->load->model('page_model');
        $footer['footer'] = $this->page_model->get_content('footer');
        $this->load->view('footer', $footer);   
    }

    function newPassword($token = NULL)
    {
        $data = array();

        if($_POST)
        {
            $token = $this->input->post('token');
            $password = $this->input->post('new_password');
            $confirm_password = $this->input->post('confirm_password');

            $this->form_validation->set_rules('new_password', 'Password', 'trim|required|matches[confirm_password]');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required');

            //validate form input
            if ($this->form_validation->run() == FALSE)
            {
                $data['error'] = 'Confirm Password doesnot match new password';
            }
            else
            {
                $user_id = $this->input->post('user_id');
                $this->user_model->changePassword($password, $user_id);
                redirect('user/successPassword');
            }
        }

        if(!$token)
            $data['error'] = 'No or invalid token. Please re-submit <a herf="' . base_url() . 'forgetPassword>" forget password form';
        
        $record = $this->user_model->getTokenDetails($token);
        if(!$record)
            $data['error'] = 'Invalid token. Please re-submit <a herf="' . base_url() . 'forgetPassword>" forget password form';
        else
        {
            $data['user_id'] = $record->user_id;
            $data['token'] = $record->token;
        }

        $this->load->view('header');    
        $this->load->view('new_password', $data);
        $this->load->model('page_model');
        $footer['footer'] = $this->page_model->get_content('footer');
        $this->load->view('footer', $footer);    
                
    }

    function successPassword()
    {
        $this->load->view('header');    
        $this->load->view('success/success_password');
        $this->load->model('page_model');
        $footer['footer'] = $this->page_model->get_content('footer');
        $this->load->view('footer', $footer); 
    }

    function rateDoctor($d_id = NULL, $appointment_id = NULL)
    {
        $user = $this->session->userdata('logged_in');
        if($_POST)
        {
            $doctorId = $this->input->post('doctorId');
            $record = array(
                'p_id' => $user['id'],
                'd_id' => $doctorId,
                'rating' => $this->input->post('rating')
            );
            
            $this->user_model->saveDoctorRating($record);
            $this->user_model->updateDoctorRating($doctorId);
            
            $record = array(
                'p_id' => $user['id'],
                'appointment_id' => $this->input->post('appointmentId'),
                'system_rating' => $this->input->post('system_rating'),
                'comments' => $this->input->post('comments')
            );
            $this->user_model->saveUserFeedback($record);
            $this->user_model->sendCommentsToAdmin($record);
            redirect('user/successRating');
        }

        if(!$user || !$d_id)
            redirect();

        $data['rating'] = $this->user_model->getDoctorRatingForUser($d_id, $user['id']);
        $data['doctor'] = $this->user_model->getUserById($d_id);
        $data['appointmentId'] = $appointment_id;

        $this->load->view('header');    
        $this->load->view('doctor_rating', $data);
        $this->load->model('page_model');
        $footer['footer'] = $this->page_model->get_content('footer');
        $this->load->view('footer', $footer);

    }

    function successRating()
    {
        $this->load->view('header');    
        $this->load->view('success/success_rating', $data);
        $this->load->model('page_model');
        $footer['footer'] = $this->page_model->get_content('footer');
        $this->load->view('footer', $footer); 
    }

    function doctorVerify($hash = NULL)
    {
        if ($this->user_model->verifyEmailID($hash))
        {
            $this->load->model('admin_model');  
            
            $record = $this->user_model->getUserByHashEmail($hash);
            
            $password = $this->admin_model->randomPassword();
            $this->admin_model->updateDoctorPassword($record->id, $password);
       
            // send login detail email to doctor
            $this->admin_model->sendDoctorEmail($record->email, $password);

            // Set doctor status to 1 
            $this->admin_model->verfiyDoctorEmail($record->id, 1);
            
            $this->session->set_flashdata('verify_msg','<div class="alert alert-success text-center">Your Email Address is successfully verified! Please check your email for login credentials and login to access your account!</div>');
            redirect();
        }
        else
        {
            $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">Sorry! There is error verifying your Email Address! Please contact the administrator.</div>');
            redirect();
        }
    }

    function changePassword()
    {
        $data = array();
        if(empty($this->session->userdata('logged_in')['id']))
            redirect();
        
        $user_id = $this->session->userdata('logged_in')['id'];
        if($_POST)
        {
            $current_password = $this->input->post('current_password');
            $password = $this->input->post('new_password');
            $confirm_password = $this->input->post('confirm_password');

            $this->form_validation->set_rules('new_password', 'Password', 'trim|required|matches[confirm_password]');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required');
            $this->form_validation->set_rules('current_password', 'Current Password', 'trim|required');
            
            //validate form input
            if ($this->form_validation->run() == FALSE)
            {
                $data['error'] = 'Confirm Password doesnot match new password';
            }
            elseif(!$this->user_model->checkPassword($current_password, $user_id))
            {
                $data['error'] = 'Current Password is not correct. Please try again.';
            }
            else
            {               
                $this->user_model->changePassword($password, $user_id);
                $this->session->set_flashdata('msg','<div class="alert alert-success text-center">Your password has been changed successfully</div>');
                redirect('user/successPassword');
            }
        }

        $this->load->view('header');    
        $this->load->view('home/change_password', $data);
        $this->load->model('page_model');
        $footer['footer'] = $this->page_model->get_content('footer');
        $this->load->view('footer', $footer);    
                
    }
}