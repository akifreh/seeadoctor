-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 11, 2016 at 04:15 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `doctor_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointment_symptoms`
--

CREATE TABLE IF NOT EXISTS `appointment_symptoms` (
  `id` int(11) NOT NULL,
  `p_id` int(11) DEFAULT NULL,
  `appointment_id` int(11) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `symptom` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appointment_symptoms`
--

INSERT INTO `appointment_symptoms` (`id`, `p_id`, `appointment_id`, `category`, `symptom`) VALUES
(1, 69, 68, 'skin', 'Bleeding'),
(2, 69, 68, 'skin', 'Itching'),
(3, 69, 68, 'pelvis', 'Frequent urination'),
(4, 69, 68, 'pelvis', 'Heartburn / reflux'),
(5, 69, 68, 'pelvis', 'Penile discharge'),
(6, 69, 68, 'headneck', 'Hearing loss / ringing'),
(7, 69, 68, 'headneck', 'Nasal Discharge');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointment_symptoms`
--
ALTER TABLE `appointment_symptoms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointment_symptoms`
--
ALTER TABLE `appointment_symptoms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
