-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Oct 28, 2016 at 12:29 PM
-- Server version: 5.6.33
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `seeadrtl_seeadoctor`
--

-- --------------------------------------------------------

--
-- Table structure for table `allergies`
--

CREATE TABLE IF NOT EXISTS `allergies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `resistant` varchar(512) NOT NULL,
  `reaction` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `allergies`
--

INSERT INTO `allergies` (`id`, `user_id`, `resistant`, `reaction`) VALUES
(17, 93, 'asdasd', 'asdassdasd'),
(18, 69, 'Power', 'Gain'),
(20, 69, 'asd', 'asdasd'),
(22, 69, 'asdasd', 'dfgdfg'),
(23, 159, 'Egg', 'Hives'),
(24, 159, 'Perfume', 'Headache');

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE IF NOT EXISTS `appointments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` int(11) NOT NULL,
  `d_id` int(11) NOT NULL,
  `meeting_approval` tinyint(4) NOT NULL DEFAULT '0',
  `type` enum('schedule','now') NOT NULL DEFAULT 'schedule',
  `brief_description` text NOT NULL,
  `med_taken` varchar(10) NOT NULL,
  `patient` varchar(255) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `longitude` double DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`id`, `p_id`, `d_id`, `meeting_approval`, `type`, `brief_description`, `med_taken`, `patient`, `start_time`, `end_time`, `longitude`, `latitude`, `created_at`) VALUES
(41, 69, 81, 1, 'schedule', 'I am feeling cough and sour throat\r\nI am anxious also', 'yes', 'me', '2016-10-08 02:15:00', '2016-10-07 02:05:00', 67.045492, 24.8570885, '2016-09-30 11:30:40'),
(45, 69, 81, 1, 'now', 'Back Pain', 'yes', 'me', '2016-09-30 15:04:31', '2016-09-30 15:19:31', NULL, NULL, '2016-09-30 12:59:31'),
(46, 69, 81, 1, 'now', 'ddd', 'yes', 'me', '2016-09-30 16:28:17', '2016-09-30 16:43:17', NULL, NULL, '2016-09-30 14:23:17'),
(47, 69, 81, 1, 'now', 'dedede', 'yes', 'me', '2016-09-30 16:56:08', '2016-09-30 17:11:08', NULL, NULL, '2016-09-30 14:51:08'),
(48, 69, 81, 1, 'schedule', 'I am feeling cough and sour throat\r\nI am anxious also', 'yes', 'me', '2016-10-22 02:15:00', '2016-10-22 02:05:00', NULL, NULL, '2016-09-30 11:30:40'),
(49, 69, 81, 1, 'schedule', 'I am feeling cough and sour throat\r\nI am anxious also', 'yes', 'me', '2016-10-23 02:15:00', '2016-10-23 02:05:00', NULL, NULL, '2016-09-30 11:30:40'),
(50, 69, 81, 0, 'schedule', 'yufufv uiujk', 'yes', 'me', '2016-10-21 17:45:00', '2016-10-21 18:00:00', NULL, NULL, '2016-10-21 15:31:31'),
(51, 69, 81, 1, 'now', 'f dfyfyytuuity t', 'yes', 'me,mychild,else', '2016-10-21 15:40:23', '2016-10-21 15:55:23', NULL, NULL, '2016-10-21 15:35:23'),
(52, 69, 81, 0, 'schedule', 'I am weak', 'yes', 'me', '2016-10-21 18:00:00', '2016-10-21 18:15:00', NULL, NULL, '2016-10-21 15:47:34'),
(53, 69, 81, 1, 'now', 'asdasd', 'yes', 'me', '2016-10-21 16:17:26', '2016-10-21 16:32:26', 67.0455921, 24.8574275, '2016-10-21 16:12:26'),
(54, 159, 160, 3, 'now', 'I am unwell', 'Yes', 'me', '2016-10-23 18:54:28', '2016-10-23 19:09:28', 67.1471164, 24.8831331, '2016-10-23 16:49:28'),
(55, 69, 160, 0, 'schedule', 'I am in pain', 'yes', 'me', '2016-10-24 18:00:00', '2016-10-24 18:15:00', NULL, NULL, '2016-10-24 14:07:44'),
(56, 69, 160, 0, 'schedule', 'I have back pain', 'yes', 'someone-else', '2016-10-24 16:45:00', '2016-10-24 17:00:00', NULL, NULL, '2016-10-24 14:27:51'),
(57, 69, 81, 0, 'schedule', 'Point', 'yes', 'me', '2016-10-25 01:00:00', '2016-10-25 01:15:00', NULL, NULL, '2016-10-24 14:28:59'),
(58, 69, 160, 3, 'now', 'Please', 'Yes', 'me', '2016-10-24 16:43:41', '2016-10-24 16:58:41', 67.028334, 24.8184586, '2016-10-24 14:38:41'),
(59, 69, 81, 0, 'schedule', 'Eye problem', 'yes', 'me', '2016-10-25 01:15:00', '2016-10-25 01:30:00', NULL, NULL, '2016-10-25 08:12:45'),
(60, 161, 160, 1, 'now', 'ghj', 'Yes', 'me', '2016-10-25 10:34:54', '2016-10-25 10:49:54', NULL, NULL, '2016-10-25 08:29:54'),
(61, 69, 81, 0, 'schedule', 'I am lost', 'yes', 'me', '2016-10-25 01:30:00', '2016-10-25 01:45:00', NULL, NULL, '2016-10-25 08:51:53'),
(63, 161, 148, 1, 'schedule', 'dowdy', 'yes', 'me', '2016-10-28 01:30:00', '2016-10-28 01:45:00', NULL, NULL, '2016-10-26 06:02:37');

-- --------------------------------------------------------

--
-- Table structure for table `appointment_now`
--

CREATE TABLE IF NOT EXISTS `appointment_now` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` int(11) NOT NULL,
  `d_id` int(11) NOT NULL,
  `meeting_approval` int(11) NOT NULL,
  `brief_description` varchar(256) NOT NULL,
  `med_taken` varchar(10) NOT NULL,
  `patient` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `appointment_now`
--

INSERT INTO `appointment_now` (`id`, `p_id`, `d_id`, `meeting_approval`, `brief_description`, `med_taken`, `patient`, `created_at`) VALUES
(5, 161, 160, 0, 'testing2', 'Yes', 'me', '2016-10-26 06:07:29');

-- --------------------------------------------------------

--
-- Table structure for table `available_doctor_days`
--

CREATE TABLE IF NOT EXISTS `available_doctor_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=82 ;

--
-- Dumping data for table `available_doctor_days`
--

INSERT INTO `available_doctor_days` (`id`, `day_id`, `user_id`) VALUES
(3, 4, 125),
(4, 1, 126),
(5, 2, 126),
(6, 5, 126),
(7, 2, 128),
(8, 1, 133),
(9, 2, 133),
(10, 3, 133),
(11, 1, 135),
(12, 2, 135),
(13, 1, 136),
(14, 5, 136),
(15, 6, 136),
(16, 1, 138),
(17, 3, 138),
(18, 3, 139),
(19, 4, 139),
(20, 5, 139),
(21, 1, 140),
(22, 3, 140),
(23, 4, 141),
(24, 5, 141),
(25, 1, 142),
(26, 3, 142),
(27, 5, 143),
(28, 7, 143),
(29, 5, 144),
(30, 7, 144),
(31, 1, 145),
(32, 3, 145),
(33, 4, 145),
(34, 5, 146),
(35, 7, 146),
(36, 1, 147),
(37, 3, 147),
(38, 3, 148),
(39, 5, 148),
(40, 6, 148),
(41, 1, 151),
(42, 3, 151),
(43, 1, 154),
(44, 2, 154),
(45, 3, 154),
(46, 5, 154),
(47, 6, 154),
(48, 7, 154),
(49, 1, 156),
(50, 2, 156),
(51, 3, 156),
(52, 4, 156),
(53, 5, 156),
(54, 7, 156),
(55, 1, 157),
(56, 3, 157),
(57, 1, 158),
(58, 2, 158),
(59, 4, 158),
(60, 1, 160),
(61, 2, 160),
(62, 1, 165),
(71, 2, 81),
(72, 3, 81),
(73, 5, 81),
(74, 1, 168),
(75, 2, 168),
(76, 3, 168),
(77, 1, 169),
(78, 2, 169),
(79, 3, 169),
(80, 4, 169),
(81, 5, 169);

-- --------------------------------------------------------

--
-- Table structure for table `card_info`
--

CREATE TABLE IF NOT EXISTS `card_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `cc_number` bigint(255) NOT NULL,
  `cc_csv_number` int(100) NOT NULL,
  `cc_exp_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=165 ;

--
-- Dumping data for table `card_info`
--

INSERT INTO `card_info` (`id`, `user_id`, `cc_number`, `cc_csv_number`, `cc_exp_date`) VALUES
(1, 10, 0, 0, '0000-00-00'),
(2, 11, 0, 0, '0000-00-00'),
(3, 12, 0, 0, '2021-01-01'),
(4, 13, 0, 0, '0000-00-00'),
(5, 16, 0, 0, '0000-00-00'),
(6, 17, 0, 0, '0000-00-00'),
(7, 18, 0, 0, '0000-00-00'),
(8, 19, 0, 0, '0000-00-00'),
(9, 20, 0, 0, '0000-00-00'),
(10, 21, 0, 0, '0000-00-00'),
(11, 22, 0, 0, '0000-00-00'),
(12, 23, 0, 0, '2016-01-19'),
(13, 24, 0, 0, '2016-01-06'),
(14, 25, 0, 0, '2015-02-02'),
(15, 26, 0, 0, '2014-05-03'),
(16, 27, 0, 0, '2016-03-02'),
(17, 28, 0, 0, '2016-01-01'),
(18, 29, 0, 0, '2016-01-01'),
(20, 31, 0, 0, '0000-00-00'),
(21, 32, 0, 0, '0000-00-00'),
(23, 34, 1232111122, 123, '2016-01-01'),
(24, 35, 658999875, 654, '2016-01-01'),
(25, 36, 1123222222, 222, '2016-01-01'),
(26, 37, 321321231, 132, '2016-01-01'),
(27, 38, 321321321, 123, '2016-01-01'),
(29, 40, 1112332222, 123, '2016-01-01'),
(30, 30, 2147483647, 999, '2017-01-07'),
(31, 42, 2147483647, 888, '2016-08-17'),
(32, 43, 2147483647, 321, '2017-02-08'),
(33, 33, 233556546, 654, '2016-01-01'),
(34, 45, 2147483647, 321, '2020-02-02'),
(35, 46, 2147483647, 123, '2021-01-14'),
(36, 47, 1231321321, 123, '2021-01-01'),
(37, 48, 2147483647, 321, '2021-01-01'),
(38, 49, 2147483647, 432, '2021-01-01'),
(39, 39, 0, 0, '2016-02-02'),
(40, 51, 2147483647, 312, '2021-01-01'),
(41, 52, 2147483647, 423, '2021-01-01'),
(42, 53, 2147483647, 0, '2021-01-01'),
(43, 54, 2147483647, 123, '2021-01-01'),
(44, 55, 2147483647, 888, '2021-01-01'),
(45, 56, 2147483647, 423, '2021-01-01'),
(46, 57, 2147483647, 424, '2021-01-01'),
(48, 59, 2147483647, 456, '2021-01-01'),
(49, 60, 2147483647, 321, '2021-01-01'),
(51, 62, 2147483647, 258, '0000-00-00'),
(58, 58, 2147483647, 465, '2005-01-14'),
(61, 61, 324234243, 321, '2021-01-01'),
(63, 63, 31321321321321, 123, '2017-03-01'),
(64, 64, 0, 0, '2021-01-01'),
(65, 65, 132123123, 21, '2010-05-01'),
(66, 66, 32321321321, 123, '2016-03-01'),
(67, 67, 321321321321321, 321, '2016-03-01'),
(68, 68, 11111222211114, 332, '2019-01-01'),
(69, 69, 45845454452121, 233, '2021-01-01'),
(70, 70, 1313215454565, 123, '2021-01-01'),
(71, 92, 4564456445644564, 123, '2021-04-01'),
(73, 93, 121212121212, 456, '2018-02-01'),
(74, 149, 123123123123123, 123, '2021-01-01'),
(75, 150, 0, 0, '2021-01-01'),
(76, 152, 321123321, 123, '2021-04-01'),
(79, 159, 4242424242424242, 111, '2018-03-01'),
(153, 153, 321123321, 123, '2021-01-01'),
(155, 155, 321123321, 123, '2021-10-01'),
(161, 161, 1234, 123, '2021-08-01'),
(162, 162, 111, 1, '2021-01-01'),
(163, 163, 94696, 968, '2021-01-01'),
(164, 164, 123, 345, '2021-01-01');

-- --------------------------------------------------------

--
-- Table structure for table `charges`
--

CREATE TABLE IF NOT EXISTS `charges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_type` varchar(255) NOT NULL,
  `rates` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `charges`
--

INSERT INTO `charges` (`id`, `doctor_type`, `rates`) VALUES
(1, 'Nutritionist', 149),
(2, 'Psychologist', 149),
(3, 'Medical Doctor', 199);

-- --------------------------------------------------------

--
-- Table structure for table `days`
--

CREATE TABLE IF NOT EXISTS `days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `days`
--

INSERT INTO `days` (`id`, `day`) VALUES
(1, 'monday'),
(2, 'tuesday'),
(3, 'wednesday'),
(4, 'thursday'),
(5, 'friday'),
(6, 'saturday'),
(7, 'sunday');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_cv`
--

CREATE TABLE IF NOT EXISTS `doctor_cv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `degree` varchar(100) DEFAULT NULL,
  `cv` varchar(100) DEFAULT NULL,
  `profile_pic` varchar(255) DEFAULT NULL,
  `description` text,
  `profession_number` varchar(255) DEFAULT NULL,
  `practice_number` varchar(255) DEFAULT NULL,
  `experience` varchar(255) DEFAULT NULL,
  `speciality` varchar(255) DEFAULT NULL,
  `type_of_doctor` int(11) NOT NULL COMMENT '1=>Nutritionist, 2=>Psychologist, 3=>Medical Doctor',
  `rating` int(11) DEFAULT NULL,
  `no_of_patient` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `doctor_cv`
--

INSERT INTO `doctor_cv` (`id`, `user_id`, `degree`, `cv`, `profile_pic`, `description`, `profession_number`, `practice_number`, `experience`, `speciality`, `type_of_doctor`, `rating`, `no_of_patient`) VALUES
(15, 112, 'asdasd', 'eye1.jpg', 'depression1.jpg', NULL, '', NULL, 'asdasd', 'asdsad', 0, 0, 0),
(16, 81, 'mbbs', 'Attribes_solution_data.docx', 'pic-81.jpg', '', '121212', '', '8 year', 'child specialist', 2, 5, 100),
(17, 114, 'mbbs', 'Attribes_solution_data1.docx', 'services.jpg', NULL, '', NULL, '5 year', 'child', 0, 0, 0),
(18, 115, 'mbbs', 'shahzaib_cv.docx', 'save-patients.png', NULL, '', NULL, '7year', 'adult', 0, 0, 0),
(19, 116, 'nbba', 'Attribes_solution_data2.docx', 'team-img1.jpg', NULL, '', NULL, '34', 'asd asd as', 2, 5, 14),
(46, 144, 'mbbs', 'Attribes_solution_data23.docx', 'services1.jpg', NULL, '', NULL, '45year', 'mbbs', 2, 4, 15),
(47, 145, 'mbsb', 'Attribes_solution_data24.docx', '12512714_170020866695510_6718679530217273720_n.jpg', NULL, '', NULL, '4year', 'childrens', 3, 3, 5),
(48, 146, 'mbbs', 'Attribes_solution_data25.docx', 'sorethorat.jpg', NULL, '', NULL, '5year', 'mbbs', 1, 0, 0),
(49, 147, 'mbbd', 'Attribes_solution_data26.docx', 'depression6.jpg', NULL, '', NULL, 'sdf', 'sdf', 2, 0, 0),
(50, 148, 'mbbs', 'Attribes_solution_data27.docx', 'team-img4.jpg', NULL, '', NULL, '10+', 'child', 1, 0, 0),
(51, 151, 'mbbs', 'Attribes_solution_data.docx', 'team-img5.jpg', NULL, '', NULL, '5+', 'skin', 2, 0, 0),
(52, 154, 'FPSC', '', '13939426_1361401873873924_453731842173157629_n.jpg', NULL, '', NULL, '5+', 'All', 2, 0, 0),
(53, 156, 'FFPSC, FFC, WWE', 'See_a_Doctor.docx', '12208750_949595125106237_7986268658043934445_n.jpg', NULL, '', NULL, 'More than 10 Year''s', 'All', 3, 0, 0),
(54, 157, 'mbbs', '', 'd47556565asd8888_n.jpg', NULL, '', NULL, '10+', 'skin', 2, 0, 0),
(55, 158, 'mbbs', '', '10727444_736967239727105_1168939899_n.jpg', NULL, '', NULL, '6+', 'bones', 3, 0, 0),
(56, 160, 'MD', 'Some_CV.docx', 'my-pic21.jpg', '', '1234567', '', 'General Physician', 'ENT', 3, 5, 2),
(57, 168, 'MD Phycology', 'cv-2.docx', 'pic-2.jpg', 'sfas dfha fjda kfjaslk jsflk gjslkj fhdK sdf sdfd sfs fsgsdfgsgdgdgfsd gsdfgfsg g', '', '', '8 years', NULL, 2, NULL, NULL),
(58, 169, 'BSc.', 'cv-7.pdf', NULL, 'Great', '0123457', '123456', '1', NULL, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `doctor_info`
--

CREATE TABLE IF NOT EXISTS `doctor_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `degree` varchar(50) DEFAULT NULL,
  `experience` text,
  `speciality` text,
  `doctor_type` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=87 ;

--
-- Dumping data for table `doctor_info`
--

INSERT INTO `doctor_info` (`id`, `user_id`, `degree`, `experience`, `speciality`, `doctor_type`, `role`) VALUES
(1, 76, 'asdasd', 'asdasdas sad ', 'sadas d asd asd asd asd ', 'Psychologist', 3),
(2, 77, 'asd', 'asdasd asdasd ', 'asdasdasd asdasd', 'Psychologist', 3),
(3, 78, 'mbbs', 'more than 15 yyears', 'child specialst', 'Medical Doctor', 3),
(4, 79, 'mbbs', 'asdd asd asd asd asd ', 'asdasd asd ', 'Medical Doctor', 3),
(5, 80, 'asdasd', 'asdasd asdasd ', 'asdasdasd sdasd', 'Psychologist', 3),
(6, 81, 'mbbs', 'adsasd asdsd ', 'asdasd  asd asd ', 'Psychologist', 3),
(7, 82, 'mbbs', 'asd asd  asd ', 'asd asd  asd sd ', 'Medical Doctor', 3),
(9, 84, 'mbbs', 'asdsad asd sd  ', 'asd asd  sd d ', 'Psychologist', 3),
(10, 85, 'asdasd', 'asdasd', 'asdasd', 'Psychologist', 3),
(12, 87, 'mbbs', 'sdfsdf dsf sdf ', 'sdf sdf sdf dsf ', 'Nutritionist', 3),
(13, 88, 'asdasd', 'asdasd', 'asdasd asdasd', 'Psychologist', 3),
(14, 89, 'asdasd', 'asdasd sadasd', 'asdasd asdasd', 'Psychologist', 3),
(15, 90, 'mbbs', 'asd asd asd ', 'as dasd asd asd ', 'Psychologist', 3),
(16, 91, 'asd', 'asd', 'asd', 'Psychologist', 3),
(83, 83, 'asdads', 'adsas sds ', 'asdsad sad asd ', 'Nutritionist', 3),
(86, 86, 'mbbs', 'more than eight years', 'dadad', 'Nutritionist', 3);

-- --------------------------------------------------------

--
-- Table structure for table `doctor_online`
--

CREATE TABLE IF NOT EXISTS `doctor_online` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session` varchar(255) NOT NULL,
  `time` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `doctor_online`
--

INSERT INTO `doctor_online` (`id`, `session`, `time`, `user_id`) VALUES
(6, '30614a4f72b5f706cf1ede2f8dde3111a1c8ee1a', 1475244972, 116),
(7, '30614a4f72b5f706cf1ede2f8dde3111a1c8ee1a', 1475244972, 144),
(8, '918688ddc4f84151529e7fcd637413430fb7355b', 1477650375, 81);

-- --------------------------------------------------------

--
-- Table structure for table `doctor_rating`
--

CREATE TABLE IF NOT EXISTS `doctor_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` int(11) NOT NULL,
  `d_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `doctor_rating`
--

INSERT INTO `doctor_rating` (`id`, `p_id`, `d_id`, `rating`) VALUES
(1, 69, 81, 5),
(2, 4, 81, 5),
(3, 7, 81, 3),
(4, 10, 81, 4),
(5, 161, 160, 5),
(6, 68, 160, 5);

-- --------------------------------------------------------

--
-- Table structure for table `doctor_type`
--

CREATE TABLE IF NOT EXISTS `doctor_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_of_doctor` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `doctor_type`
--

INSERT INTO `doctor_type` (`id`, `type_of_doctor`) VALUES
(1, 'nutritionist'),
(2, 'psychologist'),
(3, 'medical-doctor');

-- --------------------------------------------------------

--
-- Table structure for table `family_history`
--

CREATE TABLE IF NOT EXISTS `family_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `condition` varchar(512) NOT NULL,
  `relationship` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `family_history`
--

INSERT INTO `family_history` (`id`, `user_id`, `condition`, `relationship`) VALUES
(11, 69, 'asd', 'asdasd'),
(12, 93, 'asdasd', 'ada'),
(13, 69, 'sdf', 'sdf'),
(14, 159, 'Hypertension', 'Mother');

-- --------------------------------------------------------

--
-- Table structure for table `forget_password`
--

CREATE TABLE IF NOT EXISTS `forget_password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `forget_password`
--

INSERT INTO `forget_password` (`id`, `user_id`, `token`, `created_at`, `status`) VALUES
(4, 81, 'aaf3a57178baa39da20e62e10ef9641b', '2016-10-04 07:39:15', 1),
(5, 81, 'aaf3a57178baa39da20e62e10ef9641b', '2016-10-21 15:36:38', 1),
(6, 81, 'aaf3a57178baa39da20e62e10ef9641b', '2016-10-21 15:38:23', 1),
(7, 81, 'aaf3a57178baa39da20e62e10ef9641b', '2016-10-21 15:44:26', 1),
(8, 81, 'aaf3a57178baa39da20e62e10ef9641b', '2016-10-24 14:21:30', 1),
(9, 81, 'aaf3a57178baa39da20e62e10ef9641b', '2016-10-25 08:42:08', 1),
(10, 81, 'aaf3a57178baa39da20e62e10ef9641b', '2016-10-25 08:47:51', 1),
(11, 81, 'aaf3a57178baa39da20e62e10ef9641b', '2016-10-25 08:56:18', 1),
(12, 81, 'aaf3a57178baa39da20e62e10ef9641b', '2016-10-25 08:58:55', 1),
(13, 69, '4e2cbda46196b49bb9920347d0b98868', '2016-10-25 09:00:55', 1),
(14, 160, '3fbd6c23d5d7ad73427e8888c8c7e5f5', '2016-10-25 09:00:58', 1),
(15, 160, '3fbd6c23d5d7ad73427e8888c8c7e5f5', '2016-10-25 15:28:28', 1),
(16, 163, 'e44fc55f4bdc65f6d93a8d6094d3efc1', '2016-10-25 16:32:01', 1),
(17, 163, 'e44fc55f4bdc65f6d93a8d6094d3efc1', '2016-10-25 16:34:57', 1),
(18, 160, '3fbd6c23d5d7ad73427e8888c8c7e5f5', '2016-10-27 16:18:32', 1);

-- --------------------------------------------------------

--
-- Table structure for table `my_doctor`
--

CREATE TABLE IF NOT EXISTS `my_doctor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `sname` varchar(50) NOT NULL,
  `contact_number` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `my_doctor`
--

INSERT INTO `my_doctor` (`id`, `user_id`, `name`, `sname`, `contact_number`, `email`) VALUES
(7, 69, 'James', 'John', '545464656', 'aBC@ABC.COM'),
(8, 93, 'asd', 'asdasd', 'asdads', 'asdasd'),
(9, 69, 'dfgfdg', 'dfgdfg', 'dfgdfg', 'dfgdfg'),
(10, 159, 'Aijaz', 'Aslam', '123123123', '');

-- --------------------------------------------------------

--
-- Table structure for table `other_profile`
--

CREATE TABLE IF NOT EXISTS `other_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `relationship` varchar(20) NOT NULL,
  `dob` date NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `other_profile`
--

INSERT INTO `other_profile` (`id`, `name`, `relationship`, `dob`, `user_id`) VALUES
(1, 'Moiz', 'brother', '1996-08-17', 30),
(3, 'morris', 'brother', '2016-01-18', 33),
(4, 'asd', 'dasd', '2016-01-01', 33),
(5, 'Clarkein', 'Papa', '1997-05-18', 69),
(6, 'smith', 'brother', '1992-03-21', 69),
(10, 'asd', 'asd', '2021-01-01', 63),
(11, 'moiz', 'bro', '2021-01-01', 67),
(12, 'asd asd', 'asd', '2021-01-01', 67),
(13, 'aadsd asd', 'asd asdasd', '2021-01-01', 67),
(14, 'asd asd sad', 'asdasd asdasd asdasd', '2005-04-16', 67),
(15, 'test', 'test', '2016-01-01', 69),
(17, 'aamir', 'asdasd', '2016-01-01', 69),
(24, 'test', 'test', '2006-06-07', 69),
(25, 'rerer', 'sdf', '2001-11-30', 69),
(28, 'asd', 'asd', '2016-01-01', 69),
(32, 'asdasdasd', 'asdasd', '2016-01-01', 92),
(33, 'moiz khan', 'brother', '2016-01-01', 69),
(34, 'ABC', 'Dsot', '2000-01-01', 153),
(35, 'CBA', 'Dosra Dost', '1997-01-01', 153),
(36, 'asd', 'dostt', '2016-01-01', 153),
(37, 'aaa', 'aaa', '2016-01-01', 153),
(38, 'aaa', 'aaa', '2016-01-01', 69),
(39, 'ABC', 'Dosra Dost', '2016-01-01', 69),
(40, 'asd', 'tesra dost', '2016-01-01', 69),
(41, 'CBA', '4th dost', '2016-01-01', 69),
(42, 'aaa', 'aaa', '2016-01-01', 69),
(43, 'Avasd', 'Asd', '1980-09-12', 69),
(44, 'asdasd', 'asd', '2016-01-01', 69),
(45, 'Kamran', 'Brother', '1994-09-02', 159);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `type`, `title`, `content`, `status`) VALUES
(2, 'terms', 'Terms & Conditions', '<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>\r\n\r\n<p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure.</p>\r\n\r\n<p>To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure? On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee</p>\r\n', 1),
(3, 'fees', 'Fees', '<p>&nbsp;</p>\r\n\r\n<p>To see a Medical Doctor will cost you R199, for the first 15 minutes and charges are prorated thereafter.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 1),
(4, 'footer', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `patient_info`
--

CREATE TABLE IF NOT EXISTS `patient_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `history` text NOT NULL,
  `profession` varchar(50) NOT NULL,
  `diet` varchar(50) NOT NULL,
  `height` varchar(20) NOT NULL,
  `age` int(11) NOT NULL,
  `drink_alcohol` varchar(10) NOT NULL,
  `weight` int(11) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `smoke` varchar(10) NOT NULL,
  `sport` varchar(50) NOT NULL,
  `other` varchar(255) NOT NULL,
  `medical_history_date_of_visit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `medical_history_healthcare_visit` varchar(255) NOT NULL,
  `my_allergies_resistant` text NOT NULL,
  `my_allergies_reaction` text NOT NULL,
  `medical_history_prescription` text NOT NULL,
  `medical_history_doctor_comment` text NOT NULL,
  `my_allergies_status` varchar(10) NOT NULL,
  `medical_history_practitioner_name` varchar(30) NOT NULL,
  `pre_existing_condition` varchar(100) NOT NULL,
  `personal_update_feeling` text NOT NULL,
  `personal_update_symptoms` text NOT NULL,
  `personal_update_medication_taken` varchar(10) NOT NULL,
  `personal_update_medicine` text NOT NULL,
  `personal_update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `family_history_condition` text NOT NULL,
  `family_history_status` varchar(10) NOT NULL,
  `family_history_relationship` varchar(30) NOT NULL,
  `my_doctor_name` varchar(30) NOT NULL,
  `my_doctor_surname` varchar(30) NOT NULL,
  `my_doctor_contact_number` int(11) NOT NULL,
  `my_doctor_email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appointment_id` int(11) NOT NULL,
  `actual_duration` time NOT NULL,
  `charged_duration` time NOT NULL,
  `rate` double(8,2) NOT NULL,
  `price` double(9,2) NOT NULL,
  `transaction_id` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `appointment_id`, `actual_duration`, `charged_duration`, `rate`, `price`, `transaction_id`, `created_at`, `status`) VALUES
(22, 50, '09:24:51', '00:25:00', 9.93, 248.33, '8a82944957c3d4880157d71c18685d2d', '2016-10-18 04:25:25', 1),
(25, 54, '00:07:35', '00:15:00', 13.27, 199.00, NULL, NULL, 0),
(26, 58, '00:01:10', '00:15:00', 13.27, 199.00, '8a82944957f7183d0157f722f4ed59b2', '2016-10-24 12:40:45', 1);

-- --------------------------------------------------------

--
-- Table structure for table `personal_update`
--

CREATE TABLE IF NOT EXISTS `personal_update` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `feeling` varchar(512) NOT NULL,
  `symptom` varchar(512) NOT NULL,
  `med_taken` varchar(10) NOT NULL,
  `describe_med` varchar(512) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `personal_update`
--

INSERT INTO `personal_update` (`id`, `user_id`, `feeling`, `symptom`, `med_taken`, `describe_med`, `date`) VALUES
(10, 69, 'asd', 'asd', 'no', 'asda', '2016-01-01'),
(11, 93, 'asdasd', 'asdasd', 'yes', 'asdasd', '2016-01-01'),
(12, 159, 'Sore throat', 'difficulty in swallowing', 'yes', 'Azomax, Rigix', '2016-03-04');

-- --------------------------------------------------------

--
-- Table structure for table `prescription`
--

CREATE TABLE IF NOT EXISTS `prescription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appointment_id` int(11) NOT NULL,
  `pres_detail` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `prescription`
--

INSERT INTO `prescription` (`id`, `appointment_id`, `pres_detail`) VALUES
(1, 45, 'Extra Space Self Storage LLC\r\nWarehouse 6, Street 18\r\nPlot 364- 216 Al Quoz 1\r\nDubai UAE\r\nPO Box 53652\r\nTelephone: + 971 4 3306788'),
(2, 50, 'You must not eat :\r\n1. Meat\r\n2. Eggs\r\n3. Breads\r\n\r\nYou have a sore throat as well, so i recommend using warm water.'),
(3, 51, 'Disprin\r\nPanadol'),
(4, 58, '<p>Panadol</p>\r\n\r\n<p>Dispirin</p>\r\n\r\n<p>Azomax</p>\r\n\r\n<p>Rigix</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `pre_existing_condition`
--

CREATE TABLE IF NOT EXISTS `pre_existing_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `feeling` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `pre_existing_condition`
--

INSERT INTO `pre_existing_condition` (`id`, `user_id`, `feeling`) VALUES
(9, 69, 'popopop'),
(10, 69, 'saab'),
(11, 69, 'opel'),
(12, 93, 'saab'),
(13, 93, 'opel'),
(14, 69, 'ad'),
(15, 69, 'asdad'),
(16, 159, 'Hypertension'),
(17, 159, 'Migraine'),
(18, 159, 'Anxiety');

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE IF NOT EXISTS `rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `one` int(11) NOT NULL,
  `two` int(11) NOT NULL,
  `three` int(11) NOT NULL,
  `four` int(11) NOT NULL,
  `five` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`id`, `one`, `two`, `three`, `four`, `five`) VALUES
(1, 1, 2, 3, 4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `sessionkey`
--

CREATE TABLE IF NOT EXISTS `sessionkey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` int(11) NOT NULL,
  `d_id` int(11) NOT NULL,
  `session_key` varchar(50) NOT NULL,
  `appointment_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `sessionkey`
--

INSERT INTO `sessionkey` (`id`, `p_id`, `d_id`, `session_key`, `appointment_id`, `status`) VALUES
(13, 69, 81, 'ryyYoH4w0I', 46, 1),
(14, 69, 81, 'IWKoaMsRoh', 47, 1),
(15, 69, 81, 'ZHW4y1Bz3B', 41, 1),
(16, 69, 81, 'J88gIaR0bM', 41, 1),
(17, 69, 81, 'uyfR4noSLu', 51, 1),
(18, 69, 81, 'a4fwzbmb7f', 53, 1),
(19, 159, 160, '5z8TOySpRU', 54, 0),
(21, 161, 160, 'elkKO200aX', 60, 1),
(22, 161, 148, 'ZmmjyCFk0T', 63, 1);

-- --------------------------------------------------------

--
-- Table structure for table `timing`
--

CREATE TABLE IF NOT EXISTS `timing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `timing`
--

INSERT INTO `timing` (`id`, `start_time`, `end_time`, `user_id`) VALUES
(1, '00:00:07', '00:00:09', 125),
(2, '01:00:00', '01:00:00', 126),
(4, '01:00:00', '01:00:00', 128),
(5, '01:00:00', '01:00:00', 129),
(6, '00:00:00', '00:00:00', 130),
(7, '00:00:01', '00:00:01', 130),
(8, '00:00:00', '00:00:00', 131),
(9, '00:00:01', '00:00:01', 131),
(10, '13:00:00', '18:00:00', 132),
(11, '21:00:00', '23:00:00', 132),
(12, '17:00:00', '18:00:00', 133),
(13, '20:00:00', '22:00:00', 133),
(14, '08:00:00', '09:00:00', 134),
(15, '06:00:00', '08:00:00', 135),
(16, '15:00:00', '17:00:00', 136),
(17, '04:00:00', '07:00:00', 136),
(18, '01:00:00', '01:00:00', 137),
(19, '01:00:00', '01:00:00', 137),
(20, '02:00:00', '04:00:00', 138),
(21, '02:00:00', '04:00:00', 139),
(22, '14:00:00', '16:00:00', 140),
(23, '14:00:00', '16:00:00', 141),
(24, '18:00:00', '19:00:00', 141),
(25, '01:00:00', '03:00:00', 142),
(26, '03:00:00', '06:00:00', 143),
(27, '15:00:00', '17:00:00', 144),
(28, '01:00:00', '03:00:00', 145),
(29, '02:00:00', '06:00:00', 146),
(30, '02:00:00', '07:00:00', 147),
(31, '01:00:00', '03:00:00', 148),
(32, '01:00:00', '03:00:00', 151),
(33, '04:00:00', '05:00:00', 151),
(34, '10:00:00', '12:00:00', 154),
(35, '16:00:00', '22:00:00', 154),
(36, '15:00:00', '23:00:00', 156),
(37, '01:00:00', '04:00:00', 157),
(38, '02:00:00', '04:00:00', 158),
(40, '01:00:00', '17:00:00', 165),
(45, '01:00:00', '23:00:00', 81),
(46, '16:00:00', '20:00:00', 160),
(47, '16:00:00', '19:00:00', 168),
(48, '07:00:00', '10:00:00', 169);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) NOT NULL,
  `sname` varchar(50) NOT NULL,
  `role` int(11) NOT NULL COMMENT '1=> admin 2=>patient 3=>doctor',
  `email` varchar(255) NOT NULL,
  `re_email` varchar(255) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `contact_number` int(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `p_address` varchar(255) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `zip` varchar(50) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `job_title` varchar(30) DEFAULT NULL,
  `company_employed` varchar(100) DEFAULT NULL,
  `next_of_kin` varchar(50) DEFAULT NULL,
  `medical_aid` varchar(50) DEFAULT NULL,
  `profile_pic` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=170 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `sname`, `role`, `email`, `re_email`, `phone`, `contact_number`, `password`, `dob`, `gender`, `address`, `p_address`, `city`, `country`, `zip`, `status`, `job_title`, `company_employed`, `next_of_kin`, `medical_aid`, `profile_pic`) VALUES
(68, 'shahzaib', 'khan', 1, 'shahzaib@attribes.com', '', '03442205765', 0, 'd97efba289c7b62681731b0bd1ce4ae9', '1990-06-21', 'male', '', '', '', '', '', 1, '', '', '', '', ''),
(69, 'Akif', 'Rehmani', 2, 'akif00@yahoo.com', '', '1236545', 573746373, 'd97efba289c7b62681731b0bd1ce4ae9', '2000-10-30', 'male', 'abc', '545454', '', '', '', 1, 'None of Your Business', 'SELF EMPLOYED', 'asdasd', 'asdsad', 'wl.jpg'),
(81, 'Dr Akif', 'Khan', 3, 'akifreh@gmail.com', '', '4562212', 0, 'd97efba289c7b62681731b0bd1ce4ae9', '0000-00-00', '', 'AA', '', '', '', '', 1, '', '', '', '', ''),
(82, 'test', 'test', 3, 'testing@test.com', '', '45845454', 0, 'd97efba289c7b62681731b0bd1ce4ae9', '0000-00-00', 'female', '', '', '', '', '', 0, '', '', '', '', ''),
(159, 'Faraz', 'Khan', 2, 'faraazhameed1990@gmail.com', '', '123123123', 1123123123, '8aeaf2d0387182ce37c857ba48178c18', '1990-01-14', 'male', 'ASD 45/D Road 2 NYC', '1231234 asdasdlk asdlkasd', '', '', '', 1, 'Designer', 'Smaztech', 'Wife', 'none', 'wl1.jpg'),
(160, 'Saad', 'Siddiqui', 3, 'sasid2005@gmail.com', '', '12412423', 0, '2a77a67a8e6da852e3ff8d833a1c52f0', '0000-00-00', 'male', 'asdasd', '', '', '', '', 1, '', '', '', '', ''),
(161, 'lante', 'luthuli', 2, 'lante007@gmail.com', '', '0814943255', 0, '6c2be682f64c6a333ebafe0f733d1a1b', '2016-01-01', 'male', '', '', '', '', '', 1, '', '', '', '', 'FB_IMG_1473437835923.jpg'),
(168, 'Aamir', 'Ahmed', 3, 'aamir.ahmed.dev@gmail.com', '', '3423453657', 0, '', NULL, 'male', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, ''),
(169, 'Ishe', 'Mudzengi', 3, 'ishemudzengi@gmail.com', '', '0743749092', 0, '', NULL, 'male', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE IF NOT EXISTS `user_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `diet` varchar(255) NOT NULL,
  `height` varchar(10) NOT NULL,
  `weight` varchar(10) NOT NULL,
  `sport_activity` varchar(100) NOT NULL,
  `alcohol` varchar(10) NOT NULL,
  `smoke` varchar(10) NOT NULL,
  `other` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`id`, `user_id`, `diet`, `height`, `weight`, `sport_activity`, `alcohol`, `smoke`, `other`) VALUES
(15, 69, 'asd', '45', '12', 'yui', 'no', 'no', 'asd'),
(16, 159, 'none', '5 feet 8 i', '80 Kg', 'Cricket', 'no', 'no', 'none');

-- --------------------------------------------------------

--
-- Table structure for table `user_registration`
--

CREATE TABLE IF NOT EXISTS `user_registration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `registration_id` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_registration`
--

INSERT INTO `user_registration` (`id`, `user_id`, `registration_id`, `status`) VALUES
(1, 69, '8a82944957e21c930157e75b32087dd0', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
