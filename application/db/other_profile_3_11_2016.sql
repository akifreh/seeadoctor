-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 03, 2016 at 12:37 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `doctor_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `other_profile`
--

CREATE TABLE IF NOT EXISTS `other_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `sname` varchar(50) DEFAULT NULL,
  `relationship` varchar(20) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `other_profile`
--

INSERT INTO `other_profile` (`id`, `name`, `sname`, `relationship`, `dob`, `user_id`) VALUES
(1, 'Moiz', NULL, 'brother', '1996-08-17', 30),
(3, 'morris', NULL, 'brother', '2016-01-18', 33),
(4, 'asd', NULL, 'dasd', '2016-01-01', 33),
(10, 'asd', NULL, 'asd', '2021-01-01', 63),
(11, 'moiz', NULL, 'bro', '2021-01-01', 67),
(12, 'asd asd', NULL, 'asd', '2021-01-01', 67),
(13, 'aadsd asd', NULL, 'asd asdasd', '2021-01-01', 67),
(14, 'asd asd sad', NULL, 'asdasd asdasd asdasd', '2005-04-16', 67),
(32, 'asdasdasd', NULL, 'asdasd', '2016-01-01', 92),
(34, 'ABC', NULL, 'Dsot', '2000-01-01', 153),
(35, 'CBA', NULL, 'Dosra Dost', '1997-01-01', 153),
(36, 'asd', NULL, 'dostt', '2016-01-01', 153),
(37, 'aaa', NULL, 'aaa', '2016-01-01', 153),
(45, 'Kamran', NULL, 'Brother', '1994-09-02', 159);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
