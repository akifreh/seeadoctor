-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2016 at 01:04 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `doctor_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) NOT NULL,
  `sname` varchar(50) NOT NULL,
  `role` int(11) NOT NULL COMMENT '1=> admin 2=>patient 3=>doctor',
  `email` varchar(255) NOT NULL,
  `re_email` varchar(255) DEFAULT NULL,
  `phone` varchar(30) NOT NULL,
  `contact_number` int(30) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `address` text,
  `p_address` text,
  `city` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `zip` varchar(50) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `job_title` varchar(30) DEFAULT NULL,
  `company_employed` varchar(100) DEFAULT NULL,
  `next_of_kin` varchar(50) DEFAULT NULL,
  `medical_aid` varchar(50) DEFAULT NULL,
  `profile_pic` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=175 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `sname`, `role`, `email`, `re_email`, `phone`, `contact_number`, `password`, `dob`, `gender`, `address`, `p_address`, `city`, `country`, `zip`, `status`, `job_title`, `company_employed`, `next_of_kin`, `medical_aid`, `profile_pic`) VALUES
(68, 'shahzaib', 'khan', 1, 'shahzaib@attribes.com', '', '03442205765', 0, 'd97efba289c7b62681731b0bd1ce4ae9', '1990-06-21', 'male', '', '', '', '', '', 1, '', '', '', '', ''),
(69, 'Akif', 'Rehmani', 2, 'akif00@yahoo.com', '', '123', 573746373, 'd97efba289c7b62681731b0bd1ce4ae9', '2000-10-30', 'male', 'Gulshan', 'Postal', '', '', '', 1, 'Job', 'Company', 'aa', 'NO', 'ferrari-599-1122.jpg'),
(81, 'Dr Akif', 'Khan', 3, 'akifreh@gmail.com', '', '4562212', 0, 'd97efba289c7b62681731b0bd1ce4ae9', '0000-00-00', NULL, 'AA', '', '', '', '', 1, '', '', '', '', ''),
(82, 'test', 'test', 3, 'testing@test.com', '', '45845454', 0, 'd97efba289c7b62681731b0bd1ce4ae9', '0000-00-00', 'female', '', '', '', '', '', 0, '', '', '', '', ''),
(159, 'Faraz', 'Khan', 2, 'faraazhameed1990@gmail.com', '', '123123123', 1123123123, '8aeaf2d0387182ce37c857ba48178c18', '1990-01-14', 'male', 'ASD 45/D Road 2 NYC', '1231234 asdasdlk asdlkasd', '', '', '', 1, 'Designer', 'Smaztech', 'Wife', 'none', 'wl1.jpg'),
(160, 'Saad', 'Siddiqui', 3, 'sasid2005@gmail.com', '', '12412423', 0, '2a77a67a8e6da852e3ff8d833a1c52f0', '0000-00-00', 'male', 'asdasd', '', '', '', '', 1, '', '', '', '', ''),
(161, 'lante', 'luthuli', 2, 'lante007@gmail.com', '', '0814943255', 0, '6c2be682f64c6a333ebafe0f733d1a1b', '2016-01-01', 'male', '', '', '', '', '', 1, '', '', '', '', 'FB_IMG_1473437835923.jpg'),
(168, 'Aamir', 'Ahmed', 3, 'aamir.ahmed.dev@gmail.com', '', '3423453657', 0, '', NULL, 'male', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, ''),
(169, 'Ishe', 'Mudzengi', 3, 'ishemudzengi@gmail.com', '', '0743749092', 0, '', NULL, 'male', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, ''),
(172, 'Rehan', 'Khan', 3, 'khan@gmail.com', NULL, '123', NULL, 'd97efba289c7b62681731b0bd1ce4ae9', NULL, NULL, 'Guldhan', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(173, 'Parveen', 'Ahmed', 3, 'ahmed@gmail.com', NULL, '123', NULL, 'd97efba289c7b62681731b0bd1ce4ae9', NULL, 'female', 'B-20 Block5', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(174, 'Hashim', 'Amla', 3, 'hashim@gmail.com', NULL, '123', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
