<?php

if ( ! function_exists('admin_url'))
{
	function admin_url($uri = '')
	{
		$CI =& get_instance();
		$uri = $CI->config->item('DEFAULT_PATH_ADMIN') . $uri;
		return $CI->config->site_url($uri);
	}
}

if ( ! function_exists('d'))
{
	function d($obj = NULL, $exit = 1)
	{
		if ($obj)
		{
			print '<pre>';
			print_r ($obj);
			print '</pre>';
		}
		if ($exit == 1)
		{
			exit;
		}
	}
}

if ( ! function_exists('lq'))
{
	function lq($exit = 1)
	{
		$CI =& get_instance();
		echo $CI->db->last_query();
		if ($exit == 1)
		{
			exit;
		}
	}
}

function formulate_substr_words($text = NULL, $length = 30)
{
	if (strlen($text) > $length)
	{
		$text = substr($text, 0, $length);
		$text = strrev($text);
		$text = substr($text, strpos($text, ' '), strlen($text));
		$text = strrev($text) . '...';
	}

	return $text;
}

function formulate_multiline_text($text)
{
	if (!$text)
		return NULL;
	$text = $text ? str_replace("\n", '<br>', $text) : '';
	return $text;
}