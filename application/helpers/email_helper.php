<?php

function email_template($username = 'User', $content = '', $hasButton = FALSE, $buttonText = NULL, $buttonLink = '#', $afterButton = NULL, $notPrescription = TRUE)
{
  $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
  $message .= '<html xmlns="http://www.w3.org/1999/xhtml">';
  $message .= '<head>';
  $message .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
  $message .= '<title>See A Doctor</title>';
  $message .= '<style>';
  $message .= 'body{';
  $message .= 'padding:0px;';
  $message .= 'margin:0px;';
  $message .= '}';
  $message .= '</style>';
  $message .= '</head>';
  $message .= '<body>';
  $message .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
  $message .= '<tr>';
  $message .= '<td align="center">';
  $message .= '<table width="600" border="0" cellspacing="0" cellpadding="0">';
  $message .= '<tr>';
  $message .= '<td width="16" bgcolor="#0499dd">&nbsp;</td>';
  $message .= '<td width="572" bgcolor="#0499dd" align="left" valign="middle" height="80">';
  $message .= '<img src="' . base_url() . 'images/logo.jpg" width="152" height="56" /></td>';
  $message .= '<td width="12" bgcolor="#0499dd">&nbsp;</td>';
  $message .= '</tr>';
  $message .= '<tr>';
  $message .= '<td>&nbsp;</td>';
  $message .= '<td>&nbsp;</td>';
  $message .= '<td>&nbsp;</td>';
  $message .= '</tr>';
  $message .= '<tr>';
  $message .= '<td colspan="3"> <img src="' . base_url() . 'images/see-a-doctor-banner.jpg" width="100%"  /></td>';     
  $message .= '</tr>';
  $message .= '<tr>';
  $message .= '<td>&nbsp;</td>';
  $message .= '<td height="20">&nbsp;</td>';
  $message .= '<td>&nbsp;</td>';
  $message .= '</tr>';

  /*************************************************** End of Header ***********************************************/

  if($notPrescription) 
  {
    $message .= '<tr>';
    $message .= '<td>&nbsp;</td>';
    $message .= '<td style="font-size:19px; font-weight:700; font-family:Arial, Helvetica, sans-serif; color:#555;">Dear '. $username .',</td>';
    $message .= '<td>&nbsp;</td>';
    $message .= '</tr>';
  }
  $message .= '<tr>';
  $message .= '<td>&nbsp;</td>';
  $message .= '<td height="20">&nbsp;</td>';
  $message .= '<td>&nbsp;</td>';
  $message .= '</tr>';
  $message .= '<tr>';
  $message .= '<td>&nbsp;</td>';
  $message .= '<td style="font-size:16px; font-family:Arial, Helvetica, sans-serif; color:#555;" >';
  $message .= $content;
  $message .= '</td>';
  $message .= '<td>&nbsp;</td>';
  $message .= '</tr>';
  $message .= '<tr>';
  $message .= '<td>&nbsp;</td>';
  $message .= '<td hidden="15">&nbsp;</td>';
  $message .= '<td>&nbsp;</td>';
  $message .= '</tr>';
  if($hasButton)
  {
    $message .= '<tr>';
    $message .= '<td>&nbsp;</td>';
    $message .= '<td><a href="'. $buttonLink .'" style=" background:#0499dd; color:#fff; border: 0 none;';
    $message .= 'border-radius: 4px;';
    $message .= 'color: #fff;';
    $message .= 'display: inline-block;font-family:Arial, Helvetica, sans-serif; text-decoration:none; font-weight:bold; font-size:14px;';
    $message .= 'margin-top: 10px;';
    $message .= 'padding: 10px 20px; text-transform:uppercase;">' . $buttonText . '</a></td>';
    $message .= '<td>&nbsp;</td>';
    $message .= '</tr>';
    $message .= '<tr>';
    $message .= '<td>&nbsp;</td>';
    $message .= '<td height="25">&nbsp;</td>';
    $message .= '<td>&nbsp;</td>';
    $message .= '</tr>';
    if($afterButton)
    {
      $message .= '<tr>';
      $message .= '<td>&nbsp;</td>';
      $message .= '<td style="font-size:16px; font-family:Arial, Helvetica, sans-serif; color:#555;" >';
      $message .= $afterButton;
      $message .= '</td>';
      $message .= '<td>&nbsp;</td>';
      $message .= '</tr>';
    }
  }
  if($notPrescription)
  {
    $message .= '<tr>';
    $message .= '<td>&nbsp;</td>';
    $message .= '<td style="font-size:16px; font-family:Arial, Helvetica, sans-serif; color:#555;"><b>Kind regards</b><br />';
    $message .= 'See-A-Doctor Team<br />info@seeadoctor.co.za</td>';
    $message .= '<td>&nbsp;</td>';
    $message .= '</tr>';
  }
  /*********************************************** End of Content *******************************************************/

  $message .= '<tr>';
  $message .= '<td>&nbsp;</td>';
  $message .= '<td height="20">&nbsp;</td>';
  $message .= '<td>&nbsp;</td>';
  $message .= '</tr>';
  /*$message .= '<tr>';
  $message .= '<td bgcolor="#0499dd">&nbsp;</td>';
  $message .= '<td bgcolor="#0499dd" height="20">&nbsp;</td>';
  $message .= '<td bgcolor="#0499dd">&nbsp;</td>';
  $message .= '</tr>';
  $message .= '<tr>';
  $message .= '<td bgcolor="#0499dd"></td>';
  $message .= '<td bgcolor="#0499dd" style="font-size:13px; font-family:Arial, Helvetica, sans-serif; color:#ffffff;" align="center">See a doctors can treat most common non-emergency medical conditions over video. See a doctors can treat most common non-emergency medical conditions over video.See a doctors can treat most common non-emergency medical conditions over video. See a doctors can treat most common non-emergency medical conditions over video. </td>';
  $message .= '<td bgcolor="#0499dd">&nbsp;</td>';
  $message .= '</tr>';
  $message .= '<tr>';
  $message .= '<td bgcolor="#0499dd">&nbsp;</td>';
  $message .= '<td bgcolor="#0499dd" height="20">&nbsp;</td>';
  $message .= '<td bgcolor="#0499dd">&nbsp;</td>';
  $message .= '</tr>'; */
  $message .= '</table>';


  $message .= '</td>';
  $message .= '</tr>';
  $message .= '</table>';

  $message .= '</body>';
  $message .= '</html>';

  /****************************************** End of Footer *************************************************************/
  return $message;
}

function send_email($data = NULL)
{
  if(!$data)
    return NULL;
  $CI =& get_instance();

  $domain = $CI->config->item('email_domain');    
  $from_email = $CI->config->item('site_email');

  $to_email = $data['to_email'];
  $username = $data['username'];
  $subject = $data['subject'];
  $content = $data['content'];
  
  if($data['hasButton'])
  {
    $buttonLink = $data['buttonLink'];
    $hasButton = $data['hasButton'];
    $buttonLabel = $data['buttonLabel'];
    $afterButton = isset($data['afterButton']) ? $data['afterButton'] : NULL;
    $message = email_template($username, $content, $hasButton, $buttonLabel, $buttonLink, $afterButton);
  }
  else
    $message = email_template($username, $content);

  if(isset($data['notPrescription']))
    $message = email_template($username, $content, FALSE,FALSE,FALSE,FALSE,FALSE);    
  
  //configure email settings
  $config = $CI->config->item('email_settings');
  $CI->email->initialize($config);
  
  $length = 5;
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) 
      $randomString .= $characters[rand(0, $charactersLength - 1)];

  //send mail
  $CI->email->from($from_email, $domain);
  $CI->email->to($to_email);
  $CI->email->subject($subject . ' - ' . $randomString);
  $CI->email->message($message);
  return $CI->email->send();
}