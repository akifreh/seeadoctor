<?php
function generate_preset($original_image = NULL, $maintain_ratio = TRUE)
{
	if (!$original_image)
		return NULL;

	$CI =& get_instance();
	
	list($image_width, $image_height) = getimagesize($original_image);
	$image_info = pathinfo($original_image);
	
	$thumb_path = realpath(getcwd()) . '/uploads/profile/thumbnail/' ;
	$return_path = '/uploads/profile/thumbnail/';
	
	if (!is_dir($thumb_path))
	{
			mkdir($thumb_path);
			chmod($thumb_path, 0777);
	}
	
	$width = 250;
	$height =  350;
	$new_name = $image_info['filename'] . '.' . $image_info['extension'];
	$image_thumb = $thumb_path . $new_name;
	$return_image = $return_path . $new_name;
	
	// Step 1
		// Resize the Image
		$config['image_library'] = 'gd2';
		$config['source_image'] = $original_image;
		$config['new_image'] = $image_thumb;
		$config['maintain_ratio'] = $maintain_ratio;
		$config['width'] = $width;
		$config['height'] = $height;
		
		$CI->image_lib->initialize($config);
		$CI->image_lib->resize();
		$CI->image_lib->clear();

	// Step 2

		return $image_thumb ? $new_name : NULL;
}

function get_image_url($image_url = NULL)
{
        $default_image = 'images/default_picture.jpg';
        
        if(!$image_url)
          return base_url() . $default_image;
        
        $thumb_image = realpath(getcwd()) . '/uploads/profile/thumbnail/' . $image_url ;
        if(file_exists($thumb_image))
            return base_url() . 'uploads/profile/thumbnail/' . $image_url ;

        $original_path = realpath(getcwd()) . '/uploads/profile/'. $image_url ;
        if(file_exists($original_path))
        {
            generate_preset($original_path);
            return base_url() . 'uploads/profile/thumbnail/' . $image_url ;
        }

        return base_url() . $default_image;
}  


/* End of file image_helper.php */
/* Location: ./application/helpers/image_helper.php */