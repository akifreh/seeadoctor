<?php
class Admin_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function sendEmail($to_email,$password)
    {
        $data = array(
            'to_email' => $to_email, 
            'subject' => 'Verfity Email Address',
            'content' => 'Please click on the below activation link to verify your email address.',
            'hasButton' => TRUE,
            'buttonLabel' => 'Reset Password',
            'buttonLink' => base_url() .'home/verify/' . md5($to_email),
            'username' => 'User',
            'afterButton' => 'You can login with these credentials.<br/><br />Your Email : <b>'.$to_email.'</b><br/><br/> Your Password : <b>'.$password . '</b><br/><br/>',
        );
        send_email($data);
    }
    
    function verifyEmailID($key)
    {
        $data = array('status' => 1);
        $this->db->where('md5(email)', $key);
        return $this->db->update('users', $data);
    }
 
    function readUsersList()
    {
        $condition = 'role IN' . '(' . 2 . ')';
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
        $this->db->order_by('id', 'DESC');
        //$this->db->limit(6);
        $query = $this->db->get();
        
     //   echo $this->db->last_query(); die;  

        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;
    }
    
    function readDoctorRates()
    {
        $this->db->select('*');
        $this->db->from('charges');
        $query = $this->db->get();

        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;
    }
    
    function readUserDetails($id)
    {
        $condition = 'id =' . $id;
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
        //$this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1)
        {
            $result = $query->row();
            if($result)
            {
                $result->profile_pic = get_image_url($result->profile_pic);
                if($result->role == 3)
                {
                    $doctor_detail = $this->db->query("SELECT * FROM doctor_cv WHERE user_id ='".$result->id."'")->row_array();
                    $result->profile_pic = get_image_url($doctor_detail['profile_pic']);
                }    
                
            }
            return $result;
        }
        else
            return false;
    }
    
    function readDoctorDetails($id)
    {
        $condition = 'id =' . $id;
        $this->db->select('*')
            ->from('users')
            ->join('doctor_cv', 'users.id = doctor_cv.user_id')
            ->join('doctor_type', 'doctor_cv.type_of_doctor = doctor_type.id')
            ->where('users.id ='. $id );
        
        $query = $this->db->get();

        if ($query->num_rows() == 1)
        {
            $result = $query->row();
            if($result)
            {
                $result->profile_pic = get_image_url($result->profile_pic);
                if($result->role == 3)
                {
                    $doctor_detail = $this->db->query("SELECT * FROM doctor_cv WHERE user_id ='".$id."'")->row_array();
                    $result->profile_pic = get_image_url($doctor_detail['profile_pic']);
                }   
               
                $result->academics = $this->getDoctorAcademics($result->user_id);
                $result->experience = $this->getDoctorExperience($result->user_id);
            }
            return $result;
        }   
        else
            return false;
    }
    
    function readDoctorAvailableDays($id)
    {
        $condition = 'user_id =' . $id;
        $this->db->select('*')
        ->from('available_doctor_days')
        ->join('days', 'available_doctor_days.day_id = days.id')
        ->where($condition);  
         
         $query = $this->db->get();

        if ($query->num_rows() >= 1)
        {
            $result = $query->result();
            
            foreach ($result as $value) 
            {
                $array[$value->day_id] = $value->day;   
            }

            return $array;
        }
            
        else
            return false;
    }
    
    function readDoctorTimings($id)
    {
        $condition = 'user_id =' . $id;
        $this->db->select('*')
            ->from('timing')
            ->where($condition); 
       
        $query = $this->db->get();

        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;
    }
    
    function updateDoctorRates($data)
    {
        $this->db->where('id',$data['id']);
        return $this->db->update('charges', $data);
    }
    
    function readUserOtherProfiles($id)
    {
        $condition =  $condition = 'user_id =' . $id ;
       
        $this->db->select('*');
        $this->db->from('other_profile');
        $this->db->where($condition);
        $query = $this->db->get();

        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;
    }
    
    function readDoctorProfiles()
    {
        $this->db->select('users.*, doctor_cv.*, doctor_type.type_of_doctor AS doctor_type')
            ->from('users')
            ->join('doctor_cv', 'users.id = doctor_cv.user_id')
            ->join('doctor_type', 'doctor_cv.type_of_doctor = doctor_type.id');
            
        $this->db->order_by('users.id', 'DESC');
        //$this->db->limit(6);
        $query = $this->db->get();
        //echo $this->db->last_query(); die;    
        if ($query->num_rows() >= 1) 
            return $query->result();
               /* foreach ($result as $key => $value) 
                {
                    $result[$key]->timing = $this->getDoctorTimings($value->user_id);
                    $result[$key]->days = $this->getDoctorDays($value->user_id);
                }*/
            else
                return false;
    }
    
    function insertDoctor($data,$data1)
    {
        $this->db->insert('users', $data);
        $user_id = $this->db->insert_id(); // Getting last inserted id 
        
        $data1['user_id'] = $user_id;
        return $this->db->insert('doctor_info', $data1);
    }
    
    function updateDoctor($data)
    {
        $this->db->where('id',$data['id']);
        $this->db->update('users', $data);  
    }
    
    function updateDoctorInfo($data1)
    {
        $this->db->where('user_id',$data1['id']);
        $this->db->update('doctor_info', $data1); 
    }
    
    
    function doctorEmail($id)
    {
        $condition = 'id='.$id;
        $this->db->select('email,fname');
        $this->db->from('users');
        $this->db->where($condition);
        
        $query = $this->db->get();
        
        if ($query->num_rows() == 1)
            return $query->result();
        else
            return false;
    }

    function doctorStatus($id)
    {
        $condition = 'id='.$id;
        $this->db->select('status');
        $this->db->from('users');
        $this->db->where($condition);

        $query = $this->db->get();
        $result = $query->row();

        return $result ? $result->status : 0;
    }
    
    function sendDoctorVerifyEmail($to_email,$password)
    {
        $data = array(
            'to_email' => $to_email, 
            'subject' => 'Request Approved',
            'content' => 'Congratulation, your request as a doctor has been approved.<br /><br />Please click on the below activation link to verify your email address after which you will be given username and password.',
            'hasButton' => TRUE,
            'buttonLabel' => 'Activate Account',
            'buttonLink' => base_url() .'user/doctorVerify/' . md5($to_email),
            'username' => 'User'
        );
        send_email($data);
    }

    function sendDoctorEmail($to_email,$password)
    {
        $data = array(
            'to_email' => $to_email, 
            'subject' => 'Login Details',
            'content' => 'Congratulation, Your email has been verified.<br />Now you can login to your account by using the following credentials :<br /><br />Your Email : <strong>'. $to_email. '</strong><br/> Your Password : <strong>'.$password . '</strong>',
            'hasButton' => TRUE,
            'buttonLabel' => 'Login',
            'buttonLink' => base_url(),
            'username' => 'User'
        );
        send_email($data);
    }
    
    function updateDoctorPassword($id, $password)
    {
        $data = array('password' => md5($password));

        $this->db->where('id',$id);
        return $this->db->update('users', $data);
    }
    
    function verfiyDoctorEmail($id, $status = 2)
    {
        $data = array('status' => $status);
        
        $this->db->where('id',$id);
        return $this->db->update('users', $data);        
    }
    
    function randomPassword() 
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) 
        {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    function getDoctorTimings($user_id)
    {
        if(!$user_id)
            return NULL;

        $condition = 'user_id='.$user_id;
        $this->db->select('start_time, end_time');
        $this->db->from('timing');
        $this->db->where($condition);
        
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;
    }
    
    function getDoctorAcademics($user_id)
    {
        if(!$user_id)
            return NULL;

        $condition = 'user_id='.$user_id;
        $this->db->from('doctor_academics');
        $this->db->where($condition);
        $query = $this->db->get();
        
        $result = $query->result();
        return $result ? $result : NULL;
    }

    function getDoctorExperience($user_id)
    {
        if(!$user_id)
            return NULL;

        $condition = 'user_id='.$user_id;
        $this->db->from('doctor_experience');
        $this->db->where($condition);
        $query = $this->db->get();
        
        $result = $query->result();
        return $result ? $result : NULL;
    }

    function getDoctorDays($user_id)
    {
        if(!$user_id)
            return NULL;

        $condition = 'user_id='.$user_id;
        $this->db->select('available_doctor_days.day_id, days.day AS day_name');
        $this->db->join('days', 'available_doctor_days.day_id = days.id');
        $this->db->from('available_doctor_days');
        $this->db->where($condition);
        
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
           return $query->result();
        else
            return false;
    }  

    function getPayments()
    {
        $this->db->select('appointments.*, payments.*');
        $this->db->join('appointments', 'payments.appointment_id = appointments.id');
        $this->db->from('payments');
        $this->db->order_by('payments.id', 'DESC');
        $query = $this->db->get();
        $result = $query->result();

        foreach($result as $value)
        {
            $value->doctorInfo = $this->readDoctorDetails($value->d_id);
            $value->patientInfo = $this->readUserDetails($value->p_id);
        }
        return $result ? $result : NULL;
    }

    function getAppointments($condition, $limit = 5)
    {
        if($condition)
            $this->db->where($condition);
        
        $this->db->order_by('start_time', 'DESC');
        
        // if($limit)
        //     $this->db->limit($limit);
        
        $query = $this->db->get('appointments');
        $result = $query->result();

        foreach($result as $value)
        {
            $value->doctorInfo = $this->readDoctorDetails($value->d_id);
            $value->patientInfo = $this->readUserDetails($value->p_id);
        }
        return $result ? $result : NULL;
    }    
    /**
     * Available days added or removed
     * @param type $user_id     /
     */
    
    function AddAvailableDays($user_id){ 
        
        $condition = 'user_id='.$user_id;
        $this->db->where($condition);
        $query = $this->db->get('available_doctor_days');
        
        if ($query->num_rows() > 0){
            $this->remove_days($user_id);// remove existing rows
        } 

        $this->AddDays($user_id); // Add days & timing
        
    }
    
    function AddDays($user_id){
        
        $sql = "INSERT INTO available_doctor_days(day_id, user_id, timing, fixed_from_time, fixed_to_time, flexible_from_time, flexible_to_time) VALUES ";
        
        $days = $this->input->post('days');

        foreach($days["dayoftheweek"] as $key => $value){
            $data = [];
            $data['user_id'] = $user_id;
            $data['day_id'] = $value;
            $data['timing'] = ($days['timings'][$key] == 'fixed') ? 1 : 2;  //1 for fixed or 2 for flexible
            $data['fixed_from_time'] = $days['fixed_from'][$key];
            $data['fixed_to_time'] = $days['fixed_to'][$key];
            $data['flexible_from_time'] = ($days['timings'][$key] == 'fixed') ? '' : $days['flexible_from'][$key];
            $data['flexible_to_time'] = ($days['timings'][$key] == 'fixed') ? '' : $days['flexible_to'][$key];
            $insert_data[] = $data;
        }
        
        $this->db->insert_batch('available_doctor_days', $insert_data);
    }
    
    function remove_days($id){
        $this->db->where('user_id', $id);
        $this->db->delete('available_doctor_days');
    }
    
    function getDoctorAvailableDays($id)
    {
        
        $this->db->select('*')
        ->from('available_doctor_days')
        ->where('available_doctor_days.user_id', $id);  
         
        $query = $this->db->get();
//        echo $this->db->last_query(); 
//        echo $query->num_rows(); die;
        $result = $query->result();
        return $result;
        // if ($query->num_rows() >= 1){            
            
        // }else{
        //     return false;
        // }
    }
    
    function UpdateExperience($user_id){
        
            $this->load->model('Doctor_model', 'doctor_model');
        
            $company        = $this->input->post('company');
            $job_title      = $this->input->post('job_title');
            $start_from_mm_yy = $this->input->post('work_from_mm_yy');
            $end_to_mm_yy = $this->input->post('work_to_mm_yy');
            $present_here   = $this->input->post('stay_here');
            $get_present_here  =   $present_here[0];
            $doctorId = $user_id;
                
            $this->doctor_model->deleteExperience($doctorId);
            
            if(isset($company[0]) && $company[0])
            {
              $checkCounter = 1;
              foreach($company as $key => $value)
              {
                  if($value && $job_title[$key])
                  {   
                    $present_here = ($checkCounter == $get_present_here) ? '1' : '0' ;
                    $data6 = array(
                      'company'       => $value,
                      'job_title'     => $job_title[$key],
                      'start_date'    => date('Y-m-d', strtotime($start_from_mm_yy[$key])),
                      'end_date'      => date('Y-m-d', strtotime($end_to_mm_yy[$key])),
                      'present_here'  => $present_here,
                      'user_id'      => $doctorId
                    );
                    $this->doctor_model->insertExperience($data6);
                  }  
                    $checkCounter++;
              }
            }
    }
    
    function UpdateAcademicInfo($user_id){
        
//        if($shootDayEmail || $shootTimingEmail)
//          $this->doctor_model->sendTimingsChangedEmail($doctorId, $oldTimings, $oldDays);

            $this->load->model('Doctor_model', 'doctor_model');
            $qualification = $this->input->post('qualification');
            $institution = $this->input->post('institution');
            $year = $this->input->post('year');

            $doctorId = $user_id;
            $this->doctor_model->deleteAcademics($doctorId);   
              
            if(isset($qualification[0]) && $qualification[0])
            {
              
              foreach($qualification as $key => $value)
              {
                if($value)
                {
                  $data5 = array(
                    'qualification' => $value,
                    'institution' => $institution[$key],
                    'year' => $year[$key],
                    'user_id' => $doctorId
                  );
                  $this->doctor_model->insertAcademics($data5);
                }
              }
            }
        
        
    }
}

