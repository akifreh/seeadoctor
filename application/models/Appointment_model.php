<?php
class Appointment_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function readNutritionists()
    {
        $condition = 'doctor_cv.type_of_doctor =1 AND users.status = 1';
        $this->db->select('*')
            ->from('users')
            ->join('doctor_cv', 'users.id = doctor_cv.user_id');
//          ->join('available_doctor_days', 'users.id = available_doctor_days.user_id')
//          ->join('timing', 'users.id = timing.user_id');     
        
        $this->db->where($condition);
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
        {
            $result = $query->result();
            foreach($result as $row)
            {
                $row->profile_pic = get_image_url($row->profile_pic);
            }
            return $result;
        }
        else
            return false;
    }
    
    function readPsychologists()
    {
        $condition = 'type_of_doctor = 2 AND users.status = 1';
        $this->db->select('*')
            ->from('users')
            ->join('doctor_cv', 'users.id = doctor_cv.user_id')
            ->join('doctor_online', 'users.id = doctor_online.user_id');
        $this->db->order_by('rating', 'DESC');
        $this->db->order_by('no_of_patient', 'DESC');
        $this->db->group_by('doctor_online.user_id');
        $this->db->where($condition);
        $query = $this->db->get();

        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;
    }
    
     function readAllPsychologists()
     {
        $condition = 'doctor_cv.type_of_doctor = 2 AND users.status = 1';
        $this->db->select('*')
            ->from('users')
            ->join('doctor_cv', 'users.id = doctor_cv.user_id');
//         ->join('available_doctor_days', 'users.id = available_doctor_days.user_id')
//         ->join('timing', 'users.id = timing.user_id');     
        $this->db->where($condition);
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
        {
            $result = $query->result();
            foreach($result as $row)
            {
                $row->profile_pic = get_image_url($row->profile_pic);
            }
            return $result;
        }
        else
            return false;
    }
    
    function readMedicalDoctor()
    {
        $condition = 'type_of_doctor = 3 AND users.status = 1';
        $this->db->select('*')
            ->from('users')
            ->join('doctor_cv', 'users.id = doctor_cv.user_id')
            ->join('doctor_online', 'users.id = doctor_online.user_id');
        $this->db->order_by('rating', 'DESC');
        $this->db->order_by('no_of_patient', 'DESC');
        $this->db->group_by('doctor_online.user_id');
        $this->db->where($condition);
        $query = $this->db->get();

        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;
    }
    
    function readAllMedicalDoctor()
    {
       $condition = 'doctor_cv.type_of_doctor = 3 AND users.status = 1';
        $this->db->select('*')
            ->from('users')
            ->join('doctor_cv', 'users.id = doctor_cv.user_id');
//         ->join('available_doctor_days', 'users.id = available_doctor_days.user_id')
//         ->join('timing', 'users.id = timing.user_id');     
        $this->db->where($condition);
        $query = $this->db->get();

        if ($query->num_rows() >= 1)
        {
            $result = $query->result();
            foreach($result as $row)
            {
                $row->profile_pic = get_image_url($row->profile_pic);
            }
            return $result;
        }
        else
            return false; 
    }
    
    function readOneMedicalDoctor($doctorId)
    {
        //$condition = 'type_of_doctor = medical-doctor';
        $this->db->select('*')
            ->from('users')
            //->join('doctor_cv', 'users.id = doctor_cv.user_id')
            ->join('available_doctor_days', 'users.id = available_doctor_days.user_id')
            ->join('days', 'available_doctor_days.day_id = days.id');
       
         //->join('timing', 'users.id = timing.user_id');   
        $this->db->where('available_doctor_days.user_id', $doctorId);
        
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;
    }
    
    function readMedicalDoctorTiming($userId)
    {
        $condition = 'user_id =' . $userId;
        $this->db->select('*')
            ->from('users')
            ->join('timing', 'users.id = timing.user_id');
        $this->db->where($condition);
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;
    }
    
    function readDoctorDays($dayId, $user_Id)
    {
        $condition = 'day_id='. $dayId . ' AND user_id='. $user_Id;
        $this->db->select('*')
            ->from('days')
            ->join('available_doctor_days', 'days.id = available_doctor_days.day_id');
        $this->db->where($condition);
        $query = $this->db->get();
            
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;
    }
                
    function readDoctorTime($doctorId)
    {
        $condition = 'user_id='. $doctorId;
     
        $this->db->select('*')
            ->from('timing');
        $this->db->where($condition);
        $query = $this->db->get();
            
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return;
    }
    
    function readNutriTime($doctorId)
    {
        $condition = 'user_id='. $doctorId;
     
        $this->db->select('*')
            ->from('timing');
        $this->db->where($condition);
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false; 
    }
    
    function readPsychoTime($doctorId)
    {
        $condition = 'user_id='. $doctorId;
     
        $this->db->select('*')
            ->from('timing');
        $this->db->where($condition);
        $query = $this->db->get();
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false; 
    }
    
    function readDoctorAvailableTime($doctorId)
    {
        $condition = 'd_id ='. $doctorId;
        $this->db->select('*')
            ->from('appointments');
        $this->db->where($condition);
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;
    }
    
    function readPsychoAvailableTime($doctorId)
    {
        $condition = 'd_id ='. $doctorId;
        $this->db->select('*')
            ->from('appointments');
        $this->db->where($condition);
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false; 
    }
    
    function readNutriAvailableTime($doctorId)
    {
        $condition = 'd_id ='. $doctorId;
        $this->db->select('*')
            ->from('appointments');
        $this->db->where($condition);
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;  
    }

    function insertSymptoms($data)
    {
        $this->db->insert('appointment_symptoms', $data);
    }

    function insertNutritionistAppointment($data)
    {
        $data['created_at'] = date('Y-m-d H:i:s', time());
        $this->db->insert('appointments', $data);
        return $this->db->insert_id(); // Getting last inserted id  
    }
    
    function insertPsychologistAppointment($data)
    {
        $data['created_at'] = date('Y-m-d H:i:s', time());
        $this->db->insert('appointments', $data);
        return $this->db->insert_id(); // Getting last inserted id  
    }
    
    function insertMedicalDoctorAppointment($data)
    {
        $data['created_at'] = date('Y-m-d H:i:s', time());
        $this->db->insert('appointments', $data);
        return $this->db->insert_id(); // Getting last inserted id 
    }
    
    function readPatientAppointment($pID)
    {
        $condition = 'appointments.p_id =' . $pID;
        $this->db->select('*')
            ->from('users')
            ->join('appointments', 'users.id = appointments.p_id');
        $this->db->where($condition);
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;               
    }
    
    function insertNowAppointments($data)
    {
        $data['meeting_approval'] = 0;
       // $data['created_at'] = date('Y-m-d H:i:s', time());
        $this->db->insert('appointment_now', $data);
        return $this->db->insert_id(); // Getting last inserted id 
        
    }
    
    
    function insertNotification($data)
    {
        $data['created_date'] = date('Y-m-d H:i:s', time());
        $this->db->insert('notification', $data);
        return $this->db->insert_id(); // Getting last inserted id 
    }
    
    
    function sendEmailToDoctor($to_email, $doctorName, $randomString, $action = 'approve')
    {
        if($action == 'approve')
        {
            $data = array(
                'to_email' => $to_email, 
                'subject' => 'Appointment Now Link',
                'content' => 'Please click on the link below to start your call with the patient.',
                'hasButton' => TRUE,
                'buttonLabel' => 'Start Call',
                'buttonLink' => base_url() .'call?sessionkey=' . $randomString,
                'username' => 'Dr '. $doctorName
             );
        }
        else
        {
            $data = array(
                'to_email' => $to_email, 
                'subject' => 'Appointment Denied',
                'content' => 'You have recently denied an appoinment request.<br /><br />If it wasn\'t you please login and change your password.',
                'hasButton' => TRUE,
                'buttonLabel' => 'Login',
                'buttonLink' => base_url(),
                'username' => $doctorName
             );
        }

        send_email($data);
    }
    
    function sendEmailToPatient($to_email, $patientName, $str, $action = 'approve', $doctorName = NULL)
    {
        if($action == 'approve')
        {
            $data = array(
                'to_email' => $to_email, 
                'subject' => 'Appointment Approved',
                'content' => 'Dr ' . $doctorName . ' has approved your appointment request.<br /><br />Please click on the following link to start your call with the him.',
                'hasButton' => TRUE,
                'buttonLabel' => 'Start Call',
                'buttonLink' => base_url() .'call?sessionkey=' . $str,
                'username' => $patientName
             );
         
        }
        else
        {
            $data = array(
                'to_email' => $to_email, 
                'subject' => 'Appointment Denied',
                'content' => 'Dr ' . $doctorName . ' has denied your appointment now request.<br /><br />Please wait until your request is approved by other doctors online or schedule an appoinment.',
                'hasButton' => FALSE,
                'username' => $patientName
             );
        }
        send_email($data);
    }
    
    function sendEmailToPatientForAppointment($to_email, $patientName, $appDate, $appTime)
    {
        $data = array(
            'to_email' => $to_email, 
            'subject' => 'Appointment Schedule',
            'content' => 'Your Appointment has been set on <b>'. $appDate .'</b> at <b>' . $appTime . '</b><br /><br />You will recieve a meeting room url 30 minutes before your meeting time.',
            'hasButton' => FALSE,
            'username' => $patientName
        );
        send_email($data);
    }
    
    function sendEmailToDoctorForAppointment($to_email, $doctorName, $appDate, $appTime)
    {    
        $data = array(
            'to_email' => $to_email, 
            'subject' => 'Appointment Request',
            'content' => 'You have a request for appointment on <b>'. $appDate .'</b> at <b>' . $appTime . '</b><br /><br />You will recieve a meeting room url 30 minutes before your meeting time.',
            'hasButton' => FALSE,
            'username' => 'Dr ' . $doctorName
        );
        send_email($data);
    }
    
    function sendCronEmailToPatientForAppointment($to_email, $patientName, $appDate, $appTime, $str, $doctorName)
    { 
        $data = array(
            'to_email' => $to_email, 
            'subject' => 'Appointment in 30 mins',
            'content' => 'Your appointment with Dr ' . $doctorName . ' on <b>'. $appDate .'</b> at <b>' . $appTime . '</b> will start in 30 minutes.<br /><br />Click below to start your meeting after 30 minutes.',
            'hasButton' => TRUE,
            'buttonLabel' => 'Start Meeting',
            'buttonLink' => base_url() .'call?sessionkey='.$str,
            'username' => $patientName
        );
        send_email($data);
    }
    
    function sendCronEmailToDoctorForAppointment($to_email, $doctorName, $appDate, $appTime, $str, $patientName)
    {
        $data = array(
            'to_email' => $to_email, 
            'subject' => 'Appointment in 30 mins',
            'content' => 'Your meeting with ' . $patientName .' on <b>'. $appDate .'</b> at <b>' . $appTime . '</b> will start in 30 minutes.<br /><br />Click below to start your meeting after 30 minutes.',
            'hasButton' => TRUE,
            'buttonLabel' => 'Start Meeting',
            'buttonLink' => base_url() .'call?sessionkey='.$str,
            'username' => 'Dr ' . $doctorName
        );
        send_email($data);
    }
    
    function readPatientScheduledAppointment($appId)
    {
        $condition = 'id ='. $appId;
        $this->db->select('*')
            ->from('appointments');
        $this->db->where($condition);
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;  
    }
    
    function readAllAppointments()
    {
        $time = new DateTime();
        $this->db->where('type', 'schedule');
        $this->db->where('meeting_approval', 0);
        $this->db->where('start_time >', $time->format('Y-m-d H:i:s'));
        $time->modify('+1 day');
        $this->db->where('start_time <', $time->format('Y-m-d 00:00:00'));
        $query = $this->db->get('appointments');
        //echo $this->db->last_query(); die;
        $result = $query->result();
        
        return $result ? $result : NULL;        
    }
    
    function getLastestAppointmentLink($id)
    {
        // 5 mins before start of any call
        $time = new DateTime();
        $condition = 'p_id =' . $id . ' AND meeting_approval = 1';
        $this->db->where($condition);
        $this->db->where('start_time >', $time->format('Y-m-d H:i:s'));
        $time->modify('+5 mins');
        $this->db->where('start_time <', $time->format('Y-m-d H:i:s'));
        $query = $this->db->get('appointments');
        $result = $query->row();
        if($result)
        {
            $this->db->where('appointment_id', $result->id);
            $query = $this->db->get('sessionkey');
            $record = $query->row();
            if($record)
            {
                $array = array('link' => base_url() . 'call?sessionkey=' . $record->session_key,
                                'doctor' => $this->getUserById($record->d_id)
                );
            }
                return $array;
        }
        return NULL;
    }

    function readAllMyAppointments($userId)
    {
        $currentDate = date('Y-m-d H:i:s');
        $condition = 'app.p_id='. $userId . ' AND start_time > \''. $currentDate.'\'';;
        $query =  $this->db->select('app.id, app.start_time, app.end_time, doctor.fname as doctorname, patients.fname as patientname,meeting_approval')
            ->from('appointments as app')
            ->join('users as doctor', 'doctor.id=app.d_id')
            ->join('users as patients', 'patients.id=app.p_id')
            ->where($condition);
        $this->db->order_by('start_time');
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return;     
    }

    function getAppointment($id)
    {
        if(!$id)
          return NULL;

        $this->db->where('id', $id);
        $query = $this->db->get('appointments');
        
        $result = $query->row();
        if($result)
        {
            $this->db->select('category, symptom');
            $this->db->where('appointment_id', $id);
            $query = $this->db->get('appointment_symptoms');
            $symptom = $query->result();
            foreach ($symptom as $value) 
            {
                $array[$value->category][] = $value->symptom; 
            }
            $result->symptom = $array;
        }
        return $result ? $result : NULL;     
    }
    
    function deleteAppoinment($id)
    {
         if(!$id)
          return NULL;

        $this->db->where('id', $id);
        return $this->db->delete('appointments');
    }

    function sendCancelAppoinmentEmail($to_email, $doctorName, $patientName, $appointmentDate, $person = 'doctor')
    {
        if($person == 'doctor')
        {
            $data = array(
                'to_email' => $to_email, 
                'subject' => 'Appoinment Canceled',
                'content' => $patientName. ' has cancelled following appointment with you<br /><br />Date : '. date('d M Y', strtotime($appointmentDate)) . '&nbsp;&nbsp;&nbsp;Time : '. date('h:i a', strtotime($appointmentDate)),
                'hasButton' => FALSE,
                'username' => 'Dr ' . $doctorName
            );

        }
        else
        {
            $data = array(
                'to_email' => $to_email, 
                'subject' => 'Appoinment Canceled',
                'content' => 'You have cancelled following appointment with Dr '. $doctorName . '<br /><br />Date : '. date('d M Y', strtotime($appointmentDate)) . '&nbsp;&nbsp;&nbsp;Time : '. date('h:i a', strtotime($appointmentDate)) .'<br /><br />
                    Thank you for letting us know and keep us posted if you need anything else from us.',
                'hasButton' => FALSE,
                'username' => $patientName
            );

        }
        send_email($data);
    }

    function getUserById($id)
    {
        if(!$id)
            return NULL;

        $this->db->where('id', $id);
        $query = $this->db->get('users');
        $result = $query->row();

        return $result ? $result : NULL;

    }

    function sendAppointmentNowNotificationEmail($to_email, $doctorName)
    {
        $data = array(
            'to_email' => $to_email, 
            'subject' => 'Appointment Now Request',
            'content' => 'You have a request for appointment now. Please refresh your dashboard to approve or deny the request or click the link below.',
            'hasButton' => TRUE,
            'buttonLabel' => 'See Request',
            'buttonLink' => base_url() .'doctordashboard',
            'username' => 'Dr ' . $doctorName
        );
        send_email($data); 
    }

    function is_profile_incomplete($id)
    {
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        $total = 13;
        $current = 5;
        if($result)
        {
            if($result->phone)
                $current++;
            if($result->address)
                $current++;
            if($result->p_address)
                $current++;
            if($result->job_title)
                $current++;
            if($result->company_employed)
                $current++;
            if($result->next_of_kin)
                $current++;
            if($result->medical_aid)
                $current++;
            if($result->profile_pic)
                $current++;

            if($total == $current)
                return FALSE;
            else
                return $current;
        }
        
         return $current;
    }

    function has_card_expired($id)
    {
        $this->db->from('user_registration');
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        $result = $query->row();
        if(!$result || !$result->card_expiry)
            return TRUE;
        
        $now = date(('Y-m-d'), time());
        if($now >= $result->card_expiry)
            return 'expired';
        return FALSE;
    }
}