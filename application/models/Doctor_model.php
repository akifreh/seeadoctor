<?php
class Doctor_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
   function insertDoctor($data)
    {
       $this->db->insert('users', $data);
        return $user_id = $this->db->insert_id(); // Getting last inserted id 
  }
    
    function insertDoctorDays($data2){
       
       for($i =0; $i< count($data2['day']); $i++ ){
           
        $this->db->insert('available_doctor_days', array('day_id'=>$data2['day'][$i], 'user_id'=> $data2['user_id'])); 
       }
    } 
    function insertDoctorInfo($data1){
             
       
       $this->db->insert('doctor_cv', $data1);  
    }
    
    function insertDoctorTiming($data3){
       
       $this->db->insert('timing', $data3); 
    }
    
    function insertAcademics($data){
       
       $this->db->insert('doctor_academics', $data); 
    }
    
    function insertExperience($data){
       $this->db->insert('doctor_experience', $data); 
    }

    function readDoctorNowAppointment($doctorId, $timeCheck = TRUE)
    {
        $time = new DateTime();
        $time->modify('-60 mins');
        
        $condition = 'd_id = '. $doctorId . ' AND meeting_approval='. 0 ;
        $this->db->select('*')->from('appointment_now');
        $this->db->where($condition);
        $this->db->order_by('created_at', 'DESC');
        if($timeCheck)
            $this->db->where('created_at >=', $time->format('Y-m-d g:i:s'));
        $query = $this->db->get();

        if ($query->num_rows() >= 1)
        {
            $result = $query->result();
            $this->db->select('*')->from('appointment_now');
            $condition = 'p_id = '. $result[0]->p_id . ' AND meeting_approval='. 1 ;
            $this->db->where($condition);
            $this->db->where('created_at', $result[0]->created_at);
            $query = $this->db->get();
            if ($query->num_rows() >= 1)
                return false;       
            return $result; 
        }
        else
            return false;
    }
    
    function readPatient($patientId){
        $condition = 'id = '. $patientId;
        $this->db->select('*')
        ->from('users');
    $this->db->where($condition);
    $query = $this->db->get();
            if ($query->num_rows() >= 1) {
            return $query->result();
            } else {
            return;
        }   
    }
    
    function readAllPatient($patientId, $doctorId){
        $condition = 'p_id = '. $patientId . ' AND d_id = '. $doctorId;
        $this->db->select('*')
        ->from('users')
        ->join('appointments', 'users.id = appointments.p_id');
    $this->db->where($condition);
    $query = $this->db->get();
            if ($query->num_rows() >= 1) {
            return $query->result();
            } else {
            return;
        }   
    }
    
    function updateApproveStatus($data){
        
        $this->db->where('id',$data['id']);

        return $this->db->update('appointment_now', $data);    
    }
    
    function readApprovedAppointment($dId, $pId){
        $condition = 'p_id =' . $pId . ' AND d_Id =' . $dId . ' AND meeting_approval = 1';
        $this->db->select('*')
        ->from('appointments');
        $this->db->where($condition);
        $this->db->where('type', 'schedule-');
        $query = $this->db->get();
            if ($query->num_rows() >= 1) {
            return $query->result();
            } else {
            return;
        }   
    }
    
    function readApprovedNowAppointment($dId, $pId){
        $condition = 'p_id =' . $pId . ' AND d_Id =' . $dId . ' AND meeting_approval = 1';
        $this->db->select('*')
        ->from('appointment_now');
        $this->db->where($condition);
        $query = $this->db->get();
            if ($query->num_rows() >= 1) {
            return $query->result();
            } else {
            return;
        }   
    }

    function getLastestAppointmentLink($dId)
    {
        $time = new DateTime();
        $condition = 'd_Id =' . $dId . ' AND meeting_approval = 1';
        $this->db->where($condition);
        $this->db->where('start_time >', $time->format('Y-m-d H:i:s'));
        $time->modify('+5 mins');
        $this->db->where('start_time <', $time->format('Y-m-d H:i:s'));
        $query = $this->db->get('appointments');
        $result = $query->row();
        if($result)
        {
            $this->db->where('appointment_id', $result->id);
            $query = $this->db->get('sessionkey');
            $record = $query->row();
            if($record)
            {
                $array = array('link' => base_url() . 'call?sessionkey=' . $record->session_key, 'user' => $this->getUserById($record->p_id));
                return $array;
            }
        }
        return NULL;
    }

    function insertSessionKey($sessInfo){
        $this->db->insert('sessionkey',$sessInfo); 
    }
    
    function readAllScheduledAppointments($doctorId){
      $currentDate = date('Y-m-d H:i:s');  
      $condition = 'd_id='. $doctorId. ' AND start_time >= \''. $currentDate.'\'';
      $this->db->select('*, app.id AS appointment_id')
      ->from('appointments as app')
      ->join('users u', 'u.id=app.p_id');
      $this->db->where($condition);
      $this->db->order_by('start_time', 'ASC');
      //$this->db->where('type', 'schedule');
      
      $query = $this->db->get();
            if ($query->num_rows() >= 1) {
            return $query->result();
            } else {
            return;
        } 
    }
    
    function readAllPreviousAppointments($doctorId, $meeting_approval = 3)
    {
        $currentDate = date('Y-m-d H:i:s');
        $condition = 'd_id='. $doctorId. ' AND start_time < \''. $currentDate.'\'';
        $this->db->select('*, app.id AS appointment_id')->from('appointments as app')->join('users u', 'u.id=app.p_id');
        $this->db->where($condition);
        $this->db->where('meeting_approval', $meeting_approval);
        $this->db->order_by('start_time', 'DESC');
        $query = $this->db->get();
        $result = $query->result();
        if($result && $meeting_approval = 3)
        {
            foreach($result as $row)
            {
                $this->db->where('appointment_id', $row->appointment_id);
                $query = $this->db->get('payments');
                $row->payments = $query->row();
            }
        }
        return $result ? $result : NULL;
    }
    
    function readDoctorRating($doctorId){
        $condition = 'user_id='. $doctorId;
        $this->db->select('rating') 
        ->from('doctor_cv');
      
        $this->db->where($condition);
        $query = $this->db->get();
                    if ($query->num_rows() >= 1) {
                    return $query->result();
                    } else {
                    return;
                }
    }
    
    
    function addAppointment($data)
    {
       $data['created_at'] = date('Y-m-d H:i:s', time());
       $this->db->insert('appointments', $data);
       return $this->db->insert_id(); // Getting last inserted id 
    } 
    
    function deleteAllAppointmentNow($p_id, $created_at)
    {
         if(!$p_id)
          return NULL;

        $this->db->where('p_id', $p_id);
        $this->db->where('created_at', $created_at);
        return $this->db->delete('appointment_now');
    }

    function updateDoctor($data, $id){
        
        $this->db->where('id',$id);
        return $this->db->update('users', $data);    
    }
    
    function updateSymptoms($data, $p_id){
        
        $this->db->where('p_id',$p_id);
        $this->db->where('appointment_id', NULL);
        return $this->db->update('appointment_symptoms', $data);    
    }
    
    function updateDoctorInfo($data, $id){
        
        $this->db->where('user_id', $id);
        return $this->db->update('doctor_cv', $data);    
    }

    function updateDoctorDays($data, $id)
    {
        $this->db->where('user_id', $id);
        $this->db->delete('available_doctor_days');
        foreach($data as $value)
            $this->db->insert('available_doctor_days', array('day_id'=>$value, 'user_id'=> $id));

        return TRUE; 
    }

    function deleteAcademics($id)
    {
        $this->db->where('user_id', $id);
        return $this->db->delete('doctor_academics');  
         
    }

    function deleteExperience($id)
    {
        $this->db->where('user_id', $id);
        return $this->db->delete('doctor_experience');
    }

    function updateDoctorTiming($data, $id)
    {
        $this->db->where('user_id', $id);
        $this->db->delete('timing');

        foreach($data as $value)
            $this->db->insert('timing', array('start_time'=> $value['start_time'], 'end_time' => $value['end_time'], 'user_id' => $id));

        return TRUE; 
    } 

    function updateAppointment($data, $id){
        
        $this->db->where('id', $id);
        return $this->db->update('appointments', $data);    
    }

    function sendTimingsChangedEmail($id, $oldTimings, $oldDays)
    {
        if(!$id)
            return NULL;
        $appointments = $this->readAllScheduledAppointments($id);
        if($appointments)
        {
            $content = $this->generateRescheduleMessage($id, $oldTimings, $oldDays);
            $this->load->model('admin_model');
            foreach ($appointments as $key => $value) 
            {
                $patient = $this->admin_model->readUserDetails($value->p_id);
                $data = array(
                    'to_email' => $patient->email, 
                    'subject' => 'Reschedule your appointment',
                    'content' => $content,
                    'hasButton' => FALSE,
                    'username' => ucfirst($patient->fname)
                );
                send_email($data);
            }
        }
    }

    function generateRescheduleMessage($id, $oldTimings, $oldDays)
    {
        $this->load->model('admin_model');
        $newDays = $this->admin_model->readDoctorAvailableDays($id);
        $newTimings = $this->admin_model->readDoctorTimings($id);
        $doctor = $this->admin_model->readDoctorDetails($id);
        
        $content = 'Please note that Doctor ' . ucfirst($doctor->fname) . ' has changed his appointment availability.';    
        $content .= '<br /><br /><strong>Previous Appointment Availability :</strong><br />Days : ';
        $count = 1;
        foreach($oldDays as $row)
        {
            $content .= ucfirst($row);
            if(count($oldDays) != $count)
            {
                $content .= ', ';
                $count++;
            }
            else
                $content .= '.';    
        }
        
        $content .= '<br />Timings : ';
        $count = 1;
        foreach($oldTimings as $row)
        {
            $content .=  date('g:i a', strtotime($row->start_time)) . ' - ' .  date('g:i a', strtotime($row->end_time));
            if(count($oldTimings) != $count)
            {
                $content .= ', ';
                $count++;
            }
            else
                $content .= '.';    
        }

        $content .= '<br /><br /><strong>New Appointment Availability</strong> (applicable from now) :<br />Days : ';
        $count = 1;
        foreach($newDays as $row)
        {
            $content .= ucfirst($row);
            if(count($newDays) != $count)
            {
                $content .= ', ';
                $count++;
            }
            else
                $content .= '.';    
        }
        
        $content .= '<br />Timings : ';
        $count = 1;
        foreach($newTimings as $row)
        {
            $content .=  date('g:i a', strtotime($row->start_time)) . ' - ' .  date('g:i a', strtotime($row->end_time));
            if(count($newTimings) != $count)
            {
                $content .= ', ';
                $count++;
            }
            else
                $content .= '.';    
        }
        $content .= '<br /><br />Please reschedule your appointment with him if these changes have affected your appointment else ignore this email.';
        return $content;
    }

    function getUserById($id)
    {
        if(!$id)
            return NULL;

        $this->db->where('id', $id);
        $query = $this->db->get('users');
        $result = $query->row();

        return $result ? $result : NULL;
    }

    /**
     * Doctor book patient Appointment
     * @param type $doctorId
     * @param type $appointment_id
     * @return boolean
     * 
     */
    
    function DoctorBookAppointment($doctorId, $appointment_id)
    {
        $time = new DateTime();
        $time->modify('-60 mins');
        
        $condition = 'd_id = '. $doctorId . ' AND meeting_approval = 0 AND id = '.$appointment_id  ;
        $this->db->select('*')->from('appointment_now');
        $this->db->where($condition);
        $this->db->order_by('created_at', 'DESC');
        
        if($timeCheck)
            $this->db->where('created_at >=', $time->format('Y-m-d g:i:s'));
        
        $query = $this->db->get();
        
        if ($query->num_rows() == 1)
        { // check Appointment not book by other doctor
            $result = $query->result();
            $this->db->select('*')->from('appointment_now');
            $condition = 'p_id = '. $result[0]->p_id . ' AND meeting_approval='. 1 ;
            $this->db->where($condition);
            $this->db->where('created_at', $result[0]->created_at);
            $query = $this->db->get();
            
            if ($query->num_rows() >= 1){
                return false;       
            }
            return $result; 
            
        }else{
            return false;
        }
    }

    function DoctorSignupEmailSendToAdmin($data){

        $doctor_name = ucfirst($data['fname'].' '.$data['sname']);
        $domain = $this->config->item('email_domain');
        $from_email = $this->config->item('site_email');
        $to_email = $this->config->item('site_email');
        
        $subject = 'New Doctor joined';
        $message = 'Hi admin,<br/>';
        $message .= 'Dr. '.$doctor_name.' has joined our team.<br/>';
        $message .= 'Thanks,<br/>Seeadoctor Admin';
        
       //configure email settings
        $config = $this->config->item('email_settings');
        $this->email->initialize($config);
        
        //send mail
        $this->email->from($from_email, $domain);
        $this->email->to($to_email);
        $this->email->subject($subject);
        $this->email->message($message);
        return $this->email->send();
    }
}
