<?php
class Home_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function getUserCount($role = 0)
    {
        $this->db->where('role', $role);
        $this->db->where('status', 1);

        $query = $this->db->get('users');
        $result = $query->num_rows();

        return $result ? $result : 0;
    }

     function getServedPatientCount()
    {
        $query = $this->db->get('payments');
        $result = $query->num_rows();

        return $result ? $result : 0;
    }

    
    function getBestDoctors()
    {
        $this->db->where('users.status', 1);
        $this->db->join('doctor_cv', 'users.id = doctor_cv.user_id');
        $this->db->order_by('rating', 'DESC');
        $this->db->order_by('no_of_patient', 'DESC');
        $this->db->limit(9);
        
        $query = $this->db->get('users');
        $result = $query->result();
        

        foreach($result as $row)
        {
            $row->profile_pic = get_image_url($row->profile_pic);
        }
        return $result ? $result : NULL;
    }
}