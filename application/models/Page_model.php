<?php
class Page_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_content($type)
    {
        if(!$type)
            return NULL;

        $this->db->where('type', $type);

        $query = $this->db->get('pages');
        $result = $query->row();

        return $result ? $result : NULL;
    }

    function save_content($data)
    {
        if(!$data)
            return NULL;

        $record = $this->get_content($data['type']);
        if($record)
        {
            $this->db->where('id', $record->id);
            return $this->db->update('pages', $data);  
        }

        return $this->db->insert('pages', $data);
    }
}