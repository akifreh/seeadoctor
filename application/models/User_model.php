<?php
class User_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    //insert into user table
    function insertUser($data, $data1 = NULL)
    {
        $this->db->insert('users', $data);
        $user_id = $this->db->insert_id(); // Getting last inserted id 
        if($data1)
        {
            $data1['user_id'] = $user_id;
            $this->db->insert('user_registration', $data1);
        }

       return TRUE; 
    }
    
    function readAlreadyExistUser($userEmail)
    {    
        $condition = "email=" . "'" .$userEmail. "'";
        $this->db->select('*')
          ->from('users');
        $this->db->where($condition);
        $query = $this->db->get();

        if ($query->num_rows() == 1)
            return true;
        else
            return false;
    }
            
    function updateUser($data)
    {   
        
        $this->db->where('id',$data['id']);
        $this->db->update('users', $data);
        //$this->db->update('card_info', $data1);
    }

    function updateCardInfo($data1, $id)
    {      
        $this->db->where('user_id', $id);
        $query = $this->db->get('user_registration');
        $result = $query->row();
        if($result)
        {
            $this->db->where('id', $result->id);
            return $this->db->update('user_registration', $data1);
        }
        else
        {
            $data1['user_id'] = $id;
            return $this->db->insert('user_registration', $data1);
        }
        
    }
    
    function checkUserPass($emailPass)
    {
        $encPass = md5($emailPass['password']);
        $condition = "Email =" . "'" . $emailPass['email'] . "' AND " . "Password =" . "'" . $encPass . "'";
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1)
            return true;
        else
            return false;
    }
   
    function checkPassword($password, $id)
    {
        $encPass = md5($password);
        $this->db->from('users');
        $this->db->where('password', $encPass);
        $this->db->where('id', $id);
        $query = $this->db->get();

        if ($query->num_rows() == 1)
            return true;
        else
            return false;
    }

    //send verification email to user's email id
    function sendEmail($to_email)
    {   
        $data = array(
            'to_email' => $to_email, 
            'subject' => 'Verify Email Address',
            'content' => 'Please click on the below activation link to verify your email address.',
            'hasButton' => TRUE,
            'buttonLabel' => 'Activate Account',
            'buttonLink' => base_url() .'home/verify/' . md5($to_email),
            'username' => 'User'
        );
        send_email($data);
        return TRUE;
    }
    
    //activate user account
    function verifyEmailID($key)
    {
        $data = array('status' => 1);
        $this->db->where('md5(email)', $key);
        return $this->db->update('users', $data);
    }

    function getUserByHashEmail($key)
    {
        $this->db->where('md5(email)', $key);
        $query = $this->db->get('users');
        $result = $query->row();
        return $result ? $result : NULL;
    }
    
    function loginUser($data)
    {
        $encPass = md5($data['password']);
       
        $condition = "Email =" . "'" . $data['email'] . "' AND " . "Password =" . "'" . $encPass . "'" . " AND " . 'status =' .'1'. " AND " . 'role =' .'2';
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1)
            return true;
        else
            return false;
    }
    
    function loginDoctor($data)
    {
        $encPass = md5($data['password']);
       
        $condition = "Email =" . "'" . $data['email'] . "' AND " . "Password =" . "'" . $encPass . "'" . " AND " . 'status =' .'1'. " AND " . 'role =' .'3';
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1)
            return true;
        else
            return false;
    }
    
    function readDoctorSession($session) //, $id)
    {
        $condition = 'session= ' . "'".$session . "'"; 
        $this->db->select('*');
        $this->db->from('doctor_online');
        $this->db->where($condition);
       // $this->db->or_where('user_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() >= 1)
            return true;
        else
            return false;
    }
    
    function insertDoctorSession($data)
    {
      $this->db->insert('doctor_online', $data);  
    }
    
    function updateDoctorSession($data)
    {
        $this->db->where('user_id',$data['user_id']);
        return $this->db->update('doctor_online', $data);   
    }
    
    function deleteDoctorSession($id)
    {
        //$condition = "time <"."'".$time_check."'";
        $this->db->where('user_id', $id);
        $this->db->delete('doctor_online'); 
    }
    
    function readSessionInfo($userId)
    {
        $condition = 'user_id='. $userId;
        $this->db->select('*');
        $this->db->from('doctor_online');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1)
            return $query->row();
        else
            return false;
    }

    public function read_user_information($email) 
    {
        $condition = "Email =" . "'" . $email . "'";
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1)
            return $query->result();
        else
            return false;
    }
        
    public function getAdmin($data)
    {
        $encPass = md5($data['password']);
            
        $condition = "Email =" . "'" . $data['email']. "' AND " ."password ='" .$encPass . "' AND ". 'role =' .'1';
         
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();
        
        if ($query->num_rows() == 1)
            return $query->result();
        else
            return false;    
    }
        
    public function readUserCardInfo($id)
    {
        $condition = "user_id =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('card_info');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();
        
        if ($query->num_rows() == 1)
            return $query->result();
        else
            return false;
    }
        
    public function insertProfile($data)
    {        
        $this->db->insert('other_profile', $data);
//      $user_id = $this->db->insert_id(); // Getting last inserted id 
//      $data1['user_id'] = $user_id;       
    }

    public function readCardInfo($id)
    {
        $condition = "user_id =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('user_registration');
        $this->db->where($condition);
        $query = $this->db->get();
    
        $result = $query->row();
        return $result ? $result : NULL;
    }

    public function readOtherProfile($id)
    {
        $condition = "user_id =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('other_profile');
        $this->db->where($condition);
        $query = $this->db->get();

        if ($query->num_rows() >= 1)           
            return $query->result();
        else
            return false;
    }
        
    public function readOtherProfileDetails($id)
    {
        $condition = "id ="."'".$id."'";     
        $this->db->select('*');
        $this->db->from('other_profile');
        $this->db->where($condition);
        $query = $this->db->get();

        if ($query->num_rows() == 1)
            return $query->result();
        else
            return false;
    }
    
    public function deleteProfile($id)
    {
        $condition = "id ="."'".$id."'"; 
        $this->db->where('id', $id);
        $this->db->delete('other_profile');
    }
        
    public function updateOtherProfile($data)
    {
        $this->db->where('id',$data['id']);
        return $this->db->update('other_profile', $data);           
    }

    function checkUserByEmail($email)
    {
        if(!$email)
            return NULL;
        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('email', $email);
        $query = $this->db->get();
        $result = $query->row();
        if ($result)
            return $result->id;
        else
            return false;
    }

    function insertToken($id, $email)
    {
        if(!$id)
            return NULL;
        $data['token'] = md5($email);
        $data['user_id'] = $id;
        $this->db->insert('forget_password', $data);
        return $data['token'];
    }

    //send verification email to user's email id
    function sendForgetEmail($to_email, $token)
    {
        $data = array(
            'to_email' => $to_email, 
            'subject' => 'Retreive Password',
            'content' => 'Please click below button to enter new password.',
            'hasButton' => TRUE,
            'buttonLabel' => 'Reset Password',
            'buttonLink' => base_url() .'user/newPassword/' . $token,
            'username' => 'User'
        );
        send_email($data);
    }

    function getTokenDetails($token)
    {
        if(!$token)
            return NULL;

        $this->db->select('*');
        $this->db->from('forget_password');
        $this->db->where('token', $token);
        $query = $this->db->get();
        $result = $query->row();

        return $result ? $result : NULL;
    }

    function changePassword($password, $id)
    {
        if(!$password || !$id)
            return NULL;
        $data['password'] = md5($password);
        $this->db->where('id', $id);
        if($this->db->update('users', $data))
        {
            $this->db->where('user_id', $id);
            $this->db->delete('forget_password');
        }

        return TRUE; 
    }

    function getUserById($id)
    {
        if(!$id)
            return Null;

        $this->db->where('id', $id);
        $query = $this->db->get('users');
        $result = $query->row();

        return $result ? $result : NULL;

    }

    function getDoctorRatingForUser($d_id, $p_id)
    {
        if(!$d_id || !$p_id)
            return Null;

        $this->db->select('rating AS Val');
        $this->db->where('p_id', $p_id);
        $this->db->where('d_id', $d_id);

        $query = $this->db->get('doctor_rating');
        $result = $query->row();

        return $result ? $result->Val : 0;

    }

    function saveDoctorRating($data)
    {
        if(!$data)
            return NULL;

        $this->db->where('p_id', $data['p_id']);
        $this->db->where('d_id', $data['d_id']);
        $query = $this->db->get('doctor_rating');
        $result = $query->row();
        
        if($result)
        {
            $this->db->where('id',$result->id);
            return $this->db->update('doctor_rating', $data);
        }
        else
        {
            return $this->db->insert('doctor_rating', $data);
        }     
    }

    function updateDoctorRating($doctorId)
    {
        if(!$doctorId)
            return NULL;

        $this->db->select_avg('rating');
        $this->db->where('d_id', $doctorId);
        $query = $this->db->get('doctor_rating');
        $result = $query->row();
        
        $data['rating'] = round($result->rating);
        $this->db->where('user_id',$doctorId);
        return $this->db->update('doctor_cv', $data);
    }

    function saveUserFeedback($data)
    {
        $this->db->where('p_id', $data['p_id']);
        $this->db->where('appointment_id', $data['appointment_id']);
        $query = $this->db->get('user_feedback');
        $result = $query->row();
        
        if($result)
        {
            $this->db->where('id',$result->id);
            return $this->db->update('user_feedback', $data);
        }
        else
        {
            return $this->db->insert('user_feedback', $data);
        }     
    }
    
    function sendCommentsToAdmin($record)
    {   
        $user = $this->getUserById($record['p_id']);
        $data = array(
            'to_email' => $this->config->item('site_email'), 
            'subject' => 'User Feedback',
            'content' => '<a href="mailto:' . $user->email . '">'. ucfirst($user->fname) . ' ' . ucfirst($user->sname) .'</a> gave SAD system a '. $record['system_rating'] . ' star rating.<br /><br />These are his comments:<br /><br />' . formulate_multiline_text($record['comments']) ,
            'hasButton' => FALSE,
            'username' => 'Admin'
        );
        send_email($data);
        return TRUE;
    }
}