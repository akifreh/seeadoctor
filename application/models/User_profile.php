<?php
class User_profile extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    
    function insertUserPersonalInfo($data){
      $this->db->insert('user_profile', $data);
        $user_id = $this->db->insert_id(); // Getting last inserted id 
         
    }
    
    function insertAllergies($data){
       
       $this->db->insert('allergies', $data);
        $user_id = $this->db->insert_id(); // Getting last inserted id   
    }
    
    function insertPreExistingCondition($data){
       $this->db->insert('pre_existing_condition', $data);
        $user_id = $this->db->insert_id(); // Getting last inserted id      
    }
    
    function insertPersonalUpdates($data){
        
       $this->db->insert('personal_update', $data);
        $user_id = $this->db->insert_id(); // Getting last inserted id   
        
    }
    
    function insertFamilyHistory($data){
      $this->db->insert('family_history', $data);
        $user_id = $this->db->insert_id(); // Getting last inserted id     
    }
    
    function insertMyDoctor($data){
       $this->db->insert('my_doctor', $data);
        $user_id = $this->db->insert_id(); // Getting last inserted id    
    }
    
    /*
    Check user profile exist or not
     */
    function checkUserProfileExist($id){
        $condition = "user_id ="."'".$id."'";
         $this->db->select('*');
            $this->db->from('user_profile');
            $this->db->where($condition);
        $query = $this->db->get();

            if ($query->num_rows() == 1) {
               
            return $query->result();
            } else {
            return 0;
            }
    }
    
    function readAllergies($id){
        $condition = "user_id ="."'".$id."'";
        $this->db->select('*');
            $this->db->from('allergies');
            $this->db->where($condition);
        $query = $this->db->get();

            if ($query->num_rows() >= 1) {
               
            return $query->result();
            } else {
            return false;
            }
    }
    
    function readPreExistingCondition($id){
        $condition = "user_id ="."'".$id."'";
        $this->db->select('*');
            $this->db->from('pre_existing_condition');
            $this->db->where($condition);
        $query = $this->db->get();

            if ($query->num_rows() >= 1) {
               
            return $query->result();
            } else {
            return false;
            }
    }
    
    function readPersonalUpdate($id){
        $condition = "user_id ="."'".$id."'";
        $this->db->select('*');
            $this->db->from('personal_update');
            $this->db->where($condition);
        $query = $this->db->get();

            if ($query->num_rows() >= 1) {
               
            return $query->result();
            } else {
            return false;
            } 
    }
    
    function readFamilyHistory($id){
       $condition = "user_id ="."'".$id."'";
        $this->db->select('*');
            $this->db->from('family_history');
            $this->db->where($condition);
        $query = $this->db->get();

            if ($query->num_rows() >= 1) {
               
            return $query->result();
            } else {
            return false;
            }  
    }
    
    function readMyDoctor($id){
       $condition = "user_id ="."'".$id."'";
        $this->db->select('*');
            $this->db->from('my_doctor');
            $this->db->where($condition);
        $query = $this->db->get();

            if ($query->num_rows() >= 1) {
               
            return $query->result();
            } else {
            return false;
            }    
    }
    
    function updateAllergies($id,$data){
       
      $this->db->where('id',$id);
      return $this->db->update('allergies', $data);  
    }
    
    function updatePreExistingCondition($id,$data){
     $this->db->where('id',$id);
      return $this->db->update('pre_existing_condition', $data);     
    }
    
    function updatePersonalUpdate($id,$data){
      $this->db->where('id',$id);
      return $this->db->update('personal_update', $data);   
        
    }
    
    function updateFamilyHistory($id,$data){
      $this->db->where('id',$id);
      return $this->db->update('family_history', $data);    
    }
    
    function updateMyDoctor($id,$data){
      $this->db->where('id',$id);
      return $this->db->update('my_doctor', $data);      
    }
    
    function deleteMyAllergy($id){
       
        $this->db->where('id',$id);
         $this->db->delete('allergies');
    }
    
    function deletePec($id){
       $this->db->where('id',$id);
         $this->db->delete('pre_existing_condition');  
    }
    
    function deletePu($id){
      $this->db->where('id',$id);
         $this->db->delete('personal_update');   
    }
    
     function deleteFh($id){
      $this->db->where('id',$id);
         $this->db->delete('family_history');   
    }
    
    function deleteMd($id){
      $this->db->where('id',$id);
         $this->db->delete('my_doctor');   
    }
    
    function readUserInfo($id){
       $condition = 'id='.$id; 
       
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
         $query = $this->db->get();

            if ($query->num_rows() == 1) 
            {
              $result = $query->result();
              foreach($result as $row)
              {
                  $row->profile_pic = get_image_url($row->profile_pic);
              }
              return $result;
            } 
            else 
            {
              return false;
            }
    }
            
    function readUserProfiles($id)    {
        $condition = 'user_id='.$id; 
       
        $this->db->select('*');
        $this->db->from('user_profile');
        $this->db->where($condition);
         $query = $this->db->get();

            if ($query->num_rows() == 1) {
               
            return $query->result();
            } else {
            return false;
            }
       }

    function updateUserProfileInfo($id,$data){
        
      $this->db->where('user_id', $id);
      $query = $this->db->get('user_profile');
      if ($query->num_rows() == 1) 
      {    
         return $this->db->update('user_profile', $data,array('user_id' => $id));  
        
      }
      else
      {
        $data['user_id'] = $id;
        return $this->db->insert('user_profile', $data);
      }  
    }
    
    function readPreviousHistory($id)
    {
       $currentDate = date('Y-m-d H:i:s');
        $condition = 'p_id='. $id. ' AND start_time < \''. $currentDate.'\'';
        $this->db->select('*, app.id AS appointment_id')->from('appointments as app')
        ->join('users u', 'u.id=app.d_id')->join('prescription p', 'p.appointment_id=app.id');
        $this->db->where($condition);
        $this->db->where('meeting_approval', 3);
        $this->db->order_by('start_time', 'DESC');
        $query = $this->db->get();
        $result = $query->result();
        
        return $result ? $result : NULL; 
    }
    
    
}