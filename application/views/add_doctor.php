<div class="col-lg-4 col-md-4">
		<h1>Sign Up Now</h1>
		<h3>Fill in the required information</h3>
		<div class="signup-form">
                     <?php $attributes = array("name" => "registrationform");
                echo form_open("home/register", $attributes);?>
			<div class="row">
				<div class="col-lg-6 col-md-6">
				<input name="fname" type="text" class="form-control" placeholder="First name">
				</div>
				<div class="col-lg-6 col-md-6">
                                    <input name="sname" type="text" class="form-control" placeholder="Surname">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6">
                                    <input id="email" name="email" type="text" class="form-control" placeholder="Email address">
				</div>
                            <div class="col-lg-6 col-md-6">
                                  <input id="confemail" name="emailConfirm" type="text" class="form-control" placeholder="Re-enter Email"  ">
				</div>
			</div>
			 
                    <script type="text/javascript">
                    $('#registrationform').submit(function(){
                        var email = document.getElementById("email").value
                        var confemail = document.getElementById("confemail").value
                        if(email != confemail) {
                            alert('Email Not Matching!');return false;
                        }
                    })
                    function confirmEmail() {
                        var email = document.getElementById("email").value
                        var confemail = document.getElementById("confemail").value
                        if(email != confemail) {
                            alert('Email Not Matching!');
                        }
                    }
</script>

                    
                        <div class="row">
				<div class="col-lg-12 col-md-12">
                                    <input name="password" type="password" class="form-control" placeholder="Password">
				</div>
				
			</div>
                    
                    
                        <div class="row">
				<div class="col-lg-12 col-md-12">
				<input name="phone" type="text" class="form-control" placeholder="Phone number">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12">
                                    <input name="cc_number" type="text" class="form-control" placeholder="Credit card number">
				</div>
				 
			</div>
			<div class="row">
			<div class="col-lg-12 col-md-12">
				Credit card expiry
				</div></div>
			<div class="row">
				<div class="col-lg-9 col-md-9 expiry">
                                    <input type="hidden" name="ex_dd" value="01"/>
                                    <!--select name="ex_dd" class="form-control" id="select">
                                       <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select-->
                                    <select name="ex_mm" class="form-control" id="select">
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                    <select name="ex_yy" class="form-control" id="select">
                                       <?php
                                    for($i=date('Y')+5; $i>1899; $i--) {
                                        $selected = '';
                                        if ($birthdayYear == $i) $selected = ' selected="selected"';
                                        print('<option value="'.$i.'"'.$selected.'>'.$i.'</option>'."\n");
                                    }
                                    ?>
                                    </select>
				</div>
				<div class="col-lg-3 col-md-3">
                                    <input name="cc_csv_number" type="text" class="form-control" placeholder="CCV">
				</div>
			</div>
			
			<div class="row">
			<div class="col-lg-12 col-md-12">
				Birthday
				</div>
			<div class="col-lg-12 col-md-12">
                                    <select name="b_dd" class="form-control" id="select">
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                                <select name="b_mm" class="form-control" id="select">
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                <select name="b_yy" class="form-control" id="select">
                                     <?php
                                    for($i=date('Y'); $i>1899; $i--) {
                                        $selected = '';
                                        if ($birthdayYear == $i) $selected = ' selected="selected"';
                                        print('<option value="'.$i.'"'.$selected.'>'.$i.'</option>'."\n");
                                    }
                                    ?>
                                </select>
			</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12">
                                    <input type="hidden" class="form-control" name="gender" id="fieldGender" value="male" >
 
                         
                                    <a href="javascript:void(0)" id="signupMale" class="male active"> Male</a>
                                    <a href="javascript:void(0)" id="signupFemale" class="female">Female</a>
			  </div> 
			  </div>
			  <div class="row">
				<div class="col-lg-12 col-md-12">
                                    <button name="submit" type="submit" class="btn btn-default submit-button">Submit</button>
			  </div> 
			  </div>
			
                    <?php echo form_close(); ?>
                <?php echo $this->session->flashdata('msg'); ?>
		</div>
                
	</div>