<script src="<?php echo base_url() . 'js/ckeditor/ckeditor.js';?>"></script>
<div class="add-prescription">
<div class="container">
<div class="col-lg-12 col-md-12">
	<h1>Prescription</h1>
	<?php if(isset($message)): ?>
		<div class="alert alert-success text-center"><?php echo $message; ?></div>
	<?php endif; ?>
	<?php if(isset($error)) : ?>
		<div class="alert alert-danger text-center"><?php echo $error; ?></div>
	<?php endif; ?><br />
	<p>Please write a prescription for the patient</p>
	<div class="signup-form">
    <?php $attributes = array("name" => "registrationform");
        echo form_open("call/addPrescription");?>
		<div class="row">
			<input type="hidden" name="appointment_id" value="<?php echo $appointment_id; ?>">
			<input type="hidden" name="session_key" value="<?php echo $session_key; ?>">
			<div class="col-md-12 field-box">
			<label>Comments:</label>
          	<textarea class="form-control" rows="4" id="comments" name="comments" ></textarea>
			</div>
			<div class="col-md-12 field-box">
			<label>Prescription:</label>
          	<textarea class="form-control" id="content" name="pres_detail" ></textarea>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12">
                <button name="submit" type="submit" class="btn btn-default submit-button">Submit</button>
		  	</div> 
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
</div>
</div>

<script type="text/javascript">
	CKEDITOR.replace( 'content' );
</script>