<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin dashboard</title>
    <link href="<?php echo base_url("/css/bootstrap.css"); ?>" rel="stylesheet" type="text/css" />
</head>
<body>
    <?php echo $this->session->flashdata('msg_suspend');?>
	<div class="dashboard-section">
		<div class="container">
			<div class="row">
			<div class="col-lg-12 col-md-12" >
			<h2>Admin Dashboard</h2>
			</div>
                            
		<div class="col-lg-6 col-md-6" >
      <h3>Patients list</h3>
			<div class="medical-history-table table-design">
				<div class="table-header">
					<div class="table-head-col col-sm-2">Name</div>
					<div class="table-head-col col-sm-4">Email</div>
					<div class="table-head-col col-sm-2">Gender</div>
					<div class="table-head-col col-sm-4">Details</div>
		    </div>
				<div class="table-body">
                                   
        <?php 
            
            if($userslist) :
            foreach ($userslist as $row) :?>
                                             <div class="table-row ">
                                                    <div class="table-col col-sm-2"><?php echo $row->fname; ?></div>
                                                    <div class="table-col col-sm-4"><?php echo $row->email; ?></div>
                                                    <div class="table-col col-sm-2"><?php echo ucfirst($row->gender); ?></div>
                                                    
                <div class="table-col col-sm-4">
                    
                    
                    <a href="<?php echo base_url() .'admin/viewUserDetail/'. $row->id; ?>">View</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    
                    <?php if($row->status == 1){ ?>
                    <a href="<?php echo base_url() .'admin/usersuspend/'. $row->id; ?>" class="btn btn-sm btn-danger" >Suspend</a>
                    <?php } ?>
                    
                    <?php if($row->status == 3){ ?>
                    <a href="<?php echo base_url() .'admin/useractive/'. $row->id; ?>" class="btn btn-sm btn-success" >Active</a>
                    <?php } ?>
                    
                    
                </div>
              </div>
            <?php  endforeach;
            endif; ?>
					
					 
				</div>
			 
			 </div>
			
			</div>	
    <div class="clearfix on-tab-mobile"></div>
    <div class="col-lg-6 col-md-6" >
    <h3 class="doctor-list">Doctors list </h3>
    <a href="" data-toggle="modal" data-target=".add-doctor" class="btn btn-default add-doctor-btn submit-button">Add a Doctor</a>
       
			<div class="medical-history-table table-design">
				<div class="table-header">
					<div class="table-head-col col-sm-2">Name</div>
					<div class="table-head-col col-sm-4">Email</div>
					<div class="table-head-col col-sm-2">Gender</div>
                                        <div class="table-head-col col-sm-4">Details</div>
				</div>
				<div class="table-body">
                                  
                                    
                                    <?php if($doctorProfile) : 
                                    foreach ($doctorProfile as $row) :?>
					 <div class="table-row  ">
						<div class="table-col col-sm-2"><?php echo ucfirst($row->fname); ?></div>
						<div class="table-col col-sm-4"><?php echo $row->email; ?></div>
						<div class="table-col col-sm-2"><?php echo ucfirst($row->gender); ?></div>

            <div class="table-col col-sm-4">
                <a href="<?php echo base_url(). 'admin/viewDoctorDetail/'.$row->user_id; ?>">View</a>&nbsp;&nbsp;&nbsp;&nbsp;
            
                
                    <?php if($row->status == 1){ ?>
                    <a href="<?php echo base_url() .'admin/usersuspend/'. $row->user_id; ?>" class="btn btn-sm btn-danger" >Suspend</a>
                    <?php } ?>
                    
                    <?php if($row->status == 3){ ?>
                    <a href="<?php echo base_url() .'admin/useractive/'. $row->user_id; ?>" class="btn btn-sm btn-success" >Active</a>
                    <?php } ?>
            
            </div>
					</div>
                                    <?php  endforeach; 
                                    endif; ?>
					
					 
				</div>
			 
			 </div>
			
			</div>
 			
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12" >
					<hr class="dashboard-hr">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6" >
                                    <h3>Doctor's Rates</h3>
			<div class="medical-history-table dashboard-table table-design">
				<div class="table-header">
					<div class="table-head-col col-md-4 col-xs-4 col-sm-4">Doctor</div>
					<div class="table-head-col col-md-4 col-xs-4 col-sm-4">Charges /15 min</div>
					<div class="table-head-col col-md-4 col-xs-4 col-sm-4 ">Edit</div>
					 
				</div>
                            <script type="text/javascript">
                                function abc(x){
                                  // alert("hello hi"+x);
                                          $("#rateid").val(x);
                                }
                            </script>
				<div class="table-body">
                                    <?php if($doctorRates) :
                                     foreach ($doctorRates as $row2) : ?>
					<div class="table-row">
						<div class="table-col col-md-4 col-xs-4 col-sm-4"><?php echo $row2->doctor_type; ?></div>
						<div class="table-col col-md-4 col-xs-4 col-sm-4">R <?php echo $row2->rates; ?></div>
                                                <div class="table-col col-md-4 col-xs-4 col-sm-4 table-head-col-edit"><a href="" data-id="<?php echo $row2->id; ?>" onclick="abc($(this).attr('data-id'))" data-toggle="modal" data-target=".edit-doctor-rates" class="btn btn-default submit-button">Edit</a></div>
						 
					</div>
                                    <?php endforeach; 
                                    endif; ?>
					
					
					 
				</div>
			 
			 </div>
			
			</div>
      
        <!-- NEW SECTION FOR STATIC PAGES LIKE FAQS TERMS AND POLICY -->
      <div class="col-lg-6 col-md-6" >
      <h3>Pages</h3>
      <div class="medical-history-table dashboard-table table-design">
        <div class="table-header">
          <div class="table-head-col col-md-4 col-xs-4 col-sm-4">Name</div>
          <div class="table-head-col col-md-4 col-xs-4 col-sm-4">View</div>
          <div class="table-head-col col-md-4 col-xs-4 col-sm-4">Edit</div>
           
        </div>
        <div class="table-body">
        <?php if($pages) : 
        foreach ($pages as $key => $row2) : ?>
          <div class="table-row">
            <div class="table-col col-md-4 col-xs-4 col-sm-4"><?php echo $row2; ?></div>
            <div class="table-col col-md-4 col-xs-4 col-sm-4 table-head-col-edit"><a target="_blank" href="<?php echo ($key != 'fees') ? base_url() . 'page/show/' . $key : base_url() . '#our-fees';?>" class="btn btn-default submit-button">Show</a></div>
            <div class="table-col col-md-4 col-xs-4 col-sm-4 table-head-col-edit"><a target="_blank" href="<?php echo base_url() . 'page/save/' . $key;?>" class="btn btn-default submit-button">Edit</a></div>

          </div>
                                    <?php endforeach;
                                    endif; ?>
          
          
           
        </div>
       
       </div>
      
      </div>
      </div>
      </div>
		</div>
	</div>
	
    
     <!-- SEE A MEDICAL DOCTOR -->
<!--<script type="text/javascript">
         var input = document.createElement("input");

        input.setAttribute("type", "hidden");

        input.setAttribute("name", "id");

        input.setAttribute("value", "<?php //echo $row->id ?>");
        document.getElementById("rateform").appendChild(input);
//append to form element that you want .
</script>-->


     <div class="modal edit-doctor-rates sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
			<h2>Edit Rates</h2>
			<div class="appointment-form">
                            <?php $attributes = array("name" => "rateform", "id"=>"rateform");
                                echo form_open("admin/doctorRates", $attributes);?>
                            
    <div class="form-group">
        
        <input type="hidden" name="rateid" id="rateid" value="">
        <input style="color: #000;" type="text" name="rates">
    </div>
                            
                            <button onclick="form_submit()" type="submit" class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".request-a-doctor" > Submit </button>
		</div>
                        
	<?php echo form_close(); ?>
                <?php echo $this->session->flashdata('msg'); ?>	  
         
     <script type="text/javascript">
            function form_submit() {
              document.getElementById("rateform").submit();
             }    
    </script>
                        
      </div>
          
      
         
      
    </div>
  </div>
</div></div> 
     
     
     
     
     
     
     
       
         <!-- add a doctor modal --> 
          <div class="modal add-doctor sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">  
          
          <div class="schedule-appointment">
			<h2>Add a Doctor</h2>
			<div class="appointment-form">
                            <?php $attributes = array("name" => "add-doctor", "id"=>"add-doctor");
                                echo form_open("admin/addDoctor", $attributes);?>
                            
    <div class="form-group">
        
        
            <input name="fname" type="text" class="form-control" placeholder="First name"></div>
                            <div class="form-group">
            <input name="sname" type="text" class="form-control" placeholder="Surname"></div><div class="form-group">
            <input id="email" name="email" type="text" class="form-control" placeholder="Email address"></div><div class="form-group">
             <input name="password" type="password" class="form-control" placeholder="Password"></div><div class="form-group">
             <input name="phone" type="text" class="form-control" placeholder="Phone number"></div><div class="form-group">
             
             <input type="radio" name="gender" value="male" checked>Male
            <input type="radio" name="gender" value="female">Female
</div><div class="form-group">
             <input name="degree" type="text" class="form-control" placeholder="Degree"></div><div class="form-group">
           
             <textarea style="color:#000;" name="experience" placeholder="Experience"></textarea></div><div class="form-group">
           <textarea style="color:#000;" name="speciality" placeholder="Speciality"></textarea></div><div class="form-group">
           <select style="color:#000;" name="doctor_type">
               <option value="Nutritionist">Nutritionist</option>
               <option value="Psychologist">psychologist</option>
               <option value="Medical Doctor">Medical Doctor</option>
           </select>
          </div><div class="form-group">
         <button onclick="add_doctor()" type="submit" class="sad-button "  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".add-doctor" > Submit </button>
         
            
    </div>
                            
                          
		</div>
	<?php echo form_close(); ?>
                         <?php //echo $this->session->flashdata('success'); ?>
                <?php echo $this->session->flashdata('msg'); ?>	  
         
     <script type="text/javascript">
            
              function add_doctor() {
              document.getElementById("add-doctor").submit();
             }
    </script>
                        
      </div>
         
         
               
    </div>
  </div>
</div></div> 
         
         
<script src="<?php echo base_url(); ?>js/admin_dashboard.js"></script>          
     
  </body>
</html>