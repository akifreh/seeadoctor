<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Appointment Lists</title>
    <link href="<?php echo base_url("/css/bootstrap.css"); ?>" rel="stylesheet" type="text/css" />
</head>
<body>
    
	<div class="dashboard-section">
		<div class="container">
			<div class="row">
			<div class="col-lg-12 col-md-12" >
			 <h2>Appointment Lists</h2>
			</div>
                            
		<div class="col-lg-12 col-md-12" >
      <h3>Upcoming Appointments</h3>
			<div class="medical-history-table table-design">
				<div class="table-header">
					<div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Patient</div>
					<div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Doctor</div>
          <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Field</div>
          <div class="table-head-col col-lg-1 col-md-1 col-sm-1 col-xs-1">Type</div>
					<div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Date</div>
					<div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Time</div>
          <div class="table-head-col col-lg-1 col-md-1 col-sm-1 col-xs-1">&nbsp;</div>
		    </div>
				<div class="table-body">
                                   
        <?php if($upcoming_appointments) : 
        foreach ($upcoming_appointments as $row) :?>
					 <div class="table-row  ">
						<div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $row->patientInfo->fname; ?></div>
						<div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $row->doctorInfo->fname; ?></div>
            <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo ucfirst($row->doctorInfo->type_of_doctor); ?></div>
            <div class="table-col col-lg-1 col-md-1 col-sm-1 col-xs-1"><?php echo ucfirst($row->type); ?></div>
						<div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo date('d M Y', strtotime($row->start_time)); ?></div>            
            <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo date('h:i a', strtotime($row->start_time)) . ' - ' . date('h:i a', strtotime($row->end_time)); ?></div>
            <div class="table-col col-lg-1 col-md-1 col-sm-1 col-xs-1"><a href="<?php echo base_url() . 'appointment/detail/'. $row->id; ?>">details</a></div>
          </div>
        <?php  endforeach; 
        endif; ?>
					
					 
				</div>
			 
			 </div>
			</div> 			
			</div> <!-- END OF DIV CLASS ROW -->

		<div class="row">                      
    <div class="col-lg-12 col-md-12" >
      <h3>Finished Appointments</h3>
      <div class="medical-history-table table-design">
        <div class="table-header">
          <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Patient</div>
          <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Doctor</div>
          <div class="table-head-col col-lg-1 col-md-1 col-sm-1 col-xs-1">Type</div>
          <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Date</div>
          <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Time</div>
          <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">User Location</div>
          <div class="table-head-col col-lg-1 col-md-1 col-sm-1 col-xs-1">&nbsp;</div>
        </div>
        <div class="table-body">
                                   
        <?php if($finished_appointments) : 
        foreach ($finished_appointments as $row) :?>
           <div class="table-row  ">
            <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $row->patientInfo->fname; ?></div>
            <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $row->doctorInfo->fname; ?></div>
            <div class="table-col col-lg-1 col-md-1 col-sm-1 col-xs-1"><?php echo ucfirst($row->type); ?></div>
            <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo date('d M Y', strtotime($row->start_time)); ?></div>            
            <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo date('h:i a', strtotime($row->start_time)) . ' - ' . date('h:i a', strtotime($row->end_time)); ?></div>           
            <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo isset($row->user_address) ? $row->user_address : '--'; ?></div>
            <div class="table-col col-lg-1 col-md-1 col-sm-1 col-xs-1"><a href="<?php echo base_url() . 'appointment/detail/'. $row->id; ?>">details</a></div>
          </div>
        <?php  endforeach; 
        endif; ?>
          
           
        </div>
       
       </div>
      </div>      
      </div> <!-- END OF DIV CLASS ROW -->

    <div class="row">
    <div class="col-lg-12 col-md-12" >
      <h3>Missed Appointments</h3>
      <div class="medical-history-table table-design">
        <div class="table-header">
          <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Patient</div>
          <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Doctor</div>
          <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Field</div>
          <div class="table-head-col col-lg-1 col-md-1 col-sm-1 col-xs-1">Type</div>
          <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Date</div>
          <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Time</div>
          <div class="table-head-col col-lg-1 col-md-1 col-sm-1 col-xs-1">&nbsp;</div>
        </div>
        <div class="table-body">
                                   
        <?php if($missed_appointments) :
         foreach ($missed_appointments as $row) :?>
           <div class="table-row  ">
            <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $row->patientInfo->fname; ?></div>
            <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $row->doctorInfo->fname; ?></div>
            <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo ucfirst($row->doctorInfo->type_of_doctor); ?></div>
            <div class="table-col col-lg-1 col-md-1 col-sm-1 col-xs-1"><?php echo ucfirst($row->type); ?></div>
            <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo date('d M Y', strtotime($row->start_time)); ?></div>            
            <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo date('h:i a', strtotime($row->start_time)) . ' - ' . date('h:i a', strtotime($row->end_time)); ?></div>            
            <div class="table-col col-lg-1 col-md-1 col-sm-1 col-xs-1"><a href="<?php echo base_url() . 'appointment/detail/'. $row->id; ?>">details</a></div>
          </div>
        <?php  endforeach; 
        endif; ?>
          
           
        </div>
       
       </div>
      </div>      
      </div> <!-- END OF DIV CLASS ROW -->

      </div>
	</div>