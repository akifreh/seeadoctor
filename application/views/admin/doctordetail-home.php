<div class="medical-pro-section">
<div class="container">
    <div class="row">

<?php $rating = isset($doctorrating[0]) ? $doctorrating[0]->rating : 0; ?> 
<?php $ratingDiff = 5 - $rating; ?>

<?php echo $this->session->flashdata('email_success'); ?>
<?php echo $this->session->flashdata('email_fail'); ?>

                    <div class="medical-history-detail">
                <div class="row">
                 <div class="col-lg-3 ">
                 <img src="<?php echo $row ? $row->profile_pic : ''?>"/>
             
            <div class="clearfix"></div>
                  <div> <h3><?php echo $row ? $row->fname . ' '. $row->sname : '';?> </h3></div>
                    <div><h4><?php echo $row ? ucfirst($row->type_of_doctor) : '';?></h4> </div>  
                    <div class="clearfix"></div> 
                    <div> <i class="stars-doc-rate  " style="margin-top:0px;">
                      <?php for($i= 0; $i< $rating; $i++) { ?> <i class="fa fa-star active" aria-hidden="true"></i> <?php } ?>
                      <?php for($j= 0; $j<$ratingDiff; $j++) { ?>  <i class="fa fa-star" aria-hidden="true"></i> <?php } ?>
                    </div>
                 </div>
                 
                  
                  
                   



              
                  <div class="col-lg-9">
                  <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Biography</h3>
                 	 <p><?php echo $row->description; ?></p>
               
                 <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Academic Qualifications</h3>
                 	 <div ><?php if($row->academics) : foreach($row->academics as $value) :?>
                     <div class="acadmic-info">
                 <h4> <?php echo $value->qualification ? $value->qualification : '';?></h4>
                  <div  class="institution-info"><?php echo $value->institution ? $value->institution : '';?></div>
                 <div  class="date-info"> <?php echo $value->year ? $value->year : '';?><br/></div>
                 </div>
                  <?php endforeach; endif; ?></div>
              
                 <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Working Experience</h3>
                 	 
                    <div ><?php if($row->experience) : foreach($row->experience as $value) :?>
                     <div class="experience-info">
                <h4>  <?php echo $value->job_title ? $value->job_title : '';?></h4>
				        <div class="company-info">  <?php echo $value->company ? $value->company : '';?></div>
                  <div class="date-info"><?php echo $value->start_date ? date('M Y', strtotime($value->start_date)) : '';?> - <?php echo $value->end_date ? date('M Y', strtotime($value->end_date)) . '<br/>' : '';?></div>
                  </div>
                  <?php endforeach; endif; ?></div>
                 </div>
                 </div>
                  

                    
                    </div>



</div>
    </div>
</div>
</div>