<div class="medical-pro-section">
<div class="container">
    <div class="row">
<?php echo $this->session->flashdata('email_success'); ?>
<?php echo $this->session->flashdata('email_fail'); ?>

                    <div class="medical-history-detail">
                <div class="row">
                 <div class="col-lg-3 ">
                 <img src="<?php echo $row ? $row->profile_pic : ''?>"/>
                 </div>
                <div class="col-lg-3 ">
                <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Personal Detail</h3>
                 
                <div> <strong>Name :</strong> <?php echo $row ? ucfirst($row->fname) . ' '. ucfirst($row->sname) : '';?> </div>
                   <strong> Email :</strong> <?php echo $row ? $row->email : '';?>
                    <div><strong>Phone :</strong> <?php echo $row ? $row->phone : '';?></div>
                  <div><strong> Address : </strong> <?php echo $row ? $row->address : '';?> </div>
                  <div><strong> City : </strong> <?php echo $row ? $row->city : '';?> </div>
                  <div><strong> Province : </strong> <?php echo $row ? $row->province : '';?> </div>
                 </div>
                  
                  
                  <div class="col-lg-3 ">
                    <h2>&nbsp;</h2>
                 
                    <!--<div><strong> Speciality :</strong> <?php //echo $row ? $row->speciality : '';?> </div>-->
                    <div><strong>Timings :</strong> 
                        <?php $count=0; if($doctorTimings) :
                            foreach($doctorTimings as $doctorTiming) :  ?> 
                                <?php $count++; echo date('g:i a', strtotime($doctorTiming->start_time)) . ' - ' .  date('g:i a', strtotime($doctorTiming->end_time));  
                                if($count != count($doctorTimings)) 
                                  echo '<br />' ; 
                            endforeach; endif; ?></div>
                            <div> <strong>Days :</strong> <?php $count=0; if($doctorAvailableDays) : foreach($doctorAvailableDays as $doctorAvailableDay) :  ?> <?php $count++; echo ucfirst($doctorAvailableDay); if($count != count($doctorAvailableDays)) echo ', ' ; endforeach; endif;?></div>
                            <!--<div><strong> Cv :</strong> <a href="<?php // echo base_url() ?>uploads/cv/<?php // echo $row ? $row->cv : ''?>" download>Download cv </a> </div>-->
                  
                 </div>



                 <div class="col-lg-3 ">
            <h2>&nbsp;</h2>
                   <div><strong>Type of Practitioner : </strong><?php echo $row ? ucfirst($row->type_of_doctor) : '';?> </div>
                   <div><a href="<?php echo base_url() . 'user/changePassword'; ?>">Change Password</a></div>
                    
                 </div>
                  <div class="col-lg-12">
                  <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Biography</h3>
                 	 <p><?php echo $row->description; ?></p>
                 </div>
                 <div class="col-lg-12">
                 <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Academic qualifications</h3>
                 	 <div ><?php if($row->academics) : foreach($row->academics as $value) :?>
                     <div class="acadmic-info">
                 <h4> <?php echo $value->qualification ? $value->qualification : '';?></h4>
                  <div  class="institution-info"><?php echo $value->institution ? $value->institution : '';?></div>
                 <div  class="date-info"> <?php echo $value->year ? $value->year : '';?><br/></div>
                 </div>
                  <?php endforeach; endif; ?></div>
                 </div>
                 <div class="col-lg-12">
                 <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Working Experience</h3>
                 	 
                    <div ><?php if($row->experience) : foreach($row->experience as $value) :
                        
                        
                        
                       //echo $value->present_here.'<br/>';
                        
                        ?>
                    <div class="experience-info">
                      <h4>  <?php echo $value->job_title ? $value->job_title : '';?></h4>
                      <div class="company-info">  <?php echo $value->company ? $value->company : '';?></div>
                      <div class="date-info"><?php echo $value->start_date ? date('M Y', strtotime($value->start_date)) : '';?> - <?php if($value->present_here == 1) {echo "Present"; } else { echo $value->end_date ? date('M Y', strtotime($value->end_date)) . '<br/>' : ''; } ?></div>
                    </div>
                  <?php endforeach; endif; ?></div>
                 </div>
                 </div>
                 
                      <?php if($doctorStatus == 0) : ?>
                        <a class="complete-profile" href="<?php echo base_url() ?>admin/getDoctorEmail/<?php echo $row->user_id ?>">Approve</a>
                     <?php endif; ?>

                     <?php if(isset($editProfile) && ($editProfile)) : ?>
                        <a class="complete-profile" href="<?php echo base_url() ?>doctordashboard/editProfile">Edit Profile</a>
                     <?php endif; ?>
                    </div>
</div>
    </div>
</div>
</div>