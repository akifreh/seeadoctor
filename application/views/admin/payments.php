<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Payments</title>
    <link href="<?php echo base_url("/css/bootstrap.css"); ?>" rel="stylesheet" type="text/css" />
</head>
<body>
    
	<div class="dashboard-section">
		<div class="container">
			<div class="row">
			<div class="col-lg-12 col-md-12" >
			<h2>Payments</h2>
			</div>
                            
		<div class="col-lg-12 col-md-12" >
      <!--h3>Finished Appointments</h3-->
			<div class="medical-history-table table-design">
				<div class="table-header">
          <div class="table-head-col col-lg-1 col-md-1 col-sm-1 col-xs-1" style=" padding-left: 5px;    padding-right: 0px;">Appointment</div>
					<div class="table-head-col col-lg-1  col-md-1 col-sm-1 col-xs-1">Patient</div>
					<div class="table-head-col col-lg-1 col-md-1 col-sm-1 col-xs-1">Doctor</div>
					<div class="table-head-col col-lg-1 col-md-1 col-sm-1 col-xs-1">Duration</div>
          <div class="table-head-col col-lg-1 col-md-1 col-sm-1 col-xs-1">Charged</div>
					<div class="table-head-col col-lg-1 col-md-1 col-sm-1 col-xs-1">Price</div>
          <div class="table-head-col col-lg-1 col-md-1 col-sm-1 col-xs-1">Status</div>
          <div class="table-head-col col-lg-3 col-md-3 col-sm-3 col-xs-3">Transaction Id</div>
          <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Capture Date</div>
		    </div>
				<div class="table-body">
                                   
        <?php if($records) :
        foreach ($records as $row) :?>
					 <div class="table-row  ">
            <div class="table-col col-lg-1 col-md-1 col-sm-1 col-xs-1"><a href="<?php echo base_url() . 'appointment/detail/' . $row->appointment_id;?>"><?php echo $row->appointment_id; //date('d M Y - h:i a', strtotime($row->start_time)); ?></a></div>
						<div class="table-col col-lg-1 col-md-1 col-sm-1 col-xs-1"><a href="<?php echo base_url() . 'admin/viewUserDetail/' . $row->patientInfo->id; ?>"><?php echo $row->patientInfo->fname; ?></a></div>
						<div class="table-col col-lg-1 col-md-1 col-sm-1 col-xs-1"><a href="<?php echo base_url() . 'admin/viewDoctorDetail/' . $row->doctorInfo->user_id; ?>"><?php echo $row->doctorInfo->fname; ?></a></div>
            <div class="table-col col-lg-1 col-md-1 col-sm-1 col-xs-1"><?php echo date('i:s ' , strtotime($row->actual_duration)); ?></a></div>
            <div class="table-col col-lg-1 col-md-1 col-sm-1 col-xs-1"><?php echo date('i ' , strtotime($row->charged_duration)) . ' @' . round($row->rate * 15); ?></div>
            <div class="table-col col-lg-1 col-md-1 col-sm-1 col-xs-1"><?php echo $row->price; ?></div>
            <div class="table-col col-lg-1 col-md-1 col-sm-1 col-xs-1"><?php echo $row->status ? 'Captured' : 'Error'; ?></div>
            <div class="table-col col-lg-3 col-md-1 col-sm-1 col-xs-1"><?php echo $row->transaction_id ? $row->transaction_id : '-------'; ?></div>
            <div class="table-col col-lg-2 col-md-1 col-sm-1 col-xs-1"><?php echo $row->created_at ? date('d M Y - h:i a', strtotime($row->created_at)) : '------'; ?></div>
            
          </div>
        <?php  endforeach; 
        endif; ?>
					
					 
				</div>
			 
			 </div>
			
			</div>	
    
     
 			
			</div>
			 
			 
      </div>
	</div>