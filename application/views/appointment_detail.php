<div class="medical-pro-section">
  <div class="container">
    <div class="medical-history-detail">
      <div class="row">
        <div class="col-lg-3 ">
        <img src="<?php echo base_url()?>uploads/profile/thumbnail/<?php echo $patient->profile_pic ?>"/>
      </div>
       <div class="col-lg-3 ">
               <div> <strong>Name :</strong> <?php echo ucfirst($patient->fname) . ' '. ucfirst($patient->sname);?> </div>
        <div><strong>Gender : </strong> <?php echo ucfirst($patient->gender);?> </div>
        <div><strong>Age :</strong> <?php echo date_diff(date_create($patient->dob), date_create('today'))->y;?></div>
        <div> <strong>Patient :</strong> <?php echo ucfirst($record->patient); ?></div>
        </div>
         <div class="col-lg-3 ">
       
        <div> <strong>Taking Medicine :</strong> <?php echo ucfirst($record->med_taken); ?></div>
        <div><strong>Day :</strong> <?php echo date('d M Y', strtotime($record->start_time)); ?></div>
        <div><strong> Time :</strong> <?php echo date('g:i a', strtotime($record->start_time)) . ' - ' . date('g:i a', strtotime($record->end_time)); ?></div>
        <div> <strong>Brief Description :</strong> <?php echo ucfirst($record->brief_description); ?></div>
        </div>
        <div><a class="complete-profile-btn" href="javascript:history.back()">Go Back</a></div>
        <?php if($this->session->userdata['logged_in']['role'] == 3) : ?>
        <a class="complete-profile-btn" href="<?php echo base_url() . 'doctordashboard/userProfile/' . $patient->id; ?>">Medical History</a>
        <?php endif; ?>
        </div>
        
      </div>   
      <?php if($record->symptom) : ?>
      <div class="medical-history-detail  medical-symtoms-detail">
      <div class="row">
     <div class="col-lg-12"> <h2>SYMPTOMS</h2></div>
        <?php foreach ($record->symptom as $key => $value) : ?>

        <div class="col-lg-6">
    <div class="form-group">
       <div><h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;"><?php echo ucfirst($key); ?></h3></div>
        <?php foreach ($value as $val) : ?>
        <div class="checkbox">
          <label>
              <i class="fa fa-check-circle" aria-hidden="true"></i> <?php echo $val; ?>
          </label>
           
        </div>
      <?php endforeach; ?>
      </div>
      </div>
    <?php endforeach; ?>
      <?php endif; ?>   
    </div>
  </div>
</div>