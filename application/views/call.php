<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <script src="https://static.opentok.com/v2/js/opentok.js"></script>
    <script src="<?php echo base_url(); ?>js/opentok-text-chat.js"></script>
    
    <!-- jQuery -->
    <script src="<?php echo base_url();?>js/firechat/jquery.min.js"></script>

    <!-- Firebase -->
    <script src="<?php echo base_url();?>js/firechat/firebase.js"></script>

    <!-- Firechat -->
    <link rel="stylesheet" href="<?php echo base_url();?>js/firechat/firechat.min.css" />
    <script src="<?php echo base_url();?>js/firechat/firechat.min.js"></script>
    <script src="https://apis.google.com/js/client.js" type="text/javascript"></script>

    <!-- Custom CSS -->
    <style>
      #firechat-wrapper {
    
    background: rgba(246, 246, 246, 0.7) none repeat scroll 0 0;
    bottom: -2px;
 
    padding: 0px;
    position: absolute;
    right: 0;
    width: 320px;
    z-index: 999;
}
.message-self{
	text-align:right;
}
#firechat .message-self .message-content{
	padding-right:0px;
}

#firechat .message-self .fourfifth{
	float:right;
}
#firechat .message {
   
    padding: 10px;
}
#firechat-tab-content textarea{
	border-radius: 0px;
	margin:5px;
    margin-bottom: 10px;
    border: #40b2e5 solid 2px;
	    height: 60px;
		width:96%;
		
}

#firechat-header,.tab-pane-menu,.icon.user-chat {
	display:none;
}
#firechat .btn{
	padding:5px;
	/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#7db9e8+0,0484cf+100 */
background: #7db9e8; /* Old browsers */
background: -moz-linear-gradient(top, #7db9e8 0%, #0484cf 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, #7db9e8 0%,#0484cf 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, #7db9e8 0%,#0484cf 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#7db9e8', endColorstr='#0484cf',GradientType=0 ); /* IE6-9 */
color:#fff;
}
#firechat .firechat-dropdown-toggle.btn {
    height: 30px;
    padding: 5px 0 0;
}
#firechat #firechat-tabs {
    height: 489px;
}
.file-share-chat{
	position:absolute;
	    right: 28px;
    bottom: 5px;
    z-index: 9999;
	
}

.file-share-chat .alert{
	margin-bottom:0px !important;
	padding: 4px !important;
	margin-top:10px;
	font-size:11px;
}
	

.file-share-chat .progress{
	margin-bottom:0px;
	margin-top:10px;
	    height: 10px;
}

#firechat .btn:hover, #firechat .btn:focus, #firechat .btn:active, #firechat .btn.active, #firechat .btn.disabled, #firechat .btn[disabled] {
   /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#7db9e8+5,0484cf+13,7db9e8+100 */
/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#73a8d6+0,035bb2+100 */
background: #73a8d6; /* Old browsers */
background: -moz-linear-gradient(top, #73a8d6 0%, #035bb2 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, #73a8d6 0%,#035bb2 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, #73a8d6 0%,#035bb2 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#73a8d6', endColorstr='#035bb2',GradientType=0 ); /* IE6-9 */
    color: #fff;
    outline: 0 none;
}
#firechat .btn:active, #firechat .btn.active {
    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#7db9e8+5,0484cf+13,7db9e8+100 */
/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#73a8d6+0,035bb2+100 */
background: #73a8d6; /* Old browsers */
background: -moz-linear-gradient(top, #73a8d6 0%, #035bb2 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, #73a8d6 0%,#035bb2 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, #73a8d6 0%,#035bb2 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#73a8d6', endColorstr='#035bb2',GradientType=0 ); /* IE6-9 */color:#fff;}
#firechat .nav-tabs>li {
 
    max-width: 100%;
}
    </style>
  </head>

  <body>
    

    <script>
        // Initialize Firebase
        var config = {
        apiKey: "AIzaSyAa5v90ME39AacY7BUSCOSJlGXFFfqewgs",
        authDomain: "seeadoctor-3a03d.firebaseapp.com",
        databaseURL: "https://seeadoctor-3a03d.firebaseio.com",
        storageBucket: "seeadoctor-3a03d.appspot.com",
        messagingSenderId: "789741748368"
        };
        firebase.initializeApp(config);
        var dbName = '<?php echo $db_name; ?>';
        var chatRef = firebase.database().ref(dbName);
        var chatRoomExists = 'FALSE';
        var chatTitle = '<?php echo $room_name; ?>';
        var roomId = '';

    // Listen for authentication state changes
      firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          roomChatSetup(user);
        } else {
         //  If the user is not logged in, sign them in anonymously
         firebase.auth().signInAnonymously().catch(function(error) {
            console.log("Error signing user in anonymously:", error);
          });
        }
      });
        
        // Get a reference to the storage service, which is used to create references in your storage bucket
        var storage = firebase.storage();

        // Create a storage reference from our storage service
        var storageRef = storage.ref();

      function roomChatSetup(user) {

        var chat = new Firechat(chatRef);
        var chatUser = user;

        // If room already present get reference
        chat.getRoomList(function (roomList) { 
          for (var key in roomList) 
          { 
            var title = roomList[key]['name'];

            if(title == chatTitle) {
              chatRoomExists = 'TRUE';
              roomId = roomList[key]['id'];
              break;
            }
          }
          if(chatRoomExists == 'FALSE') {

            chat.createRoom(chatTitle, "private", function(roomId) {
                chat.setUser(chatUser.uid, "<?php echo $username;?>", function(user) {
                  chat.enterRoom(roomId);
                });
              location.reload();
            });
            
          } else {
            chat.setUser(chatUser.uid, "<?php echo $username;?>", function(user) {
                document.getElementById('roomId').value = roomId;
                document.getElementById('user').value = chatUser.uid;
                chat.enterRoom(roomId);

            });
            initChat(user);
          }  
        });
      }

      function initChat(user) {
        var chat = new FirechatUI(chatRef, document.getElementById("firechat-wrapper"));
        chat.setUser(user.uid, "<?php echo $username;?>");
      }

       function sendMessage(message) 
       {
          var chat = new Firechat(chatRef);  
          var roomId = document.getElementById("roomId").value;
          var chatUserId = document.getElementById("user").value;
          chat.setUser(chatUserId, "<?php echo $username;?>", function(user) 
          {
              chat.sendMessage(roomId, message, messageType='default');
          });
      }

    function uploadFile(file)
    {
       // File or Blob named mountains.jpg
        var file = file;
        var dbName = '<?php echo $db_name; ?>';
        var room = '<?php echo $room_name; ?>';

        // Create the file metadata
        var metadata = {
          contentType: file.type
        };
        // Upload file and metadata to the object 'images/room_name/mountains.jpg'
        //var uploadTask = storageRef.child('images/'+room+'/' + file.name).put(file, metadata);
        var uploadTask = storageRef.child('uploads/'+dbName+'/'+room+'/' + file.name).put(file, metadata);

        // Listen for state changes, errors, and completion of the upload.
        uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
          function(snapshot) {
            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
			      $('.progress').show();
			      $('.alert-success').hide();
			      $('.progress-bar.progress-bar-success').width(progress+'%');
            switch (snapshot.state) {
              case firebase.storage.TaskState.PAUSED: // or 'paused'
                console.log('Upload is paused');
                break;
              case firebase.storage.TaskState.RUNNING: // or 'running'
                console.log('Upload is running');
                break;
            }
          }, function(error) {
          switch (error.code) {
            case 'storage/unauthorized':
              console.log('User doesn\'t have permission to access the object'); // User doesn't have permission to access the object
              break;

            case 'storage/canceled':
              // User canceled the upload
              break;

            case 'storage/unknown':
            console.log(error.serverResponse);
              // Unknown error occurred, inspect error.serverResponse
              break;
          }
        }, function() {
          // Upload completed successfully, now we can get the download URL
          var downloadURL = uploadTask.snapshot.downloadURL;
          generateShortUrl(downloadURL);
        });

    }

    function handleFiles()
    {
      var selectedFile = document.getElementById('input').files[0];
      uploadFile(selectedFile);
    }

    function generateShortUrl(url) 
    {
      var apikey = '<?php echo $mapKey; ?>'
      gapi.client.setApiKey(apikey);
      gapi.client.load('urlshortener', 'v1', function() { 

      var Url = url;
      var request = gapi.client.urlshortener.url.insert({
        'resource': {
        'longUrl': Url
         }
      });
      request.execute(function(response) {

            if (response.id != null) {
                document.getElementById('input').value='';
                message = "My shared file is at " + response.id;
                sendMessage(message);
				        $('.progress-bar.progress-bar-success').width('0%');
				        $('.progress').hide();
				        $('.alert-success').show();
            }
            else {
                alert("Error: creating short url \n" + response.error);
                console.log(response.error);
            }
        });
    });
    }
	
    </script>

  </body>
</html>

<?php $sessionValue = $sesskey->session_key; ?>

<?php if($_GET['sessionkey'] && $_GET['sessionkey'] == $sessionValue )  { ?>

<div class="video-section">
    	   
       <div id="videos">
        <div id="subscriber"></div>
        <div id="publisher"></div>
    </div>
   
   
<?php if($patientInfo->id == $this->session->userdata['logged_in']['id']) { ?><div class="dotor-name-video-sec"><?php echo $doctorInfo->fname; ?></div> <?php } ?>
<?php if($doctorInfo->id == $this->session->userdata['logged_in']['id']) { ?><div class="dotor-name-video-sec"><?php echo $patientInfo->fname; ?></div> <?php } ?>    
<div class="video-chat-options">
    	<div class="container">
            <div class=" col-lg-8 col-xs-offset-1" >
            <?php if($session['role'] == 3) : ?> 
                <a style="color:#FFFFFF;" href="<?php echo base_url() . 'call/callEnd/' . $session_key; ?>"><img src="<?php echo base_url() ?>images/call-end.png" width="98px" style="margin-right:10px;"> <span>End Call</span></a>
                <!-- <i class="fa fa-camera-retro" aria-hidden="true"></i>
                <i class="fa fa-calendar" aria-hidden="true"></i> -->  
                <a style="color:#FFFFFF;" target="_blank" href="<?php echo base_url() . 'doctordashboard/userProfile/' . $patientInfo->id; ?>"><i class="fa fa-medkit" aria-hidden="true"></i> <span>Medical History</span>
</a>
                
            <?php endif; ?>
            <i class="fa fa-comments chat-open" aria-hidden="true"></i > <span>Chat</span>
          
              
          </div>
	<div class=" col-lg-3 pull-right text-right">
            <div class="time"></div>
    </div>
        </div>
    <!--div id="firechat-wrapper"></div-->  
    </div>
  </div>


 
    <div id="firechat-wrapper">
  
    </div>
     <div class="file-share-chat">
        <input type="file" id="input" onchange="handleFiles(this.files)">
        <input type="hidden" name="roomId" id="roomId"/>
        <input type="hidden" name="user" id="user"/>
        <input type="hidden" name="progress" id="progress"/>
        <div class="progress progress-striped" hidden><div class="progress-bar progress-bar-success" style="width:0%"></div></div>
        <div class="alert alert-dismissible alert-success" hidden>
          Your file has been shared successfully.
        </div>
    </div>
    <div id="chat"></div>

    <script type="text/javascript" src="<?php echo base_url(); ?>js/sampleconfig.js"></script>
    
    
    <?php } ?>
	
    <input style="display: none;" type="button" id="startCallBut" value="Start Call" />
    <input style="display: none;" type="button" id="endCallBut" value="End Call" />
    <input type="button" id="latButton" value="End Call" hidden="true" />
    <input type="hidden" name="elapsedtime" value="0" id="elapsedTimeVal" />
    <input type="hidden" id="lat" name="lat" value="">
    <input type="hidden" id="lng" name="lng" value="">
    
   <?php if(isset($this->session->userdata('logged_in')['role']) && $this->session->userdata('logged_in')['role'] == 3): ?> 
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
		 
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
    <div id="map"  hidden></div>
 <?php endif;?> 

    <script> 
    // $( "#publisher" ).toggleClass( 'video-leftshow' );
 
    $( ".chat-open" ).click(function() {
 $( "#firechat-wrapper" ).toggle('slow');
   
 });
  

// record start time
var startTime;

function displayTime() {
    // later record end time
    var endTime = new Date();

    // time difference in ms
    var timeDiff = endTime - startTime;

    // strip the miliseconds
    timeDiff /= 1000;

    // get seconds
    var seconds = Math.round(timeDiff % 60);

    // remove seconds from the date
    timeDiff = Math.floor(timeDiff / 60);

    // get minutes
    var minutes = Math.round(timeDiff % 60);

    // remove minutes from the date
    timeDiff = Math.floor(timeDiff / 60);

    // get hours
    var hours = Math.round(timeDiff % 24);

    // remove hours from the date
    timeDiff = Math.floor(timeDiff / 24);

    // the rest of timeDiff is number of days
    var days = timeDiff;
	
	//$("#elapsedTimeVal").val(hours + ":" + minutes + ":" + seconds);
	
    $(".time").text(hours + ":" + minutes + ":" + seconds);
    
	if($("#elapsedTimeVal").val() == 0) {
		setTimeout(displayTime, 1000);
	}
}

$("input#startCallBut").click(function () {
    var _BASE_ = '<?php echo base_url();?>'
        $.ajax(
          {url: _BASE_+"call/getTimerSession", success: function(result){
              console.log('Result is '+result);
              startTime = new Date(result*1000);
              console.log('Start time is '+startTime);
              setTimeout(displayTime, 1000);
          }});

    var _BASE_ = '<?php echo base_url();?>'
    var Id  = '<?php echo $sesskey->appointment_id;?>'
    $.ajax(
        {url: _BASE_+"call/getUserLatLng/"+Id , success: function(result){ 
           var data = result.split(',');
           showMarker(parseFloat(data[0]), parseFloat(data[1]));
        }          
    });    
    $('#map').show();
});

$("input#endCallBut").click(function () {
    var elapsedtime = $(".time").html();
	$("#elapsedTimeVal").val(elapsedtime);
 //alert($("#elapsedTimeVal").val());
});

$("#latButton").click(function () {
    var _BASE_ = '<?php echo base_url();?>'
    var lat = $('#lat').val()
    var lng = $('#lng').val()
    var Id  = '<?php echo $sesskey->appointment_id;?>'
    $.ajax(
        {url: _BASE_+"call/saveUserLatLng/"+Id+"/"+lat+"/"+lng , success: function(result){ 
            console.log(result);
            }          
        });
});

 

    </script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/app.js"></script>

    <!-- Check every 15 sec, if call has ended to redirect patient to success call page -->
    <script type="text/javascript">
      $(document).ready(function(){
       var _BASE_ = '<?php echo base_url();?>'
       var TOKEN  = '<?php echo $session_key;?>'
       var APPOINTMENT_ID  = '<?php echo $sesskey->appointment_id;?>'
       var D_ID   = '<?php echo $doctorInfo->id;?>'
            window.setInterval(
              function() {
                  $.ajax({
                        url: _BASE_+"call/checkCallEnd/"+TOKEN, 
                        success: function(result){
                            if(result == 'yes'){
                              window.location.replace(_BASE_+"user/rateDoctor/"+D_ID+"/"+APPOINTMENT_ID);
                            }
                        }
                   });
              }, 15000); 
      });
    </script>

    <?php $script = 
    '<script>
      function initMap() {

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            console.log(pos);
             document.getElementById(\'lat\').value=pos.lat;
             document.getElementById(\'lng\').value=pos.lng;
             $(\'#latButton\').click();
          }, function() {
            handleLocationError(true);
          });
        } else {
          handleLocationError(false);
        }
      }

      function handleLocationError(browserHasGeolocation) {
        console.log(\'Error: The Geolocation service failed.\');
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=' . $mapKey . '&callback=initMap">
    </script>';

    if( isset($this->session->userdata('logged_in')['role']) && $this->session->userdata('logged_in')['role'] == 2) :
      echo $script;
    endif; ?>
    <script>
      function showMarker(latt,long) {
        if(latt === undefined) {
          var latt = parseFloat(-26.195);
        }
        if(long === undefined) {
          var long = parseFloat(28.034);
        }

        var myLatLng = {lat: latt, lng: long};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 8,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Patient Location!'
        });
      }
    </script>
    <?php echo '<script async defer src="https://maps.googleapis.com/maps/api/js?key=' . $mapKey . '&callback=showMarker">
    </script>'; ?>
	
 
    
   