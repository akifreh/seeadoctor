<?php 
if($patientsInfo) :
  foreach ($patientsInfo as $patientInfo) :
  endforeach;
endif; ?>

<?php
if($patientsInfo) :
$from = new DateTime($patientInfo->dob);
$to   = new DateTime('today');
$dob = $from->diff($to)->y;
endif;

# procedural
//echo date_diff(date_create($patientInfo->dob), date_create('today'))->y;
?>

<div class="medical-pro-section">
<div class="container">
   <?php $rating = isset($doctorrating[0]) ? $doctorrating[0]->rating : 0; ?> 
    <?php $ratingDiff = 5 - $rating; ?>
  <div class="row">
  <div class=" ">
  <h2 class="pull-left">Doctor's Dashboard</h2>
  <i class="stars-doc-rate pull-right">Doctor's Rating 
     <?php for($i= 0; $i< $rating; $i++) { ?> <i class="fa fa-star active" aria-hidden="true"></i> <?php } ?>
     <?php for($j= 0; $j<$ratingDiff; $j++) { ?>  <i class="fa fa-star" aria-hidden="true"></i> <?php } ?>
  </i>
  </div>
  </div>
 <div class="row">
     <?php if($patientsInfo) : ?>
     
     
     <?php $attributes = array("name" => "approvalform" , "id" => "approvalform", "enctype" => 'multipart/form-data');
       echo form_open("doctordashboard/approveAppointmentRequest", $attributes);?>
  <div class="now-appointment">
  <div class="alert   alert-warning">
 
  
  <div class="col-lg-8"><h3>Appointment Now</h3>
  <div class="row"><div class="col-lg-3"><?php echo $patientInfo->fname .' '. $patientInfo->sname ?></div>  <div class="col-lg-2"><?php echo $dob; ?> years</div>  <div class="col-lg-2"><?php echo ucfirst($patientInfo->gender) ?> </div><div class="col-lg-5"> Wants an Appointment Now With You </div></div></div>
      <input type="hidden" name="appointment_id" value="<?php echo $apppoinment_now_id;?>" />
      
      <button type="button" class="btn btn-default submit-button" data-toggle="modal" data-target=".see-opt-psychologist">view details</button>
      <button class="btn btn-default submit-button pull-right deny-button btndenydisable" name="button" id="0" value="deny" type="submit">Deny</button>
      <button class="btn btn-default btn-green submit-button pull-right btnapprovedisable" name="button" id="0" value="approve" type="submit">Approve</button>
  <div class="clearfix"></div>
</div>
</div>
     
     <div class="modal see-opt-psychologist sad-modal">
  <div class="modal-dialog ">
      <div class="modal-content" style="width: 500px;">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
          <div class="see-doctor-now" style="width: 100%;">
            <h2>Patient Detail</h2>
            <hr>
            
             <div class="col-lg-3 ">
                 <img src="<?php echo base_url()?>uploads/profile/thumbnail/<?php echo $patientInfo->profile_pic ?>" height="100" width="100"/>
      </div>
            
            
            <div class="col-lg-7" style="margin-left: 10px;">
        
                <div style="padding-bottom: 10px;"> <strong>Name :</strong> <?php echo ucfirst($patientInfo->fname) . ' '. ucfirst($patientInfo->sname);?> </div>
                     <div style="padding-bottom: 10px;"> <strong>Gender :</strong> <?php echo ucfirst($patientInfo->gender);?> </div>
                     <!--<div style="padding-bottom: 10px;"> <strong>Email :</strong> <?php //echo $patientInfo->email;?> </div>-->
                     <div style="padding-bottom: 10px;"> <strong>Age :</strong> <?php echo date_diff(date_create($patientInfo->dob), date_create('today'))->y;?></div>
                     <div style="padding-bottom: 10px;"> <strong>Patient :</strong> <?php echo ucfirst($now_appointment_detail->patient); ?></div>
                     <div style="padding-bottom: 10px;"> <strong>Taking Medicine :</strong> <?php echo ucfirst($now_appointment_detail->med_taken); ?></div>
                     <div style="padding-bottom: 10px;"> <strong>Brief Description :</strong> <?php echo ucfirst($now_appointment_detail->brief_description); ?></div>
                     <!--<div style="padding-bottom: 10px;"><strong>Day :</strong> <?php //echo date('d M Y', strtotime($now_appointment_detail->created_at)); ?></div>-->
            
	
            
            </div>  
	</div>
		 
      </div>
      
    </div>
  </div>
</div> 
     
     
     
     <?php echo form_close(); ?>
     <?php endif; ?>
</div>
       <script type="text/javascript">
           //27122016
        $(document).ready(function(){
           $('.btndenydisable').on('click', function(){
              
                if($('.btndenydisable').attr("id") == 0){
                $('.btndenydisable').attr("id", '1');
                $('.btndenydisable').click();
                $('.btndenydisable').attr("disabled", true);
                }else{
                    return true;
                }
                
           });
           $('.btnapprovedisable').on('click', function(){
                
               if($('.btnapprovedisable').attr("id") == 0){
                $('.btnapprovedisable').attr("id", '1');
                $('.btnapprovedisable').click();
                $('.btnapprovedisable').attr("disabled", true);
                }else{
                    return true;
                }
              
                
           });
        });
    </script>
<div class="row">
    <?php if($appointment_link) : ?>
    <div class="now-appointment">
      <div class="alert   alert-warning">
        <div class="col-lg-9"><h3>Appointment Link</h3>
          <div class="row"><div class="col-lg-12">Please click on the link to start your call with <?php echo $appointment_link['user']->fname . ' ' . $appointment_link['user']->sname; ?></div></div></div>
          <a class="btn btn-default submit-button pull-right disableappointment" href="<?php echo $appointment_link['link']; ?>">Start Call</a>
          <div class="clearfix"></div>
        </div>
      </div>
     <script type="text/javascript">
$(document).ready(function(){
   $('.disableappointment').on('click', function(){
       //27122016
        $('.disableappointment').attr("disabled", true);
     });
});
             
    </script>
     <?php endif; ?>
    </div>
    <?php echo $this->session->flashdata('approved'); ?>
  <div class="row med-detail-wrapp">
<h3>Upcoming Appointment</h3>
 <div class="medical-history-table table-design">
				<div class="table-header">
            <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Name</div>
            <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Type</div>
            <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Date</div>
            <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2 ">Timing</div>
					   <!--div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Email Address</div-->
                    <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Who Visited</div>
                    <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp; </div>
                                        
					
				</div>
     
                               
                              <?php if($allappointments) :
                              for($i=0; $i<count($allappointments); $i++) : ?>
				<div class="table-body" id="row ">
                                    <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $allappointments[$i]->fname ; echo ' ' ; echo $allappointments[$i]->sname ; ?></div>
                                    <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $allappointments[$i]->type;?></div>
                                    <div  class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo date('d M Y', strtotime($allappointments[$i]->start_time)); ?></div>
                                     <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo date('g:i a', strtotime($allappointments[$i]->start_time)) . ' - ' . date('g:i a', strtotime($allappointments[$i]->end_time)); ?></div>
                                    <!--div  class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $allappointments[$i]->brief_description; ?></div-->
                                    <div  class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $allappointments[$i]->patient; ?></div>
                                    <div  class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                        
                                        <?php if($allappointments[$i]->meeting_approval == 0 OR $allappointments[$i]->meeting_approval == 1)
                                        { ?>    
                                        <a href="<?php echo base_url() . 'appointment/startcall/'. $allappointments[$i]->appointment_id; ?>">Start Call</a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <?php } ?>
                                        
                                        <?php if($allappointments[$i]->meeting_approval == 3)
                                        { ?>    
                                        <a href="javascript:void(0);" style="cursor: default;text-decoration: none;">Completed</a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <?php } ?>
                                        <a href="<?php echo base_url() . 'appointment/detail/'. $allappointments[$i]->appointment_id; ?>">view details</a>
                                    
                                    </div>
                                        
                                </div> 
                              <?php endfor;
                              endif; ?>          
                           
			 </div>
  </div>
   
<div class="row med-detail-wrapp">
<h3>Previous Appointments</h3>
 <div class="medical-history-table table-design">
				<div class="table-header">
					<div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Name</div>
                    <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Date</div>
                    <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Timing</div>
					<div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Call Duration</div>
                    <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Who Visited</div>
                    <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp; </div>
                                        
					
				</div>
                              
                              
				<?php if($finished_appointments) : 
        for($i=0; $i<count($finished_appointments); $i++) : ?>
				<div class="table-body" id="row ">
                                    <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $finished_appointments[$i]->fname ; echo ' ' ; echo $finished_appointments[$i]->sname ; ?></div>
                                    <div  class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo date('d M Y', strtotime($finished_appointments[$i]->start_time)); ?></div>
                                     <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo date('h:i a', strtotime($finished_appointments[$i]->start_time)) . ' - ' . date('h:i a', strtotime($finished_appointments[$i]->end_time)); ?></div>
                                    <div  class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $finished_appointments[$i]->payments ? date('i:s ' , strtotime($finished_appointments[$i]->payments->actual_duration)) : ''; ?></div>
                                    <div  class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $finished_appointments[$i]->patient; ?></div>
                                    <div  class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><a href="<?php echo base_url() . 'appointment/detail/'. $finished_appointments[$i]->appointment_id; ?>">view details</a></div>
                                        
                                </div> 
                              <?php endfor;
                              endif; ?>              
			 </div>
  </div>
 
  <div class="row med-detail-wrapp">
<h3>Missed Appointment</h3>
 <div class="medical-history-table table-design">
        <div class="table-header">
          <div class="table-head-col col-lg-4 col-md-4 col-sm-4 col-xs-4">Name</div>
                    <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2 ">Date</div>
                    <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Timing</div>
                    <!--div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Email Address</div-->
                    <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Who Visited</div>
                    <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp; </div>
                                        
          
        </div>
                              
                              
        <?php if($missed_appointments) : 
        for($i=0; $i<count($missed_appointments); $i++) :?>
        <div class="table-body" id="row ">
                                    <div class="table-col col-lg-4 col-md-4 col-sm-4 col-xs-4"><?php echo $missed_appointments[$i]->fname ; echo ' ' ; echo $missed_appointments[$i]->sname ; ?></div>
                                    <div  class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo date('d M Y', strtotime($missed_appointments[$i]->start_time)); ?></div>
                                     <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo date('h:i a', strtotime($missed_appointments[$i]->start_time)) . ' - ' . date('h:i a', strtotime($missed_appointments[$i]->end_time)) ?></div>
                                    <!--div  class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $missed_appointments[$i]->email; ?></div-->
                                    <div  class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $missed_appointments[$i]->patient; ?></div>
                                    <div  class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><a href="<?php echo base_url() . 'appointment/detail/'. $missed_appointments[$i]->appointment_id; ?>">view details</a></div>
                                        
                                </div> 
                              <?php endfor;
                              endif; ?>              
       </div>
  </div>
</div>
</div>
</div>