   <div class="form-register-doctor">
    <div class="container">
        <div class="col-lg-8 col-md-offset-2">
            <h1 style="margin-bottom:30px;">Edit Profile</h1>
<?php $attributes = array("name" => "doctorinfo" , "id" => "doctorinfo", "enctype" => 'multipart/form-data');
       echo form_open("doctordashboard/editProfile", $attributes);?>

<?php echo $this->session->flashdata('msg'); ?>
<?php echo $this->session->flashdata('fail'); ?>
    
    <div class="personal-detail">
        
        <div class="col-lg-3 col-md-3 user-pro-pic-main padding-right-0">
                            <div style="position:relative; width:100%;margin-top:51px;">
                      <img src="<?php echo isset($record->profile_pic) ? $record->profile_pic : ''; ?>" id="pro-pic-id" alt="Profile-Image" style="width:100%;height: 210px;">
                            <input type="file" name="profile_pic" id="add-pro-pic" class="add-profile-pic"> 
                            <span class="add-pro-pic-link"><i class="fa fa-camera" aria-hidden="true"></i> Add Photo</span>
                         </div>
                         <!--<div class="prfoile-pic-update" style="display:none;" >Please Click Update Button to Change your Profile Picture</div>-->
                         </div>
    <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Personal Detail</h3>
    <div class="row">
        <div class="col-lg-6">
            <label for="inputEmail" class="  control-label">*Name </label>
            <div class=" ">
                <input disabled="disabled" name="fname" type="text" value="<?php echo isset($record->fname) ? $record->fname : ''; ?>" class="form-control" required>
            </div>
    </div>
    
    <div class="col-lg-6">
        <label for="inputEmail" class="  control-label">*Surname </label>
        <div class=" ">
            <input disabled="disabled" name="sname" type="text" value="<?php echo isset($record->sname) ? $record->sname : ''; ?>" class="form-control" required>
        </div>
    </div>
    
    <div class="col-lg-6">
    <label for="inputEmail" class="  control-label">*Email </label>
    <div class=" ">                   
        <input disabled="disabled" name="email" type="email" value="<?php echo isset($record->email) ? $record->email : ''; ?>" class="form-control" required>
    </div>
</div>
    <div class="col-lg-6">
    <label for="inputEmail" class="  control-label">*Phone </label>
    <div class=" ">                                        
        <input name="phone" type="text" value="<?php echo isset($record->phone) ? $record->phone : ''; ?>" class="form-control" required>
    </div>
</div>

<div class="col-lg-6">
    <label for="inputEmail" class="  control-label">*Address </label>
    <div class=" ">                                        
        <input type="text" name="address" class="form-control" value="<?php echo isset($record->address) ? $record->address : ''; ?>">
    </div>
</div>
    
<div class="col-lg-6">
    <label for="inputCity" class="  control-label">City </label>
    <div class=" ">                                        
        <input type="text" name="city" class="form-control" value="<?php echo isset($record->city) ? $record->city : ''; ?>">
    </div>
</div>
        
<div class="col-lg-6">
    <label for="inputProvince" class="  control-label">Province </label>
    <div class=" ">                                        
        <input type="text" name="province" class="form-control" value="<?php echo isset($record->province) ? $record->province : ''; ?>">
    </div>
</div>        
        
<div class="col-lg-12">
    <label for="inputEmail" class="  control-label">*Gender </label>
    <div class=" ">
        <input type="radio" name="gender" value="male" <?php echo isset($record->gender) && ($record->gender == 'male') ? 'checked' : ''; ?>> Male&nbsp;&nbsp;
        <input type="radio" name="gender" value="female" <?php echo isset($record->gender) && ($record->gender == 'female') ? 'checked' : ''; ?>> Female<br><br>
    </div>
</div>

</div>
</div>
 
             <!--<div class="form-group">
                    <label for="inputEmail" class="  control-label">Upload Profile Picture</label>
                    <div class=" ">
                                            
                                            <input name="profile_pic" type="file" value="" class="form-control">
                    </div>
                </div>-->

                            <div class="form-group">
                    <label for="inputEmail" class="  control-label">Upload CV</label>
                    <div class=" ">
                                            
                                            <input name="cv" type="file" value="" class="form-control">
                    </div>
                </div>

 
   <div class="personal-detail">
    <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Academic qualifications</h3>
    <div class="row main-qualification-section">
    <?php 
    
    
    if($record->academics) : 
    $count = 1;
    
      
        
      
    foreach($record->academics as $key => $row) :?>
    <input type="hidden" name="aqcount" id="aqcount" value="<?php echo count($record->academics) +1; ?>" /> 
    <div class="count-ac col-lg-12"><span><?php echo $count; $count++;?></span></div>
    <div class="acadmeic-quali-repeater" id="repeatID1">
        <div class="col-lg-4">
    
            <label for="inputEmail" class="  control-label"><?php echo ($key == 0) ? '*Qualification' : '*Qualification';?> </label>
            <div class=" ">
                <input name="qualification[]" value="<?php echo isset($row->qualification) ? $row->qualification : ''; ?>" class="form-control" required>
            </div>
 
        </div>
        <div class="col-lg-4">
    
            <label for="inputEmail" class="  control-label"><?php echo ($key == 0) ? '*Institution' : '*Institution ';?> </label>
            <div class=" ">
                <input name="institution[]" type="text" value="<?php echo isset($row->institution) ? $row->institution : ''; ?>" class="form-control" required>
            </div>
 
        </div>
        <div class="col-lg-4">
    
            <label for="inputEmail" class="  control-label"><?php echo ($key == 0) ? '*Year' : ' *Year';?> </label>
            <div class=" ">
                <input name="year[]" type="text" value="<?php echo isset($row->year) ? $row->year : ''; ?>" class="form-control" required>
            </div>
 
        </div>
         
        </div>
    <?php endforeach; ?>
    <?php else : ?>
    <input type="hidden" name="aqcount" id="aqcount" value="2" /> 
    <div class="count-ac col-lg-12"><span>1</span></div>
    <div class="acadmeic-quali-repeater" id="repeatID1">
        <div class="col-lg-4">
    
            <label for="inputEmail" class="  control-label">*Qualification </label>
            <div class=" ">
                <input name="qualification[]" value="<?php echo isset($row->qualification) ? $row->qualification : ''; ?>" class="form-control" required>
            </div>
 
        </div>
        <div class="col-lg-4">
    
            <label for="inputEmail" class="  control-label">*Institution </label>
            <div class=" ">
                <input name="institution[]" type="text" value="<?php echo isset($row->institution) ? $row->institution : ''; ?>" class="form-control" required>
            </div>
 
        </div>
        <div class="col-lg-4">
    
            <label for="inputEmail" class="  control-label">*Year  </label>
            <div class=" ">
                <input name="year[]" type="text" value="<?php echo isset($row->year) ? $row->year : ''; ?>" class="form-control" required>
            </div>
 
        </div>
         
        </div> 
    <?php endif; ?>

    <div id="aq-repeter"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0)" class="add-more-academic-link submit-button pull-right">Add More Academic</a></div>
        
     </div>
    </div>
       <div class="personal-detail">
    <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Biography</h3>
    <div class="row">
  
        <div class="col-lg-12">
 
    <label for="inputEmail" class="  control-label">Description <span style="font-size:10px;">(Maximum 150 Words)</span> </label>
    <textarea class="form-control" id="content" name="description" ><?php echo isset($record->description) ? $record->description : ''; ?></textarea>
     
</div>


</div></div>


                <div class="personal-detail">
    <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Type</h3>
    <div class="row">
  <div class="col-lg-12">
    <label for="inputEmail" class="  control-label">*Area of training </label>
    <div class=" ">
        <input disabled="disabled" id="type-of-doctor-2" type="radio" name="type_of_doctor" value="2" <?php echo isset($record->type_of_doctor) && ($record->type_of_doctor == 'psychologist') ? 'checked' : '' ;?>> Psychologist &nbsp;&nbsp;
        <input disabled="disabled" id="type-of-doctor-1" type="radio" name="type_of_doctor" value="1" <?php echo isset($record->type_of_doctor) && ($record->type_of_doctor == 'nutritionist') ? 'checked' : '' ;?>> Nutritionist &nbsp;&nbsp;
        <input disabled="disabled" id="type-of-doctor-3" type="radio" name="type_of_doctor" value="3" <?php echo isset($record->type_of_doctor) && ($record->type_of_doctor == 'medical-doctor') ? 'checked' : '' ;?>> Medical Doctor &nbsp;&nbsp;
    </div>
</div>
  <div class="clearfix"></div>
<br /> 
      <div class="clearfix"></div>

<div class="col-lg-6" id="only-medical-doctor-1">
    <label for="inputEmail" class="  control-label">Profession Number </label>
    <div class=" ">                                        
        <input name="profession_number" type="text" value="<?php echo isset($record->profession_number) ? $record->profession_number : ''; ?>" class="form-control">
    </div>
</div>

<div class="col-lg-6" id="only-medical-doctor-2">
    <label for="inputEmail" class="  control-label">Practice Number </label>
    <div class=" ">                                        
        <input name="practice_number" type="text" value="<?php echo isset($record->practice_number) ? $record->practice_number : ''; ?>" class="form-control">
    </div>
</div>     

<div  class="col-lg-6">
<label for="inputEmail" class="  control-label"> Speciality </label>
    <div class=" ">
        <input name="speciality" type="text" value="<?php echo isset($record->speciality) ? $record->speciality : ''; ?>" class="form-control">
    </div>
    </div>
</div></div>

<div class="personal-detail">
    <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Working Experience</h3>
    <div class="row">
  <?php 
  
  //echo '<pre>'; print_r($record->experience); die;
  
  $result_for_exp_check_present = array();
  
    if($record->experience) :
        
        
        $condition = 'user_id='.$_SESSION['logged_in']['id'].' AND present_here = 1';
        $this->db->from('doctor_experience');
        $this->db->where($condition);
        $query = $this->db->get();
        
        $result_for_exp_check_present = $query->result();
        
        
    $count=1;
    $checkUniq = 1;
    $linkButtonStatus = '';
    foreach($record->experience as $key => $row) : ?>
    <input type="hidden" name="wecount" id="wecount" value="<?php echo count($record->experience) + 1;?>" /> 
    <div class="count-we col-lg-12"><span><?php echo $count; $count += 1;?></span></div>
  <div class="exp-repeat" id="repeatID2">
        <div class="col-lg-3">
 <label for="inputEmail" class="  control-label"><?php echo ($key == 0) ? '*Company' : '*Company';?> </label>
    <div class=" ">
        <input name="company[]" type="text" value="<?php echo isset($row->company) ? $row->company : ''; ?>" class="form-control" required>
    </div>
</div>
 <div class="col-lg-3">
 <label for="inputEmail" class="  control-label"><?php echo ($key == 0) ? '*Job Title' : '*Job Title';?> </label>
    <div class=" ">
        <input name="job_title[]" type="text" value="<?php echo isset($row->job_title) ? $row->job_title : ''; ?>" class="form-control" required>
    </div>
</div>
 <div class="col-lg-2 select-half-on">
 <label for="inputEmail" class="  control-label"><?php echo ($key == 0) ? '*Start Date' : '*Start Date ';?> </label>
    <div class=" ">
        <select name="start_mm[]" class="form-control" id="select">
            <option value="">Month</option>
            <option value="01" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '01' ? 'selected' : ''; ?>>Jan</option>
            <option value="02" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '02' ? 'selected' : ''; ?>>Feb</option>
            <option value="03" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '03' ? 'selected' : ''; ?>>Mar</option>
            <option value="04" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '04' ? 'selected' : ''; ?>>Apr</option>
            <option value="05" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '05' ? 'selected' : ''; ?>>May</option>
            <option value="06" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '06' ? 'selected' : ''; ?>>June</option>
            <option value="07" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '07' ? 'selected' : ''; ?>>July</option>
            <option value="08" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '08' ? 'selected' : ''; ?>>Aug</option>
            <option value="09" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '09' ? 'selected' : ''; ?>>Sep</option>
            <option value="10" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '10' ? 'selected' : ''; ?>>Oct</option>
            <option value="11" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '11' ? 'selected' : ''; ?>>Nov</option>
            <option value="12" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '12' ? 'selected' : ''; ?>>Dec</option>
        </select>
        <select name="start_yy[]" class="form-control" id="select">
            <option value="">Year</option>
            <?php for($i=date('Y')-80; $i<=date('Y'); $i++): ?>
                <option value="<?php echo $i; ?>" <?php echo isset($row->start_date) && date('Y', strtotime($row->start_date)) == $i ? 'selected' : ''; ?>><?php echo $i; ?></option>
            <?php endfor; ?>
        </select>
        
        <!--input name="start_date[]" type="text" value="?php echo isset($row->start_date) ? $row->start_date : ''; ?>" class="form-control" required-->
    </div>
</div>

<div class="col-lg-2 select-half-on" id="end_date_wrapper_<?php echo $checkUniq; ?>" style="display:<?php echo ($row->present_here == 1) ? 'none' : 'block'; ?>">
 <label for="inputEmail" class="  control-label"><?php echo ($key == 0) ? '*End Date' : '*End Date';?> </label>
    <div class=" " >
        <select name="end_mm[]" class="form-control" id="select">
            <option value="">Month</option>
            <option value="01" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '01' ? 'selected' : ''; ?>>Jan</option>
            <option value="02" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '02' ? 'selected' : ''; ?>>Feb</option>
            <option value="03" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '03' ? 'selected' : ''; ?>>Mar</option>
            <option value="04" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '04' ? 'selected' : ''; ?>>Apr</option>
            <option value="05" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '05' ? 'selected' : ''; ?>>May</option>
            <option value="06" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '06' ? 'selected' : ''; ?>>June</option>
            <option value="07" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '07' ? 'selected' : ''; ?>>July</option>
            <option value="08" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '08' ? 'selected' : ''; ?>>Aug</option>
            <option value="09" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '09' ? 'selected' : ''; ?>>Sep</option>
            <option value="10" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '10' ? 'selected' : ''; ?>>Oct</option>
            <option value="11" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '11' ? 'selected' : ''; ?>>Nov</option>
            <option value="12" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '12' ? 'selected' : ''; ?>>Dec</option>
        </select>
        <select name="end_yy[]" class="form-control" id="select">
            <option value="">Year</option>
            <?php for($i=date('Y')-80; $i<=date('Y'); $i++): ?>
                <option value="<?php echo $i; ?>" <?php echo isset($row->end_date) && date('Y', strtotime($row->end_date)) == $i ? 'selected' : ''; ?>><?php echo $i; ?></option>
            <?php endfor; ?>
        </select>
        <!--input name="end_date[]" type="text" value="?php echo isset($row->end_date) ? $row->end_date : ''; ?>" class="form-control" required-->
    </div>
</div>
      <?php  
      $allChekBoxStatus = '';
      if($result_for_exp_check_present)
      {
          if($row->present_here)
          {
              $allChekBoxStatus = "";
          }
          else
          {
              $allChekBoxStatus = "disabled='disabled'";
          }    
      }    
        //$allChekBoxStatus  = ($row->present_here == 1) ? 'show' : 'none';
        //$linkButtonStatus  = ($row->present_here == 1) ? 'none' : 'block';
      ?>
<div class="col-lg-2 select-half-on">
 <label for="inputEmail" class="  control-label"> Present </label>
    <div class=" "> 
            <input name="stay_here[]" type="checkbox" value="<?php echo $checkUniq; ?>" class="pull-left present_here" id="present_here_<?php echo $checkUniq; ?>" <?php echo ($row->present_here == 1) ? 'checked' : ''; ?>  
                data-indexof="<?php echo $checkUniq; ?>" onclick="present_here(this.id);" <?php echo $allChekBoxStatus;?> />
    </div>
 
</div>      
    <?php  
    
   
    
    $checkUniq++; //update counter
    //echo '<h1>'.$row->present_here.'</h1><br/>'; 
    ?>  
</div>
    
<?php endforeach; ?>
<?php else : ?>
    <input type="hidden" name="wecount" id="wecount" value="2" /> 
    <div class="count-we col-lg-12"><span>1</span></div>
    <div class="exp-repeat" id="repeatID2">
        <div class="col-lg-3">
 <label for="inputEmail" class="  control-label">*Company </label>
    <div class=" ">
        <input name="company[]" type="text" value="<?php echo isset($row->company) ? $row->company : ''; ?>" class="form-control" required>
    </div>
</div>
 <div class="col-lg-3">
 <label for="inputEmail" class="  control-label">*Job Title </label>
    <div class=" ">
        <input name="job_title[]" type="text" value="<?php echo isset($row->job_title) ? $row->job_title : ''; ?>" class="form-control" required>
    </div>
</div>
 <div class="col-lg-2 select-half-on">
 <label for="inputEmail" class="  control-label">*Start Date </label>
    <div class=" ">
        <select name="start_mm[]" class="form-control" id="select">
            <option value="">Month</option>
            <option value="01" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '01' ? 'selected' : ''; ?>>Jan</option>
            <option value="02" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '02' ? 'selected' : ''; ?>>Feb</option>
            <option value="03" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '03' ? 'selected' : ''; ?>>Mar</option>
            <option value="04" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '04' ? 'selected' : ''; ?>>Apr</option>
            <option value="05" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '05' ? 'selected' : ''; ?>>May</option>
            <option value="06" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '06' ? 'selected' : ''; ?>>June</option>
            <option value="07" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '07' ? 'selected' : ''; ?>>July</option>
            <option value="08" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '08' ? 'selected' : ''; ?>>Aug</option>
            <option value="09" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '09' ? 'selected' : ''; ?>>Sep</option>
            <option value="10" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '10' ? 'selected' : ''; ?>>Oct</option>
            <option value="11" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '11' ? 'selected' : ''; ?>>Nov</option>
            <option value="12" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '12' ? 'selected' : ''; ?>>Dec</option>
        </select>
        <select name="start_yy[]" class="form-control" id="select">
            <option value="">Year</option>
            <?php for($i=date('Y')-80; $i<=date('Y'); $i++): ?>
                <option value="<?php echo $i; ?>" <?php echo isset($row->start_date) && date('Y', strtotime($row->start_date)) == $i ? 'selected' : ''; ?>><?php echo $i; ?></option>
            <?php endfor; ?>
        </select>
        <!--input name="start_date[]" type="text" placeholder="DD-MM-YYYY" value="?php echo isset($row->start_date) ? $row->start_date : ''; ?>" class="form-control" required-->
    </div>
</div>

<div class="col-lg-2 select-half-on">
 <label for="inputEmail" class="  control-label">*End Date</label>
    <div class=" ">
        <select name="end_mm[]" class="form-control" id="select">
            <option value="">Month</option>
            <option value="01" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '01' ? 'selected' : ''; ?>>Jan</option>
            <option value="02" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '02' ? 'selected' : ''; ?>>Feb</option>
            <option value="03" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '03' ? 'selected' : ''; ?>>Mar</option>
            <option value="04" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '04' ? 'selected' : ''; ?>>Apr</option>
            <option value="05" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '05' ? 'selected' : ''; ?>>May</option>
            <option value="06" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '06' ? 'selected' : ''; ?>>June</option>
            <option value="07" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '07' ? 'selected' : ''; ?>>July</option>
            <option value="08" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '08' ? 'selected' : ''; ?>>Aug</option>
            <option value="09" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '09' ? 'selected' : ''; ?>>Sep</option>
            <option value="10" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '10' ? 'selected' : ''; ?>>Oct</option>
            <option value="11" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '11' ? 'selected' : ''; ?>>Nov</option>
            <option value="12" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '12' ? 'selected' : ''; ?>>Dec</option>
        </select>
        <select name="end_yy[]" class="form-control" id="select">
            <option value="">Year</option>
            <?php for($i=date('Y')-80; $i<=date('Y'); $i++): ?>
                <option value="<?php echo $i; ?>" <?php echo isset($row->end_date) && date('Y', strtotime($row->end_date)) == $i ? 'selected' : ''; ?>><?php echo $i; ?></option>
            <?php endfor; ?>
        </select>
        
        
        <!--input name="end_date[]" type="text" placeholder="DD-MM-YYYY" value="?php echo isset($row->end_date) ? $row->end_date : ''; ?>" class="form-control" required-->
    </div>
</div>
        
 <div class="col-lg-2 select-half-on">
    <label for="inputEmail" class="control-label"> Present here </label>
    <div class=" "> 
            <input 
                name="stay_here[]" 
                type="checkbox" 
                value="1" 
                class="present_here pull-left"
                id="present_here_1"   
                data-indexof="1" 
                onclick="present_here(this.id);" 
             />
    </div>
 </div> 
</div>
<?php endif; 
    // if all the chcek box is hide 
  /*  if($linkButtonStatus == 'block') { ?> <style> .present_here{ display:block !important; }</style> <?php } */ ?>

 
    
    
<div id="we-repeter"></div>
<div class="add-academic col-lg-12" ><a href="javascript:void(0)" class="add-more-exp-link submit-button pull-right" style="display:<?php echo $linkButtonStatus; ?>">Add More Experience</a></div>
</div></div> 
 
                                
<!--div class="form-group">
    <label for="inputEmail" class="  control-label"> Speciality </label>
    <div class=" ">
        <input name="speciality" type="text" value="" class="form-control">
    </div>
</div-->

             
                                 

                           <!-- <div class="form-group">
                    <label for="inputEmail" class="  control-label">*Upload CV</label>
                    <div class=" ">
                                            
                                            <input name="cv" type="file" value="" class="form-control" required>
                    </div>
                </div> -->
                               
                            

<div class="form-group">
<div class="day-container">
    <div class="senior-airman">
       <dl class="dropdown"> 
            <dt>
               <label for="inputEmail" class="control-label">*Days</label>
            </dt>
            <dd>
                <div class="mutliSelect">
                    <ul>
                       <li>
                            <input type="checkbox" name="dayoftheweek[]" value="1" <?php echo (isset($doctorDays) && in_array('monday', $doctorDays)) ? 'checked' : ''; ?>/><label>Monday</label></li>
                        <li>
                            <input type="checkbox" name="dayoftheweek[]" value="2" <?php echo (isset($doctorDays) && in_array('tuesday', $doctorDays)) ? 'checked' : ''; ?>/><label>Tuesday</label></li>
                        <li>
                            <input type="checkbox" name="dayoftheweek[]" value="3" <?php echo (isset($doctorDays) && in_array('wednesday', $doctorDays)) ? 'checked' : ''; ?>/><label>Wednesday<label></li>
                        <li>
                            <input type="checkbox" name="dayoftheweek[]" value="4" <?php echo (isset($doctorDays) && in_array('thursday', $doctorDays)) ? 'checked' : ''; ?>/><label>Thursday<label></li>
                        <li>
                            <input type="checkbox" name="dayoftheweek[]" value="5" <?php echo (isset($doctorDays) && in_array('friday', $doctorDays)) ? 'checked' : ''; ?>/><label>Friday</label></li>
                        <li>
                            <input type="checkbox" name="dayoftheweek[]" value="6" <?php echo (isset($doctorDays) && in_array('saturday', $doctorDays)) ? 'checked' : ''; ?>/><label>Saturday</label></li>
                        <li>
                            <input type="checkbox" name="dayoftheweek[]" value="7" <?php echo (isset($doctorDays) && in_array('sunday', $doctorDays)) ? 'checked' : ''; ?>/><label>Sunday</label></li>
                    </ul>
                </div>
            </dd>
         </dl>
    </div>
</div>
</div>

<div class="form-group">
<label for="inputEmail" class="control-label">*Timings</label>
<select size="1" id="timings" title="" name="timings">
    <option value="">Select your timing</option>
    <option value="fixed" <?php echo (count($doctorTimings) == 1) ? 'selected' : '' ;?>>Fixed</option>
    <option value="flexible" <?php echo (count($doctorTimings) == 2) ? 'selected' : '' ;?>>Flexible</option>
</select>

<div class="timing-container">
    <div class="fixed">
        <select name="from-timimg" id="from-timimg" class="from-timimg">
           <option value="">-From-</option>
            <option value="1 AM" <?php echo ($doctorTimings[0]->start_time=='01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
            <option value="2 AM" <?php echo ($doctorTimings[0]->start_time=='02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
            <option value="3 AM" <?php echo ($doctorTimings[0]->start_time=='03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
            <option value="4 AM" <?php echo ($doctorTimings[0]->start_time=='04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
            <option value="5 AM" <?php echo ($doctorTimings[0]->start_time=='05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
            <option value="6 AM" <?php echo ($doctorTimings[0]->start_time=='06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
            <option value="7 AM" <?php echo ($doctorTimings[0]->start_time=='07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
            <option value="8 AM" <?php echo ($doctorTimings[0]->start_time=='08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
            <option value="9 AM" <?php echo ($doctorTimings[0]->start_time=='09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
            <option value="10 AM" <?php echo ($doctorTimings[0]->start_time=='10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
            <option value="11 AM" <?php echo ($doctorTimings[0]->start_time=='11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
            <option value="12 PM" <?php echo ($doctorTimings[0]->start_time=='12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
            <option value="1 PM" <?php echo ($doctorTimings[0]->start_time=='13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
            <option value="2 PM" <?php echo ($doctorTimings[0]->start_time=='14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
            <option value="3 PM" <?php echo ($doctorTimings[0]->start_time=='15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
            <option value="4 PM" <?php echo ($doctorTimings[0]->start_time=='16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
            <option value="5 PM" <?php echo ($doctorTimings[0]->start_time=='17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
            <option value="6 PM" <?php echo ($doctorTimings[0]->start_time=='18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
            <option value="7 PM" <?php echo ($doctorTimings[0]->start_time=='19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
            <option value="8 PM" <?php echo ($doctorTimings[0]->start_time=='20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
            <option value="9 PM" <?php echo ($doctorTimings[0]->start_time=='21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
            <option value="10 PM" <?php echo ($doctorTimings[0]->start_time=='22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
            <option value="11 PM" <?php echo ($doctorTimings[0]->start_time=='23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
            <option value="12 AM" <?php echo ($doctorTimings[0]->start_time=='24:00:00') || ($doctorTimings[0]->end_time=='00:00:00') ? 'selected' : ''; ?>>12:00 AM</option>
        </select>

        <select name="to-timimg" id="to-timimg" class="from-timimg">
            <option value="">-To-</option>
            <option value="1 AM" <?php echo ($doctorTimings[0]->end_time=='01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
            <option value="2 AM" <?php echo ($doctorTimings[0]->end_time=='02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
            <option value="3 AM" <?php echo ($doctorTimings[0]->end_time=='03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
            <option value="4 AM" <?php echo ($doctorTimings[0]->end_time=='04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
            <option value="5 AM" <?php echo ($doctorTimings[0]->end_time=='05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
            <option value="6 AM" <?php echo ($doctorTimings[0]->end_time=='06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
            <option value="7 AM" <?php echo ($doctorTimings[0]->end_time=='07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
            <option value="8 AM" <?php echo ($doctorTimings[0]->end_time=='08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
            <option value="9 AM" <?php echo ($doctorTimings[0]->end_time=='09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
            <option value="10 AM" <?php echo ($doctorTimings[0]->end_time=='10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
            <option value="11 AM" <?php echo ($doctorTimings[0]->end_time=='11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
            <option value="12 PM" <?php echo ($doctorTimings[0]->end_time=='12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
            <option value="1 PM" <?php echo ($doctorTimings[0]->end_time=='13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
            <option value="2 PM" <?php echo ($doctorTimings[0]->end_time=='14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
            <option value="3 PM" <?php echo ($doctorTimings[0]->end_time=='15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
            <option value="4 PM" <?php echo ($doctorTimings[0]->end_time=='16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
            <option value="5 PM" <?php echo ($doctorTimings[0]->end_time=='17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
            <option value="6 PM" <?php echo ($doctorTimings[0]->end_time=='18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
            <option value="7 PM" <?php echo ($doctorTimings[0]->end_time=='19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
            <option value="8 PM" <?php echo ($doctorTimings[0]->end_time=='20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
            <option value="9 PM" <?php echo ($doctorTimings[0]->end_time=='21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
            <option value="10 PM" <?php echo ($doctorTimings[0]->end_time=='22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
            <option value="11 PM" <?php echo ($doctorTimings[0]->end_time=='23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
            <option value="12 AM" <?php echo ($doctorTimings[0]->end_time=='24:00:00') || ($doctorTimings[0]->end_time=='00:00:00') ? 'selected' : ''; ?>>12:00 AM</option>         
        </select>

    </div>
    
    <div class="flexible">
       <select name="flex-from-timing[]" id="from-timimg" class="from-timimg">
           <option value="">-From-</option>
           <option value="1 AM" <?php echo ($doctorTimings[0]->start_time=='01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
            <option value="2 AM" <?php echo ($doctorTimings[0]->start_time=='02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
            <option value="3 AM" <?php echo ($doctorTimings[0]->start_time=='03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
            <option value="4 AM" <?php echo ($doctorTimings[0]->start_time=='04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
            <option value="5 AM" <?php echo ($doctorTimings[0]->start_time=='05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
            <option value="6 AM" <?php echo ($doctorTimings[0]->start_time=='06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
            <option value="7 AM" <?php echo ($doctorTimings[0]->start_time=='07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
            <option value="8 AM" <?php echo ($doctorTimings[0]->start_time=='08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
            <option value="9 AM" <?php echo ($doctorTimings[0]->start_time=='09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
            <option value="10 AM" <?php echo ($doctorTimings[0]->start_time=='10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
            <option value="11 AM" <?php echo ($doctorTimings[0]->start_time=='11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
            <option value="12 PM" <?php echo ($doctorTimings[0]->start_time=='12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
            <option value="1 PM" <?php echo ($doctorTimings[0]->start_time=='13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
            <option value="2 PM" <?php echo ($doctorTimings[0]->start_time=='14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
            <option value="3 PM" <?php echo ($doctorTimings[0]->start_time=='15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
            <option value="4 PM" <?php echo ($doctorTimings[0]->start_time=='16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
            <option value="5 PM" <?php echo ($doctorTimings[0]->start_time=='17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
            <option value="6 PM" <?php echo ($doctorTimings[0]->start_time=='18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
            <option value="7 PM" <?php echo ($doctorTimings[0]->start_time=='19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
            <option value="8 PM" <?php echo ($doctorTimings[0]->start_time=='20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
            <option value="9 PM" <?php echo ($doctorTimings[0]->start_time=='21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
            <option value="10 PM" <?php echo ($doctorTimings[0]->start_time=='22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
            <option value="11 PM" <?php echo ($doctorTimings[0]->start_time=='23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
            <option value="12 AM" <?php echo ($doctorTimings[0]->start_time=='24:00:00') || ($doctorTimings[0]->end_time=='00:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
            
        </select>
        
        <select name="flex-to-timing[]" id="to-timimg" class="from-timimg">
           <option value="">-To-</option>
            <option value="1 AM" <?php echo ($doctorTimings[0]->end_time=='01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
            <option value="2 AM" <?php echo ($doctorTimings[0]->end_time=='02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
            <option value="3 AM" <?php echo ($doctorTimings[0]->end_time=='03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
            <option value="4 AM" <?php echo ($doctorTimings[0]->end_time=='04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
            <option value="5 AM" <?php echo ($doctorTimings[0]->end_time=='05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
            <option value="6 AM" <?php echo ($doctorTimings[0]->end_time=='06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
            <option value="7 AM" <?php echo ($doctorTimings[0]->end_time=='07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
            <option value="8 AM" <?php echo ($doctorTimings[0]->end_time=='08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
            <option value="9 AM" <?php echo ($doctorTimings[0]->end_time=='09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
            <option value="10 AM" <?php echo ($doctorTimings[0]->end_time=='10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
            <option value="11 AM" <?php echo ($doctorTimings[0]->end_time=='11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
            <option value="12 PM" <?php echo ($doctorTimings[0]->end_time=='12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
            <option value="1 PM" <?php echo ($doctorTimings[0]->end_time=='13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
            <option value="2 PM" <?php echo ($doctorTimings[0]->end_time=='14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
            <option value="3 PM" <?php echo ($doctorTimings[0]->end_time=='15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
            <option value="4 PM" <?php echo ($doctorTimings[0]->end_time=='16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
            <option value="5 PM" <?php echo ($doctorTimings[0]->end_time=='17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
            <option value="6 PM" <?php echo ($doctorTimings[0]->end_time=='18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
            <option value="7 PM" <?php echo ($doctorTimings[0]->end_time=='19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
            <option value="8 PM" <?php echo ($doctorTimings[0]->end_time=='20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
            <option value="9 PM" <?php echo ($doctorTimings[0]->end_time=='21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
            <option value="10 PM" <?php echo ($doctorTimings[0]->end_time=='22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
            <option value="11 PM" <?php echo ($doctorTimings[0]->end_time=='23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
            <option value="12 AM" <?php echo ($doctorTimings[0]->end_time=='24:00:00') || ($doctorTimings[0]->end_time=='00:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
            
        </select>
 
  <div class="text-center">  AND</div>     
 
         <select name="flex-from-timing[]" id="from-timimg" class="from-timimg">
           <option value="">-From-</option>
            <option value="1 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
            <option value="2 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
            <option value="3 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
            <option value="4 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
            <option value="5 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
            <option value="6 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
            <option value="7 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
            <option value="8 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
            <option value="9 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
            <option value="10 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
            <option value="11 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
            <option value="12 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
            <option value="1 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
            <option value="2 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
            <option value="3 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
            <option value="4 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
            <option value="5 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
            <option value="6 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
            <option value="7 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
            <option value="8 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
            <option value="9 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
            <option value="10 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
            <option value="11 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
            <option value="12 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->start_time=='24:00:00') || ($doctorTimings[0]->end_time=='00:00:00') ? 'selected' : ''; ?>>12:00 AM</option>
            
        </select>
        
        <select name="flex-to-timing[]" id="to-timimg" class="from-timimg">
           <option value="">-To-</option>
            <option value="1 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
            <option value="2 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
            <option value="3 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
            <option value="4 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
            <option value="5 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
            <option value="6 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
            <option value="7 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
            <option value="8 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
            <option value="9 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
            <option value="10 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
            <option value="11 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
            <option value="12 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
            <option value="1 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
            <option value="2 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
            <option value="3 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
            <option value="4 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
            <option value="5 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
            <option value="6 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
            <option value="7 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
            <option value="8 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
            <option value="9 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
            <option value="10 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
            <option value="11 PM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
            <option value="12 AM" <?php echo isset($doctorTimings[1]) && ($doctorTimings[1]->end_time=='24:00:00') || ($doctorTimings[0]->end_time=='00:00:00') ? 'selected' : ''; ?>>12:00 AM</option>
            
        </select>
        
    </div>
</div>
</div>

<button type="submit" class="btn btn-default submit-button">Submit</button>
<!--<div class="second-level-container">
    <div class="basic-ore-1">
        Line of text for basic ore miner 1
    </div>
    <div class="basic-ore-2">
        Line of text for basic ore miner 2
    </div>
    <div class="omber-miner-1">
        Line of text for omber miner 1
    </div>
    <div class="omber-miner-2">
        Line of text for omber miner 2
    </div>    
</div>-->


     <?php echo form_close(); ?>
        </div>
    </div>
   </div>
<script type="text/javascript">


    $("#doctorinfo").validate({rules: {
            'dayoftheweek[]': {
                required: true,
                minlength: 1
            }
            
        },
        
        messages: {
            'dayoftheweek[]': {
                required: "You must check at least 1 box",
                maxlength: "Check no more than {0} boxes"
            }
        }
    });
</script>
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script> 
<script type="text/javascript">
$(document).ready(function() {
    $(document).on('click change', '#doctorinfo,.exp-repeat, select', function(){
  var total_error = checkExprience();
   if(total_error > 0){
       return false;
   }
});



function checkExprience(){ // validate Experience 
   var count = 0;
   var total_error = 0;
   
   $( ".exp-repeat" ).each(function() {

        var startClass = 'start' + count; // classname define
        var endClass = 'end' + count;// classname define
        
        $('.'+endClass+'end_yy').remove(); // remove exist classname
        $('.'+startClass+'start_yy').remove(); // remove exist classname
        
        var error = 0;
        count++;  // use for classname unique define
        
        $(this).addClass(startClass); // add class
        
        var display_property = $('.'+ startClass +' select[name="end_mm[]"] ').parent().parent().css('display'); // check property of end date div for validation
        
        
        var start_month = $('.'+ startClass +' select[name="start_mm[]"] ').val();
        var start_year = $('.'+ startClass +' select[name="start_yy[]"] ').val();
        
        if(display_property == 'block'){ // start and end date compare
            var end_month = $('.'+ startClass +' select[name="end_mm[]"] ').val();
            var end_year = $('.'+ startClass +' select[name="end_yy[]"] ').val();


            if(end_year == '' || end_month == ''){
                $('.'+ startClass +' select[name="end_yy[]"] ').after('<label id="end_yy-error" class="error '+endClass+'end_yy" for="end_yy">Please select end date.</label>');
                error++;
            }
            
            if(start_year == '' || start_month == ''){
                $('.'+ startClass +' select[name="start_yy[]"] ').after('<label id="start_yy-error" class="error '+startClass+'start_yy" for="end_yy">Please select start date.</label>');
                error++;
            }
        
            if(error == 0){
                if(start_year > end_year || (start_year == end_year && start_month > end_month)){
                   $('.'+ startClass +' select[name="end_yy[]"] ').after('<label id="end_yy-error" class="error '+endClass+'end_yy" for="end_yy">End date Should be greater than Start date.</label>');
                   error++;
                }
            }
            
        }else if(start_year == '' || start_month == ''){ // check present data
            $('.'+ startClass +' select[name="start_yy[]"] ').after('<label id="start_yy-error" class="error '+startClass+'start_yy" for="end_yy">Please select start date.</label>');
            error++;
        }
    
        total_error = total_error + error; // count errors
    
   });

   
        return total_error;
   
}
    $('#Rank').bind('change', function() {
        var elements = $('div.day-container').children().hide(); // hide all the elements
        var value = $(this).val();

        if (value.length) { // if somethings' selected
            elements.filter('.' + value).show(); // show the ones we want
        }
    }).trigger('change');
    /*
    $('#type-of-doctor-3').bind('change', function() {
             $('#only-medical-doctor-1').show();
             $('#only-medical-doctor-2').show();
     });
     $('#type-of-doctor-1').bind('change', function() {
             $('#only-medical-doctor-1').hide();
             $('#only-medical-doctor-2').hide();
     });
     $('#type-of-doctor-2').bind('change', function() {
             $('#only-medical-doctor-1').hide();
             $('#only-medical-doctor-2').hide();
     });
    */
     $('#timings').bind('change', function() {
        var elements = $('div.timing-container').children().hide(); // hide all the elements
        var value = $(this).val();

        if (value.length) { // if somethings' selected
            elements.filter('.' + value).show(); // show the ones we want
        }
    }).trigger('change');
//    $('.second-level-select').bind('change', function() {
//        var elements = $('div.second-level-container').children().hide(); // hide all the elements
//        var value = $(this).val();
//
//        if (value.length) { // if somethings' selected
//            elements.filter('.' + value).show(); // show the ones we want
//        }
//    }).trigger('change');
    
    
    
//    $('.second-level-select').bind('change', function() {
//        var elements = $('div.second-level-container').children().hide(); // hide all the elements
//        var value = $(this).val();
//
//        if (value.length) { // if somethings' selected
//            elements.filter('.' + value).show(); // show the ones we want
//        }
//    }).trigger('change');
    
    
    
});

$(function() {
    /*$('#doctorinfo').submit(function() {
        var rank = $('#Rank').val();
        var doctorAirman = $('#doctor-'+rank).val();
        $()
        console.log(rank);
        console.log(doctorAirman);
        console.log('i am working');
        return false; // return false to cancel form action
    });*/
});
/*

$(".dropdown dt a").on('click', function() {
  $(".dropdown dd ul").slideToggle('fast');
});

$(".dropdown dd ul li a").on('click', function() {
  $(".dropdown dd ul").hide();
});

function getSelectedValue(id) {
  return $("#" + id).find("dt a span.value").html();
}*/

/*
$(document).bind('click', function(e) {
  var $clicked = $(e.target);
  if (!$clicked.parents().hasClass("dropdown")){
  // alert('Here');
   //$(".dropdown dd ul").hide();
}
});*/

/*************** Closing now 
$('.mutliSelect input[type="checkbox"]').on('click', function() {
  
  var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
    title = $(this).next('label').text() + "  ";
  if ($(this).is(':checked')) {

    var html = '<span title="' + title + '">' + title + '</span>';
    $('.multiSel').show();
    $('.multiSel').append(html);
    $(".hida").hide();
  } else {
    $('span[title="' + title + '"]').remove();
    var ret = $(".hida");
    $('.dropdown dt a').append(ret);

  }
});
*/
  function delete_md() {
              document.getElementById("del_md").submit();
             }
             
             
             
             
function readURL(input) {

    if (input.files && input.files[0]) {
        
         var url = input.value;
    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        
        
        if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#pro-pic-id').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            
        }
        else
        {
            alert("Please upload a valid image file.");
            $("#add-pro-pic").val('');
        }    
        
    }
}

$("#add-pro-pic").change(function(){
    readURL(this);
});
             
</script>