<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />

    <!-- jQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <!-- Firebase -->
    <script src="https://www.gstatic.com/firebasejs/3.4.0/firebase.js"></script>

    <!-- Firechat -->
    <link rel="stylesheet" href="https://cdn.firebase.com/libs/firechat/3.0.1/firechat.min.css" />
    <script src="https://cdn.firebase.com/libs/firechat/3.0.1/firechat.min.js"></script>

    <!-- Custom CSS -->
    <style>
      #firechat-wrapper {
        height: 475px;
        max-width: 325px;
        padding: 10px;
        border: 1px solid #ccc;
        background-color: #fff;
        margin: 50px auto;
        text-align: center;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        -webkit-box-shadow: 0 5px 25px #666;
        -moz-box-shadow: 0 5px 25px #666;
        box-shadow: 0 5px 25px #666;
      }
    </style>
  </head>

  <!--
    Example: Anonymous Authentication

    This example uses Firebase Simple Login to create "anonymous" user sessions in Firebase,
    meaning that user credentials are not required, though a user has a valid Firebase
    authentication token and security rules still apply.

    Requirements: in order to use this example with your own Firebase, you'll need to do the following:
      1. Apply the security rules at https://github.com/firebase/firechat/blob/master/rules.json
      2. Enable the "Anonymous" authentication provider in Forge
      3. Update the URL below to reference your Firebase
      4. Update the room id for auto-entry with a public room you have created
   -->
  <body>
    <div id="firechat-wrapper"></div>

    <script>
        // Initialize Firebase
        var config = {
          apiKey: "AIzaSyDR4pBuYM0qz-KeR4nr-Hb7fx1xSSimTnc",
          authDomain: "seeadoctor-2a576.firebaseapp.com",
          databaseURL: "https://seeadoctor-2a576.firebaseio.com",
          storageBucket: "seeadoctor-2a576.appspot.com",
          messagingSenderId: "815158931735",
        };
        firebase.initializeApp(config);
        var chatRef = firebase.database().ref("firebase");
        var chatAvailable = 'FALSE';
        var chatTitle = 'MyChat';
        var roomId = '';

    // Listen for authentication state changes
      firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          roomChatSetup(user);
        } else {
         //  If the user is not logged in, sign them in anonymously
         firebase.auth().signInAnonymously().catch(function(error) {
            console.log("Error signing user in anonymously:", error);
          });
        }
      });

      function roomChatSetup(user) {

        var chat = new Firechat(chatRef);

        // If room already present get reference
        chat.getRoomList(function (roomList) {
          console.log(roomList);         
          for (var key in roomList) 
          { 
            var title = roomList[key]['name'];
            if(title == chatTitle) {
              chatAvailable = 'TRUE';
              roomId = roomList[key];
              console.log(roomId);
            } else {
              console.log(mychat + ' : ' + roomList[key]['name']);
            }
          }
        });

        if(chatAvailable=='FALSE') {   
          alert('creating new chat room');
          chat.createRoom(chatTitle, "private", function(roomId) {
            chat.enterRoom(roomId);
          });
        }
        else {
          alert('ROOOOOOOOOOOOM FOUND');
          chat.enterRoom(roomId);
        }
        initChat(user);
        /*
        chat.setUser(user.uid, "<?php echo $username;?>", function(user) {
            console.log('Creating Room');
            chat.createRoom("MyChat", "private", function(roomId) {
            console.log('Entering Room');
            chat.enterRoom(roomId);
            console.log('Initializing chat');
          });
        initChat(user);
       });*/
      }

      function initChat(user) {

        // Create a Firechat instance
        var chat = new FirechatUI(chatRef, document.getElementById("firechat-wrapper"));

        chat.setUser(user.uid, "<?php echo $username;?>", function() {
        });

        //chat.createRoom("TestAkifWaqar", "public");
        // Set the Firechat user

      }

      //var roomId = chat.createRoom("TestAkifWaqar", "public");

      //chat.enterRoom(roomId);
  
    </script>
  </body>
</html>