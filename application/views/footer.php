    <footer>
      <div class="container">
      	<div class="footer-nav">
        	<div class="foot-nav-inner">
            	<ul>
                	 <!--li><a href="#">Meet Doctors</a></li-->
                    <li><a href="<?php echo base_url() . 'page/show/faqs'; ?>">FAQs</a></li>
                    <li><a href="<?php echo base_url() . 'page/show/policy'; ?>">Privacy Policy</a></li>
                    <li class="border-0"><a href="<?php echo base_url() . 'page/show/terms'; ?>">Terms Of Use</a></li>
                </ul>
                    
            </div>
			<div class="clear"></div>
                        <div class="foot-contact text-center"><span class="footer-icon"><!--<img src="<?php echo base_url(); ?>images/footer-phone-icon.jpg" alt=""> Phone--></span><span class="footer-icon"><a href="mailto:info@seeadoctor.co.za"><!--<img src="<?php echo base_url();?>images/email-icon.jpg" alt="">--> info@seeadoctor.co.za</a></span>
                        
                        <span class="socail-icon"><a target="_blank" href="https://www.facebook.com/SeeADoctorSA"><img src="<?php echo base_url(); ?>images/fb-icon.png" alt=""></a></span>
                    <span class="socail-icon"><a target="_blank" href="https://twitter.com/SeeADoctorSA"><img src="<?php echo base_url(); ?>images/twitter-icon.png" alt=""></a></span>
                        </div>
        </div>
        <div class="footer-text">
        	<?php echo isset($footer->content) ? $footer->content : ''; ?>
        </div>
      </div>
    </footer>
    <div class="ajaxloader" style="display:none;"><img src="<?php echo base_url(); ?>/images/preloader.gif" /></div>
    <!-- bxSlider Javascript file -->
    <script src="<?php echo base_url(); ?>js/jquery.bxslider.min.js"></script>
   
    <link href="<?php echo base_url(); ?>css/jquery.bxslider.css" rel="stylesheet" />
    
     <!--<script src="<?php // echo base_url(); ?>js/js/jquery.js"></script>--> 
    <!--<script src="<?php // echo base_url(); ?>js/js/bootstrap.js"></script>--> 
    <script src="<?php echo base_url(); ?>js/js/responsive-tabs.js"></script> 
    <script src="<?php echo base_url(); ?>js/js/jquery.dataTables.min.js"></script> 
    <script src="<?php echo base_url(); ?>js/js/dataTables.bootstrap.min.js"></script> 
    <script type="text/javascript">

      $( 'ul.nav.nav-tabs  a' ).click( function ( e ) {
        e.preventDefault();
        $( this ).tab( 'show' );
      } );

      ( function( $ ) {
          // Test for making sure event are maintained
          $( '.js-alert-test' ).click( function () {
            alert( 'Button Clicked: Event was maintained' );
          } );
          fakewaffle.responsiveTabs( [ 'xs', 'sm' ] );
      } )( jQuery );

    </script> 
    <script>
            function myClick() {
            var _BASE_ = '<?php echo base_url();?>'
            window.setInterval(
              function() {
                  $.ajax(
                   {url: _BASE_+"Doctorsession/checkSession", success: function(result){
                        console.log(result);                        
                    }});
              }, 100000);
          }

 
      $(document).ready(function(){

		  $(function() {
			  
  $(".add-pro-pic-link").click(function() {
    $("#add-pro-pic").trigger('click');
  });
   $('#add-pro-pic').change(function(evt) {
        $(".prfoile-pic-update").show( );
    });
})
		 

        $('.add-more-academic-link').click(function(){
			var aqval = $("#aqcount").val();
          $('#aq-repeter').append('<div class="acadmeic-quali-repeater" id="repeatID1"><div class="count-ac col-lg-12"><span >'+aqval+'</span></div><div class="col-lg-4"><label for="inputEmail" class="  control-label">*Qualification</label><div class=" "><input name="qualification[]" type="text" value="" class="form-control" required></div></div><div class="col-lg-4"><label for="inputEmail" class="  control-label">*Institution</label><div class=" "><input name="institution[]" type="text" value="" class="form-control" required></div></div><div class="col-lg-4"><label for="inputEmail" class="  control-label">*Year</label><div class=" "><input name="year[]" type="text" value="" class="form-control" required></div></div></div>');
		  aqval = parseInt(aqval) + 1;
		  $("#aqcount").val(aqval);
		     
    
        });
	 
		
        $('.add-more-exp-link').click(function(){
                var weval = $("#wecount").val();
                $('#we-repeter').append('<div class="exp-repeat" id="repeatID2"><div class="count-we col-lg-12"><span >'+weval+'</span></div><div class="col-lg-3"><label for="inputEmail" class="  control-label">*Company</label><div class=" "><input name="company[]" type="text" value="" class="form-control" required></div></div><div class="col-lg-3"><label for="inputEmail" class="  control-label">*Job Title</label><div class=" "><input name="job_title[]" type="text" value="" class="form-control" required></div></div><div class="col-lg-2 select-half-on"><label for="inputEmail" class="  control-label">*Start Date</label><div class=" "><select name="start_mm[]" class="form-control" id="select" style=" height: 2.2em !important;"><option value="">Month</option><option value="01">Jan</option><option value="02">Feb</option><option value="03">Mar</option><option value="04">Apr</option><option value="05">May</option><option value="06">June</option><option value="07">July</option><option value="08">Aug</option><option value="09">Sep</option><option value="10">Oct</option><option value="11">Nov</option><option value="12">Dec</option></select><select name="start_yy[]" class="" id="select" style=" height: 2.2em !important;"><option value="">Year</option><?php for($i=date("Y")-80; $i<=date("Y"); $i++): ?><option value="<?php echo $i; ?>"><?php echo $i; ?></option><?php endfor; ?></select></div></div><div class="col-lg-2 select-half-on" id="end_date_wrapper_'+weval+'"><label for="inputEmail" class="  control-label">*End Date </label><div class=" "><select name="end_mm[]" class="form-control" id="select" style=" height: 2.2em !important;"><option value="">Month</option><option value="01" <?php echo date("m") == "01" ? "selected" : ''; ?>>Jan</option><option value="02" <?php echo date("m") == "02" ? "selected" : ''; ?>>Feb</option><option value="03" <?php echo date("m") == "03" ? "selected" : ''; ?>>Mar</option><option value="04" <?php echo date("m") == '04' ? "selected" : ''; ?>>Apr</option><option value="05" <?php echo date("m") == "05" ? "selected" : ''; ?>>May</option><option value="06" <?php echo date("m") == "06" ? "selected" : ''; ?>>June</option><option value="07" <?php echo date("m") == "07" ? "selected" : ''; ?>>July</option><option value="08" <?php echo date("m") == "08" ? "selected" : ''; ?>>Aug</option><option value="09" <?php echo date("m") == "09" ? "selected" : ''; ?>>Sep</option><option value="10" <?php echo date("m") == "10" ? "selected" : ''; ?>>Oct</option><option value="11" <?php echo date("m") == "11" ? "selected" : ''; ?>>Nov</option><option value="12" <?php echo date("m") == "12" ? "selected" : ''; ?>>Dec</option></select><select name="end_yy[]" class="form-control" id="select" style=" height: 2.2em !important;"><option value="">Year</option><?php for($i=date("Y")-80; $i<=date("Y"); $i++): ?><option value="<?php echo $i; ?>" <?php echo $i == date("Y") ? "selected" : ''; ?>><?php echo $i; ?></option><?php endfor; ?></select></div></div><div class="col-lg-2 select-half-on"><label class=" control-label" for="inputEmail"> Present </label><div class=""><input name="stay_here[]" value="'+weval+'" class="present_here pull-left" onclick="present_here(this.id);" id="present_here_'+weval+'" data-indexof="'+weval+'" type="checkbox"></div></div></div>');   
                
               
                var atLeastOneIsChecked = false;
  $('.present_here').each(function () {
       
    if ($(this).is(':checked')) {
      atLeastOneIsChecked = true;
      // Stop .each from processing any more items
      return false;
    }
  });
            
                    
                if(atLeastOneIsChecked)
                {
                    $("#present_here_"+weval).attr('disabled','disabled');
                }    
                
                
                weval = parseInt(weval) + 1;
                $("#wecount").val(weval);     
        });
        
        // hide and show based on checkbox checked
        present_here = function(id)
        {
            var indexOf     =  $("#"+id).data('indexof');
            var endwrapper  = "end_date_wrapper_"+indexOf;
            var checkstatus = $("#"+id).is(':checked') ? 1 : 0;
            if(checkstatus == 1){
                $(".present_here").attr('disabled','disabled');
               // $(".present_here").checked(false);
                $("#"+id).removeAttr('disabled');
               // $("#"+id).checked(true);    
                $("#"+endwrapper).css('display','none');
              //  $(".add-more-exp-link").css('display','none'); // hide add more link
            } else {
                
                $(".present_here").removeAttr('disabled');
                
                $(".present_here").css('display','block');
                $("#"+endwrapper).css('display','block');
             //   $(".add-more-exp-link").css('display','block');
            }  
        }
      
        myClick();

		 $('.male').click(function(){
			$(".male").addClass( "active" );
			$(".female").removeClass("active");
                        $("#fieldGender").val("Male");
                        
		});
		$('.female').click(function(){
			$(".female").addClass( "active" );
			$(".male").removeClass("active");
                        $("#fieldGender").val("Female");
		});
	     $('.bxslider').bxSlider({
			
        onSliderLoad: function(){
            $(".bx-wrapper").css("visibility", "visible");
        },
		 mode: 'fade',
          auto: true,
        controls: false
    });
      });
      
      
     $('<input>').attr({
    type: 'hidden',
    id: 'id',
    name: 'bar'
    }).appendTo('rateform');
    </script>
    
    
    
    <script type="text/javascript">
setInterval(function(){get_count();$('.alert-info').fadeOut(30000)}, 10000);




        jQuery(document).ready(function () {
            
            $('#notificationicon').click(function(){
                
                $.ajax({
                url: "<?php echo base_url();?>appointment/viewed",
                success: function(result){
                    $("#countnotification").html('');
                    //alert(result);
                }
                
                });
               
            });
            
        });
        
        

function get_count(){
    var last_notification_id = $("#last_notification_id").val();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>appointment/getcount",
        data:{"last_notification_id":last_notification_id},
        async: false,
        success: function(result){
                
                   var obj = jQuery.parseJSON(result);
                  
                   if(obj.notification_count)
                   {
                       var prevcount = parseInt($("#countnotification").html());
                       if($("#countnotification").html() == "")
                       {
                           prevcount = 0;
                       }    
                       
                       var newcount = prevcount+obj.notification_count;
                       $('#notificationul li:eq(0)').before(obj.listring);
                       $("#last_notification_id").val(obj.last_notification_id);
                       $("#countnotification").html(newcount);
                       $('#runtimenotify_divid').append(obj.divstring);
                       $('#runtimenotify_divid').fadeIn('slow');
                       $(".norecordfoundli").remove();
                       
                   }    
                }
        
        });
    
   
    
}

$(document).on('click','.notifyclose',function(){
    $(this).parent().fadeOut('slow');
    
});

</script>

<!--<div class="col-xs-11 col-sm-4 alert alert-info"  style="">
    <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
    <span>Turning standard Bootstrap alerts into "Growl-like" notifications</span>
</div>
<div class="col-xs-11 col-sm-4 alert alert-info">
    <button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>
    <span>Turning standard Bootstrap alerts into "Growl-like" notifications</span>
</div>-->

<div class="runtimenotify" id="runtimenotify_divid">
    
</div>    

<style>
 
    
</style>
  </body>
</html>