<?php $role = $this->session->userdata('logged_in'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>See a Doctor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css">
   <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>css/AdminLTE.min.css"> -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/video.css">
    <link rel="icon" type="image/png" href="images/favicon.png" />
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="../bower_components/html5shiv/dist/html5shiv.js"></script>
    <script src="../bower_components/respond/dest/respond.min.js"></script>
    <![endif]-->
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,400italic,500,500italic,600,600italic,700,700italic,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,300italic,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>/images/favicon.png" type="image/x-icon"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery-1.10.2.min.js"></script>
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>    
    <script src="<?php echo base_url(); ?>js/jquery.validate.js"></script>
        <!--<script src="<?php echo base_url(); ?>js/owl.carousel.min.js"></script>
 <link href="<?php echo base_url(); ?>js/owl.carousel.css" rel="stylesheet">-->
 
 
 
 <link href="<?php echo base_url(); ?>js/owlcarousel/owl.carousel.css" rel="stylesheet">
 <link href="<?php echo base_url(); ?>js/owlcarousel/owl.theme.default.min.css" rel="stylesheet">
 <script src="<?php echo base_url(); ?>js/owlcarousel/owl.carousel.min.js"></script>
  
    <!-- Frontpage Demo -->
    <script>

    $(document).ready(function($) {
     
 $(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
}); 
    </script>
  </head>
  <body>
    <!-- Header Section -->
    <header>
      <div class="container">
        <div class="row">
          <div class="col-lg-2 col-md-2 col-sm-3">
            <div class="logo">
                <a href="<?php echo base_url(); ?>">  <img src="<?php echo base_url() ?>images/logo.jpg" alt="" ></a>
            </div>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
          </div>
   <?php if(isset($role)) { ?> <div class="col-lg-8 col-md-8 col-sm-8 padding-0-full userlogin"><?php } else { ?>        
   <div class="col-lg-5 col-md-5 col-sm-9 padding-0-full"> <?php }?>
       
			 
	  <div class="container-fluid">
		<div class="navbar-header">
		
		   
		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
		  <ul class="nav navbar-nav">
			                
          <?php if(isset($this->session->userdata['logged_in'])) : ?>
            <?php if($this->session->userdata['logged_in']['role'] == 2) : ?> 
              <li><a href="<?php echo base_url(); ?>appointment" <?php if(isset($breadcrumbs['appointment'])) echo 'class="active"'; ?>>SEE A DOCTOR</a></li>
              <li><a href="<?php echo base_url()?>medical-profile" <?php if(isset($breadcrumbs['medical_profile'])) echo 'class="active"'; ?>>MEDICAL FILE</a></li>
              <li><a href="<?php echo base_url()?>home/complete-profile" <?php if(isset($breadcrumbs['user_profile'])) echo 'class="active"'; ?>>MY PROFILE</a></li> 
            <?php elseif($this->session->userdata['logged_in']['role'] == 1): ?> 
              <li><a href="<?php echo base_url(); ?>admin/userlist" <?php if(isset($breadcrumbs['admin_dashboard'])) echo 'class="active"'; ?>>DASHBOARD</a></li>
              <li><a href="<?php echo base_url(); ?>admin/payments" <?php if(isset($breadcrumbs['admin_payment'])) echo 'class="active"'; ?>>PAYMENTS</a></li> 
              <li><a href="<?php echo base_url(); ?>admin/appointments" <?php if(isset($breadcrumbs['admin_appointment'])) echo 'class="active"'; ?>>APPOINTMENTS</a></li> 
            <?php elseif($this->session->userdata['logged_in']['role'] == 3): ?> 
              <li><a href="<?php echo base_url(); ?>doctordashboard" <?php if(isset($breadcrumbs['doctor_dashboard'])) echo 'class="active"'; ?>>DASHBOARD</a></li>
              <li><a href="<?php echo base_url(); ?>doctordashboard/profile" <?php if(isset($breadcrumbs['doctor_profile'])) echo 'class="active"'; ?>>MY PROFILE</a></li> 
            <?php endif; ?>
          <?php else :?>
            <li><a href="<?php echo base_url(); ?>#services-sad" <?php if(isset($breadcrumbs['services'])) echo 'class="active"'; ?>>OUR SERVICES</a></li>
            <li><a href="<?php echo base_url(); ?>#ww-treat-sad" <?php if(isset($breadcrumbs['treat'])) echo 'class="active"'; ?>>WHAT WE TREAT</a></li>
            <li><a href="<?php echo base_url(); ?>#team-sad" <?php if(isset($breadcrumbs['team'])) echo 'class="active"'; ?> >OUR TEAM</a></li>
            <!--<li><a href="<?php //echo base_url(); ?>registerdoctor" <?php /*if(isset($breadcrumbs['apply_doctor'])) echo 'class="active"'; */?>>FOR DOCTORS</a></li> -->
          <?php endif; ?>            
    </ul>
		  <!-- Mobile login-form-->
			  <div class="mobile-login">
			    <?php if(!isset($this->session->userdata['logged_in']) )
         { ?>
  <div class="col-lg-5 col-md-5 col-sm-12">
      <?php $attributes = array("name" => "registrationform", "id" => "loginform-mob" , "class" => "navbar-form navbar-right login-form");
                echo form_open("home/user_login_process", $attributes);?>
  <!--form class="navbar-form navbar-right login-form"-->
        
  <div class="pull-left ">   <input name="email" type="email" class="form-control " placeholder="Email" required> </div>
	
  <div class="pull-left ">   <input name="password" type="password" class="form-control" placeholder="Password" required> </div>
	
        <button type="submit" class="btn btn-default login-button">Login</button>
         <?php echo form_close(); ?>
          <a href="user/forgetPassword">Forgot Password?</a>   
          <script type="text/javascript">
             $("#loginform-mob").validate() ;
          </script>
  <!--/form-->
  </div>

  <?php echo $this->session->flashdata('error'); //echo $this->session->flashdata('admin_message'); ?>  
  <?php echo $this->session->flashdata('error_message'); ?>
            <?php } else { 
         
 ?>
            <a class="btn btn-default submit-button log-out-button" type="button" href="<?php echo base_url() ?>home/logout">Logout</a>
            <?php }  ?>
			  
			  </div>
			  
			<!-- -->
		</div>
	  </div>

   
  </div>
  <div class="mobile-off">
            <?php if(!isset($this->session->userdata['logged_in']) ) { ?>
  <div class="col-lg-5 col-md-5">
      <?php $attributes = array("name" => "registrationform", "id" => "login-form" , "class" => "navbar-form navbar-right login-form");
                echo form_open("home/user_login_process", $attributes);?>
  <!--form class="navbar-form navbar-right login-form"-->
        
  <div class="pull-left ">  <input name="email" type="email" class="form-control login-email" placeholder="Email" required> </div>
	
  <div class="pull-left ">   <input name="password" type="password" class="form-control" placeholder="Password" required> </div>
	
        <button type="submit" class="btn btn-default login-button">Login</button>
         <?php echo form_close(); ?>
          <a href="user/forgetPassword" class="forgot-password">Forgot Password?</a>    
          <script type="text/javascript">
              $("#login-form").validate();
          </script>
          <?php if($this->session->flashdata('error')) : ?>
            <div class="new-error">
              <?php echo $this->session->flashdata('error'); //echo $this->session->flashdata('admin_message'); ?> 
            </div>
          <?php endif; ?>
  <!--/form-->
  </div>  
      <?php echo $this->session->flashdata('error_message'); ?>
            <?php } else { 
         
 ?>
  
      
      
      
      
      <?php 
        
      
        $notificationshow = '';
        $query = $this->db->query("SELECT * FROM notification WHERE user_id ='".$this->session->userdata['logged_in']['id']."' ORDER BY notification_id DESC LIMIT 25");
        $notificationdetail = $query->result_array();
        if($notificationdetail)
        {
           $query = $this->db->query("SELECT * FROM notification WHERE is_view = 0 AND user_id ='".$this->session->userdata['logged_in']['id']."' ORDER BY notification_id DESC");
           $notification_count_result = $query->result_array();
           if(count($notification_count_result))
           {    
                $notificationshow = count($notification_count_result);
           }     
           
        }
        
        $last_notification_id = 0;
        if($notificationdetail)
        {
            $last_notification_id = $notificationdetail[0]['notification_id'];
        }    
        ?>
      
      <input type="hidden" id="last_notification_id" value="<?php echo $last_notification_id;?>" name="last_notification_id">
  <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown notifications-menu">
              <a data-toggle="dropdown" id="notificationicon" class="dropdown-toggle" href="#" aria-expanded="true">
              <i class="fa fa-bell away"></i>
              <span class="label label-warning lbls" id="countnotification"><?php echo $notificationshow;?></span>
            </a>
            <div class="dropdown-menu notification" id="notificationul">
            <ul>   
                <?php 
                
                if($notificationdetail){ 
                foreach ($notificationdetail as $value) { ?>
                
                <li>
                    <a href="<?php echo base_url();?>">
                            <?php if($value['is_view'] == 0 ) { ?> <strong><?php echo $value['message'];?></strong><?php } else{ ?> <?php echo $value['message'];?><?php }?>
                        </a> 
                  </li>
                  
                
                <?php } }else{ ?>
             
                  
                    <li class="norecordfoundli">No record found</li>
                <?php } ?>
            </ul>
            </div>    
          </li>
         
          <?php
          $userinfo = $this->db->query("SELECT * FROM users WHERE id ='".$this->session->userdata['logged_in']['id']."'")->row_array();
          $image_url = get_image_url($userinfo['profile_pic']);
          if($userinfo['role'] == "3")
          {
              $doctor_detail = $this->db->query("SELECT * FROM doctor_cv WHERE user_id ='".$this->session->userdata['logged_in']['id']."'")->row_array();
              $image_url = get_image_url($doctor_detail['profile_pic']);
              
          }    
          
          
          ?>
          
      <li class="dropdown user user-menu">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
           <img alt="User Image" class="user-image" src="<?php echo $image_url;?>">
              <span class="hidden-xs"><?php echo ucfirst($userinfo['fname']).' '.ucfirst($userinfo['sname']);?></span>
            </a>
            <ul class="dropdown-menu headearrow">
              <!-- User image -->
              <li class="user-header">
                  <img alt="User Image" class="img-circle" src="<?php echo $image_url;?>">

                <p>
<?php echo ucfirst($userinfo['fname']).' '.ucfirst($userinfo['sname']);?>
                </p>
              </li>
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <!--<div class="pull-left">
                  <a class="btn btn-default btn-flat" href="#">Profile</a>
                </div>-->
                <div class="pull-right">
                  <a class="btn btn-default btn-flat" href="<?php echo base_url() ?>home/logout">Logout</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
  
  
  
        </div>
      
      
            <!--<a class="btn btn-default submit-button log-out-button" type="button" href="<?php echo base_url() ?>home/logout">Logout</a>-->
            <?php }  ?>
			</div>
          
        </div>
      </div>
    </header>