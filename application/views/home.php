<script>
    /*
    $(document).ready(function($) {
      $("#ourteam").owlCarousel({
      items : 3,
      startPosition:2,
      navigation: true,
      pagination:false,
     afterAction: function(el){
   //remove class active
   this
   .$owlItems
   .removeClass('active')

   //add class active
   this
   .$owlItems //owl internal $ object containing items
   .eq(this.currentItem + 1)
   .addClass('active')    
    } 
   
       
      });
      owl = $("#ourteam").data('owlCarousel');
    owl.jumpTo(1);
    });
    */
 
    </script>
   
<div class="save-money-section">
     <div class="container">
        <div class="save-money-section-inner">
            <h2>We save you Money and Time </h2>
            <p>Avoid long waits at the doctor's office, emergency room or urgent care center</p>
            <div class="clearfix"></div>
        </div>
         <div class="save-money-inner-icon">
             <div class="col-lg-5 col-md-5 col-md-offset-2"><img src="<?php echo base_url() ?>images/save-doctor-icon.png"  class="pull-left" style="    margin-top: 0px; " alt=""><span><?php  //echo $doctor_count; ?></span>
             <div class="pull-left" style="    margin-top: 16px;
    margin-left: 8px;"><i class="fa fa-check" aria-hidden="true"></i> Doctor available 24/7<br />
<i class="fa fa-check" aria-hidden="true"></i> Prescription issues<br />
<i class="fa fa-check" aria-hidden="true"></i> Certified Doctors</div></div>
             <div class="col-lg-5 col-md-5"><img src="<?php echo base_url() ?>images/save-patients.png" alt=""><span><?php echo $patient_count? $patient_count + 80 : '80'; ?></span>Treated Patients</div>
         </div>
     </div>
 </div>

 <div id="services-sad" class="our-service-section">
     <div class="container">
      <div class="our-serivce-inner">
            <h1 class="title-blue">Our Service</h1>
            <div class="our-service-box-wrapp">
              <div class="col-lg-4 col-md-4 our-service-box">
                  <div class="service-title">Dietitian</div>
                    <div class="service-image"><img src="<?php echo base_url() ?>images/nutritionist.jpg" alt=""></div>
                    <div class="service-text">Our Dietitians assess and deliver scientific evidence-based nutritional advice in a variety of settings to improve your health and well being</div>
                </div>
                <div class="col-lg-4 col-md-4 our-service-box">
                  <div class="service-title">Medical Doctor</div>
                    <div class="service-image"><img src="<?php echo base_url() ?>images/services-doctor.jpg" alt=""></div>
                    <div class="service-text">Our certified Doctors can treat most common non-emergency medical conditions to make you feel better</div>
                </div>
                <div class="col-lg-4 col-md-4 our-service-box">
                  <div class="service-title">Psychologist</div>
                    <div class="service-image"><img src="<?php echo base_url() ?>images/services-doc-2.jpg" alt=""></div>
                    <div class="service-text">Our Psychologists can address emotional issues to help you feel better</div>
                </div>
            </div>
        
        </div>
     </div>
 </div>

<div id="ww-treat-sad" class="what-we-treat-section">
  <div class="container">
      <div class="what-we-treat-inner text-center">
        <h1 class="title-blue">What We Treat</h1>
         <div class="what-we-treat-text">Medical Doctors and Psychologists available 24/7 for urgent but not life threatening cases</div>
            <div class="what-we-treat-boxes">
               <div class="what-we-treat-box">
                  <div class="treat-title">Cold & Flu</div>
                    <div class="treat-image"><img src="<?php echo base_url() ?>images/cold-flu.jpg" alt="Cold & Flu"></div>
                </div>
                <div class="what-we-treat-box">
                  <div class="treat-title">Allergies</div>
                    <div class="treat-image"><img src="<?php echo base_url() ?>images/allergies.jpg" alt="Cold & Flu"></div>
                </div>
                <div class="what-we-treat-box">
                  <div class="treat-title">Sore Throat</div>
                    <div class="treat-image"><img src="<?php echo base_url() ?>images/sorethorat.jpg" alt="Cold & Flu"></div>
                </div>
                <div class="what-we-treat-box">
                  <div class="treat-title">Eye</div>
                    <div class="treat-image"><img src="<?php echo base_url() ?>images/eye.jpg" alt="Cold & Flu"></div>
                </div>
                <div class="what-we-treat-box">
                  <div class="treat-title">Skin</div>
                    <div class="treat-image"><img src="<?php echo base_url() ?>images/skin.jpg" alt="Cold & Flu"></div>
                </div>
                <div class="what-we-treat-box">
                  <div class="treat-title">Minor injuries</div>
                    <div class="treat-image"><img src="<?php echo base_url() ?>images/minor-injuries.jpg" alt="Cold & Flu"></div>
                </div>
                <div class="what-we-treat-box">
                  <div class="treat-title">UTIS</div>
                    <div class="treat-image"><img src="<?php echo base_url() ?>images/utis.jpg" alt="Cold & Flu"></div>
                </div>
                <div class="what-we-treat-box">
                  <div class="treat-title">Vomiting</div>
                    <div class="treat-image"><img src="<?php echo base_url() ?>images/vomiting.jpg" alt="Cold & Flu"></div>
                </div>
                <div class=" what-we-treat-box">
                  <div class="treat-title">Anxiety</div>
                    <div class="treat-image"><img src="<?php echo base_url() ?>images/anxiety.jpg" alt="Cold & Flu"></div>
                </div>
                <div class="what-we-treat-box">
                  <div class="treat-title">Depression</div>
                    <div class="treat-image"><img src="<?php echo base_url() ?>images/depression.jpg" alt="Cold & Flu"></div>
                </div>
            </div>
           
        </div>
    </div> 
 </div>

<div class="cost-section" id="our-fees">
  <div class="container">
      <div class="cost-sec-inner text-center">
          <h1 class="title-blue"><?php echo $fee_section ? $fee_section->title : ' Fees'; ?></h1>
            <p><?php echo $fee_section ? $fee_section->content : 'This is fees content'; ?></p>
        </div>
    </div>
 </div>

 <div id="team-sad" class="our-team">
  <div class="container">
      <div class="our-team-inner">
        <div class="row">
          <h1 class="title-blue text-center col-md-12">Our Team</h1>
        </div>
        <div class="row">
          <div class="col-md-9">
            <div class="white-box doc-box-left"></div>
            <div class="white-box doc-box-right"></div>
                <div  id="ourteam" class="owl-carousel our-team-boxes">
                
                    <?php foreach($best_doctors as $value) : ?>
                        <div class="our-team-box item">
                            <div class="team-img"><img src="<?php echo $value->profile_pic; ?>" alt="Cold & Flu"></div>
                            <div class="team-name"><?php echo ucfirst($value->fname); ?></div>
                            <div class="text-center">
                            <?php $rating = $value->rating; ?> 
                            <?php $ratingDiff = 5 - $rating; ?>
                            <i class="stars-doc-rate ">
                                <?php for($i= 0; $i< $rating; $i++) { ?> <i class="fa fa-star active" aria-hidden="true"></i> <?php } ?>
                                <?php for($j= 0; $j<$ratingDiff; $j++) { ?>  <i class="fa fa-star" aria-hidden="true"></i> <?php } ?>
                            </i>
                            </div>
                            <div class="team-text"><?php echo $value->description ? formulate_substr_words($value->description,150) : 'Doctors can treat most common non-emergency medical conditions over video'; ?> </div>
                            <div class="text-center team-links"><a href="<?php echo base_url() . 'home/viewDoctorDetail/' . $value->user_id; ?>">read more</a></div>
                        </div>
                    <?php endforeach; ?>
                 </div>
          </div>
          <div class="col-md-3">
            <div class="our-team-box item">
                          <div class="team-img"><img src="<?php echo base_url().'uploads/profile/thumbnail/join_team.jpg';?>" alt="Join Team"></div>
                          <a href="<?php echo base_url().'registerdoctor';?>"><div class="team-name"><?php echo ucfirst('join team'); ?></div></a>
                          <div class="text-center">
                          <i class="stars-doc-rate ">
                              <?php for($i= 0; $i< 5; $i++) { ?> <i class="fa fa-star active" aria-hidden="true"></i> <?php } ?>
                              <?php for($j= 0; $j< 0; $j++) { ?>  <i class="fa fa-star" aria-hidden="true"></i> <?php } ?>
                          </i>
                          </div>
                          <div class="team-text">Doctors can treat most common non-emergency medical conditions over video</div>
  <!--                        <div class="text-center team-links"><a href="<?php // echo base_url().'registerdoctor';?>">read more</a></div>-->
                      </div>
          </div>
        </div>
        
        
               
        </div>
    </div>
 </div>
    <script>

    $(document).ready(function($) {
      $(".owl-carousel").owlCarousel({
                items : 3,
    startPosition:2,
                nav: true,
                loop: true,
                dots:false,
                
                responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 2
                  },
                  1000: {
                    items: 3
                  }
                },
     afterAction: function(el){
   //remove class active
   this
   .$owlItems
   .removeClass('active')

   //add class active
   this
   .$owlItems //owl internal $ object containing items
   .eq(this.currentItem + 1)
   .addClass('active')    
    } 
   
       
      });
      owl = $("#ourteam").data('owlCarousel');
    owl.jumpTo(1);
    });
  
 
    </script>