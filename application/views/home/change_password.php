<div class="forget-password">
<div class="container">
<div class="col-lg-6 col-md-6">
	<h1>Change Password</h1>
	<?php if(isset($message)): ?>
		<div class="alert alert-success text-center"><?php echo $message; ?></div>
	<?php endif; ?>
	<?php if(isset($error)) : ?>
		<div class="alert alert-danger text-center"><?php echo $error; ?></div>
	<?php endif; ?>
	<p>Please enter current and new password</p>
	<div class="signup-form">
    <?php $attributes = array("name" => "registrationform");
            echo form_open("user/changePassword");?>
		<div class="row">
			<div class="col-lg-6 col-md-6">
                <input id="current_password" name="current_password" type="text" class="form-control" placeholder="Current Password" required="">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-md-6">
                <input id="new_password" name="new_password" type="text" class="form-control" placeholder="New Password" required="">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-md-6">
                <input id="confirm_password" name="confirm_password" type="text" class="form-control" placeholder="Re-Enter Password" required="">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12">
                <button name="submit" type="submit" class="btn btn-default submit-button">Submit</button>
		  	</div> 
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
</div>
</div>