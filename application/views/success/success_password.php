<div class="forget-password">
<div class="container">
<?php if($this->session->flashdata('msg')) : ?>
	<div class="col-lg-12 col-md-12" ><?php echo $this->session->flashdata('msg'); ?></div>
<?php else : ?>
<div class="col-lg-12 col-md-12" >
	<div class="alert alert-success text-center" style="margin: 0 auto;"> <h4>Your password has successfully been changed. <br>Please sign-in to your account using new password</h4></div>
</div>
<?php endif; ?>
</div>
</div>