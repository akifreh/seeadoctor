<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Appointment</title>
    <link href="<?php echo base_url("/css/bootstrap.css"); ?>" rel="stylesheet" type="text/css" />
</head>
<body>
<?php echo $this->session->flashdata('msg'); ?>
<?php echo $this->session->flashdata('online'); ?>
<div class="appointment-links">
    <div class="container">
      <div class="row">
    <?php if($appointment_link) : ?>
    <div class="now-appointment">
      <div class="alert   alert-warning">
        <div class="col-lg-9"><h3>Appointment Link</h3>
          <div class="row"><div class="col-lg-12">Doctor <?php echo $appointment_link['doctor']->fname . ' '. $appointment_link['doctor']->sname; ?> is now ready to see you please click proceed</div></div></div>
          <a class="btn btn-default submit-button pull-right disableappointment" href="<?php echo $appointment_link['link']; ?>">Proceed</a>
          <div class="clearfix"></div>
        </div>
      </div>
      <script type="text/javascript">   
 $(document).ready(function(){   
    $('.disableappointment').on('click', function(){   
        //27122016   
         $('.disableappointment').attr("disabled", true);    
      });    
 });   
                 
     </script>
     <?php endif; ?>
    </div>
      <div class="row">
        <?php if($is_profile_incomplete) : ?>
        <div class="col-lg-12 col-md-12">
          <!--a href="<?php echo base_url(); ?>home/complete-profile" class="complete-profile" style="width: auto !important;" class=" pull-right">Complete your profile</a-->  
          <div class="clearfix"></div>
            <a href="<?php echo base_url(); ?>home/complete-profile" class="" style="width: auto !important;" class=" pull-right">
            <div class="pull-right comp-pro-full" style="">
            <h3>complete my profile</h3>
              <div class="progress complete-pro-proress">
                <div class="progress-bar" style="width: <?php echo $is_profile_incomplete  *100/13; ?>%;"></div>
              </div> 
              <div class="pull-right  complete-pro-proress-txt" >
                <?php echo $is_profile_incomplete . '/13 complete'; ?> 
              </div>
            </div>
            </a> 
        </div>
        <?php endif; ?>
      </div>
                    <div class="row">
                       
                    </div>
      <?php if($has_card_expired === 'expired') : ?>
      <div class="alert alert-danger text-center">Your card has expired. Please edit your card details.  <a href="<?php echo base_url() . 'home/complete-profile';?>">Edit Card Details</a></div>
      <?php elseif($has_card_expired) : ?>
      <div class="alert alert-danger text-center">To see a doctor, please enter your bank card details.  <a href="<?php echo base_url() . 'home/complete-profile/add';?>">Enter Card Details</a></div>
      <?php endif; ?>
      <div class="appointment-link-inner">
        <div class="row">
        <?php if($has_card_expired) : ?>
        <div class="col-lg-4 col-md-4">
                                            
            <a href="" class="inactive"><span>See a</span> Psychologist</a>
          </div>
          <div class="col-lg-4 col-md-4">
            <a href="" class="inactive"><span>See a</span> Medical Doctor</a>
          </div>
          <div class="col-lg-4 col-md-4">
            <a href="" class="inactive"><span>See a</span> Dietitian</a>
          </div>
        <?php else : ?>
          <div class="col-lg-4 col-md-4">
                                            
            <a href="" data-toggle="modal" data-dismiss="modal" data-target=".see-opt-psychologist"><span>See a</span> Psychologist</a>
          </div>
          <div class="col-lg-4 col-md-4">
            <a href="" data-toggle="modal" data-target=".see-medical-doctor"><span>See a</span> Medical Doctor</a>
          </div>
          <div class="col-lg-4 col-md-4">
            <a href="" data-toggle="modal" data-target=".schedule-nutritionest-appointment"><span>See a</span> Dietitian</a>
          </div>
        <?php endif; ?>                 
        </div>
      </div>
    </div>
   </div>
  <div class="appointment-table">
    <div class="container">
      <h2>Scheduled Appointments</h2>
      <div class="appointment-table-inner">
          <div class="table-header name-tab">Healthcare Person</div>
          <div class="table-header name-tab">Date</div>
          <div class="table-header">Time</div>
          <?php if($myappointments) : ?>
          <?php foreach ($myappointments as $myappointment): ?>
          <div class="table-data border-right"><?php echo $myappointment->doctorname; ?></div>
          <div class="table-data border-right"><?php echo date('d M Y', strtotime($myappointment->start_time)); ?></div>
          <div class="table-data" style="text-align: left;padding-left: 10px;">
              
              
              <?php echo date('h:i a', strtotime($myappointment->start_time)) . ' - ' . date('h:i a', strtotime($myappointment->end_time)); ?>
              
              <?php if(($myappointment->meeting_approval) == 0 || $myappointment->meeting_approval == 1) { ?>
            &nbsp; &nbsp;<a href="<?php echo base_url() . 'appointment/startcall/'.$myappointment->id; ?>">Start Call</a>  
              
          &nbsp; &nbsp;<a class="" href="<?php echo base_url() . 'appointment/cancelAppoinment/' . $myappointment->id; ?>">Cancel</a>
          
         <?php }else{  ?> 
          &nbsp; &nbsp;<a href="javascript:void(0);" style="cursor: default;text-decoration: none;">Completed</a>
         <?php } ?>
          </div>
                                        
          <?php endforeach;?>
          <?php endif; ?>
        </div>
    </div>
   </div>
    
   <div class="our-service-section">
     <div class="container">
      <div class="our-serivce-inner">
         
            <div class="our-service-box-wrapp">
            <div class="row">
                 <div class="col-lg-4 col-md-4 our-service-box">
                  <div class="service-title">Psychologist</div>
                    <div class="service-image"><img src="<?php echo base_url() ?>images/sad-4.jpg" alt=""></div>
                    <div class="service-text">Stress/Anxiety, Depression, Mood Swings, Panic, Anger, Concentration, obsession,

Trauma, Grief, Relationships, Diet</div>
</div>
              
                <div class="col-lg-4 col-md-4 our-service-box">
                  <div class="service-title">Medical Doctor</div>
                    <div class="service-image"><img src="<?php echo base_url() ?>images/sad-3.jpg" alt=""></div>
                    <div class="service-text">Cold &amp; Flu, Sore Throat, UTIs, Skin Issues &amp; Rashes, Diarrhea &amp; Vomiting, Eye Issues, Sports Injuries, Travel Illness, Smoking Cessation, Allergies, Arthritic Pain, Asthma, Bronchitis, Infections, Insect Bites, “Pink eye” or Conjunctivitis, Rashes, Respiratory Infections, Sinusitis, Sprains &amp; Strains, Other non-emergency conditions</div>
                </div>
                
                <div class="col-lg-4 col-md-4 our-service-box">
                  <div class="service-title">Dietitian</div>
                    <div class="service-image"><img src="<?php echo base_url() ?>images/sad-2.jpg" alt=""></div>
                    <div class="service-text">Dietitians available who can offer diet and weight management help. They can provide recipes, grocery lists, meal plans, and more</div>
                </div>
               
                </div>
            </div>
        
        </div>
     </div>
 </div>

 <div class="appointment-list-section" style="display:none;">
  <div class="container">
    <div class="appointment-list-box">
    <h3>Doctor</h3>
      <div class="clearfix">
        
      </div>
      <div class="row">
        <div class="col-lg-3 col-md-4"><img src="<?php echo base_url(); ?>images/doctor-img.jpg" alt=" " style="width:100%;"></div>
        <div class="col-lg-9 col-md-8"><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, </p></div>
      </div>
    </div>
    <div class="appointment-list-box">
    <h3>Doctor</h3>
      <div class="clearfix">
        
      </div>
      <div class="row">
        <div class="col-lg-3 col-md-4"><img src="<?php echo base_url(); ?>images/doctor-img.jpg" alt=" " style="width:100%;"></div>
        <div class="col-lg-9 col-md-8"><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, </p></div>
      </div>
    </div>
    <div class="appointment-list-box">
    <h3>Doctor</h3>
      <div class="clearfix">
        
      </div>
      <div class="row">
        <div class="col-lg-3 col-md-4"><img src="<?php echo base_url(); ?>images/doctor-img.jpg" alt=" " style="width:100%;"></div>
        <div class="col-lg-9 col-md-8"><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, </p></div>
      </div>
    </div>
  
  </div>
 </div>
 <!-- 
  <div class="cost-section">
  <div class="container">
      <div class="cost-sec-inner text-center">
          <h1 class="title-blue">Cost</h1>
            <p>To see a Doctor will cost you R250, unless stated otherwise by the specialist</p>
        </div>
    </div>
 </div>
 -->
 <!-- SEE A MEDICAL DOCTOR -->
<div class="modal see-medical-doctor sad-modal">
  <div class="modal-dialog ">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="see-doctor-now">
            <h2>See a Medical Doctor Now</h2>
                        
                        <a href="#" class="sad-button" data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".schedule-doctor-now">Click Here ></a>
    </div>
     <div class="schedule-doctor">
      <h2>Schedule An Appointment</h2>
      <a href="#" class="sad-button" data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".schedule-doctor-appointment">Click Here ></a>
    </div>
      </div>
      
    </div>
  </div>
</div> 
 
 
 <!-- SEE A PSYCHOLOGIST -->
<div class="modal see-opt-psychologist sad-modal">
  <div class="modal-dialog ">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="see-doctor-now">
      <h2>See a Psychologist Now</h2>
      <a href="#" class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".see-psychologist-now">Click Here ></a>
    </div>
     <div class="schedule-doctor">
      <h2>Schedule An Appointment</h2>
      <a href="#" class="sad-button" data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".schedule-psychologist-appointment">Click Here ></a>
    </div>
      </div>
      
    </div>
  </div>
</div> 
 

  <!-- SEE A MEDICAL DOCTOR -->
 <?php $attributes = array("name" => "apply-for-md", "id"=>"apply-for-md");
       echo form_open("appointment/setAppointmentWithMedicalDoctor", $attributes);?>  
  
<div class="modal schedule-doctor-appointment sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
      <h2>Schedule An Appointment</h2>
      <div class="appointment-form">
       
    <div class="form-group">
       
        <div class="checkbox">
          <label>
              <input name="patient[]" value="me" type="checkbox" checked> Me
          </label>
        </div>
       
    </div>
    <div class="form-group">
       
        <div class="checkbox">
          <label>
            <input name="patient[]" value="child" type="checkbox"> My Child
          </label>
        </div>
       
    </div>
    <div class="form-group">
       
        <div class="checkbox">
          <label>
            <input name="patient[]" value="someone-else" type="checkbox"> Someone else
          </label>
        </div>
       
    </div>
   <div class="form-group">
      <label for="select" class="  control-label">Select a doctor</label>
       
      <select name="medicaldoctor" class="form-control" id="doctorselector" required>
            <option value="0">Please choose a doctor</option>
         <?php if($allmedicaldoctors) :
          foreach ($allmedicaldoctors as $allmedicaldoctor ) :?>
            <option value="<?php echo $allmedicaldoctor->user_id ?>"><?php echo $allmedicaldoctor->fname.' '.$allmedicaldoctor->sname; ?></option>
          <?php endforeach; 
          endif; ?>
        </select>
     
      
      </div>
        <div class="form-group">
            <?php if($allmedicaldoctors) :
            foreach ($allmedicaldoctors as $allmedicaldoctor) :?>
            <div id="<?php echo $allmedicaldoctor->user_id; ?>" class="colors" style="display:none"> 
                <span class="doc-name"> <img style="width:60px" src="<?php echo $allmedicaldoctor->profile_pic; ?>"/> </span> 
                 <span  class="doc-discription"  >
                        <h3 ><?php echo $allmedicaldoctor->fname .' '. $allmedicaldoctor->sname ?></h3>
                        <p>Patient Served: <?php echo $allmedicaldoctor->no_of_patient ?> 
                        <?php $count = $allmedicaldoctor->rating;?>
                        <?php if($count == 0) { 
                        for($j=0; $j<5; $j++){?> <i class="fa fa-star" aria-hidden="true"></i><?php } } else {?>
                        <i class="stars-doc pull-right"><?php for($i=0; $i<$count; $i++) {?><i class="fa fa-star active" aria-hidden="true"></i><?php } }?></i>
                        </p>
                        
                        
                        
                        <p><?php echo $allmedicaldoctor->speciality; ?></p>
                 </span>
            </div>
           <?php endforeach; 
           endif; ?>
        </div>
     
        <div class="form-group">
            <script type="text/javascript">
                function showUser(str) {
                  myDoctorElementId = 'doctor'+$('#doctorselector').val();
                  console.log($('#'+myDoctorElementId).val());
                  console.log((myDoctorElementId));
                  
                   $.ajax(
                   {url: "appointment/doctorDays", 
                    data: {id: $('#'+myDoctorElementId).val(),userid:$('#doctorselector').val()},
                    method: "POST"
                   }).done(function(response) {
                    $('#availableDays').html(response);
                    });
                }
                $(document).ready(function(){
                    $('#doctorselector').on('change', function(){
                        showUser('');
                    });
                });
                
                
                function showTime(str) {
                  myDoctorElementId = 'doctor'+$('#doctorselector').val();
                  console.log($('#'+myDoctorElementId).val());
                  console.log((myDoctorElementId));
                  
                   $.ajax(
                   {url: "appointment/doctorAvailableTiming", 
                    data: {id: $('#'+myDoctorElementId).val(),userid:$('#doctorselector').val(),'appointmentDate':$('#availableDays').val()},
                    method: "POST",
                    beforeSend: function() {
                      $('.ajaxloader').show();
                    }
                   }).done(function(response) {
                    $('#availableTime').html(response);
                     $('.ajaxloader').hide();
                    });
                }
                $(document).ready(function(){
                    $('#availableDays').on('change', function(){
                        showTime('');
                    });
                });
                
            </script>
            
            <?php
//                        // Set timezone
//                        date_default_timezone_set('UTC');
//
//                        // Start date
//                        $date = date('Y-m-d');
//                        // End date
//                        $end_date = strtotime ( '+60 days' , strtotime ( $date ) ) ;
//                        $end_date = date ( 'Y-m-d' , $end_date );
                    if($allmedicaldoctors) :
                    foreach ($allmedicaldoctors as $allmedicaldoctor) :
                        echo '<input type="hidden" id="doctor'.$allmedicaldoctor->user_id.'" value="'.$allmedicaldoctor->user_id.'" />';
                    endforeach;
                    endif;
?>
                
                <?php //} ?>
            
            <select class="form-control" id="availableDays" name="availableDays" required>
                    
            </select>
            
           
            <select class="form-control" id="availableTime" name="availableTime" required>
               
            </select>
           
        </div>                    
        <script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script> 
        <script type="text/javascript">
            $(function() {
                 $('#doctorselector').change(function(){
                 $('.colors').hide();
                 $('#' + $(this).val()).show();
            });
              
            
        });
        
       
        </script>
      <div class="form-group">
      <label class="  control-label">Are you currently taking any medication?</label>
      
        <div class="radio">
          <label>
            <input type="radio" name="medication" id="optionsRadios1" value="yes" checked>
            Yes
          </label>
       <label>
            <input type="radio" name="medication" id="optionsRadios2" value="no">
            No
          </label>
        </div>
         
       
    </div>
   <div class="form-group">
       
       <label for="select" class=" control-label">Briefly describe what is wrong</label>
       <textarea class="form-control" name="desc" rows="3" id="textArea" required></textarea>
         
      
    </div>
                            <button  class="sad-button apply-for-md-btn-next" aria-hidden="true" data-toggle="modal" data-target="" >Next</button>
    </div>
      
      </div>
      
    </div>
  </div>
</div></div> 
  
   <script type="text/javascript">
            
//              function applyForMedicalDoctor() {
//              document.getElementById("apply-for-md").submit();
//             }
   </script>
    
    
    <div class="modal mdoctor-symptomps sad-modal symptoms-form-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
      <h2>SYMPTOMS</h2>
      <div class="appointment-form">
       
    <div class="form-group">
       <div><h3>Skin</h3></div>
        <div class="checkbox">
          <label>
              <input type="checkbox" name="symptom[skin][]" value="Bites" > Bites
          </label>
           <label>
              <input type="checkbox" name="symptom[skin][]" value="Bleeding" > Bleeding
          </label>
           <label>
              <input type="checkbox" name="symptom[skin][]" value="Bruising / discoloration" > Bruising / discoloration
          </label>
           <label>
              <input type="checkbox" name="symptom[skin][]" value="Itching" > Itching
          </label>
           <label>
              <input type="checkbox" name="symptom[skin][]" value="Skin rashes / bumps" > Skin rashes / bumps
          </label>
           <label>
              <input type="checkbox" name="symptom[skin][]" value="Bleeding" > Sores
          </label>
           <label>
              <input type="checkbox" name="symptom[skin][]" value="Bleeding" > Swelling
          </label>
           <label>
              <input type="checkbox" name="symptom[skin][]" value="Bleeding" > Bleeding
          </label>
           <label>
              <input type="checkbox" name="symptom[skin][]" value="Bleeding" > Bleeding
          </label>
        </div>
       <div class="clearfix"></div>
         <div><h3>Chest</h3></div>
        <div class="checkbox">
          <label>
              <input type="checkbox" name="symptom[chest][]" value="Chest pain" > Chest pain
          </label>
           <label>
              <input type="checkbox" name="symptom[chest][]" value="Cough" > Cough
          </label>
         
           <label>
              <input type="checkbox" name="symptom[chest][]" value="History of smoking" > History of smoking
          </label>
           <label>
              <input type="checkbox" name="symptom[chest][]" value="Palpitations" > Palpitations
          </label>
           <label>
              <input type="checkbox" name="symptom[chest][]" value="Shortness of breath" > Shortness of breath
          </label>
           <label>
              <input type="checkbox" name="symptom[chest][]" value="Sputum" > Sputum
          </label>
              <label>
              <input type="checkbox" name="symptom[chest][]" value="Decreased exercise tolerance" > Decreased exercise tolerance
          </label>
        </div>
       <div class="clearfix"></div>
         <div><h3>Muscle & Joints</h3></div>
        <div class="checkbox">
          <label>
              <input type="checkbox" name="symptom[musclejoints][]" value="Back Pain" > Back Pain
          </label>
           <label>
              <input type="checkbox" name="symptom[musclejoints][]" value="Joint Replacements" > Joint Replacements
          </label>
           <label>
              <input type="checkbox" name="symptom[musclejoints][]" value="Limited Motion" > Limited Motion
          </label>
           <label>
              <input type="checkbox" name="symptom[musclejoints][]" value="Muscle Pain" > Muscle Pain
          </label>
           <label>
              <input type="checkbox" name="symptom[musclejoints][]" value="Muscle weakness" > Muscle weakness
          </label>
           <label>
              <input type="checkbox" name="symptom[musclejoints][]" value="Swelling" > Swelling
          </label>
        
            
        </div>
       <div class="clearfix"></div>
       <div><h3>General</h3></div>
        <div class="checkbox">
          <label>
              <input type="checkbox" name="symptom[general][]" value="Difficulty Sleep" > Difficulty Sleep
          </label>
           <label>
              <input type="checkbox" name="symptom[general][]" value="Fatigue / weakness" > Fatigue / weakness
          </label>
           <label>
              <input type="checkbox" name="symptom[general][]" value="Fever" > Fever
          </label>
           <label>
              <input type="checkbox" name="symptom[general][]" value="Foreign travel (past month)" > Foreign travel (past month)
          </label>
         
           <label>
              <input type="checkbox" name="symptom[general][]" value="Loss of appetite" > Loss of appetite
          </label>
        
             <label>
              <input type="checkbox" name="symptom[general][]" value="Mood changes" > Mood changes
          </label>
           <label>
              <input type="checkbox" name="symptom[general][]" value="Weight loss" > Weight loss
          </label>
            <label>
              <input type="checkbox" name="symptom[general][]" value="Hospitalised (past six months)" > Hospitalised (past six months)
          </label>
        </div>
       <div class="clearfix"></div>
        <div><h3>Pelvis</h3></div>
        <div class="checkbox">
         
           <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Blood in urine" > Blood in urine
          </label>
           <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Constipation" > Constipation
          </label>
           <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Diarrhea" > Diarrhea
          </label>
           <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Difficulty / pain swallowing" > Difficulty / pain swallowing
          </label>
           <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Digestive tract" > Digestive tract
          </label>
        
          
           <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Flank pain" > Flank pain
          </label>
            <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Frequent urination" > Frequent urination
          </label>
            <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Heartburn / reflux" > Heartburn / reflux
          </label>
            <label>
              <input type="checkbox" name="symptom[pelvis][]" value="History of STIs" > History of STIs
          </label>
            <label>
              <input type="checkbox" name="symptom[pelvis][]" value="irregular periods" > irregular periods
          </label>
            <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Nausea / vomiting" > Nausea / vomiting
          </label>
            <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Penile discharge" > Penile discharge
          </label>  <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Sore Throat" > Sore Throat
          </label>  <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Testicular pain" > Testicular pain
          </label>
           <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Testicular swelling" > Testicular swelling
          </label>
           <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Vaginal bleeding" > Vaginal bleeding
          </label>
           <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Vaginal discharge" > Vaginal discharge
          </label>
          <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Abdominal pain / discomfort" > Abdominal pain / discomfort
          </label>
             <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Discomfort / burning with urination" > Discomfort / burning with urination
          </label>
        </div>
       <div class="clearfix"></div>
       <div><h3>Head and Neck</h3></div>
        <div class="checkbox">
          <label>
              <input type="checkbox" name="symptom[headneck][]" value="Allergies" > Allergies
          </label>
           <label>
              <input type="checkbox" name="symptom[headneck][]" value="Congestion/Sinus problem" > Congestion/Sinus problem
          </label>
           <label>
              <input type="checkbox" name="symptom[headneck][]" value="Dizzy / lightheaded" > Dizzy / lightheaded
          </label>
           <label>
              <input type="checkbox" name="symptom[headneck][]" value="Ear drainage" > Ear drainage
          </label>
           
           <label>
              <input type="checkbox" name="symptom[headneck][]" value="Hearing loss / ringing" > Hearing loss / ringing
          </label>
            <label>
              <input type="checkbox" name="symptom[headneck][]" value="History of fainting / seizure" > History of fainting / seizure
          </label>
            <label>
              <input type="checkbox" name="symptom[headneck][]" value="History of falls" > History of falls
          </label>
            <label>
              <input type="checkbox" name="symptom[headneck][]" value="History of stroke" > History of stroke
          </label>
            <label>
              <input type="checkbox" name="symptom[headneck][]" value="Memory loss" > Memory loss
          </label>
            <label>
              <input type="checkbox" name="symptom[headneck][]" value="Nasal Discharge" > Nasal Discharge
          </label>
          <label>
              <input type="checkbox" name="symptom[headneck][]" value="Numberness / tingling" > Numberness / tingling
          </label>
          <label>
              <input type="checkbox" name="symptom[headneck][]" value="Sore Throat" > Sore Throat
          </label>
           <label>
              <input type="checkbox" name="symptom[headneck][]" value="Vision changes" > Vision changes
          </label>
        </div>
       <div class="clearfix"></div>
    </div>
     
   
       <!-- onclick="applyForMedicalDoctor()" -->
    
                            <button onclick="applyForMedicalDoctor()" class="sad-button btndisableapplyForMedicalDoctor"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".apply-for-md-now" >Submit</button>
    </div>
      
      </div>
      
    </div>
  </div>
</div></div>
<?php echo form_close(); ?>
 <!-- SEE A MEDICAL DOCTOR NOW -->
  <?php $attributes = array("name" => "apply-for-md-now", "id"=> "apply-for-md-now");
       echo form_open("appointment/setAppointmentWithMedicalDoctorNow", $attributes);?>
<div class="modal schedule-doctor-now sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
      <h2>SEE A MEDICAL DOCTOR NOW</h2>
      <div class="appointment-form">
       
    <div class="form-group">
       
        <div class="checkbox">
          <label>
              <input type="checkbox" name="patient[]" value="me" checked> Me
          </label>
        </div>
       
    </div>
    <div class="form-group">
       
        <div class="checkbox">
          <label>
              <input type="checkbox" name="patient[]" value="child"> My Child
          </label>
        </div>
       
    </div>
    <div class="form-group">
       
        <div class="checkbox">
          <label>
              <input type="checkbox" name="patient[]" value="someone-else"> Someone else
          </label>
        </div>
       
    </div>
   <!--div class="form-group">
      <label for="select" class="  control-label">Select a doctor</label>
       
<!--        <select class="form-control" id="select">
        <php foreach ($medicalDoctors as $medicalDoctor ) :?>
            <option value="<php echo $medicalDoctor->user_id ; ?>"><php echo $medicalDoctor->fname.' '.$medicalDoctor->sname; ?></option>
          <php endforeach; ?>
        </select>
     
      
      </div-->   
      <div class="form-group">
      <label class="  control-label">Are you currently taking any medication?</label>
      
        <div class="radio">
          <label>
            <input type="radio" name="medtaken" id="optionsRadios1" value="Yes" checked="">
            Yes
          </label>
       <label>
            <input type="radio" name="medtaken" id="optionsRadios2" value="No">
            No
          </label>
        </div>
         
       
    </div>
   <div class="form-group">
       
       <label for="select" class=" control-label">Briefly describe what is wrong</label>
       <textarea name="brief-des" class="form-control" rows="3" id="textArea"></textarea>
         
      
    </div>
                            <button  class="sad-button apply-for-md-now-next" aria-hidden="true" data-toggle="modal" data-target="" >Next</button>
    </div>
      
      </div>
      
    </div>
  </div>
</div></div> 
    <script type="text/javascript">
            
              function applyForMdNow() {
              document.getElementById("apply-for-md-now").submit();
             }
   </script>
 
 <div class="modal mdoctor-symptomps-now sad-modal symptoms-form-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
      <h2>SYMPTOMS</h2>
      <div class="appointment-form">
       
    <div class="form-group">
       <div><h3>Skin</h3></div>
        <div class="checkbox">
          <label>
              <input type="checkbox" name="symptom[skin][]" value="Bites" > Bites
          </label>
           <label>
              <input type="checkbox" name="symptom[skin][]" value="Bleeding" > Bleeding
          </label>
           <label>
              <input type="checkbox" name="symptom[skin][]" value="Bruising / discoloration" > Bruising / discoloration
          </label>
           <label>
              <input type="checkbox" name="symptom[skin][]" value="Itching" > Itching
          </label>
           <label>
              <input type="checkbox" name="symptom[skin][]" value="Skin rashes / bumps" > Skin rashes / bumps
          </label>
           <label>
              <input type="checkbox" name="symptom[skin][]" value="Bleeding" > Sores
          </label>
           <label>
              <input type="checkbox" name="symptom[skin][]" value="Bleeding" > Swelling
          </label>
           <label>
              <input type="checkbox" name="symptom[skin][]" value="Bleeding" > Bleeding
          </label>
           <label>
              <input type="checkbox" name="symptom[skin][]" value="Bleeding" > Bleeding
          </label>
        </div>
       <div class="clearfix"></div>
         <div><h3>Chest</h3></div>
        <div class="checkbox">
          <label>
              <input type="checkbox" name="symptom[chest][]" value="Chest pain" > Chest pain
          </label>
           <label>
              <input type="checkbox" name="symptom[chest][]" value="Cough" > Cough
          </label>
         
           <label>
              <input type="checkbox" name="symptom[chest][]" value="History of smoking" > History of smoking
          </label>
           <label>
              <input type="checkbox" name="symptom[chest][]" value="Palpitations" > Palpitations
          </label>
           <label>
              <input type="checkbox" name="symptom[chest][]" value="Shortness of breath" > Shortness of breath
          </label>
           <label>
              <input type="checkbox" name="symptom[chest][]" value="Sputum" > Sputum
          </label>
              <label>
              <input type="checkbox" name="symptom[chest][]" value="Decreased exercise tolerance" > Decreased exercise tolerance
          </label>
        </div>
       <div class="clearfix"></div>
         <div><h3>Muscle & Joints</h3></div>
        <div class="checkbox">
          <label>
              <input type="checkbox" name="symptom[musclejoints][]" value="Back Pain" > Back Pain
          </label>
           <label>
              <input type="checkbox" name="symptom[musclejoints][]" value="Joint Replacements" > Joint Replacements
          </label>
           <label>
              <input type="checkbox" name="symptom[musclejoints][]" value="Limited Motion" > Limited Motion
          </label>
           <label>
              <input type="checkbox" name="symptom[musclejoints][]" value="Muscle Pain" > Muscle Pain
          </label>
           <label>
              <input type="checkbox" name="symptom[musclejoints][]" value="Muscle weakness" > Muscle weakness
          </label>
           <label>
              <input type="checkbox" name="symptom[musclejoints][]" value="Swelling" > Swelling
          </label>
        
            
        </div>
       <div class="clearfix"></div>
       <div><h3>General</h3></div>
        <div class="checkbox">
          <label>
              <input type="checkbox" name="symptom[general][]" value="Difficulty Sleep" > Difficulty Sleep
          </label>
           <label>
              <input type="checkbox" name="symptom[general][]" value="Fatigue / weakness" > Fatigue / weakness
          </label>
           <label>
              <input type="checkbox" name="symptom[general][]" value="Fever" > Fever
          </label>
           <label>
              <input type="checkbox" name="symptom[general][]" value="Foreign travel (past month)" > Foreign travel (past month)
          </label>
         
           <label>
              <input type="checkbox" name="symptom[general][]" value="Loss of appetite" > Loss of appetite
          </label>
        
             <label>
              <input type="checkbox" name="symptom[general][]" value="Mood changes" > Mood changes
          </label>
           <label>
              <input type="checkbox" name="symptom[general][]" value="Weight loss" > Weight loss
          </label>
            <label>
              <input type="checkbox" name="symptom[general][]" value="Hospitalised (past six months)" > Hospitalised (past six months)
          </label>
        </div>
       <div class="clearfix"></div>
        <div><h3>Pelvis</h3></div>
        <div class="checkbox">
         
           <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Blood in urine" > Blood in urine
          </label>
           <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Constipation" > Constipation
          </label>
           <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Diarrhea" > Diarrhea
          </label>
           <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Difficulty / pain swallowing" > Difficulty / pain swallowing
          </label>
           <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Digestive tract" > Digestive tract
          </label>
        
          
           <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Flank pain" > Flank pain
          </label>
            <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Frequent urination" > Frequent urination
          </label>
            <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Heartburn / reflux" > Heartburn / reflux
          </label>
            <label>
              <input type="checkbox" name="symptom[pelvis][]" value="History of STIs" > History of STIs
          </label>
            <label>
              <input type="checkbox" name="symptom[pelvis][]" value="irregular periods" > irregular periods
          </label>
            <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Nausea / vomiting" > Nausea / vomiting
          </label>
            <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Penile discharge" > Penile discharge
          </label>  <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Sore Throat" > Sore Throat
          </label>  <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Testicular pain" > Testicular pain
          </label>
           <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Testicular swelling" > Testicular swelling
          </label>
           <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Vaginal bleeding" > Vaginal bleeding
          </label>
           <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Vaginal discharge" > Vaginal discharge
          </label>
          <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Abdominal pain / discomfort" > Abdominal pain / discomfort
          </label>
             <label>
              <input type="checkbox" name="symptom[pelvis][]" value="Discomfort / burning with urination" > Discomfort / burning with urination
          </label>
        </div>
       <div class="clearfix"></div>
       <div><h3>Head and Neck</h3></div>
        <div class="checkbox">
          <label>
              <input type="checkbox" name="symptom[headneck][]" value="Allergies" > Allergies
          </label>
           <label>
              <input type="checkbox" name="symptom[headneck][]" value="Congestion/Sinus problem" > Congestion/Sinus problem
          </label>
           <label>
              <input type="checkbox" name="symptom[headneck][]" value="Dizzy / lightheaded" > Dizzy / lightheaded
          </label>
           <label>
              <input type="checkbox" name="symptom[headneck][]" value="Ear drainage" > Ear drainage
          </label>
           
           <label>
              <input type="checkbox" name="symptom[headneck][]" value="Hearing loss / ringing" > Hearing loss / ringing
          </label>
            <label>
              <input type="checkbox" name="symptom[headneck][]" value="History of fainting / seizure" > History of fainting / seizure
          </label>
            <label>
              <input type="checkbox" name="symptom[headneck][]" value="History of falls" > History of falls
          </label>
            <label>
              <input type="checkbox" name="symptom[headneck][]" value="History of stroke" > History of stroke
          </label>
            <label>
              <input type="checkbox" name="symptom[headneck][]" value="Memory loss" > Memory loss
          </label>
            <label>
              <input type="checkbox" name="symptom[headneck][]" value="Nasal Discharge" > Nasal Discharge
          </label>
          <label>
              <input type="checkbox" name="symptom[headneck][]" value="Numberness / tingling" > Numberness / tingling
          </label>
          <label>
              <input type="checkbox" name="symptom[headneck][]" value="Sore Throat" > Sore Throat
          </label>
           <label>
              <input type="checkbox" name="symptom[headneck][]" value="Vision changes" > Vision changes
          </label>
        </div>
       <div class="clearfix"></div>
    </div>
     
   
       <!-- onclick="applyForMedicalDoctor()" -->
    
                            <button onclick="applyForMdNow()" class="sad-button btndisableapplyForMdNow"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".apply-for-md-now" >Submit</button>
    </div>
      
      </div>
      
    </div>
  </div>
</div></div>
<?php echo form_close(); ?>
 
 <!-- SEE A PSYCHOLOGIST NOW -->
  <?php $attributes = array("name" => "apply-for-psy-now", "id"=> "apply-for-psy-now");
       echo form_open("appointment/setAppointmentWithPsychologistNow", $attributes);?>
 
<div class="modal see-psychologist-now sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
      <h2>SEE A PSYCHOLOGIST NOW</h2>
      <div class="appointment-form">
       
    <div class="form-group">
       
        <div class="checkbox">
          <label>
              <input name="patient[]" value="me" type="checkbox" checked> Me
          </label>
        </div>
       
    </div>
    <div class="form-group">
       
        <div class="checkbox">
          <label>
              <input name="patient[]" value="mychild" type="checkbox"> My Child
          </label>
        </div>
       
    </div>
    <div class="form-group">
       
        <div class="checkbox">
          <label>
              <input name="patient[]" value="else" type="checkbox"> Someone else
          </label>
        </div>
       
    </div>
   <!--div class="form-group">
      <label for="select" class="  control-label">Select a doctor</label>
       
<!--       <select name="psychologistnow" class="form-control" id="select">
         <?php foreach ($psychologists as $psychologist ) :?>
            <option value="<?php echo $psychologist->user_id ; ?>"><?php echo $psychologist->fname.' '.$psychologist->sname; ?></option>
          <?php endforeach; ?>
        </select>
     
      
      </div-->   
      <div class="form-group">
      <label class="  control-label">Are you currently taking any medication?</label>
      
        <div class="radio">
          <label>
            <input type="radio" name="medtaken" id="optionsRadios1" value="yes" checked="">
            Yes
          </label>
       <label>
            <input type="radio" name="medtaken" id="optionsRadios2" value="no">
            No
          </label>
        </div>
         
       
    </div>
   <div class="form-group">
       
       <label for="select" class=" control-label">Briefly describe what is wrong</label>
       <textarea name="briefdesc" class="form-control" rows="3" id="textArea"></textarea>
         
      
    </div>
                            <button class="sad-button see-psychologist-now-submit btndisableapplyForPsyNow"   aria-hidden="true" data-toggle="modal" data-target=".apply-for-psy-now" >Submit</button>
    </div>
      
      </div>
      
    </div>
  </div>
</div></div>
 <?php echo form_close(); ?>
 
 
   <script type="text/javascript">
            //  onclick="applyForPsyNow()" 
//             function applyForPsyNow() {
//              document.getElementById("apply-for-psy-now").submit();
//             }
//             
             $(document).on('click', '.see-psychologist-now-submit', function(e){
                e.preventDefault();
                var validated = validateform();                
                if(!validated){
                  
                  return false;
                    //$('.see-psychologist-now').modal('show');
                }else{
                    document.getElementById("apply-for-psy-now").submit();
                }
                
            });
    
            function validateform(){
                var errorID = 'briefdesc-error';
                $('#'+errorID).remove();
                var briefdesc = $.trim( $('#apply-for-psy-now textarea').val());
                if(briefdesc == ''){
                      $('#apply-for-psy-now textarea').after('<label id="'+errorID+'" class="error" for="briefdesc">This field is required.</label>');
                    return false;
                }
                return true;
            }
    
   </script>
   
   
   
  <!--  PSYCHOLOGIST  -->
  
   <?php $attributes = array("name" => "apply-for-psy", "id"=> "apply-for-psy");
       echo form_open("appointment/setAppointmentWithPsychologist", $attributes);?>
<div class="modal schedule-psychologist-appointment sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
      <h2>Schedule An Appointment</h2>
      <div class="appointment-form">
       
    <div class="form-group">
       
        <div class="checkbox">
          <label>
              <input name="patient[]" value="me" type="checkbox" checked> Me
          </label>
        </div>
       
    </div>
    <div class="form-group">
       
        <div class="checkbox">
          <label>
            <input name="patient[]" value="child" type="checkbox"> My Child
          </label>
        </div>
       
    </div>
    <div class="form-group">
       
        <div class="checkbox">
          <label>
            <input name="patient[]" value="someone-else" type="checkbox"> Someone else
          </label>
        </div>
       
    </div>
   <div class="form-group">
      <label for="select" class="  control-label">Select a doctor</label>
       
      <select name="psychologist" class="form-control" id="psychologistselector">
           <option value="0">Please choose a doctor</option>
         <?php if($allpsychologists) :
          foreach ($allpsychologists as $allpsychologist ) :?>
            <option value="<?php echo $allpsychologist->user_id ?>"><?php echo $allpsychologist->fname.' '.$allpsychologist->sname; ?></option>
          <?php endforeach; 
          endif; ?>
        </select>
      </div> 
                            
      <div class="form-group">
            <?php if($allpsychologists) :
            foreach ($allpsychologists as $allpsychologist) :?>
            <div id="<?php echo $allpsychologist->user_id; ?>" class="psycho" style="display:none"> 
                <span class="doc-name" > <img style="width:60px" src="<?php echo $allpsychologist->profile_pic; ?>"/> </span>
                <span  class="doc-discription"  >
                        <h3 ><?php echo $allpsychologist->fname .' '. $allpsychologist->sname ?></h3>
                        
                        <p>Patients Served: <?php echo $allpsychologist->no_of_patient ?> 
                        <?php $count = $allpsychologist->rating;?>
                        <?php if($count == 0) { 
                        for($j=0; $j<5; $j++){?> <i class="fa fa-star" aria-hidden="true"></i><?php } } else {?>
                        <i class="stars-doc pull-right"><?php for($i=0; $i<$count; $i++) {?><i class="fa fa-star active" aria-hidden="true"></i><?php } }?></i>
                        </p>
                        
                        <p><?php echo $allpsychologist->speciality; ?></p>
                </span>
            </div>
           <?php endforeach; 
           endif; ?>
        </div>
                            
      
       <div class="form-group">
            <script type="text/javascript">
                function showPsycho(str) {
                  myDoctorElementId = 'doctor'+$('#psychologistselector').val();
                  console.log($('#'+myDoctorElementId).val());
                  console.log((myDoctorElementId));
                  
                   $.ajax(
                   {url: "appointment/psychologistDays", 
                    data: {id: $('#'+myDoctorElementId).val(),userid:$('#psychologistselector').val()},
                    method: "POST",
                    beforeSend: function() {
                      $('.ajaxloader').show();
                    }
                   }).done(function(response) {
                    $('#availablePsychoDays').html(response);
                      $('.ajaxloader').hide();
                    });
                }
                $(document).ready(function(){
                    $('#psychologistselector').on('change', function(){
                        showPsycho('');
                    });
                });
                
                
                function showPsychoTime(str) {
                  myDoctorElementId = 'doctor'+$('#psychologistselector').val();
                  console.log($('#'+myDoctorElementId).val());
                  console.log((myDoctorElementId));
                  
                   $.ajax(
                   {url: "appointment/psychologistAvailableTiming", 
                    data: {id: $('#'+myDoctorElementId).val(),userid:$('#psychologistselector').val(),'appointmentDate':$('#availablePsychoDays').val()},
                    method: "POST",
                    beforeSend: function() {
                      $('.ajaxloader').show();
                    }
                   }).done(function(response) {
                    $('#availablePsychoTime').html(response);
                      $('.ajaxloader').hide();
                    });
                }
                $(document).ready(function(){
                    $('#availablePsychoDays').on('change', function(){
                        showPsychoTime('');
                    });
                });
                
            </script>
            
            <?php
//                        // Set timezone
//                        date_default_timezone_set('UTC');
//
//                        // Start date
//                        $date = date('Y-m-d');
//                        // End date
//                        $end_date = strtotime ( '+60 days' , strtotime ( $date ) ) ;
//                        $end_date = date ( 'Y-m-d' , $end_date );
                    if($allpsychologists) :
                    foreach ($allpsychologists as $allpsychologist) :
                        echo '<input type="hidden" id="doctor'.$allpsychologist->user_id.'" value="'.$allpsychologist->user_id.'" />';
                    endforeach;
                    endif; ?>
                
                <?php //} ?>
            
            <select class="form-control" id="availablePsychoDays" name="availablePsychoDays">
                    
            </select>
            
           
            <select class="form-control" id="availablePsychoTime" name="availablePsychoTime">
                     
            </select>
           
            <script type="text/javascript">
            $(function() {
                 $('#psychologistselector').change(function(){
                 $('.psycho').hide();
                 $('#' + $(this).val()).show();
            });
              
            
        });
        
       
        </script>
        
        </div>  
     
      
        
      <div class="form-group">
      <label class="  control-label">Are you currently taking any medication?</label>
      
        <div class="radio">
          <label>
            <input type="radio" name="medication" id="optionsRadios1" value="yes" checked="">
            Yes
          </label>
       <label>
            <input type="radio" name="medication" id="optionsRadios2" value="no">
            No
          </label>
        </div>
         
       
    </div>
   <div class="form-group">
       
       <label for="select" class=" control-label">Briefly describe what is wrong</label>
        <textarea class="form-control" name="description" rows="3" id="textArea"></textarea>
         
      
    </div>
         <button id="mysubmitbotton" class="sad-button btndisablemysubmitbotton" aria-hidden="true" data-toggle="modal" data-target=".apply-for-psy" >Submit</button>
    </div>
      
      </div>
      
    </div>
  </div>
</div></div> 
  <?php echo form_close(); ?>
 
  
   <script type="text/javascript">
//$(document).ready(function(){
//   $('#mysubmitbotton').on('click', function(){
//       $('#apply-for-psy').submit();
//       console.log('submit fired.');
//   });
//});
 
   $(document).on('click', '#mysubmitbotton', function(e){
        e.preventDefault();
        var validated = validateform1();

        if(!validated){
          return false;
            //$('.schedule-psychologist-appointment').modal('show');
        }else{
            document.getElementById("apply-for-psy").submit();
        }

    });


 
function validateform1(){
    var errorID = 'description-error';
    $('#'+errorID).remove();
    var briefdesc = $.trim( $('#apply-for-psy textarea').val());
    
    if(briefdesc == ''){
          $('#apply-for-psy textarea').after('<label id="'+errorID+'" class="error" for="description">This field is required.</label>');
        return false;
    }
    return true;
}
            
    
    /*function applyForPsychologist() {
              document.getElementById("apply-for-psy").submit();
             }*/
    </script>
    
    
 <!-- SEE A NUTRITIONIST -->
 <?php $attributes = array("name" => "apply-for-nutr", "id"=>"apply-for-nutr");
       echo form_open("appointment/setAppointmentWithNutritionist", $attributes);?>
 
<div class="modal schedule-nutritionest-appointment sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
      <h2> Dietitian Appointment</h2>
      <div class="appointment-form">
       
    <div class="form-group">
        
        <div class="checkbox">
          <label>
              <input name="patient[]" value="me" type="checkbox" checked> Me
          </label>
        </div>
       
    </div>
    <div class="form-group">
       
        <div class="checkbox">
          <label>
            <input name="patient[]" value="child" type="checkbox"> My Child
          </label>
        </div>
       
    </div>
    <div class="form-group">
       
        <div class="checkbox">
          <label>
            <input name="patient[]" value="someone-else" type="checkbox"> Someone else
          </label>
        </div>
       
    </div>
   <div class="form-group">
      <label for="select" class="  control-label">Select a doctor</label>
       
      <select name="nutritionist" class="form-control" id="nutritionistselector">
          <option value="0">Please choose a doctor</option>

            <?php if($nutritionists): 
            foreach ($nutritionists as $nutritionist) :?>
            <option value="<?php echo $nutritionist->user_id ?>"><?php echo $nutritionist->fname .' '. $nutritionist->sname ?></option>
         
          <?php endforeach; endif;?>
      </select>
     
      
      </div>  
     
      <div class="form-group">
            <?php if($nutritionists): 
            foreach ($nutritionists as $nutritionist) :?>
            <div id="<?php echo $nutritionist->user_id; ?>" class="nutritionist" style="display:none"> 
                <span class="doc-name" >  <img style="width:60px" src="<?php echo $nutritionist->profile_pic; ?>"/> </span>
                <span  class="doc-discription"  >
                        <h3 ><?php echo $nutritionist->fname .' '. $nutritionist->sname ?></h3>
                        
                        <p>Patients Served: <?php echo $nutritionist->no_of_patient ?> 
                        <?php $count = $nutritionist->rating;?>
                        <?php if($count == 0) { 
                        for($j=0; $j<5; $j++){?> <i class="fa fa-star" aria-hidden="true"></i><?php } } else {?>
                        <i class="stars-doc pull-right"><?php for($i=0; $i<$count; $i++) {?><i class="fa fa-star active" aria-hidden="true"></i><?php } }?></i>
                        </p>
                        
                        <p><?php echo $nutritionist->speciality; ?></p>
                </span>
            </div>
           <?php endforeach; 
           endif; ?>
        </div>
                            
          <div class="form-group">
            <script type="text/javascript">
                function showNutri(str) {
                  myDoctorElementId = 'doctor'+$('#nutritionistselector').val();
                  console.log($('#'+myDoctorElementId).val());
                  console.log((myDoctorElementId));
                  
                   $.ajax(
                   {url: "appointment/nutritionistDays", 
                    data: {id: $('#'+myDoctorElementId).val(),userid:$('#nutritionistselector').val(),'appointmentDate':$('#nutravailableDays').val()},
                    method: "POST",
                    beforeSend: function() {
                      $('.ajaxloader').show();
                    }
                   }).done(function(response) {
                    $('#nutravailableDays').html(response);
                       $('.ajaxloader').hide();
                    });
                }
                $(document).ready(function(){
                    $('#nutritionistselector').on('change', function(){
                        showNutri('');
                    });
                });
                
                
                function showNutriTime(str) {
                  myDoctorElementId = 'doctor'+$('#nutritionistselector').val();
                  console.log($('#'+myDoctorElementId).val());
                  console.log((myDoctorElementId));
                  
                   $.ajax(
                   {url: "appointment/nutritionistAvailableTiming", 
                    data: {id: $('#'+myDoctorElementId).val(),userid:$('#nutritionistselector').val()},
                    method: "POST",
                    beforeSend: function() {
                      $('.ajaxloader').show();
                    }
                   }).done(function(response) {
                    $('#nutravailableTime').html(response);
                       $('.ajaxloader').hide();
                    });
                }
                $(document).ready(function(){
                    $('#nutravailableDays').on('change', function(){
                        showNutriTime('');
                    });
                });
                
            </script>
            
            <?php
//                        // Set timezone
//                        date_default_timezone_set('UTC');
//
//                        // Start date
//                        $date = date('Y-m-d');
//                        // End date
//                        $end_date = strtotime ( '+60 days' , strtotime ( $date ) ) ;
//                        $end_date = date ( 'Y-m-d' , $end_date );
                    if($nutritionists) :
                    foreach ($nutritionists as $nutritionist) :
                        echo '<input type="hidden" id="doctor'.$nutritionist->user_id.'" value="'.$nutritionist->user_id.'" />';
                    endforeach; 
                    endif; ?>
                
                <?php //} ?>
            
            <select class="form-control" id="nutravailableDays" name="nutravailableDays">
                    
            </select>
            
           
            <select class="form-control" id="nutravailableTime" name="nutravailableTime">
                     
            </select>
            
                 <script type="text/javascript">
            $(function() {
                 $('#nutritionistselector').change(function(){
                 $('.nutritionist').hide();
                 $('#' + $(this).val()).show();
            });
              
            
        });
        
       
        </script>
        
           
        </div> 
     
                            
      <div class="form-group">
      <label class="  control-label">Are you currently taking any medication?</label>
      
        <div class="radio">
          <label>
            <input type="radio" name="medication" id="optionsRadios1" value="yes" checked="">
            Yes
          </label>
       <label>
            <input type="radio" name="medication" id="optionsRadios2" value="no">
            No
          </label>
        </div>
         
       
    </div>
   <div class="form-group">
       
       <label for="select" class=" control-label">Briefly describe what is wrong</label>
       <textarea class="form-control" name="problem-description" rows="3" id="textArea"></textarea>
         
      
    </div>
                            <button class="sad-button applyForNutritionist"  aria-hidden="true" data-toggle="modal" data-target=".apply-for-nutr" >Submit</button>
    </div>
      
      </div>
      
    </div>
  </div>
</div></div> 
<?php echo form_close(); ?>
 
 
  <script type="text/javascript">
            
//              function applyForNutritionist() {
//              document.getElementById("apply-for-nutr").submit();
//             }
    </script>
 
  <!-- Request A DOCTOR -->
<div class="modal request-a-doctor sad-modal">
  <div class="modal-dialog ">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="see-doctor-now">
      <h2>Request A Doctor</h2>
      <p>
      You have requested to have a video consultation with the Doctor and R199 will be deducted from your account
      </p>
      <a href="" class="text-link-modal">Decline</a><a href="#" class="sad-button"  data-dismiss="modal" aria-hidden="true">Continue ></a>
    </div>
      
      </div>
      
    </div>
  </div>
</div>  
 </body>
</html>
 
 
<script>
               
$(document).on('click', '.apply-for-md-now-next', function(e){
     e.preventDefault();
     var validated = validateform2();
     if(!validated){
      return false;
         //$('.schedule-doctor-now').modal('show');
     }else{
         $(this).attr('data-target', '.mdoctor-symptomps-now');
     }

 });

function validateform2(){
    var errorID = 'brief-des-error';
    $('#'+errorID).remove();
    var briefdesc = $.trim( $('#apply-for-md-now textarea').val());
    if(briefdesc == ''){
          $('#apply-for-md-now textarea').after('<label id="'+errorID+'" class="error" for="brief-des">This field is required.</label>');
        return false;
    }
    return true;
}

$(document).on('click', '.apply-for-md-btn-next', function(e){
     e.preventDefault();
     var validated = validateform3();
     if(!validated){
         console.log('fail');
         return false;
         //$('.schedule-doctor-appointment').modal('show');
     }else{
         $(this).attr('data-target', '.mdoctor-symptomps');
     }

 });

function validateform3(){
    console.log('check');
    var errorID = 'desc-error';
    $('#'+errorID).remove();
    var briefdesc = $.trim( $('#apply-for-md textarea').val());
    if(briefdesc == ''){
        console.log('not found');
          $('#apply-for-md textarea').after('<label id="'+errorID+'" class="error" for="desc">This field is required.</label>');
        return false;
    }
    return true;
}


 $(document).on('click', '.applyForNutritionist', function(e){
//                e.preventDefault();
    var validated = validateform4();

    if(!validated){
      return false;
       // $('.schedule-nutritionest-appointment').modal('show');
    }else{
        document.getElementById("apply-for-nutr").submit();
    }

});

function validateform4(){
    var errorID = 'problem-description-error';
    $('#'+errorID).remove();
    var briefdesc = $.trim( $('#apply-for-nutr textarea').val());
    if(briefdesc == ''){
          $('#apply-for-nutr textarea').after('<label id="'+errorID+'" class="error" for="problem-description">This field is required.</label>');
        return false;
    }
    return true;
}

</script>
    