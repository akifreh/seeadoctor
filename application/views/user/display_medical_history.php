<div class="medical-pro-section">
<div class="container">
    <div class="row">
     	<?php if($userinfo) :
     			foreach ($userinfo as $row):   
        		endforeach; 
        	endif; ?>
        
        <?php if($userprofile) : 
        		foreach ($userprofile as $row1) : 
        		endforeach; 
        	endif; ?>

    <h2>Medical Profile</h2>
    <?php if(!isset($dontShowUpdate)) :?>
    	<a class="btn btn-default submit-btn" href ="<?php echo base_url() ?>userprofile/updateMedicalHistory">Add or update your medical history</a>
	<?php endif; ?>
<br/>

<div class="medical-history-detail">
<div class="row">
<div class="col-lg-4 ">
  <strong>Name :</strong> <?php echo isset($row->fname) ? ucfirst($row->fname) . ' ' . ucfirst($row->sname) : '';?> 
  </div>
<?php if(!isset($dontShowUpdate)) :?>
<div class="col-lg-4 ">
<strong> Email :</strong> <?php echo isset($row) ? $row->email : '';?>
 </div>
  <div class="col-lg-4 ">
 <strong>Job Title :</strong> <?php echo isset($row) ? ucfirst($row->job_title) : '';?>
 </div>
 <?php endif; ?>
 </div>
 <div class="row">
<div class="col-lg-4 ">
 <strong>Gender : </strong><?php echo isset($row) ? ucfirst($row->gender) : '';?>
 </div>
<div class="col-lg-4 ">
<strong> Address : </strong>
</strong><?php echo isset($row) ? ucfirst($row->address) : '';?>
 </div>

 <div class="col-lg-4 ">
<strong> Next of Kin :</strong> <?php echo isset($row) ? ucfirst($row->next_of_kin) : '';?>
 </div>
 </div>
  <div class="row">
<div class="col-lg-4 ">

 <strong>Diet :</strong> <?php echo isset($row1) ? ucfirst($row1->diet) : '';?>
 </div>
 
 <div class="col-lg-4 ">
<strong> Height (cm) :</strong> <?php echo isset($row1) ? $row1->height : '' ;?>
 </div>
 
 <div class="col-lg-4 ">
 <strong>Weight (kg) : </strong><?php echo isset($row1) ? $row1->weight : '';?>
 </div>
      
      
 <div class="col-lg-4 ">
 <strong>BMI : </strong><?php echo isset($row1) ? $row1->bmi : '';?>
 </div>     
      
  <div class="col-lg-4 ">
 <strong>BMR : </strong><?php echo isset($row1) ? $row1->bmr : '';?>
 </div>     
      
       <?php $active_level = $this->config->item('active_level'); ?>
      <div class="col-lg-4">
 <strong>Activity Level : </strong> <?php echo isset($row1) ? $active_level[$row1->activity_level]: '';?>
 </div>
 
      
  </div>
  <div class="row">
 <div class="col-lg-4 ">
 <strong>Sport Activity :</strong> <?php echo isset($row1) ? ucfirst($row1->sport_activity) : '';?>
  </div>
 
 <div class="col-lg-4 ">
 <strong>Alcohol :</strong> <?php echo isset($row1) ? ucfirst($row1->alcohol) : '';?>
  </div>
 
 <div class="col-lg-4 ">
<strong> Smoke :</strong> <?php echo isset($row1) ? ucfirst($row1->smoke) : '';?>
  </div>
 
 <div class="col-lg-4">
 <strong>Other Pursuits : </strong> <?php echo isset($row1) ? ucfirst($row1->other) : '';?>
 </div>

  
  </div>
</div>
  <div class="row med-histrory-wrapp">
<h3> Medical History</h3>

 <div class="medical-history-table table-design">
        <div class="table-header"> 
          <div class="table-head-col col-md-2 col-lg-2  col-sm-2 col-xs-2">Date of Visit </div>
          <div class="table-head-col col-md-2  col-sm-2 col-xs-2">Location </div>
          <div class="table-head-col col-md-2  col-sm-2 col-xs-2">Practitioner Name</div>
          <div class="table-head-col col-md-3  col-sm-3 col-xs-3">Doctor comments</div> 
          <div class="table-head-col col-md-3  col-sm-3 col-xs-3">Prescription </div>
        </div>  
  <?php if($previousHistory) :
    foreach ($previousHistory as $row5): ?>
        <div class="table-body" id="row<?php echo $userallergy->id?>">
          <div class="table-col col-md-2  col-sm-2 col-xs-2">
          
          <?php echo date('d M Y', strtotime($row5->start_time));?>
          </div>
          <div class="table-col col-md-2  col-sm-2 col-xs-2">
           <?php echo 'Site';?>
          </div>
          <div class="table-col col-md-2  col-sm-2 col-xs-2">
            <?php echo 'Dr ' . $row5->fname;?>
          </div>
          <div class="table-col col-md-3  col-sm-3 col-xs-3">
            <?php echo isset($row5->comments) ? formulate_multiline_text($row5->comments) : '';?>
          </div> 
           <div class="table-col  col-sm-3 col-xs-3">
                 <?php echo isset($row5->pres_detail) ? $row5->pres_detail : '';?>
             </div>                              
                                        </div> 
                                        
                            <?php endforeach; 
                            endif; ?>
       </div>

  </div>
  <div class="row med-detail-wrapp">
<h3> Allergies</h3>

 <div class="medical-history-table table-design">
				<div class="table-header"> 
					<div class="table-head-col col-lg-6 col-md-6 col-xs-6 col-sm-6">Resistant</div>
					<div class="table-head-col col-lg-6 col-md-6 col-xs-6 col-sm-6">Reaction</div>
                                        
					
				</div>
                         <div class="table-body" id="row<?php echo $userallergy->id?>">
                                    <div class="table-col col-lg-6 col-md-6 col-xs-6 col-sm-6"><span style="font-size:13px; color:#a09b9b;  display:inline-block;">Resistant: e.g Nuts ... </span></div>
                                    <div  class="table-col col-lg-6 col-md-6 col-xs-6 col-sm-6"><span style="font-size:13px; color:#a09b9b;   display:inline-block;"> Reaction: e.g Swelling ...</span></div>
                                        
                                        </div>     
                             <?php if($userallergies) : 
                             foreach ($userallergies as $userallergy):  ?>
				<div class="table-body" id="row<?php echo $userallergy->id?>">
                                    <div class="table-col col-lg-6 col-md-6 col-xs-6 col-sm-6"><?php echo ucfirst($userallergy->resistant); ?></div>
                                    <div  class="table-col col-lg-6 col-md-6 col-xs-6 col-sm-6"><?php echo ucfirst($userallergy->reaction); ?></div>
                                        
                                        </div> 
                                        
                            <?php endforeach; 
                            endif; ?>
			 </div>

  </div>


  <div class="row med-detail-wrapp">
<h3>Pre Existing Condition</h3>
<div class="medical-history-table table-design no-responsive">
				<div class="table-header">
					<div class="table-head-col col-lg-12 col-md-12 col-xs-12 col-sm-12">How are you feeling?</div>
                                       
					 
				</div>
                             <?php if($userpecs) : 
                             foreach ($userpecs as $userpec):  ?>
				<div class="table-body">
					<div class="table-col col-lg-12 col-md-12 col-xs-12 col-sm-12"><?php echo ucfirst($userpec->feeling); ?></div>
					 
                                        
				</div>
                             <?php endforeach; 
                             endif; ?>
			 </div>


  </div>




  <div class="row med-detail-wrapp">
<h3>My Updates </h3>
                        <div class="medical-history-table table-design">
				<div class="table-header">
					<div class="table-head-col col-lg-3 col-md-3 col-xs-3 col-sm-3">How are you feeling?</div>
					<div class="table-head-col col-lg-2 col-md-2 col-xs-2 col-sm-2">Symptoms</div>
					<div class="table-head-col col-lg-2 col-md-2 col-xs-2 col-sm-2">Medication taken</div>
					<div class="table-head-col col-lg-3 col-md-3 col-xs-3 col-sm-3">Medicine Description</div>
					<div class="table-head-col col-md-2">Date</div>
                                        
				</div>
                            <?php if($userpersonalupdates) :
                             foreach ($userpersonalupdates as $userpersonalupdate) : ?>
				<div class="table-body">
					<div class="table-col col-lg-3 col-md-3 col-xs-3 col-sm-3"><?php echo ucfirst($userpersonalupdate->feeling); ?></div>
					<div class="table-col col-lg-2 col-md-2 col-xs-2 col-sm-2"><?php echo ucfirst($userpersonalupdate->symptom); ?></div>
					<div class="table-col col-lg-2 col-md-2 col-xs-2 col-sm-2"><?php echo ucfirst($userpersonalupdate->med_taken); ?></div>
					<div class="table-col col-lg-3 col-md-3 col-xs-3 col-sm-3"><?php echo ucfirst($userpersonalupdate->describe_med); ?></div>
					<div class="table-col col-lg-2 col-md-2 col-xs-2 col-sm-2"><?php echo date('d M Y', strtotime($userpersonalupdate->date)); ?></div>
                                      
					
				</div>
                            <?php endforeach; 
                            endif; ?>
			 </div>




    </div>

<div class="row med-detail-wrapp">
   <h3>Family History </h3> 
    <div class="medical-history-table table-design">
                                   <div class="table-header">
                                           <div class="table-head-col col-lg-6 col-md-6 col-xs-6 col-sm-6">Condition</div>

                                           <div class="table-head-col col-lg-6 col-md-6 col-xs-6 col-sm-6">Relationship</div>
                                          

                                   </div>
                                <?php if($userfamilyhistory) :
                                	foreach ($userfamilyhistory as $row4): ?>
                                   <div class="table-body">
                                           <div class="table-col col-lg-6 col-md-6 col-xs-6 col-sm-6" ><?php echo ucfirst($row4->condition); ?></div>

                                           <div class="table-col  col-lg-6 col-md-6 col-xs-6 col-sm-6"><?php echo ucfirst($row4->relationship); ?></div>
                                          
                                   </div>
                                <?php endforeach; 
                                endif; ?>
                            </div>
    
</div>

<div class="row med-detail-wrapp">
    <h3>My Doctors </h3> 
			
			<div class="">
			 <div class="medical-history-table table-design">
				<div class="table-header">
					<div class="table-head-col col-lg-3 col-md-3 col-xs-3 col-sm-3">Name</div>
					<div class="table-head-col col-lg-3 col-md-3 col-xs-3 col-sm-3">Surname</div>
					<div class="table-head-col col-lg-3 col-md-3 col-xs-3 col-sm-3">Contact number</div>
					<div class="table-head-col col-lg-3 col-md-3 col-xs-3 col-sm-3">Email address</div> 
                                        
				</div>
                             <?php if($usermydoctor) : 
                             foreach ($usermydoctor as $row5): ?>
				<div id="row5<?php echo $row5->id?>" class="table-body">
					<div class="table-col col-lg-3 col-md-3 col-xs-3 col-sm-3"><?php echo ucfirst($row5->name);?></div>
					<div class="table-col col-lg-3 col-md-3 col-xs-3 col-sm-3"><?php echo ucfirst($row5->sname);?></div>
					<div class="table-col col-lg-3 col-md-3 col-xs-3 col-sm-3"><?php echo $row5->contact_number;?></div>
					<div class="table-col col-lg-3 col-md-3 col-xs-3 col-sm-3"><?php echo $row5->email;?></div> 
					
				</div>
                            <?php endforeach; 
                            endif; ?>
			 </div>
			</div>
		</div>   
</div>
</div>
</div>