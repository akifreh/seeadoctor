<?php if($userInfo) : 
  foreach ($userInfo as $row0) :
  endforeach; 
endif;
if($profileInfo) :
  foreach ($profileInfo as $row6) : 
  endforeach;
endif;
$attributes = array("name" => "updateprofileinfo");
echo form_open("userprofile/updateProfileInfo", $attributes);?>
<div class="user-profile-section">
	<div class="container">
		<div class="user-pro-inner">
 <div class="row">
			<!--div class="col-lg-2 col-md-2 padding-right-0">
				<img src="<php echo base_url(); ?>uploads/profile/thumbnail/<php echo isset($row0->profile_pic) ? $row0->profile_pic : ''; ?>" alt="" style="width:100%;">
			</div-->

			<div class="col-lg-4 col-md-4">
			 
				<div class="form-group">
					<label  for="inputEmail" class="col-lg-12 control-label">Profession</label>
					<div class="col-lg-12">
                                            <input name="profession" type="text" value="<?php echo $row0->job_title; ?>" class="form-control" id="inputEmail" readonly>
					</div>
				</div>
                                    
				<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Age</label>
					<div class="col-lg-12">
              <input name="age" type="text" value="<?php echo isset($row0->dob) ? date_diff(date_create($row0->dob), date_create('today'))->y : '';?>" class="form-control" id="inputAge" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Gender</label>
						<div class="col-lg-12 col-md-12">
                                                    <input name="gender" id="inputGender" value="<?php echo isset($row0->gender) ? ucfirst($row0->gender) : ''; ?>" type="text" class="form-control" readonly>
				</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Sport activity</label>
						<div class="col-lg-12 col-md-12">
                                                    <input name="sport_activity" value="<?php echo isset($row6->sport_activity) ? ucfirst($row6->sport_activity) : ''; ?>" type="text" class="form-control" id="inputEmail">
				</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
			 
		<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Special diet</label>
					<div class="col-lg-12">
                                            <input name="diet" value="<?php echo isset($row6->diet) ? ucfirst($row6->diet) : ''?>" type="text" class="form-control" id="inputEmail">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Drink alcohol</label>
					<div class="col-lg-12">
						<div class="radio">
          <label>
              <input name="alcohol" type="radio" name="alcohol" value="yes" id="optionsRadios1" <?php if(isset($row6->alcohol) && $row6->alcohol == 'yes') echo "checked"?>>
            Yes
          </label>
		   <label>
		  <input name="alcohol" type="radio" name="alcohol" value="no" id="optionsRadios1"  <?php if(isset($row6->alcohol) && $row6->alcohol == 'no') echo "checked"?> >
            No
          </label>
        </div>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Smoke </label>
						<div class="col-lg-12 col-md-12">
					<div class="radio">
          <label>
            <input type="radio" name="smoke" id="optionsRadios1" value="yes"  <?php if(isset($row6->smoke) && $row6->smoke == 'yes') echo "checked"?>>
            Yes
          </label>
		   <label>
		  <input type="radio" name="smoke" id="optionsRadios1" value="no"  <?php if(isset($row6->smoke) && $row6->smoke == 'no') echo "checked"?>>
            No
          </label>
        </div>
				</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Other Pursuits</label>
						<div class="col-lg-12 col-md-12">
                                                    <input name="other" type="text"  value="<?php echo isset($row6->other) ? $row6->other : ''?>" class="form-control" id="inputEmail">
				</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 contact-detail-last">
			
                           <?php
                           
                           $active_level = $this->config->item('active_level');
                          
                           ?>
                            <div class="form-group">
					<label for="inputActivity" class="col-lg-12 control-label">Activity level</label>
					<div class="col-lg-12">
                                            <select name="activity_level" id="activity_level" class="form-control" style="width:100%;">
                                                 <option value=""></option>
                                                 <?php
                                                      foreach ($active_level as $key => $value)
                                                       {
                                                          if(isset($row6->activity_level) && $row6->activity_level == $key) 
                                                          {    
                                                            echo '<option value="'.$key.'" selected="selected">'.$value.'</option>';
                                                          }
                                                          else
                                                          {
                                                            echo '<option value="'.$key.'">'.$value.'</option>';
                                                          }    
                                                       }
                                                    
                                                 ?>    
                                            </select>    
                                          
					</div>
				</div>
                            
                            
			<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Height (cm)</label>
					<div class="col-lg-12">
                                            <input name="height" type="text" class="bmiandbmr form-control" value="<?php echo isset($row6->height) ? $row6->height : ''?>" id="inputHeight">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Weight (kg)</label>
					<div class="col-lg-12">
                                            <input name="weight" type="text" class="bmiandbmr form-control"  value="<?php echo isset($row6->weight) ? $row6->weight : ''?>" id="inputWeight"> <div class="button-submit">
                    
	   </div>
					</div>
				</div>
                            
                                <div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">BMI</label>
					<div class="col-lg-12">
                                            <input name="bmi" id="inputBmi" type="text" class="onlynumber form-control" value="<?php echo isset($row6->bmi) ? $row6->bmi : ''?>"> <div class="button-submit">
                                            </div>
					</div>
				</div>
                            
                                <div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">BMR</label>
					<div class="col-lg-12">
                                            <input name="bmr" id="inputBmr" type="text" class="onlynumber form-control" value="<?php echo isset($row6->bmr) ? $row6->bmr : ''?>"> <div class="button-submit">
                                            <button type="submit" class="btn btn-default submit-button">Submit</button>
                                            </div>
					</div>
				</div>
                                
				 
			</div>
       
           
     
		</div>
		</div>
	</div>
  </div>
     <?php echo form_close(); ?>



  <div class="user-profile">
	<div class="container">
		<div class="user-pro-inner">
			<ul class="nav nav-tabs">
<!--<li class="active"><a href="#medical-history" data-toggle="tab" aria-expanded="true">Medical History </a></li>-->
<li class="tabLinkClass <?php echo $oldTab == 'medical-history' ? 'active' : '';?>" id="medicalhistoryTabLink"><a href="javascript:void(0);" onclick="toggleCondTab('medicalhistoryTabLink', 'medical-history');" data-toggle="tab" aria-expanded="false" style="">Medical History</a></li>

<li class="tabLinkClass <?php echo $oldTab == 'my-allergies' ? 'active' : '';?>" id="myAllergiesTabLink"><a href="javascript:void(0);" onclick="toggleCondTab('myAllergiesTabLink', 'my-allergies');" data-toggle="tab" aria-expanded="true">My Allergies</a></li>

<li class="tabLinkClass <?php echo $oldTab == 'pec' ? 'active' : '';?>" id="myExistingTabLink"><a href="javascript:void(0);" onclick="toggleCondTab('myExistingTabLink', 'existing-condition');" data-toggle="tab" aria-expanded="false">Pre-Existing Condition</a></li>

<li class="tabLinkClass <?php echo $oldTab == 'health-updates' ? 'active' : '';?>" id="myPersnalTabLink"><a href="javascript:void(0);" onclick="toggleCondTab('myPersnalTabLink', 'personal-updates');" data-toggle="tab" aria-expanded="false">My Updates</a></li>

<li class="tabLinkClass <?php echo $oldTab == 'family-history' ? 'active' : '';?>" id="myFamilyTabLink"><a href="javascript:void(0);" onclick="toggleCondTab('myFamilyTabLink', 'family-history');" data-toggle="tab" aria-expanded="false">Family History</a></li>

<li class="tabLinkClass <?php echo $oldTab == 'my-doctors' ? 'active' : '';?>" id="myDoctorTabLink"><a href="javascript:void(0);" onclick="toggleCondTab('myDoctorTabLink', 'my-doctors');" data-toggle="tab" aria-expanded="false" style="margin-right: 0px; ">My Doctors</a></li>

			</ul>
<div id="myTabContent" class="tab-content">
<?php echo $this->session->flashdata('msg');?>
  
  <!-- My Allergies -->
  <script type="text/javascript" src="https://code.jquery.com/jquery.min.js"></script>   
  <script type="text/javascript">
                                $(document).ready(function(){
                                    $('.modal-foredit-allergies').on('click', function(){
                                       console.log($(this).attr('id'));
                                      
                                       // fetch id from a text.
                                       var rowId = $(this).attr('id');
                                      var trimrowID = rowId.replace("editmodal", "");
                                      console.log(trimrowID);
                                       $("#allergyid").val(trimrowID);
                                         $('#resistant').val($( "#row"+trimrowID+" div:nth-child(1)" ).html());
                                         $('#reaction').val($( "#row"+trimrowID+" div:nth-child(2)" ).html());
                                    });
                                })
                                
                                $(document).ready(function(){
                                    $('.modal-fordelete-allergy').on('click', function(){
                                       console.log($(this).attr('id'));
                                      
                                       // fetch id from a text.
                                       var rowId = $(this).attr('id');
                                      var trimrowID = rowId.replace("deletemodal", "");
                                      console.log(trimrowID);
                                       $("#allergy_id").val(trimrowID);
//                                       
                                    });
                                })
                            </script>
                            
  <div class="tab-pane in fade" id="my-allergies" <?php echo $oldTab == 'my-allergies' ? 'style="display:block;"' : '';?>>
   <div class="row">
       <div class="col-lg-12 col-md-12"><a href="javascript:void(0)"  data-toggle="modal" data-target=".add-more-allergy-modal" class="pull-right btn btn-default form-button">Add New</a></div>
   </div>
    <div class="row">
		
        
			<div class="col-lg-12 col-md-12">
			 <div class="medical-history-table table-design">
				<div class="table-header">
					<div class="table-head-col col-lg-5 col-md-5 col-sm-5 col-xs-5">Resistant</div>
					<div class="table-head-col col-lg-5 col-md-5 col-sm-5 col-xs-5">Reaction</div>
          <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Action</div>
				</div>
                              <div class="table-body" id="row<?php echo $row->id?>">
                                    <div class="table-col col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                       <span style="font-size:13px; color:#a09b9b;  display:inline-block;">Resistant: e.g Nuts ... </span>
                                    </div>
                                    <div  class="table-col col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                      <span style="font-size:13px; color:#a09b9b;   display:inline-block;"> Reaction: e.g Swelling ...</span>
                                    </div>
                                    <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                        &nbsp;
                                    </div>

                            </div>
                        <?php if($getallergies) :
                         foreach ($getallergies as $row): ?>
                             
                          <?php $attributes = array("name" => "update_allergy", "id"=> "update_allergy-".$row->id , "enctype" => 'multipart/form-data','class'=>'formevenclass');
                          echo form_open("userprofile/editAllergies", $attributes);?>
				                    <div class="table-body" id="row<?php echo $row->id?>">
                                    <div class="table-col col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                      <input id="allergyid" type="hidden" name="allergyid" class="form-control" value="<?php echo $row->id; ?>">
                                      <input id="resistant-<?php echo $row->id; ?>"  class="form-control" readonly name="resistant" type="text" value="<?php echo $row->resistant; ?>">
                                    </div>
                                    <div  class="table-col col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                      <input id="reaction-<?php echo $row->id; ?>" class="form-control" readonly name="reaction" type="text" value="<?php echo $row->reaction; ?>">
                                    </div>
                                    <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                      <input type="submit" style="display:none;" class="submit-button" id="save-<?php echo $row->id; ?>" onclick="submitform(<?php echo $row->id; ?>)" value="Save"/>
                                      <a href="javascript:void(0);" style="display:none;" id="cancel-<?php echo $row->id; ?>" onclick="cancelform('update_allergy-<?php echo $row->id; ?>')">Cancel</a>
                               <!--       <input type="button" id="edit-<?php echo $row->id; ?>" onclick="enableInput(<?php echo $row->id; ?>)" value="edit"/>--> 
                                      <a href="javascript:void(0)" id="edit-<?php echo $row->id; ?>" onclick="enableInput(<?php echo $row->id; ?>)" class="p-edit-link" >Edit</a> &nbsp;&nbsp;
                                      <!--a id="editmodal<?php echo $row->id?>" class="modal-foredit-allergies" href="#" data-toggle="modal" data-id="<?php echo $row->id; ?>" data-target=".edit-my-allergies-modal">Edit</a> / -->
                                      <a id="deletemodal<?php echo $row->id?>" class="modal-fordelete-allergy" data-toggle="modal" data-target=".del_allergy" href="javascript:void(0)">Delete</a>
                                    </div>

                            </div> 
                            <div class="clearfix"></div>
                          <?php echo form_close();           
                            endforeach; 
                            endif; ?>
			 </div>
			</div>
		</div>
  </div>
  
  <script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>  
   <script type="text/javascript"> 
    $(".p-edit-link").click(function(){  
	$(".table-body").removeClass("now-editing");
				$(this).parent().parent().addClass("now-editing");
	});
	
   function enableInput(id)
   {
    $('#resistant-'+id).removeAttr("readonly");
    $('#reaction-'+id).removeAttr("readonly");
    $('#edit-'+id).hide();
    $('#deletemodal'+id).hide();
    $('#save-'+id).show();
    $('#cancel-'+id).show();
   }
   </script>
  
  <!-- Existing Condition -->
   <script type="text/javascript">
       
       
       $(document).on("keydown",".onlynumber", function (e) {
        
       if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        
        
    });
    
          
   $(document).on("keydown",".bmiandbmr", function (e) {
        
       if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        
        
    });
    
    
                                $(document).ready(function(){
                                    
                                    
                                    $('.bmiandbmr').on('keyup',function(){
                                    

                                    var weight = $("#inputWeight").val();
                                    var height = $("#inputHeight").val();
                                    if(weight > 0 && height > 0)
                                    {	
                                        var finalBmi = weight/(height/100*height/100);
                                        $("#inputBmi").val(finalBmi.toFixed(2));
                                        
                                    }
                                    else
                                    {
                                        $("#inputBmi").val('');
                                    }    
                                   
                                   
                                   var gender = $("#inputGender").val();
                                   var age = $("#inputAge").val();
                                   
                                   
                                   $("#inputBmr").val('');
                                   
                                   if(gender == "Male" && age > 0 && weight > 0 && height > 0)
                                   {
                                       var finalBmr = 66+(13.8*weight)+(5*height)-(6.8*age);
                                       $("#inputBmr").val(finalBmr.toFixed(2));
                                      
                                   } 
                                   
                                   
                                   if(gender == "Female" && age > 0 && weight > 0 && height > 0)
                                   {
                                       
                                       var finalBmr = 655+(9.6*weight)+(1.8*height)-(4.7*age);
                                       $("#inputBmr").val(finalBmr.toFixed(2));
                                      
                                   } 
                                   
                                  
                                   //66 + (13.8 x weight in kg.) + (5 x height in cm) - (6.8 x age in years)
                                   
                                   
                                   
                                   
                                   
                                   
                                   
                                   
                                   
                                    
                                    });
                                    
                                    
                                    
                                    $('.modal-foredit-pre-exist').on('click', function(){
                                       console.log($(this).attr('id'));
                                      
                                       // fetch id from a text.
                                       var rowId = $(this).attr('id');
                                      var trimrowID = rowId.replace("editmodal", "");
                                      console.log(trimrowID);
                                       $("#pecid").val(trimrowID);
                                         $('#pec_feeling').val($( "#row2"+trimrowID+" div:nth-child(1)" ).html());
                                    });
                                })
                                
                                 $(document).ready(function(){
                                    $('.modal-fordelete-pec').on('click', function(){
                                       console.log($(this).attr('id'));
                                      
                                       // fetch id from a text.
                                       var rowId = $(this).attr('id');
                                      var trimrowID = rowId.replace("deletemodal", "");
                                      console.log(trimrowID);
                                       $("#pec_id").val(trimrowID);
//                                       
                                    });
                                })
                                
                            </script>
                            
   <div class="tab-pane fade in" id="existing-condition" <?php echo $oldTab == 'pec' ? 'style="display:block;"' : '';?>>
    <?php $attributes = array("name" => "add_new_pec", "id"=> "add_new_pec" , "enctype" => 'multipart/form-data');
                echo form_open("userprofile/addNewPec", $attributes);?>
    <div class="row">
       <div class="col-lg-12 col-md-12">
        <div class="col-lg-4 col-md-4">
         <input name="add_feeling[]" value="Cancer" type="checkbox" <?php echo in_array('Cancer', $preExisting) ? 'checked' : ''; ?>> Cancer <br />
         <input name="add_feeling[]" value="Diabetes" type="checkbox" <?php echo in_array('Diabetes', $preExisting) ? 'checked' : ''; ?>> Diabetes <br />
         <input name="add_feeling[]" value="Heart Disease" type="checkbox" <?php echo in_array('Heart Disease', $preExisting) ? 'checked' : ''; ?>> Heart Disease <br />
         <input name="add_feeling[]" value="Stroke" type="checkbox" <?php echo in_array('Stroke', $preExisting) ? 'checked' : ''; ?>> Stroke <br />
         </div>
         <div class="col-lg-4 col-md-4">
         <input name="add_feeling[]" value="High blood pressure" type="checkbox" <?php echo in_array('High blood pressure', $preExisting) ? 'checked' : ''; ?>> High blood pressure <br />
         <input name="add_feeling[]" value="High cholesterol" type="checkbox" <?php echo in_array('High cholesterol', $preExisting) ? 'checked' : ''; ?>> High cholesterol <br />
         <input name="add_feeling[]" value="Asthma / COPD" type="checkbox" <?php echo in_array('Asthma / COPD', $preExisting) ? 'checked' : ''; ?>> Asthma / COPD <br />
         <input name="add_feeling[]" value="Depression" type="checkbox" <?php echo in_array('Depression', $preExisting) ? 'checked' : ''; ?>> Depression <br />
          </div>
         <div class="col-lg-4 col-md-4">
         <input name="add_feeling[]" value="Arthritis" type="checkbox" <?php echo in_array('Arthritis', $preExisting) ? 'checked' : ''; ?>> Arthritis <br />
         <input name="add_feeling[]" value="Abnormal Thyroid" type="checkbox" <?php echo in_array('Abnormal Thyroid', $preExisting) ? 'checked' : ''; ?>> Abnormal Thyroid <br />
         <input name="add_feeling[]" value="Pregnant" type="checkbox" <?php echo in_array('Pregnant', $preExisting) ? 'checked' : ''; ?>> Pregnant <br />
         <input name="add_feeling[]" value="Other" type="checkbox" <?php echo in_array('Other', $preExisting) ? 'checked' : ''; ?>> Other <br />

        <!--a href="javascript:void(0)"  data-toggle="modal" data-target=".add_new_pec" class="pull-right btn btn-default form-button">Add New</a-->
         <!--<input type="submit" class="pull-right btn btn-default submit-button form-button" value="Update">-->
         <br /><br />
         <div class="clearfix" style="height:20px;"></div>
      </div>
     
   </div>
   <?php echo form_close(); ?>
    <!--div class="row">
			
			<div class="col-lg-12 col-md-12">
			 <div class="medical-history-table table-design">
				<div class="table-header">
					<div class="table-head-col col-lg-6 col-md-6 col-sm-6 col-xs-6">Pre-Existing Conditions</div>
                                        <div class="table-head-col col-lg-6 col-md-6 col-sm-6 col-xs-6">&nbsp; </div>
					 
				</div>
                             <php if($getPreExisting) :
                             foreach ($getPreExisting as $row2): ?>
				  <div id="row2<php echo $row2->id?>" class="table-body">
					<div class="table-col col-lg-6 col-md-6 col-sm-6 col-xs-6"><php echo $row2->feeling; ?></div>
					 <div class="table-col col-lg-6 col-md-6 col-sm-6 col-xs-6"><!--a id="editmodal<php echo $row2->id?>" class="modal-foredit-pre-exist" href="#" data-toggle="modal" data-id="<php echo $row2->id; ?>" data-target=".edit-my-pre-exist-modal">Edit</a> /> <a id="deletemodal<php echo $row2->id?>" class="modal-fordelete-pec" data-toggle="modal" data-target=".del_pec" href="#">Delete</a></div>
                                        
				</div>
                             <php endforeach; 
                             endif; ?>
			 </div>
			</div>
		</div-->
   </div>
   </div>
   <!-- Personal UPDATE SECTION -->
      <script type="text/javascript">
//                                function abc(x){
//                                  // alert("hello hi"+x);
//                                          $("#profile_id").val(x);
//                                          
//                                }
                                $(document).ready(function(){
                                    
                                    
                                    
                                    $('form#add_new_pec input[type=checkbox]').on('click',function(){
                                        
                                       $("#add_new_pec").submit();
                                       
                                    });
                                    
                                    
                                    $('.modal-foredit-personal-update').on('click', function(){
                                       console.log($(this).attr('id'));
                                      
                                       // fetch id from a text.
                                       var rowId = $(this).attr('id');
                                      var trimrowID = rowId.replace("editmodal", "");
                                      console.log(trimrowID);
                                       $("#puid").val(trimrowID);
//                                       console.log($( "#row"+trimrowID+" div:nth-child(1)" ).html());
//                                        console.log($( "#row"+trimrowID+" div:nth-child(2)" ).html());
//                                        
                                         $('#pu_feeling').val($( "#row3"+trimrowID+" div:nth-child(1)" ).html());
                                         $('#pu_symptom').val($( "#row3"+trimrowID+" div:nth-child(2)" ).html());
                                         $('#pu_med').val($( "#row3"+trimrowID+" div:nth-child(3)" ).html());
                                         $('#pu_desc_med').val($( "#row3"+trimrowID+" div:nth-child(4)" ).html());
                                         var year = $( "#row3"+trimrowID+" div:nth-child(5)" ).html();
                                         var dob = year.split('-');

                                         //$('#pu_date').val($( "#row3"+trimrowID+" div:nth-child(5)" ).html());
                                         
                                         $('#year').val(dob[0]);
                                         $('#month').val(dob[1]);
                                         $('#day').val(dob[2]);
                                    });
                                })
                                
                                
                                $(document).ready(function(){
                                    $('.modal-fordelete-pu').on('click', function(){
                                       console.log($(this).attr('id'));
                                      
                                       // fetch id from a text.
                                       var rowId = $(this).attr('id');
                                      var trimrowID = rowId.replace("deletemodal-", "");
                                      console.log(trimrowID);
                                       $("#pu_id").val(trimrowID);
//                                       
                                    });
                                })
                                
                                
                            </script>
                            
                            
                            
   <div class="tab-pane fade in" id="personal-updates" <?php echo $oldTab == 'health-updates' ? 'style="display:block;"' : '';?>>
  <div class="row">
       <div class="col-lg-12 col-md-12"><a href="javascript:void(0)"  data-toggle="modal" data-target=".add_new_pu" class="pull-right btn btn-default form-button">Add More</a></div>
   </div>
    <div class="row">
			
			<div class="col-lg-12 col-md-12">
			 <div class="medical-history-table table-design">
				<div class="table-header">
					<div class="table-head-col col-md-2 col-lg-2 col-sm-2 col-xs-2">How are you feeling?</div>
					<div class="table-head-col col-md-2 col-lg-2 col-sm-2 col-xs-2">Symptoms</div>
					<div class="table-head-col col-md-2 col-lg-2 col-sm-2 col-xs-2">Medication taken</div>
					<div class="table-head-col col-md-2 col-lg-2 col-sm-2 col-xs-2">Medicine Description</div>
					<div class="table-head-col col-md-3 col-lg-3 col-sm-3 col-xs-3">Date</div>
                                        <div class="table-head-col col-md-1  col-lg-1 col-sm-1 col-xs-1 padding-lr-o">&nbsp; </div>
				</div>
          <?php if($getPersonalUpdate): ?>
          <?php foreach ($getPersonalUpdate as $row3): ?>
				    <?php $attributes = array("name" => "personal_update", "id"=> "personal_update-" . $row3->id , "enctype" => 'multipart/form-data','class'=>'myupdateformevenclass');
                echo form_open("userprofile/editPersonalUpdate", $attributes);?>
                <div id="row3<?php echo $row3->id?>" class="table-body">
					<div class="table-col col-md-2 col-lg-2 col-sm-2 col-xs-2">
            <input id="puid" type="hidden" name="puid" value="<?php echo $row3->id;?>">
            <input id="pu_feeling-<?php echo $row3->id; ?>" readonly name="pu_feeling" type="text" value="<?php echo $row3->feeling; ?>" class="form-control">
          </div>
					<div class="table-col col-md-2 col-lg-2 col-sm-2 col-xs-2">
            <input id="pu_symptom-<?php echo $row3->id; ?>" readonly name="pu_symptom" type="text" value="<?php echo $row3->symptom; ?>" class="form-control" readonly>
          </div>
					<div class="table-col col-md-2 col-lg-2 col-sm-2 col-xs-2">
            <input id="pu_med-<?php echo $row3->id; ?>" readonly name="pu_med" type="text" value="<?php echo $row3->med_taken; ?>" class="form-control" readonly>
          </div>
					<div class="table-col col-md-2 col-lg-2 col-sm-2 col-xs-2">
            <input id="pu_desc_med-<?php echo $row3->id; ?>" readonly name="pu_desc_med" type="text" value="<?php echo $row3->describe_med; ?>" class="form-control" readonly>
          </div>
					<div class="table-col col-md-3 col-lg-3 col-sm-3 col-xs-3">
            <input name="date" class="form-control" type="text" value="<?php echo date('d M Y', strtotime($row3->date)); ?>" class="form-control" readonly>
            <?php /*
            <select name="b_dd" class="form-control" id="pu-day-<?php echo $row3->id; ?>" disabled>
              <php for($i=1; $i<=31; $i++) : ?>
                <option value="<?php echo $i; ?>" <?php echo date('d', strtotime($row3->date)) == $i ? 'selected' : ''; ?> ><?php echo $i; ?></option>
              <?php endfor; ?>
            </select>
            <select name="b_mm" class="form-control" id="pu-mon-<?php echo $row3->id; ?>" disabled style="width:40%">
            <option value="01" <?php echo date('m', strtotime($row3->date)) == '01' ? 'selected' : ''; ?> >January</option>
            <option value="02" <?php echo date('m', strtotime($row3->date)) == '02' ? 'selected' : ''; ?> >February</option>
            <option value="03" <?php echo date('m', strtotime($row3->date)) == '03' ? 'selected' : ''; ?> >March</option>
            <option value="04" <?php echo date('m', strtotime($row3->date)) == '04' ? 'selected' : ''; ?> >April</option>
            <option value="05" <?php echo date('m', strtotime($row3->date)) == '05' ? 'selected' : ''; ?> >May</option>
            <option value="06" <?php echo date('m', strtotime($row3->date)) == '06' ? 'selected' : ''; ?> >June</option>
            <option value="07" <?php echo date('m', strtotime($row3->date)) == '07' ? 'selected' : ''; ?> >July</option>
            <option value="08" <?php echo date('m', strtotime($row3->date)) == '08' ? 'selected' : ''; ?> >August</option>
            <option value="09" <?php echo date('m', strtotime($row3->date)) == '09' ? 'selected' : ''; ?> >September</option>
            <option value="10" <?php echo date('m', strtotime($row3->date)) == '10' ? 'selected' : ''; ?> >October</option>
            <option value="11" <?php echo date('m', strtotime($row3->date)) == '11' ? 'selected' : ''; ?> >November</option>
            <option value="12" <?php echo date('m', strtotime($row3->date)) == '12' ? 'selected' : ''; ?> >December</option>
            </select>
            <select name="b_yy" class="form-control" id="pu-year-<?php echo $row3->id; ?>" disabled style="width:26%">
            <?php for($i=date('Y'); $i>date('Y')-10; $i--) : ?>
            <option value="<?php echo $i; ?>" <?php echo date('Y', strtotime($row3->date)) == $i ? 'selected' : ''; ?> ><?php echo $i; ?></option>
            <?php endfor; ?>
            </select> */ ?>
          </div>
          <div class="table-col col-md-1  col-lg-1 col-sm-1 col-xs-1 padding-lr-o">
            <input type="submit" style="display:none;" class="submit-button" id="pu-save-<?php echo $row3->id; ?>" onclick="submitform(<?php echo $row3->id; ?>)" value="Save"/>
            <a href="javascript:void(0)" style="display:none;" class="cancel-button" id="pu-cancel-<?php echo $row3->id; ?>" onclick="cancelform('personal_update-<?php echo $row3->id; ?>');">Cancel</a>
            
            <a href="javascript:void(0)" id="pu-edit-<?php echo $row3->id; ?>" onclick="enableInputPU(<?php echo $row3->id; ?>)" class="pu-edit-link" >Edit</a> &nbsp;&nbsp;
            <!--a id="editmodal<?php echo $row3->id?>" class="modal-foredit-personal-update" href="#" data-toggle="modal" data-id="<?php echo $row3->id; ?>" data-target=".edit-my-personal-update-modal">Edit</a-->
            <a id="deletemodal-<?php echo $row3->id?>" class="modal-fordelete-pu" data-toggle="modal" data-target=".del_pu" href="#">Delete</a>
          </div>
					
				</div>
         <?php echo form_close(); ?>
<!--         <div class="clearfix"></div>-->
          <?php endforeach; 
          endif; ?>
			 </div>
			</div>
		</div>
   </div>
    
  <script type="text/javascript"></script>  
   <script type="text/javascript"> 
       
       function cancelform(form)
       {
           $('#'+form).trigger("reset");
           $("#"+form).submit();
       }   
       
       
    $(".pu-edit-link").click(function(){  
  $(".table-body").removeClass("now-editing");
        $(this).parent().parent().addClass("now-editing");
  });
  
   function enableInputPU(id)
   {
    $('#pu_feeling-'+id).removeAttr("readonly");
    $('#pu_symptom-'+id).removeAttr("readonly");
    $('#pu_med-'+id).removeAttr("readonly");
    $('#pu_desc_med-'+id).removeAttr("readonly");
    $('#pu-day-'+id).removeAttr("disabled");
    $('#pu-mon-'+id).removeAttr("disabled");
    $('#pu-year-'+id).removeAttr("disabled");
    $('#pu-edit-'+id).hide();
    $('#deletemodal-'+id).hide();
    $('#pu-save-'+id).show();
    $('#pu-cancel-'+id).show();
   }
   </script>

   <!--Family History Section --> 
    <script type="text/javascript">
//                                function abc(x){
//                                  // alert("hello hi"+x);
//                                          $("#profile_id").val(x);
//                                          
//                                }
                                $(document).ready(function(){
                                    $('.modal-foredit-family-history').on('click', function(){
                                       console.log($(this).attr('id'));
                                      
                                       // fetch id from a text.
                                       var rowId = $(this).attr('id');
                                      var trimrowID = rowId.replace("editmodal", "");
                                      console.log(trimrowID);
                                       $("#fhid").val(trimrowID);
//                                       console.log($( "#row"+trimrowID+" div:nth-child(1)" ).html());
//                                        console.log($( "#row"+trimrowID+" div:nth-child(2)" ).html());
//                                        var year = $( "#row"+trimrowID+" div:nth-child(3)" ).html();
//                                          var dob = year.split('-');
                                         $('#fh_condition').val($( "#row4"+trimrowID+" div:nth-child(1)" ).html());
                                         $('#fh_relationship').val($( "#row4"+trimrowID+" div:nth-child(2)" ).html());
//                                         $('#year').val(dob[0]);

//                                         $('#month').val(dob[1]);
//                                         $('#day').val(dob[2]);
                                    });
                                })
                                
                                $(document).ready(function(){
                                    $('.modal-fordelete-fh').on('click', function(){
                                       console.log($(this).attr('id'));
                                      
                                       // fetch id from a text.
                                       var rowId = $(this).attr('id');
                                      var trimrowID = rowId.replace("deletemodal-", "");
                                      console.log(trimrowID);
                                       $("#fh_id").val(trimrowID);
//                                       
                                    });
                                })
                                
                            </script>
                            
   
   <div class="tab-pane fade in" id="family-history" <?php echo $oldTab == 'family-history' ? 'style="display:block;"' : '';?>>
    <div class="row">
       <div class="col-lg-12 col-md-12"><a href="javascript:void(0)"  data-toggle="modal" data-target=".add_new_fh" class="pull-right btn btn-default form-button">Add New</a></div>
   </div>
     <div class="row">
			
			<div class="col-lg-12 col-md-12">
			 <div class="medical-history-table table-design">
				<div class="table-header">
					<div class="table-head-col col-lg-4 col-md-4 col-sm-4 col-xs-4">Condition</div>
					
					<div class="table-head-col col-lg-4 col-lg-4 col-md-4 col-sm-4 col-xs-4">Relationship</div>
                                        <div class="table-head-col col-lg-4 col-lg-4 col-md-4 col-sm-4 col-xs-4">&nbsp; </div>
					 
				</div>
                             <?php if($familyHistory) :
                             foreach ($familyHistory as $row4): ?>
                             <?php $attributes = array("name" => "family_history", "id"=> "family_history-".$row4->id, "enctype" => 'multipart/form-data','class'=>'myupdateformevenclass');
                echo form_open("userprofile/editFamilyHistory", $attributes);?>
				<div id="row4<?php echo $row4->id?>" class="table-body">
					<div class="table-col col-lg-4 col-lg-4 col-md-4 col-sm-4 col-xs-4" >
                                            <input id="fhid" type="hidden" name="fhid" value="<?php echo $row4->id?>">
                                            <input id="fh_condition-<?php echo $row4->id?>" name="fh_condition" readonly type="text" value="<?php echo $row4->condition; ?>" class="form-control">
                                          </div>

                                                                        <div class="table-col col-lg-4 col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <input id="fh_relationship-<?php echo $row4->id?>" name="fh_relationship" readonly type="text" value="<?php echo $row4->relationship; ?>" class="form-control">
                                          </div>
                                         <div class="table-col col-lg-4 col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                          <input type="submit" style="display:none;" class="submit-button" id="fh-save-<?php echo $row4->id; ?>" value="Save"/>
                                          <a href="javascript:void(0)" style="display:none;" id="fh-cancel-<?php echo $row4->id; ?>" onclick="cancelform('family_history-<?php echo $row4->id; ?>')">Cancel</a>
                                          <a href="javascript:void(0)" id="fh-edit-<?php echo $row4->id; ?>" onclick="enableInputFH(<?php echo $row4->id; ?>)" class="fh-edit-link" >Edit</a> &nbsp;&nbsp;

                                          <!--a id="editmodal<?php echo $row4->id?>" class="modal-foredit-family-history" href="#" data-toggle="modal" data-id="<?php echo $row4->id; ?>" data-target=".edit-my-family-history-modal">Edit</a-->
                                          <a id="deletemodal-<?php echo $row4->id?>" class="modal-fordelete-fh" data-toggle="modal" data-target=".del_fh" href="#">Delete</a></div> 
				</div>
                             <?php echo form_close();
                             endforeach; 
                             endif; ?>
			 </div>
			</div>
		</div>
   </div>
 
   <script type="text/javascript"> 
    $(".fh-edit-link").click(function(){  
    $(".table-body").removeClass("now-editing");
        $(this).parent().parent().addClass("now-editing");
    });
  
   function enableInputFH(id)
   {
    $('#fh_condition-'+id).removeAttr("readonly");
    $('#fh_relationship-'+id).removeAttr("readonly");
    $('#fh-edit-'+id).hide();
    $('#deletemodal-'+id).hide();
    $('#fh-save-'+id).show();
    $('#fh-cancel-'+id).show();
   }
   </script>

                            
   <!-- My Doctor Section --> 
     <script type="text/javascript">
//                                function abc(x){
//                                  // alert("hello hi"+x);
//                                          $("#profile_id").val(x);
//                                          
//                                }
                                $(document).ready(function(){
                                    $('.modal-foredit-my-doctor-modal').on('click', function(){
                                       console.log($(this).attr('id'));
                                      
                                       // fetch id from a text.
                                       var rowId = $(this).attr('id');
                                      var trimrowID = rowId.replace("editmodal", "");
                                      console.log(trimrowID);
                                       $("#mdid").val(trimrowID);
//                                       console.log($( "#row"+trimrowID+" div:nth-child(1)" ).html());
//                                        console.log($( "#row"+trimrowID+" div:nth-child(2)" ).html());
//                                        var year = $( "#row"+trimrowID+" div:nth-child(3)" ).html();
//                                          var dob = year.split('-');
                                         $('#md_name').val($( "#row5"+trimrowID+" div:nth-child(1)" ).html());
                                         $('#md_sname').val($( "#row5"+trimrowID+" div:nth-child(2)" ).html());
                                          $('#md_number').val($( "#row5"+trimrowID+" div:nth-child(3)" ).html());
                                           $('#md_email').val($( "#row5"+trimrowID+" div:nth-child(4)" ).html());
//                                         $('#year').val(dob[0]);
//                                         $('#month').val(dob[1]);
//                                         $('#day').val(dob[2]);
                                    });
                                })
                                
                                $(document).ready(function(){
                                    $('.modal-fordelete-md').on('click', function(){
                                       console.log($(this).attr('id'));
                                      
                                       // fetch id from a text.
                                       var rowId = $(this).attr('id');
                                      var trimrowID = rowId.replace("deletemodal", "");
                                      console.log(trimrowID);
                                       $("#md_id").val(trimrowID);
//                                       
                                    });
                                })
                                
                            </script>
                            
                            
   <div class="tab-pane fade in" id="my-doctors" <?php echo $oldTab == 'my-doctors' ? 'style="display:block;"' : '';?>>
       <div class="row">
       <div class="col-lg-12 col-md-12"><a href="javascript:void(0)"  data-toggle="modal" data-target=".add_new_md" class="pull-right btn btn-default form-button">Add More</a></div>
   </div>
    <div class="row">
			
			<div class="col-lg-12 col-md-12">
			 <div class="medical-history-table table-design">
				<div class="table-header">
					<div class="table-head-col col-md-2 col-lg-2  col-sm-2 col-xs-2">Name</div>
					<div class="table-head-col col-md-2  col-sm-2 col-xs-2">Surname</div>
					<div class="table-head-col col-md-3  col-sm-3 col-xs-3">Contact number</div>
					<div class="table-head-col col-md-3  col-sm-3 col-xs-3">Email address</div> 
                                        <div class="table-head-col col-md-2  col-sm-2 col-xs-2">&nbsp; </div>
				</div>
                             <?php if($myDoctor) :
                             foreach ($myDoctor as $row5): ?>
				<div id="row5<?php echo $row5->id?>" class="table-body">
          <?php $attributes = array("name" => "my_doctor", "id"=> "my_doctor-".$row5->id, "enctype" => 'multipart/form-data');
                echo form_open("userprofile/editMyDoctor", $attributes);?>
					<div class="table-col col-md-2  col-sm-2 col-xs-2">
            <input id="mdid" type="hidden" name="mdid" value="<?php echo $row5->id?>">
            <input id="md_name-<?php echo $row5->id?>" readonly name="md_name" type="text" value="<?php echo $row5->name;?>" class="form-control">
          </div>
					<div class="table-col col-md-2  col-sm-2 col-xs-2">
            <input id="md_sname-<?php echo $row5->id?>" readonly name="md_sname" type="text" value="<?php echo $row5->sname;?>" class="form-control">
          </div>
					<div class="table-col col-md-3  col-sm-3 col-xs-3">
            <input id="md_number-<?php echo $row5->id?>" readonly name="md_number" type="text" value="<?php echo $row5->contact_number;?>" class="form-control">
          </div>
					<div class="table-col col-md-3  col-sm-3 col-xs-3">
            <input id="md_email-<?php echo $row5->id?>" readonly name="md_email" type="text" value="<?php echo $row5->email;?>" class="form-control">
          </div> 
					 <div class="table-col  col-sm-2 col-xs-2">
            <!--a id="editmodal<?php echo $row5->id?>" class="modal-foredit-my-doctor-modal" href="#" data-toggle="modal" data-id="<?php echo $row4->id; ?>" data-target=".edit-my-doctor-modal">Edit</a-->
            <input type="submit" style="display:none;" class="submit-button" id="md-save-<?php echo $row5->id; ?>" value="Save"/>
            <a href="javascript:void(0)" style="display:none;" id="md-cancel-<?php echo $row5->id; ?>" onclick="cancelform('my_doctor-<?php echo $row5->id; ?>')">Cancel</a>
            
            <a href="javascript:void(0)" id="md-edit-<?php echo $row5->id; ?>" onclick="enableInputMD(<?php echo $row5->id; ?>)" class="md-edit-link" >Edit</a> &nbsp;&nbsp;
            <a id="deletemodal<?php echo $row5->id?>" class="modal-fordelete-md" data-toggle="modal" data-target=".del_md" href="#">Delete</a></div>
				</div>
                            <?php echo form_close();
                            endforeach; 
                            endif; ?>
			 </div>
			</div>
		</div>  
  </div>
  
  
  <div class="tab-pane fade active in" id="medical-history" <?php echo $oldTab == 'medical-history' ? 'style="display:block;"' : 'style="display:none;"';?>>
       <div class="row">
 
   </div>
    <div class="row">
			
			<div class="col-lg-12 col-md-12">
			 <div class="medical-history-table table-design">
				<div class="table-header">
					<div class="table-head-col col-md-2 col-lg-2  col-sm-2 col-xs-2">Date of Visit </div>
					<div class="table-head-col col-md-2  col-sm-2 col-xs-2">Location </div>
					<div class="table-head-col col-md-2  col-sm-2 col-xs-2">Practitioner Name</div>
					<div class="table-head-col col-md-3  col-sm-3 col-xs-3">Doctor comments</div> 
                                        <div class="table-head-col col-md-3  col-sm-3 col-xs-3">Prescription </div>
                                         
				</div>
                             <?php if($previousHistory) :
                             foreach ($previousHistory as $row5): ?>
				<div id="row5<?php echo $row5->id?>" class="table-body">
          <?php $attributes = array("name" => "my_doctor", "id"=> "my_doctor" , "enctype" => 'multipart/form-data');
                echo form_open("userprofile/editMyDoctor", $attributes);?>
					<div class="table-col col-md-2  col-sm-2 col-xs-2">
          
        <?php echo date('d M Y', strtotime($row5->start_time));?>
          </div>
					<div class="table-col col-md-2  col-sm-2 col-xs-2">
           <?php echo 'Site';?>
          </div>
					<div class="table-col col-md-2  col-sm-2 col-xs-2">
            <?php echo 'Dr ' . $row5->fname;?>
          </div>
					<div class="table-col col-md-3  col-sm-3 col-xs-3">
            <?php echo isset($row5->comments) ? formulate_multiline_text($row5->comments) : '';?>
          </div> 
					 <div class="table-col  col-sm-3 col-xs-3">
                 <?php echo isset($row5->pres_detail) ? $row5->pres_detail : '';?>
             </div>
            
				</div>
                            <?php echo form_close();
                            endforeach; 
                            endif; ?>
			 </div>
			</div>
		</div>  
  </div>
</div>
		</div>
           
            
	</div>
  </div>
<br /><br />

<script type="text/javascript"> 
    $(".md-edit-link").click(function(){  
    $(".table-body").removeClass("now-editing");
        $(this).parent().parent().addClass("now-editing");
    });
  
   function enableInputMD(id)
   {
    $('#md_name-'+id).removeAttr("readonly");
    $('#md_sname-'+id).removeAttr("readonly");
    $('#md_number-'+id).removeAttr("readonly");
    $('#md_email-'+id).removeAttr("readonly");
    $('#md-edit-'+id).hide();
    $('#deletemodal'+id).hide();
    $('#md-save-'+id).show();
    $('#md-cancel-'+id).show();
   }
   </script>

 <!-- MODAL EDIT My Allergies -->
<?php $attributes = array("name" => "update_allergy", "id"=> "update_allergy" , "enctype" => 'multipart/form-data');
                echo form_open("userprofile/editAllergies", $attributes);?>
 <div class="modal edit-my-allergies-modal sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
			 
			<div class="appointment-form">
                           
                        <div class="form-group">
					<label for="inputEmail" class="  control-label">Resistant </label>
					<div class=" ">
                                            <input id="allergyid" type="hidden" name="allergyid" value="">
                                            <input id="resistant" name="resistant" type="text" value="" class="form-control" id="inputEmail">
					</div>
				</div>
		 <div class="form-group">
					<label for="inputEmail" class="  control-label">Reaction </label>
					<div class=" ">
                                            <input id="reaction" name="reaction" type="text" class="form-control" id="inputEmail">
					</div>
				</div>
				 
                           
                            <button onclick="update_allergies()" class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".update_allergy" >Save</button>
		</div>
		  
      </div>
      
    </div>
  </div>
  </div></div>
     <?php echo form_close(); ?>
 
  <script type="text/javascript">
            
              function update_allergies() {
              document.getElementById("update_allergy").submit();
             }
    </script>

    
    
    
    
     <!-- MODAL EDIT Pre Existing Condition -->
<?php $attributes = array("name" => "pre_exist", "id"=> "pre_exist" , "enctype" => 'multipart/form-data');
                echo form_open("userprofile/editPreExistingCondition", $attributes);?>
 <div class="modal edit-my-pre-exist-modal sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
			 
			<div class="appointment-form">
                           
                        <div class="form-group">
					<label for="inputEmail" class="  control-label">Feeling </label>
					<div class=" ">
                                            <input id="pecid" type="hidden" name="pecid" value="">
                                            <input id="pec_feeling" name="pec_feeling" type="text" value="" class="form-control">
					</div>
				</div>
		 
                            <button onclick="update_pre_exist()" class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".pre_exist" >Save</button>
		</div>
		  
      </div>
      
    </div>
  </div>
  </div></div>
     <?php echo form_close(); ?>
 
  <script type="text/javascript">
            
              function update_pre_exist() {
              document.getElementById("pre_exist").submit();
             }
    </script>
    
    
    
    
    
    
         <!-- MODAL EDIT Personal update -->
<?php $attributes = array("name" => "personal_update", "id"=> "personal_update" , "enctype" => 'multipart/form-data');
                echo form_open("userprofile/editPersonalUpdate", $attributes);?>
 <div class="modal edit-my-personal-update-modal sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
			 
			<div class="appointment-form">
                           
                        <div class="form-group">
					<label for="inputEmail" class="  control-label">Feeling </label>
					<div class=" ">
                                            <input id="puid" type="hidden" name="puid" value="">
                                            <input id="pu_feeling" name="pu_feeling" type="text" value="" class="form-control">
					</div>
				</div>
                            <div class="form-group">
					<label for="inputEmail" class="  control-label">Symptom </label>
					<div class=" ">
                                            
                                            <input id="pu_symptom" name="pu_symptom" type="text" value="" class="form-control">
					</div>
				</div>
                            <div class="form-group">
					<label for="inputEmail" class="  control-label">Medicine Taken </label>
					<div class=" ">
                                            
                                            <input id="pu_med" name="pu_med" type="text" value="" class="form-control">
					</div>
				</div>
                            <div class="form-group">
					<label for="inputEmail" class="  control-label">Describe Medicine </label>
					<div class=" ">
                                            
                                            <input id="pu_desc_med" name="pu_desc_med" type="text" value="" class="form-control">
					</div>
				</div>
                            <div class="form-group">
					<label for="inputEmail" class="  control-label">Date </label>
					<div class=" date-select-small">
                                             <select id="day" name="b_dd" class="form-control" id="select">
                                         <?php
                                        for($i=1; $i<=31; $i++){
                                            $selected = '';
                                        //if ($cardExpDate[2] == $i) //$selected = ' selected="selected"';
                                        print('<option value="'.str_pad($i, 2, "0", STR_PAD_LEFT).'"'.$selected.'>'.str_pad($i, 2, "0", STR_PAD_LEFT).'</option>'."\n"); 
                                        }
                                        ?>
                                            </select>
                                        <select id="month" name="b_mm" class="form-control" id="select">
                                         <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                        </select>
                                        <select id="year" name="b_yy" class="form-control" id="select">
                                        <?php
                                        for($i=date('Y'); $i>date('Y')-10; $i--) {
                                        $selected = '';
                                        //if ($cardExpDate[0] == $i) //$selected = ' selected="selected"';
                                        print('<option value="'.$i.'"'.$selected.'>'.$i.'</option>'."\n");
                                    }
                                    ?>
                                        </select>
					</div>
				</div>
                            
		 
                            <button onclick="update_personal_update()" class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".personal_update" >Save</button>
		</div>
		  
      </div>
      
    </div>
  </div>
  </div></div>
     <?php echo form_close(); ?>
 
  <script type="text/javascript">
            
              function update_personal_update() {
              document.getElementById("personal_update").submit();
             }
    </script>
    
    
    
    
    
    
     <!-- MODAL EDIT Family History -->
<?php $attributes = array("name" => "family_history", "id"=> "family_history" , "enctype" => 'multipart/form-data');
                echo form_open("userprofile/editFamilyHistory", $attributes);?>
 <div class="modal edit-my-family-history-modal sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
			 
			<div class="appointment-form">
                           
                        <div class="form-group">
					<label for="inputEmail" class="  control-label">Condition </label>
					<div class=" ">
                                            <input id="fhid" type="hidden" name="fhid" value="">
                                            <input id="fh_condition" name="fh_condition" type="text" value="" class="form-control">
					</div>
				</div>
                            <div class="form-group">
					<label for="inputEmail" class="  control-label">Relationship </label>
					<div class=" ">
                                            
                                            <input id="fh_relationship" name="fh_relationship" type="text" value="" class="form-control">
					</div>
				</div>
                           
                          
                            <button onclick="update_family_history()" class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".family_history" >Save</button>
		</div>
		  
      </div>
      
    </div>
  </div>
  </div></div>
     <?php echo form_close(); ?>
 
  <script type="text/javascript">
            
              function update_family_history() {
              document.getElementById("family_history").submit();
             }
    </script>
    
    
 
         <!-- MODAL EDIT My Doctor -->
<?php $attributes = array("name" => "my_doctor", "id"=> "my_doctor" , "enctype" => 'multipart/form-data');
                echo form_open("userprofile/editMyDoctor", $attributes);?>
 <div class="modal edit-my-doctor-modal sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
			 
			<div class="appointment-form">
                           
                        <div class="form-group">
					<label for="inputEmail" class="  control-label">Name </label>
					<div class=" ">
                                            <input id="mdid" type="hidden" name="mdid" value="">
                                            <input id="md_name" name="md_name" type="text" value="" class="form-control">
					</div>
				</div>
                            <div class="form-group">
					<label for="inputEmail" class="  control-label">Surname </label>
					<div class=" ">
                                            
                                            <input id="md_sname" name="md_sname" type="text" value="" class="form-control">
					</div>
				</div>
                            
                            <div class="form-group">
					<label for="inputEmail" class="  control-label">Contact Number </label>
					<div class=" ">
                                            
                                            <input id="md_number" name="md_number" type="text" value="" class="form-control">
					</div>
				</div>
                            
                            <div class="form-group">
					<label for="inputEmail" class="  control-label">Email Address </label>
					<div class=" ">
                                            
                                            <input id="md_email" name="md_email" type="text" value="" class="form-control">
					</div>
				</div>
                           
                          
                            <button onclick="update_my_doctor()" class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".my_doctor" >Save</button>
		</div>
		  
      </div>
      
    </div>
  </div>
  </div></div>
     <?php echo form_close(); ?>
 
  <script type="text/javascript">
            
              function update_my_doctor() {
              document.getElementById("my_doctor").submit();
             }
    </script>
    
    
   
    
    

  <!--ADD NEW ALLERGY MODAL -->
  <?php $attributes = array("name" => "add_new_allergy", "id"=> "add_new_allergy" , "enctype" => 'multipart/form-data');
                echo form_open("userprofile/addNewAllergy", $attributes);?>
  
 <div class="modal add-more-allergy-modal sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
			 
			<div class="appointment-form">
                           
            <div class="form-group">
					<label for="inputEmail" class="  control-label">Resistant </label>
					<div class=" ">
                                            <input id="profile_id" type="hidden" name="id" value="">
                                            <input name="add_resistant" type="text" value="" placeholder="Nuts"  onfocus="this.placeholder = ''" class="form-control" id="inputEmail " onblur="this.placeholder = 'Nuts'">
					</div>
				</div>
		 <div class="form-group">
					<label for="inputEmail" class="  control-label">Reaction </label>
					<div class=" ">
                                            <input name="add_reaction" type="text" placeholder="Swelling" class="form-control" id="inputEmail" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Swelling'">
					</div>
				</div>
				 
                           
                            <button onclick="add_allergy()" class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".add_new_allergy" >Submit</button>
		</div>
		  
      </div>
      
    </div>
  </div>
</div></div>
    <?php echo form_close(); ?>
    <script type="text/javascript">
            
              function add_allergy() {
              document.getElementById("add_new_allergy").submit();
             }
    </script>
    
    
    
    
     <!--ADD NEW PRE EXISITING CONDITION -->
  <?php $attributes = array("name" => "add_new_pec", "id"=> "add_new_pec" , "enctype" => 'multipart/form-data');
                echo form_open("userprofile/addNewPec", $attributes);?>
  
 <div class="modal add_new_pec sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
			 
			<div class="appointment-form">
                           
            <div class="form-group">
					<label for="inputEmail" class="  control-label">Feeling </label>
					<div class=" ">
                                            
                                            <input name="add_feeling" type="text" value="" class="form-control" id="inputEmail">
					</div>
				</div>
		 
				 
                           
                            <button onclick="add_pec()" class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".add_new_pec" >Submit</button>
		</div>
		  
      </div>
      
    </div>
  </div>
</div></div>
    <?php echo form_close(); ?>
    <script type="text/javascript">
            
              function add_pec() {
              document.getElementById("add_new_pec").submit();
             }
    </script>
    
    
    
    
    
     <!--ADD NEW PERSONAL UPDATE MODAL -->
  <?php $attributes = array("name" => "add_new_pu", "id"=> "add_new_pu" , "enctype" => 'multipart/form-data');
                echo form_open("userprofile/addNewPersonalUpdate", $attributes);?>
  
 <div class="modal add_new_pu sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
			 
			<div class="appointment-form">
                           
            <div class="form-group">
					<label for="inputEmail" class="  control-label">How are you feeling?</label>
					<div class=" ">
                                            
                                            <input name="add_pu_feeling" type="text" value="" class="form-control" id="inputEmail">
					</div>
				</div>
		  <div class="form-group">
					<label for="inputEmail" class="  control-label">Symptoms </label>
					<div class=" ">
                                            
                                            <input name="add_symptom" type="text" value="" class="form-control" id="inputEmail">
					</div>
				</div>
		 
                             <div class="form-group">
					<label for="inputEmail" class="  control-label">Medication taken</label>
					<div class=" ">
                                            
                                            <input name="add_med_taken" type="text" value="" class="form-control" id="inputEmail">
					</div>
				</div>
		 
                             <div class="form-group">
					<label for="inputEmail" class="  control-label">Medicine Description </label>
					<div class=" ">
                                            
                                            <input name="add_desc_med" type="text" value="" class="form-control" id="inputEmail">
					</div>
				</div>
		    <?php /*
                             <div class="form-group">
					<label for="inputEmail" class="  control-label">Date </label>
					<div class="date-select-small ">
                                            
                                            <select name="dd" class="form-control" id="select">
                                         <?php
                                        for($i=1; $i<=31; $i++){
                                            $selected = '';
                                        if (date('d') == $i) $selected = ' selected="selected"';
                                        print('<option value="'.str_pad($i, 2, "0", STR_PAD_LEFT).'"'.$selected.'>'.str_pad($i, 2, "0", STR_PAD_LEFT).'</option>'."\n"); 
                                        }
                                        ?>
        </select>
                                        <select name="mm" class="form-control" id="select">
                                        <option value="01" <?php echo date('m') == '01' ? 'selected' : ''; ?>>January</option>
                                        <option value="02" <?php echo date('m') == '02' ? 'selected' : ''; ?>>February</option>
                                        <option value="03" <?php echo date('m') == '03' ? 'selected' : ''; ?>>March</option>
                                        <option value="04" <?php echo date('m') == '04' ? 'selected' : ''; ?>>April</option>
                                        <option value="05" <?php echo date('m') == '05' ? 'selected' : ''; ?>>May</option>
                                        <option value="06" <?php echo date('m') == '06' ? 'selected' : ''; ?>>June</option>
                                        <option value="07" <?php echo date('m') == '07' ? 'selected' : ''; ?>>July</option>
                                        <option value="08" <?php echo date('m') == '08' ? 'selected' : ''; ?>>August</option>
                                        <option value="09" <?php echo date('m') == '09' ? 'selected' : ''; ?>>September</option>
                                        <option value="10" <?php echo date('m') == '10' ? 'selected' : ''; ?>>October</option>
                                        <option value="11" <?php echo date('m') == '11' ? 'selected' : ''; ?>>November</option>
                                        <option value="12" <?php echo date('m') == '12' ? 'selected' : ''; ?>>December</option>
        </select>
                                                <select name="yy" class="form-control" id="select">
          <?php
                                        for($i=date('Y'); $i>date('Y')-10; $i--) {
                                        $selected = '';
                                        //if ($cardExpDate[0] == $i) //$selected = ' selected="selected"';
                                        print('<option value="'.$i.'"'.$selected.'>'.$i.'</option>'."\n");
                                    }
                                    ?>
        </select>
					</div>
				</div>
		    */ ?>
				 
                           
                            <button onclick="add_pu()" class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".add_new_pu" >Submit</button>
		</div>
		  
      </div>
      
    </div>
  </div>
</div></div>
    <?php echo form_close(); ?>
    <script type="text/javascript">
            
              function add_pu() {
              document.getElementById("add_new_pu").submit();
             }
    </script>
    
    
    
        
     <!--ADD NEW FAMILY HISTORY -->
  <?php $attributes = array("name" => "add_new_fh", "id"=> "add_new_fh" , "enctype" => 'multipart/form-data');
                echo form_open("userprofile/addNewFamilyHistory", $attributes);?>
  
 <div class="modal add_new_fh sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
			 
			<div class="appointment-form">
                           
                                <div class="form-group">
					<label for="inputEmail" class="  control-label">Condition </label>
					<div class=" ">
                                            
                                            <input name="add_condition" type="text" value="" class="form-control" id="inputEmail">
					</div>
				</div>
                            
                            <div class="form-group">
					<label for="inputEmail" class="  control-label">Relationship </label>
					<div class=" ">
                                            
                                            <input name="add_relation" type="text" value="" class="form-control" id="inputEmail">
					</div>
				</div>
		 
				 
                           
                            <button onclick="add_fh()" class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".add_new_fh" >Submit</button>
		</div>
		  
      </div>
      
    </div>
  </div>
</div></div>
    <?php echo form_close(); ?>
    <script type="text/javascript">
            
              function add_fh() {
              document.getElementById("add_new_fh").submit();
             }
    </script>
    
 
    
    
    <!--ADD NEW MY DOCTOR -->
  <?php $attributes = array("name" => "add_new_md", "id"=> "add_new_md" , "enctype" => 'multipart/form-data');
                echo form_open("userprofile/addNewMyDoctor", $attributes);?>
  
 <div class="modal add_new_md sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
			 
			<div class="appointment-form">
                           
                                <div class="form-group">
					<label for="inputEmail" class="  control-label">Name </label>
					<div class=" ">
                                            
                                            <input name="add_name" type="text" value="" class="form-control" id="inputEmail">
					</div>
				</div>
                            
                            <div class="form-group">
					<label for="inputEmail" class="  control-label">Surname </label>
					<div class=" ">
                                            
                                            <input name="add_sname" type="text" value="" class="form-control" id="inputEmail">
					</div>
				</div>
                            
                            <div class="form-group">
					<label for="inputEmail" class="  control-label">Contact number </label>
					<div class=" ">
                                            
                                            <input name="add_number" type="text" value="" class="form-control" id="inputEmail">
					</div>
				</div>
                            <div class="form-group">
					<label for="inputEmail" class="  control-label">Email address </label>
					<div class=" ">
                                            
                                            <input name="add_email" type="text" value="" class="form-control" id="inputEmail">
					</div>
				</div>
				 
                           
                            <button onclick="add_md()" class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".add_new_md" >Submit</button>
                            
                        </div>
		  
      </div>
      
    </div>
  </div>
</div></div>
    <?php echo form_close(); ?>
    
    
    <script type="text/javascript">
            
              function add_md() {
              document.getElementById("add_new_md").submit();
             }
    </script>
    
    
    
    <!-- Delete MODAL FOR ALLERGY -->
    
  <?php $attributes = array("name" => "del_allergy", "id"=> "del_allergy" , "enctype" => 'multipart/form-data');
                echo form_open("userprofile/deleteAllergy", $attributes);?>
  
 <div class="modal del_allergy sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
			 
			<div class="appointment-form">
                            <input id="allergy_id" name="allergy_id" type="hidden"/> 
                           Are you Sure you want to delete?
                            <button onclick="delete_allergy()" class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".del_allergy" >Yes</button>
                            <button   class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal"  >No</button>
		</div>
		  
      </div>
      
    </div>
  </div>
</div></div>
    <?php echo form_close(); ?>
    <script type="text/javascript">
            
              function delete_allergy() {
              document.getElementById("del_allergy").submit();
             }
    </script>
    
    
    
    
    
     <!-- Delete MODAL FOR PEC -->
    
  <?php $attributes = array("name" => "del_pec", "id"=> "del_pec" , "enctype" => 'multipart/form-data');
                echo form_open("userprofile/deletePreExistingCondition", $attributes);?>
  
 <div class="modal del_pec sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
			 
			<div class="appointment-form">
                            <input id="pec_id" name="pec_id" type="hidden"/> 
                           Are you Sure you want to delete?
                            <button onclick="delete_pec()" class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".del_pec" >Yes</button>
                            <button   class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal"  >No</button>
		</div>
		  
      </div>
      
    </div>
  </div>
</div></div>
    <?php echo form_close(); ?>
    <script type="text/javascript">
            
              function delete_pec() {
              document.getElementById("del_pec").submit();
             }
    </script>



<!-- Delete MODAL FOR Personal update -->
    
  <?php $attributes = array("name" => "del_pu", "id"=> "del_pu" , "enctype" => 'multipart/form-data');
                echo form_open("userprofile/deletePersonalUpdate", $attributes);?>
  
 <div class="modal del_pu sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
			 
			<div class="appointment-form">
                            <input id="pu_id" name="pu_id" type="hidden"/> 
                           Are you Sure you want to delete?
                            <button onclick="delete_pu()" class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".del_pu" >Yes</button>
                            <button   class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal"  >No</button>
		</div>
		  
      </div>
      
    </div>
  </div>
</div></div>
    <?php echo form_close(); ?>
    <script type="text/javascript">
            
              function delete_pu() {
              document.getElementById("del_pu").submit();
             }
    </script>
    
    
    <!-- Delete MODAL FOR FAMILY HISTORY -->
    
  <?php $attributes = array("name" => "del_fh", "id"=> "del_fh" , "enctype" => 'multipart/form-data');
                echo form_open("userprofile/deleteFamilyHistory", $attributes);?>
  
 <div class="modal del_fh sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
			 
			<div class="appointment-form">
                            <input id="fh_id" name="fh_id" type="hidden"/> 
                           Are you Sure you want to delete?
                            <button onclick="delete_fh()" class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".del_fh" >Yes</button>
                            <button   class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal"  >No</button>
                            
		</div>
		  
      </div>
      
    </div>
  </div>
</div></div>
    <?php echo form_close(); ?>
    <script type="text/javascript">
            
              function delete_fh() {
              document.getElementById("del_fh").submit();
             }
    </script>
    
    
       <!-- Delete MODAL FOR FAMILY HISTORY -->
    
  <?php $attributes = array("name" => "del_md", "id"=> "del_md" , "enctype" => 'multipart/form-data');
                echo form_open("userprofile/deleteMyDoctor", $attributes);?>
  
 <div class="modal del_md sad-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
			 
			<div class="appointment-form">
                            <input id="md_id" name="md_id" type="hidden"/> 
                           Are you Sure you want to delete?
                            <button onclick="delete_md()" class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".del_md" >Yes</button>
                            <button   class="sad-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal"  >No</button>
		</div>
		  
      </div>
      
    </div>
  </div>
</div></div>
 <script type="text/javascript">
                                   
			function toggleCondTab(lid,did) {
				$('.tabLinkClass').removeClass('active');
				
				$('#my-allergies').hide();
				$('#existing-condition').hide();
				$('#personal-updates').hide();
				$('#family-history').hide();
				$('#my-doctors').hide();
				$('#medical-history').hide();
				
				
				$('#'+lid).addClass('active');
				$('#'+did).show();
				
				
			}
   
   </script>
    <?php echo form_close(); ?>
    <script type="text/javascript">
            
              function delete_md() {
              document.getElementById("del_md").submit();
             }
    </script>
    
    
       
    
