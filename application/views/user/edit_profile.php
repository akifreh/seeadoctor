<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Profile</title>
    <link href="<?php echo base_url("/css/bootstrap.css"); ?>" rel="stylesheet" type="text/css" />
</head>
<body>
<?php echo $this->session->flashdata('msg'); ?>
<?php echo $this->session->flashdata('other_msg'); ?> 
<?php echo $this->session->flashdata('other_error'); ?> 

<?php 
if($userinfo) :
	foreach ($userinfo as $row) :
	endforeach; 
	$dateOfBirth = explode("-", $row->dob);
endif;
?>
    <?php if(isset($error)) : ?>
    <div class="alert alert-danger text-center"><?php echo $error; ?></div>
    <?php endif; ?>
    
     <div class="signup-form-step">
           <?php $attributes = array("name" => "registrationform", "enctype" => 'multipart/form-data');
                echo form_open("home/complete-profile", $attributes);?>
         
	 <div class="container">
		<div class="personal-info">
		<div class="row">
			<div class="col-lg-2 col-md-2 user-pro-pic-main padding-right-0">
                            <div style="position:relative; width:100%;">
                      <img src="<?php echo isset($row->profile_pic) ? $row->profile_pic : ''; ?>" id="pro-pic-id" alt="Profile-Image" style="width:100%;height: 210px;">
                            <input type="file" name="profile_pic" id="add-pro-pic" class="add-profile-pic"> 
                            <span class="add-pro-pic-link"><i class="fa fa-camera" aria-hidden="true"></i> Add Photo</span>
                         </div>
                         <!--<div class="prfoile-pic-update" style="display:none;" >Please Click Update Button to Change your Profile Picture</div>-->
                         </div>
                    
			<div class="col-lg-3 col-md-3">
			<div class="col-lg-12 col-md-12"><h2>Personal Details</h2></div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-2 control-label">Name</label>
					<div class="col-lg-12">
                        <input name="id" type="hidden" class="form-control" value="<?php echo isset($row->id) ? $row->id : '';?>" />
                        <input name="fname" type="text" class="form-control" value="<?php echo isset($row->fname) ? $row->fname : ''; ?>" placeholder="First name" id="inputEmail" readOnly>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-2 control-label">Surname</label>
					<div class="col-lg-12">
                        <input name="sname" type="text" class="form-control" value="<?php echo isset($row->sname) ? $row->sname : '';?>"  placeholder="Surname" id="inputEmail" readOnly>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-2 control-label">Date of birth</label>
						<div class="col-lg-12 col-md-12">
				<select class="form-control" name="b_dd" id="select" value="<?php echo isset($dateOfBirth[2]) ? $dateOfBirth[2] : '' ?>">
                                    <?php
                                        for($i=1; $i<=31; $i++){
                                            $selected = '';
                                        if (isset($dateOfBirth[2]) &&  $dateOfBirth[2] == $i) $selected = ' selected="selected"';
                                        print('<option value="'.str_pad($i, 2, "0", STR_PAD_LEFT).'"'.$selected.'>'.str_pad($i, 2, "0", STR_PAD_LEFT).'</option>'."\n"); 
                                        }
                                    ?>
                         </select>
                        <select name="b_mm" class="form-control" id="select">
                          <option value="01" <?php echo isset($dateOfBirth[1]) && $dateOfBirth[1] == 1 ? 'selected' : '';?>>Jan</option>
                          <option value="02" <?php echo isset($dateOfBirth[1]) && $dateOfBirth[1] == 2 ? 'selected' : '';?>>Feb</option>
                          <option value="03" <?php echo isset($dateOfBirth[1]) && $dateOfBirth[1] == 3 ? 'selected' : '';?>>Mar</option>
                          <option value="04" <?php echo isset($dateOfBirth[1]) && $dateOfBirth[1] == 4 ? 'selected' : '';?>>Apr</option>
                          <option value="05" <?php echo isset($dateOfBirth[1]) && $dateOfBirth[1] == 5 ? 'selected' : '';?>>May</option>
                          <option value="06" <?php echo isset($dateOfBirth[1]) && $dateOfBirth[1] == 6 ? 'selected' : '';?>>June</option>
                          <option value="07" <?php echo isset($dateOfBirth[1]) && $dateOfBirth[1] == 7 ? 'selected' : '';?>>July</option>
                          <option value="08" <?php echo isset($dateOfBirth[1]) && $dateOfBirth[1] == 8 ? 'selected' : '';?>>Aug</option>
                          <option value="09" <?php echo isset($dateOfBirth[1]) && $dateOfBirth[1] == 9 ? 'selected' : '';?>>Sep</option>
                          <option value="10" <?php echo isset($dateOfBirth[1]) && $dateOfBirth[1] == 10 ? 'selected' : '';?>>Oct</option>
                          <option value="11" <?php echo isset($dateOfBirth[1]) && $dateOfBirth[1] == 11 ? 'selected' : '';?>>Nov</option>
                          <option value="12" <?php echo isset($dateOfBirth[1]) && $dateOfBirth[1] == 12 ? 'selected' : '';?>>Dec</option>
                        </select>
							<select name="b_yy" class="form-control" id="select" value="<?php //echo $row->dob; ?>">
                                    <?php
                                    for($i=date('Y'); $i>1899; $i--) {
                                        $selected = '';
                                        if (isset($dateOfBirth[0]) && $dateOfBirth[0] == $i) $selected = ' selected="selected"';
                                        print('<option value="'.$i.'"'.$selected.'>'.$i.'</option>'."\n");
                                    }
                                    ?>
        </select>
				</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
			<div class="col-lg-12 col-md-12"><h2>Contact Details</h2></div>
            
            
	     			<div class="form-group">
      <label for="textArea" class="col-lg-2 control-label">Residential Address</label>
      <div class="col-lg-12">
          <input type="text" name="address" class="form-control" placeholder="Address" id="textAreaAddress" value="<?php echo isset($row->address) ? $row->address : '';?>">
      </div>
      <div class="col-lg-12">
          <input type="text" name="city" class="form-control" placeholder="City" id="textCity" value="<?php echo isset($row->city) ? $row->city : '';?>">
      </div>
      <div class="col-lg-12">
          <input type="text" name="province" class="form-control" placeholder="Province" id="textProvince" value="<?php echo isset($row->province) ? $row->province : '';?>">
      </div>
      
    </div>
    <div class="form-group">
        
            
        <label for="textArea" class="col-lg-2 control-label">Postal Address &nbsp;&nbsp;<input type="checkbox" name="sameasres" id="sameasres" value="1" <?php echo ($row->sameasres =='1') ? 'chacked' : ''; ?> /> <small style="font-weight:normal">Same as above</small></label> 
        <div class="col-lg-12">
            <input type="text"  name="p_address"  class="form-control" placeholder="Address" id="textAreaPostal" value="<?php echo isset($row->p_address) ? $row->p_address : '';?>">
        </div>
        <div class="col-lg-12">
          <input type="text" name="p_city" class="form-control" placeholder="City" id="textCityPostal" value="<?php echo isset($row->p_city) ? $row->p_city : '';?>">
      </div>
      <div class="col-lg-12">
          <input type="text" name="p_province" class="form-control" placeholder="Province" id="textProvincePostal" value="<?php echo isset($row->p_province) ? $row->p_province : '';?>">
      </div>
    </div>
                        
    <script type="text/javascript">
    $(document).ready(function() {
        $("#sameasres").click(function(){
            var checkstatus = $("#sameasres").is(':checked') ? 1 : 0;
            if(checkstatus == 1){
                var PostaddressVal = $("#textAreaAddress").val();
                var PostcityVal = $("#textCity").val();
                var PostprovinceVal = $("#textProvince").val();
            } else {
                var PostaddressVal = '';
                var PostcityVal = '';
                var PostprovinceVal = '';
            }
            $("#textAreaPostal").val(PostaddressVal);
            $("#textCityPostal").val(PostcityVal);
            $("#textProvincePostal").val(PostprovinceVal);
        });
    });
    </script>
                
    
			</div>
			<div class="col-lg-3 col-md-3 contact-detail-last">
			
			
				<!--div class="form-group">
					<label for="inputEmail" class="col-lg-2 control-label">Contact Number</label>
					<div class="col-lg-12">
                                            <input name="contact_number" type="text" class="form-control" value="<?php echo isset($row->contact_number) ? $row->contact_number : '';?>" placeholder="Contact number" id="inputEmail">
					</div>
				</div-->
				
            <div class="form-group">
					<label for="inputEmail" class="col-lg-2 control-label">Cell Number </label>
					<div class="col-lg-12">
                                            <input name="phone" type="text" class="form-control" value="<?php echo isset($row->phone) ? $row->phone : '';?>" placeholder="Phone number" id="inputEmail">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-2 control-label">Email Address </label>
					<div class="col-lg-12">
                                            <input name="email" type="text" class="form-control" value="<?php echo isset($row->email) ? $row->email : '';?>" placeholder="Email address" id="inputEmail" readonly>
					</div>
				</div>
           <div class="col-lg-12">
	               <a href="<?php echo base_url() . 'user/changePassword'; ?>">Change Password</a>
                   </div>
                <button name="submit" type="submit" value="personal" class="btn btn-default submit-button pull-right">Update</button> 
                
                
			</div>
		</div>
		</div>
	 </div>
     <?php echo form_close(); ?>        
 </div>
    
    
     <div class="statutory-section">
	<div class="container">
		<div class="statutory-section-inner">
		
<div class="statutory-section-heading col-lg-12">
			<h2>Statutory</h2>
		</div>
		<div class="row">
        <div class="col-lg-4 col-md-4 ">
           <?php $attributes = array("name" => "registrationform", "enctype" => 'multipart/form-data');
                echo form_open("home/complete-profile", $attributes);?>
   <div class="row">
      <div class="col-lg-6 col-md-6 expiry">
      <label for="textArea" class="  control-label">Credit Card Type</label>
                    <input type="radio" name="cc_type"  id="card_brand-1" value="VISA" <?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'VISA' ? 'checked' : ''; ?> disabled>&nbsp;&nbsp;VISA
                    <input type="radio" name="cc_type"  id="card_brand-2" value="MASTER" <?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'MASTER' ? 'checked' : ''; ?> disabled>&nbsp;&nbsp;MASTER      
                    
                        <!--select name="cc_type" class="form-control" id="card_brand" disabled>
                        <option value="VISA" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'VISA' ? 'selected' : ''; ?>>VISA</option>
                        <option value="MASTER" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'MASTER' ? 'selected' : ''; ?>>MASTER</option>
                        <option value="ELO" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'ELO' ? 'selected' : ''; ?>>ELO</option>
                        <option value="JCB" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'JCB' ? 'selected' : ''; ?>>JCB</option>
                        <option value="AMEX" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'AMEX' ? 'selected' : ''; ?>>AMEX</option>
                        <option value="VPAY" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'VPAY' ? 'selected' : ''; ?>>VPAY</option>
                        <option value="AXESS" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'AXESS' ? 'selected' : ''; ?>>AXESS</option>
                        <option value="BONUS" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'BONUS' ? 'selected' : ''; ?>>BONUS</option>
                        <option value="CABAL" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'CABAL' ? 'selected' : ''; ?>>CABAL</option>
                        <option value="WORLD" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'WORLD' ? 'selected' : ''; ?>>WORLD</option>
                        <option value="DINERS" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'DINERS' ? 'selected' : ''; ?>>DINERS</option>
                        <option value="BITCOIN" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'BITCOIN' ? 'selected' : ''; ?>>BITCOIN</option>
                        <option value="DANKORT" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'DANKORT' ? 'selected' : ''; ?>>DANKORT</option>
                        <option value="MAESTRO" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'MAESTRO' ? 'selected' : ''; ?>>MAESTRO</option>
                        <option value="MAXIMUM" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'MAXIMUM' ? 'selected' : ''; ?>>MAXIMUM</option>
                        <option value="AFTERPAY" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'AFTERPAY' ? 'selected' : ''; ?>>AFTERPAY</option>
                        <option value="ASYACARD" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'ASYACARD' ? 'selected' : ''; ?>>ASYACARD</option>
                        <option value="DISCOVER" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'DISCOVER' ? 'selected' : ''; ?>>DISCOVER</option>
                        <option value="POSTEPAY" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'POSTEPAY' ? 'selected' : ''; ?>>POSTEPAY</option>
                        <option value="SERVIRED" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'SERVIRED' ? 'selected' : ''; ?>>SERVIRED</option>
                        <option value="HIPERCARD" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'HIPERCARD' ? 'selected' : ''; ?>>HIPERCARD</option>
                        <option value="VISADEBIT" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'VISADEBIT' ? 'selected' : ''; ?>>VISADEBIT</option>
                        <option value="CARDFINANS" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'CARDFINANS' ? 'selected' : ''; ?>>CARDFINANS</option>
                        <option value="CARTEBLEUE" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'CARTEBLEUE' ? 'selected' : ''; ?>>CARTEBLEUE</option>
                        <option value="VISAELECTRON" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'VISAELECTRON' ? 'selected' : ''; ?>>VISAELECTRON</option>
                        <option value="CARTEBANCAIRE" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'CARTEBANCAIRE' ? 'selected' : ''; ?>>CARTEBANCAIRE</option>
                        <option value="KLARNA_INVOICE" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'KLARNA_INVOICE' ? 'selected' : ''; ?>>KLARNA_INVOICE</option>
                        <option value="DIRECTDEBIT_SEPA" ?php echo isset($cardInfo->card_brand) && $cardInfo->card_brand == 'DIRECTDEBIT_SEPA' ? 'selected' : ''; ?>>DIRECTDEBIT_SEPA</option>
                    </select-->         
    </div>
     <div class="col-lg-6 col-md-6">
    <label for="textArea" class="control-label">CSV</label>
        <input type="text" class="form-control" name="cc_csv_number" id="card_csv" value="" disabled maxlength="3" required>
    </div>
   </div>
   <div class="row">
    <div class="col-lg-12 col-md-12">
      <input name="id" type="hidden" class="form-control" value="<?php echo isset($row->id) ? $row->id : '';?>" />
    <label for="textArea" class="control-label">Credit Card Number</label>
                                <input type="text" id="cc" class="form-control" name="cc_number" placeholder="XXXX XXXX XXXX <?php echo $cardInfo ? $cardInfo->card_number : 'XXXX'; ?>" value="" disabled required>
    </div>
  </div>
  <div class="row">
   <div class="col-lg-12 col-md-12 expiry">
     <label for="textArea" class="  control-label">Credit Card Expiry</label>
   </div>
 <div class="col-lg-6 col-md-6" style="text-align:left;">

                       <select name="ex_mm" class="form-control" id="card_month" disabled>
                                        <option value="">Month</option>
                                        <option value="01" <?php echo isset($cardInfo->card_expiry) && date('m', strtotime($cardInfo->card_expiry)) == '01' ? 'selected' : ''; ?>>January</option>
                                        <option value="02" <?php echo isset($cardInfo->card_expiry) && date('m', strtotime($cardInfo->card_expiry)) == '02' ? 'selected' : ''; ?>>February</option>
                                        <option value="03" <?php echo isset($cardInfo->card_expiry) && date('m', strtotime($cardInfo->card_expiry)) == '03' ? 'selected' : ''; ?>>March</option>
                                        <option value="04" <?php echo isset($cardInfo->card_expiry) && date('m', strtotime($cardInfo->card_expiry)) == '04' ? 'selected' : ''; ?>>April</option>
                                        <option value="05" <?php echo isset($cardInfo->card_expiry) && date('m', strtotime($cardInfo->card_expiry)) == '05' ? 'selected' : ''; ?>>May</option>
                                        <option value="06" <?php echo isset($cardInfo->card_expiry) && date('m', strtotime($cardInfo->card_expiry)) == '06' ? 'selected' : ''; ?>>June</option>
                                        <option value="07" <?php echo isset($cardInfo->card_expiry) && date('m', strtotime($cardInfo->card_expiry)) == '07' ? 'selected' : ''; ?>>July</option>
                                        <option value="08" <?php echo isset($cardInfo->card_expiry) && date('m', strtotime($cardInfo->card_expiry)) == '08' ? 'selected' : ''; ?>>August</option>
                                        <option value="09" <?php echo isset($cardInfo->card_expiry) && date('m', strtotime($cardInfo->card_expiry)) == '09' ? 'selected' : ''; ?>>September</option>
                                        <option value="10" <?php echo isset($cardInfo->card_expiry) && date('m', strtotime($cardInfo->card_expiry)) == '10' ? 'selected' : ''; ?>>October</option>
                                        <option value="11" <?php echo isset($cardInfo->card_expiry) && date('m', strtotime($cardInfo->card_expiry)) == '11' ? 'selected' : ''; ?>>November</option>
                                        <option value="12" <?php echo isset($cardInfo->card_expiry) && date('m', strtotime($cardInfo->card_expiry)) == '12' ? 'selected' : ''; ?>>December</option>
                                    </select>
                                     </div>
                                     <div class="col-lg-6 col-md-6" style="text-align:left;">
                                    <select name="ex_yy" class="form-control" id="card_year" disabled>
                                       <option value="">Year</option>
                                       <?php
                                    for($i=date('Y')+5; $i>2015; $i--) : ?>
                                        <option value="<?php echo $i; ?>" <?php echo isset($cardInfo->card_expiry) && date('Y', strtotime($cardInfo->card_expiry)) == $i ? 'selected' : ''; ?>><?php echo $i; ?></option>
                                    <?php endfor; ?>
                                    </select>
                  </div>
			</div>
      <div class="row">
                      <div class="col-lg-12 col-md-12" style="text-align:right;">
                        <button name="button" id="edit" type="button" onclick="enableInput()" class="btn btn-default submit-button">Edit</button>&nbsp;&nbsp;
                        <div id="save-card" class="pull-right" hidden> <button name="submit" value="card" type="submit" class="btn   submit-button">Save</button></div>
                       
                  </div>
      </div>
      <?php echo form_close(); ?>
		</div>
			<div class="col-lg-8 col-md-8 border-left-cl">
        <?php $attributes = array("name" => "registrationform", "enctype" => 'multipart/form-data');
                echo form_open("home/complete-profile", $attributes);?>
			<div class="row">
        <div class="col-lg-6 col-md-6">
          <input name="id" type="hidden" class="form-control" value="<?php echo isset($row->id) ? $row->id : '';?>" />
        <div class="form-group">
          <label for="inputEmail" class="col-lg-12 control-label">Do you have a job</label>
				  <div class="col-lg-12">
          <input type="radio" id="noJob" name="has_job" value="no" <?php echo $row->job_title == 'No' ? 'checked' : ''; ?>> No &nbsp;&nbsp;
          <input type="radio" id="yesJob" name="has_job" value="yes" <?php echo $row->job_title != 'No' ? 'checked' : ''; ?>> Yes
        </div>
      </div> 
       
       
      <div class="clearfix"></div>
      <div   id="job-fields" <?php echo $row->job_title == 'No' ? 'hidden' : ''; ?> style="margin-top:10px;">
					<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Job Title </label>
					<div class="col-lg-12">
                                            <input name="job_title" type="text" class="form-control" id="job_title" value="<?php echo isset($row->job_title) && $row->job_title != 'No' ? $row->job_title : '';?>" placeholder="Enter current job title" <?php echo $row->job_title == 'No' ? 'disabled' : ''; ?> required>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Company Employed</label>
					<div class="col-lg-12">
						<input name="company_employed" type="text" class="form-control" id="company_employed" value="<?php echo isset($row->company_employed) && $row->company_employed != 'None' ? $row->company_employed : '';?>" placeholder="Enter company name" <?php echo $row->job_title == 'No' ? 'disabled' : ''; ?> required>
					</div>
				</div>
				 
				</div>
    </div>
      
				<div class="col-lg-6 col-md-6">
					<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Next of Kin </label>
					<div class="col-lg-12">
						<input name="next_of_kin" type="text" class="form-control" value="<?php echo isset($row->next_of_kin) ? $row->next_of_kin : '';?>" placeholder="Name and Surname">
					</div>
				</div>
        <div class=" ">
        <div class="form-group">
          <label for="inputEmail" class="col-lg-12 control-label">Do you have medical aid</label>
          <div class="col-lg-12">
          <input type="radio" id="noAid" name="has_aid" value="no" <?php echo empty($row->medical_aid) || $row->medical_aid == 'No' ? 'checked' : ''; ?>> No &nbsp;&nbsp;
          <input type="radio" id="yesAid" name="has_aid" value="yes"<?php echo isset($row->medical_aid) && $row->medical_aid != 'No' ? 'checked' : ''; ?>> Yes
        </div>
      </div>
    </div>
    
      <div class="clearfix"></div>
				<div class="form-group" id="aid-fields" <?php echo empty($row->medical_aid) || $row->medical_aid == 'No' ? 'hidden' : ''; ?> style="margin-top:10px; ">
					<label for="inputEmail" class="col-lg-12 control-label">Medical Aid </label>
					<div class="col-lg-12">
						<input name="medical_aid" type="text" class="form-control" id="medical_aid" value="<?php echo isset($row->medical_aid) && $row->medical_aid != 'No' ? $row->medical_aid : '';?>" placeholder="Enter medical aid" required <?php echo empty($row->medical_aid) || $row->medical_aid == 'No' ? 'disabled' : ''; ?>>
					</div>
				</div>
				 
				</div>
				</div>
                <div class="pull-right"> <button name="submit" type="submit" value="job" class="btn   submit-button">Save</button></div>
			<?php echo form_close(); ?>
    </div>
            
	</div>
         
 </div>
                        </div> </div>
<script type="text/javascript">
  function enableInput()
   {
      $('#cc').removeAttr("disabled");
      $('#card_month').removeAttr("disabled");
      $('#card_brand-1').removeAttr("disabled");
      $('#card_brand-2').removeAttr("disabled");
      $('#card_year').removeAttr("disabled");
      $('#card_csv').removeAttr("disabled");
      $('#edit').hide();
      $('#save-card').show();
   }
   </script>                        
 <div class="other-profile-section">
	<div class="container">
		<div class="row">
			   
                     <div class="col-lg-12" style="margin-bottom:15px;">
                     <div class="pro-heading pull-left"><h2>Other Profiles</h2></div>
                     <a href="javascripvoid:(0);" class="pull-right submit-button" data-toggle="modal" data-target=".add-other-profile">Add Other Profile</a>
                     </div>
                    
                      <div class="col-lg-12 col-md-12 table-responsive-res">
        <div class="profile-table table-responsive">
                                   <div class="table-head">
          <div class="table-header name-tab col-lg-2 col-md-2">Name</div>
          <div class="table-header name-tab col-lg-2 col-md-2">Sname</div>
          <div class="table-header col-lg-2 col-md-2">Relationship</div>
                    <div class="table-header name-tab col-lg-3 col-md-3">Date of Birth</div>
          <div class="table-header col-lg-3 col-md-3">&nbsp;</div>
                    </div>
              
              <?php if($otherprofile) :
                  foreach ($otherprofile as $row3) :?>

              <?php $attributes = array("name" => "request-a-doctor", "id" => "request-a-doctor");
                 echo form_open("user/editOtherProfile", $attributes) ;?>
                        <div class="table-row" id="row<?php echo $row3->id?>">
                        <div  class="table-data border-right col-lg-2 col-md-2">  
                        <input id="profile_id" type="hidden" name="id" value="<?php echo $row3->id; ?>">
                        <input id="profile_name" readonly name="name" type="text" value="<?php echo ucfirst($row3->name); ?>" class="form-control" id="inputEmail">
      </div>
                        <div id="p_sname" class="table-data border-right col-lg-2 col-md-2"> 
                       
                        <input type="text" class="form-control" readonly name="sname" value="<?php echo ucfirst($row3->sname); ?>" id="inputEmail" />
                        
                        </div> 
                        <div id="relation" class="table-data border-right col-lg-2 col-md-2"> 
                       
                        <input type="text" class="form-control" readonly name="relationship" value="<?php echo ucfirst($row3->relationship); ?>" id="profile_relation" />
                        
                        </div> 
                        <div id="dob" class="table-data border-right col-lg-3 col-md-3">  
     
                                     <select name="b_dd" class="form-control" disabled id="day">
                                        <?php
                                        $bDate = array();
                                        $bDate = explode('-', $row3->dob);
                                        
                                        for($i=1; $i<=31; $i++){
                                            $selected = '';
                                        if ($bDate[2] == $i) $selected = ' selected="selected"';
                                        print('<option value="'.str_pad($i, 2, "0", STR_PAD_LEFT).'"'.$selected.'>'.str_pad($i, 2, "0", STR_PAD_LEFT).'</option>'."\n"); 
                                        }
                                        ?>
                                      </select>
      
                                            
                                     <select name="b_mm" class="form-control" disabled id="month">
                          <option value="01" <?php echo isset($bDate[1]) && $bDate[1] == 1 ? 'selected' : '';?>>January</option>
                          <option value="02" <?php echo isset($bDate[1]) && $bDate[1] == 2 ? 'selected' : '';?>>February</option>
                          <option value="03" <?php echo isset($bDate[1]) && $bDate[1] == 3 ? 'selected' : '';?>>March</option>
                          <option value="04" <?php echo isset($bDate[1]) && $bDate[1] == 4 ? 'selected' : '';?>>April</option>
                          <option value="05" <?php echo isset($bDate[1]) && $bDate[1] == 5 ? 'selected' : '';?>>May</option>
                          <option value="06" <?php echo isset($bDate[1]) && $bDate[1] == 6 ? 'selected' : '';?>>June</option>
                          <option value="07" <?php echo isset($bDate[1]) && $bDate[1] == 7 ? 'selected' : '';?>>July</option>
                          <option value="08" <?php echo isset($bDate[1]) && $bDate[1] == 8 ? 'selected' : '';?>>August</option>
                          <option value="09" <?php echo isset($bDate[1]) && $bDate[1] == 9 ? 'selected' : '';?>>September</option>
                          <option value="10" <?php echo isset($bDate[1]) && $bDate[1] == 10 ? 'selected' : '';?>>October</option>
                          <option value="11" <?php echo isset($bDate[1]) && $bDate[1] == 11 ? 'selected' : '';?>>November</option>
                          <option value="12" <?php echo isset($bDate[1]) && $bDate[1] == 12 ? 'selected' : '';?>>December</option>
                                        
                                      </select>
     
                                               
         <select name="b_yy" class="form-control" disabled id="year">
                                    <?php
                                        for($i=date('Y'); $i>1899; $i--) {
                                        $selected = '';
                                        if ($bDate[0] == $i) $selected = ' selected="selected"';
                                        print('<option value="'.$i.'"'.$selected.'>'.$i.'</option>'."\n");
                                    }
                                    ?>
                                      </select>
   
         </div>
                        <div class="table-data col-lg-3 col-md-3">
                        <button id="saveButton<?php echo $row3->id?>" style="display:none;" onclick="update_otherprofile()" class="save-button"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".request-a-doctor" >Save</button>   
<!--                        <a href="<?php echo base_url(); ?>User/editOtherProfile" class="save-button" style="display:none;" id="saveButton<?php echo $row3->id?>" href="">Save</a>-->
                            <a href="javascript:void(0);" class="p-edit-link" id="editLink<?php echo $row3->id?>" data-id="<?php echo $row3->id; ?>" data-target=".p-edit-link">Edit Profile</a> 
                            
                            <!-- <a href="#" class="modal-for-profile" id="editmodal<?php // echo $row3->id?>" data-id="<?php // echo $row3->id; ?>" data-toggle="modal" data-target=".edit-profile-modal">Edit Profile</a> -->  <a href="<?php echo base_url(); ?>User/deleteOtherProfile/<?php echo $row3->id ?>" class="p-delete-link">Delete</a></div>
                    </div>
      <?php echo form_close();
                  endforeach;
                  endif; ?> 
          
        </div>
      </div>                     



                     <div class="modal add-other-profile sad-modal  "  >
  <div class="modal-dialog ">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <div class="modal-body">
      <?php $attributes = array("name" => "addprofile", "id"=> "addmore-form");
              echo form_open("user/addProfile", $attributes);?>
         <div class="  add-form">
                                <div class="form-group">
					<label for="inputEmail" class="  control-label">Name </label>
					<div class=" ">
                                            <input type="hidden" name="id" value="<?php echo $row->id; ?>" >
                                            <input name="name[]" type="text" class="form-control" id="inputEmail" required>
					</div>
				</div>

        <div class="  add-form">
                                <div class="form-group">
          <label for="inputEmail" class="  control-label">Sname </label>
          <div class=" ">
                                            <input name="sname[]" type="text" class="form-control" id="inputEmail" required>
          </div>
        </div>        
                            
				<div class="form-group">
					<label for="inputEmail" class="  control-label">Relationship </label>
					<div class=" ">
                                            <input name="relationship[]" type="text" class="form-control" id="inputEmail" required>
					</div>
				</div>
				<div class="form-group">
					
                                    <div id="idd" class=" ">
						<label for="inputEmail" class="control-label">Date of birth</label>
                                      <select name="b_dd[]" class="form-control" id="select">
                                        <?php
                                        for($i=1; $i<=31; $i++){
                                            $selected = '';
                                        //if ($cardExpDate[2] == $i) //$selected = ' selected="selected"';
                                        print('<option value="'.str_pad($i, 2, "0", STR_PAD_LEFT).'"'.$selected.'>'.str_pad($i, 2, "0", STR_PAD_LEFT).'</option>'."\n"); 
                                        }
                                        ?>
                                      </select>
                                      <select name="b_mm[]" class="form-control" id="select">
                          <option value="01" <?php echo isset($dateOfBirth2[1]) && $dateOfBirth[1] == 1 ? 'selected' : '';?>>January</option>
                          <option value="02" <?php echo isset($dateOfBirth2[1]) && $dateOfBirth[1] == 2 ? 'selected' : '';?>>February</option>
                          <option value="03" <?php echo isset($dateOfBirth2[1]) && $dateOfBirth[1] == 3 ? 'selected' : '';?>>March</option>
                          <option value="04" <?php echo isset($dateOfBirth2[1]) && $dateOfBirth[1] == 4 ? 'selected' : '';?>>April</option>
                          <option value="05" <?php echo isset($dateOfBirth2[1]) && $dateOfBirth[1] == 5 ? 'selected' : '';?>>May</option>
                          <option value="06" <?php echo isset($dateOfBirth2[1]) && $dateOfBirth[1] == 6 ? 'selected' : '';?>>June</option>
                          <option value="07" <?php echo isset($dateOfBirth2[1]) && $dateOfBirth[1] == 7 ? 'selected' : '';?>>July</option>
                          <option value="08" <?php echo isset($dateOfBirth2[1]) && $dateOfBirth[1] == 8 ? 'selected' : '';?>>August</option>
                          <option value="09" <?php echo isset($dateOfBirth2[1]) && $dateOfBirth[1] == 9 ? 'selected' : '';?>>September</option>
                          <option value="10" <?php echo isset($dateOfBirth2[1]) && $dateOfBirth[1] == 10 ? 'selected' : '';?>>October</option>
                          <option value="11" <?php echo isset($dateOfBirth2[1]) && $dateOfBirth[1] == 11 ? 'selected' : '';?>>November</option>
                          <option value="12" <?php echo isset($dateOfBirth2[1]) && $dateOfBirth[1] == 12 ? 'selected' : '';?>>December</option>
                                       
                                      </select>
                                      <select name="b_yy[]" class="form-control" id="select">
                                    <?php
                                        for($i=date('Y'); $i>1899; $i--) {
                                        $selected = '';
                                        //if ($cardExpDate[0] == $i) //$selected = ' selected="selected"';
                                        print('<option value="'.$i.'"'.$selected.'>'.$i.'</option>'."\n");
                                    }
                                    ?>
                                      </select>
				
				</div>
			
			</div>
                             
				<div class="button-submit">
					<button type="submit" class="btn btn-default submit-button">Submit</button>
				</div>
                            
                     </div>
		   <?php echo form_close(); ?>		
      </div>
      
    </div>
  </div>
</div>
                     
                    <script type="text/javascript">
                         $("#addmore-form").validate();
                    </script>
                           
                              <script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>     
                               <script type="text/javascript">
                                  
                                    $(document).ready(function(){
                                       
                                        $(document).on("click", "a.remove" , function() {
                                            $(this).parent().remove();
                                        });
                                    });
                                </script>
                            
		
                                         
                             
                                
                            
                    
                
			
		
                    <script type="text/javascript">
//                                function abc(x){
//                                  // alert("hello hi"+x);
//                                          $("#profile_id").val(x);
//                                          
//                                }
                                $(document).ready(function(){
                                    $('.modal-for-profile').on('click', function(){
                                       console.log($(this).attr('id'));
                                       // fetch id from a text.
                                       var rowId = $(this).attr('id');
                                      var trimrowID = rowId.replace("editmodal", "")
                                      console.log(trimrowID);
                                       $("#profile_id").val(trimrowID);
                                       console.log($( "#row"+trimrowID+" div:nth-child(1)" ).html());
                                        console.log($( "#row"+trimrowID+" div:nth-child(2)" ).html());
                                        var year = $( "#row"+trimrowID+" div:nth-child(3)" ).html();
                                          var dob = year.split('-');
                                         $('#profile_name').val($( "#row"+trimrowID+" div:nth-child(1)" ).html());
                                         $('#profile_relation').val($( "#row"+trimrowID+" div:nth-child(2)" ).html());
                                         $('#year').val(dob[0]);
                                         $('#month').val(dob[1]);
                                         $('#day').val(dob[2]);
                                    });
                                })
                            </script>        
                    
                    
	</div>
 </div>                 
		</div>
    
    <script type="text/javascript">
            
              function update_otherprofile() {
              document.getElementById("request-a-doctor").submit();
             }
			 $(".p-edit-link").click(function(){
				$(".profile-table .table-row").removeClass("now-editing");
				$(".profile-table .table-row").removeClass("now-editing");
				$(".profile-table .table-row").children('.table-data').children('input').attr("readonly","readonly");
				$(".profile-table .table-row").children('.table-data').children('select').attr("disabled","disabled");
				$(".profile-table .table-row").children('.table-data').children('.save-button').hide();
				$(".profile-table .table-row").children('.table-data').children('.p-delete-link').show();
				$(".profile-table .table-row").children('.table-data').children('.p-edit-link').show();
				$(this).parent().parent().addClass("now-editing");
				$(this).parent().parent().children('.table-data').children('input').removeAttr("readonly");
				$(this).parent().parent().children('.table-data').children('select').removeAttr("disabled");
				$(this).parent().children('.save-button').show();
				$(this).hide();
				$(this).parent().children('.p-delete-link').hide();
				
			});

    </script>
    <script>
function cc_format(value) {
  var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
  var matches = v.match(/\d{4,16}/g);
  var match = matches && matches[0] || ''
  var parts = []
  for (i=0, len=match.length; i<len; i+=4) {
    parts.push(match.substring(i, i+4))
  }
  if (parts.length) {
    return parts.join(' ')
  } else {
    return value
  }
}

onload = function() {
  document.getElementById('cc').oninput = function() {
    this.value = cc_format(this.value)
  }
}
</script>

<script type="text/javascript">
$(document).ready(function() {
     card = '<?php echo $add_card; ?>';
     if(card === 'add'){
       enableInput();
     }
     $('#yesJob').bind('change', function() {
             $('#job-fields').show();
             $('#job_title').removeAttr("disabled");
             $('#company_employed').removeAttr("disabled");
     });
     $('#noJob').bind('change', function() {
            $('#job-fields').hide();
            $('#job_title').attr('disabled', true);
            $('#company_employed').attr('disabled', true);
     });

      $('#yesAid').bind('change', function() {
            $('#aid-fields').show();
            $('#medical_aid').removeAttr("disabled");
     });
     $('#noAid').bind('change', function() {
            $('#aid-fields').hide();
            $('#medical_aid').attr('disabled', true);
     });
     
     
});


function readURL(input) {

    if (input.files && input.files[0]) {
        
         var url = input.value;
    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        
        
        if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#pro-pic-id').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            
        }
        else
        {
            alert("Please upload a valid image file.");
            $("#add-pro-pic").val('');
        }    
        
    }
}

$("#add-pro-pic").change(function(){
    readURL(this);
});

</script>


</div>
</body>
</html>