<div class="container">
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <?php echo $this->session->flashdata('msg');?>
        <?php echo $this->session->flashdata('verify_msg'); ?>
        <?php echo $this->session->flashdata('allreadyexist'); ?>
    </div>
</div>
    
 <div class="home-slider-signup">
  <div class="container1">
   <div class="home-slider-signup-inner">
   <div class="row"> 
    <?php if(isset($this->session->userdata['logged_in'])) : ?> 
        <div class="col-lg-12">  
            <div class="slider-sec">
				<img src="<?php echo base_url('images/slider.jpg') ?>" alt="" style="width:100%; " >
    <?php else : ?>        
        <div class="col-lg-8 col-md-8 col-sm-12">  
            <div class="slider-sec">
				<img src="<?php echo base_url('images/slider2.jpg') ?>" alt="" style="width:100%;  " >
    <?php endif; ?>
             
				<div class="slider-text">
					<h1>See A Doctor at Anytime From Anywhere</h1>
					<!--a href="#" class="slider-btn">GET STARTED</a-->
				</div>
			</div>
	</div>
       <?php if(!isset($this->session->userdata['logged_in'])) { ?>
	<div class="col-lg-4 col-md-4">
		<h1>Sign Up Now</h1>
		<h3>Fill in the required information</h3>
		<div class="signup-form">
                     <?php $attributes = array("name" => "registrationform", "id"=> "registrationform");
                echo form_open("home/register", $attributes);?>
            <div class="row">
				<div class="col-lg-6 col-md-6">
                    <label>*First Name</label>
                                    <input name="fname" type="text" class="form-control" placeholder="First name" required>
				</div>
				<div class="col-lg-6 col-md-6">
                    <label>*Surname</label>
                                    <input name="sname" type="text" class="form-control" placeholder="Surname" required>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6">
                    <label>*Email</label>
                                    <input id="email" name="email" type="email" class="form-control" placeholder="Email address" required>
				</div>  <div class="col-lg-6 col-md-6">
                                <label>*Re-Enter Email</label>
                                <input id="confemail" name="emailConfirm" type="email" class="form-control" placeholder="Re-enter Email"   required>
				</div>
                        </div>
                            <div class="row">
                          
			</div>
			 
                    <script type="text/javascript">
                    $('#registrationform').submit(function(){
                        var email = document.getElementById("email").value
                        var confemail = document.getElementById("confemail").value
                        if(email != confemail) {
                            alert('Email Not Matching!');return false;
                        }
                    })
                    function confirmEmail() {
                        var email = document.getElementById("email").value
                        var confemail = document.getElementById("confemail").value
                        if(email != confemail) {
                            alert('Email Not Matching!');
                        }
                    }
</script>

                    
                        <div class="row">
				<div class="col-lg-6 col-md-6">
                    <label>*Password</label>
                                    <input name="password" type="password" class="form-control" placeholder="Password" required>
				</div>
				
			
				<div class="col-lg-6 col-md-6">
                    <label>*Employer</label>
                                    <input name="phone" type="text" class="form-control" placeholder="Employer" required>
				</div>
			</div>
            <div class="row">
			<div class="col-lg-12 col-md-12">
				<lable>*Date of Birth</label>
				</div>
			<div class="col-lg-12 col-md-12">
                                    <select name="b_dd" class="form-control" id="select">
                                    <option value="" selected="selected" >Day</option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                                <select name="b_mm" class="form-control" id="select">
                                    <option value="">Month</option>
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                <select name="b_yy" class="form-control" id="select">
                                    <option value="">Year</option>
                                    <?php
                                    for($i=date('Y'); $i>date('Y')-100; $i--) {
                                        $selected = '';
                                        if ($birthdayYear == $i) $selected = ' selected="selected"';
                                        print('<option value="'.$i.'"'.$selected.'>'.$i.'</option>'."\n");
                                    }
                                    ?>
                                </select>
			</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12">
          <label>
            <input type="radio" name="gender"    id="signupFemale" value="female" checked="">
            Female
          </label>
           <label>
            <input type="radio" name="gender"  id="signupMale" value="male" checked="">
            Male
          </label>
        </div>
                         
                                    <!-- <a href="javascript:void(0)" id="signupMale" class="male active"> Male</a>
                                    <a href="javascript:void(0)" id="signupFemale" class="female">Female</a> -->
			  </div> 
			<?php /* ****** START OF OMITTING CARD ***********
            <div class="credit-card-info">
		 

            <div class="row">
            <div class="col-lg-5 col-md-5 card-brand">
                    <label>*Card Brand</label>
                    <label><input type="radio" name="cc_type"    id="signupFemale" value="VISA" checked>VISA CARD</label>
                    <label><input type="radio" name="cc_type"  id="signupMale" value="MASTER">MASTERCARD</label>
                    <!--select name="cc_type" class="form-control" id="select">
                        <option value="VISA">VISA</option>
                        <option value="MASTER">MASTER</option>
                        <option value="ELO">ELO</option>
                        <option value="JCB">JCB</option>
                        <option value="AMEX">AMEX</option>
                        <option value="VPAY">VPAY</option>
                        <option value="AXESS">AXESS</option>
                        <option value="BONUS">BONUS</option>
                        <option value="CABAL">CABAL</option>
                        <option value="WORLD">WORLD</option>
                        <option value="DINERS">DINERS</option>
                        <option value="BITCOIN">BITCOIN</option>
                        <option value="DANKORT">DANKORT</option>
                        <option value="MAESTRO">MAESTRO</option>
                        <option value="MAXIMUM">MAXIMUM</option>
                        <option value="AFTERPAY">AFTERPAY</option>
                        <option value="ASYACARD">ASYACARD</option>
                        <option value="DISCOVER">DISCOVER</option>
                        <option value="POSTEPAY">POSTEPAY</option>
                        <option value="SERVIRED">SERVIRED</option>
                        <option value="HIPERCARD">HIPERCARD</option>
                        <option value="VISADEBIT">VISADEBIT</option>
                        <option value="CARDFINANS">CARDFINANS</option>
                        <option value="CARTEBLEUE">CARTEBLEUE</option>
                        <option value="VISAELECTRON">VISAELECTRON</option>
                        <option value="CARTEBANCAIRE">CARTEBANCAIRE</option>
                        <option value="KLARNA_INVOICE">KLARNA_INVOICE</option>
                        <option value="DIRECTDEBIT_SEPA">DIRECTDEBIT_SEPA</option>
                    </select-->
                </div>
                <div class="col-lg-7 col-md-7">
                    <label>*Credit Card Number</label>
                                    <input name="cc_number" type="text" id="cc" class="form-control" placeholder="XXXX XXXX XXXX XXXX" required>
				</div>
				 
                <!--div class="col-lg-6 col-md-6">
                    <label>*Card Holder Name</label>
                        <input name="cc_name" type="text" class="form-control" placeholder="Card holder name" required>
                </div-->
                
                 
            </div>
          <!--  <div class="row">
				<div class="col-lg-12 col-md-12">
                    <label>*Credit Card Number</label>
                                    <input name="cc_number" type="text" class="form-control" placeholder="Credit card number" required>
				</div>
				 
			</div>-->
			<div class="row">
            <div class="col-lg-9 col-md-9 col-sm-6 col-xs-6">
               <label> *Expiration Date </label>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                    <label>*CCV</label>
                                    
				</div>
                </div>
			<div class="row">
				<div class="col-lg-9 col-md-9 expiry">
                   
                                    <input type="hidden" name="ex_dd" value="01"/>
                                    <!--select name="ex_dd" class="form-control" id="select">
                                       <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select-->
                                    <select name="ex_mm" class="form-control" id="select">
                                        <option value="">Month</option>
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                    <select name="ex_yy" class="form-control" id="select">
                                        <option value="">Year</option>
                                       <?php
                                    for($i=date('Y')+5; $i>2015; $i--) {
                                        $selected = '';
                                        if ($birthdayYear == $i) $selected = ' selected="selected"';
                                        print('<option value="'.$i.'"'.$selected.'>'.$i.'</option>'."\n");
                                    }
                                    ?>
                                    </select>
				</div>
				<div class="col-lg-3 col-md-3">
                    
                                    <input name="cc_csv_number" type="text" class="form-control" placeholder="CCV" maxlength="3" required>
				</div>
			</div>
			</div>
			<************** END OF OMIT CARD SECTION ***********> */ ?>
			  </div>
               
              <div class="row">
                <div class="col-lg-12 col-md-12 by-click" style="padding-right:0px;">
                                   <input type="checkbox" id="terms" unchecked> <label>By clicking Sign Up, you agree to our <a target="_blank" href="<?php echo base_url() . 'page/show/terms'; ?>">Terms</a> and <a target="_blank" href="<?php echo base_url() . 'page/show/policy'; ?>">Privacy Policy</a> </label></input> 
              </div> 
              </div>

			  <div class="row">
				<div class="col-lg-12 col-md-12">
                                   <button name="submit" id="submit" type="submit" disabled class="btn btn-default submit-button">Create An Account</button>
			  </div> 
			  </div>
			
                    <?php echo form_close(); ?>
                <?php //echo $this->session->flashdata('msg'); ?>
<script type="text/javascript">
    $("#registrationform").validate();
</script>
		</div>
                
	</div>
       <?php } ?>
	</div>
	</div>
	</div>
 </div>
    
<!--div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>User Registration Form</h4>
            </div>
            <div class="panel-body">
                <?php //$attributes = array("name" => "registrationform");
                //echo form_open("home/register", $attributes);?>
                <div class="form-group">
                    <label for="name">First Name</label>
                    <input class="form-control" name="fname" placeholder="Your First Name" type="text" value="<?php echo set_value('fname'); ?>" />
                    <span class="text-danger"><?php echo form_error('fname'); ?></span>
                </div>

                <div class="form-group">
                    <label for="name">Last Name</label>
                    <input class="form-control" name="sname" placeholder="Last Name" type="text" value="<?php echo set_value('lname'); ?>" />
                    <span class="text-danger"><?php echo form_error('lname'); ?></span>
                </div>
                
                <div class="form-group">
                    <label for="email">Email ID</label>
                    <input class="form-control" name="email" placeholder="Email-ID" type="text" value="<?php echo set_value('email'); ?>" />
                    <span class="text-danger"><?php echo form_error('email'); ?></span>
                </div>

                <div class="form-group">
                    <label for="subject">Password</label>
                    <input class="form-control" name="password" placeholder="Password" type="password" />
                    <span class="text-danger"><?php echo form_error('password'); ?></span>
                </div>

                <div class="form-group">
                    <label for="subject">Confirm Password</label>
                    <input class="form-control" name="cpassword" placeholder="Confirm Password" type="password" />
                    <span class="text-danger"><?php echo form_error('cpassword'); ?></span>
                </div>

                <div class="form-group">
                    <button name="submit" type="submit" class="btn btn-default">Signup</button>
                    <button name="cancel" type="reset" class="btn btn-default">Cancel</button>
                </div>
                
            </div>
        </div>
    </div>
</div-->
</div>
<script>
$('#terms').on('click', function() {
if ($('#terms').is(':checked')) {
 $('#submit').prop('disabled', false);
}else{
  $('#submit').prop('disabled', true);   
}
});
</script>

<script>
function cc_format(value) {
  var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
  var matches = v.match(/\d{4,16}/g);
  var match = matches && matches[0] || ''
  var parts = []
  for (i=0, len=match.length; i<len; i+=4) {
    parts.push(match.substring(i, i+4))
  }
  if (parts.length) {
    return parts.join(' ')
  } else {
    return value
  }
}

onload = function() {
  document.getElementById('cc').oninput = function() {
    this.value = cc_format(this.value)
  }
}
</script>
 
